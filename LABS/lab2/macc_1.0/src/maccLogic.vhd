library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity maccLogic is
Port (clk : in std_logic;
      rst : in std_logic;
      regEnable : in std_logic;
      regAddr : in std_logic_Vector(1 downto 0);
      dataIn : in std_logic_Vector(31 downto 0);
      dataOut : out std_logic_vector(31 downto 0)
);
end maccLogic;

architecture Behavioral of maccLogic is

signal reg0, reg1, reg2 : std_logic_vector(31 downto 0);

begin

process(clk)
begin
    if (rst = '0') then
        dataOut <= (others => '0');
    elsif (clk'event and clk = '1') then
        if (regEnable = '1' and regAddr = "00") then
            reg0 <= dataIn;
        elsif (regEnable = '1' and regAddr = "01") then
            reg1 <= dataIn;
        elsif (regEnable = '1' and regAddr = "10") then
            reg2 <= dataIn;
        end if;
        dataOut <= reg0(15 downto 0) * reg1(15 downto 0) + reg2;
     end if;
 end process;   

end Behavioral;
