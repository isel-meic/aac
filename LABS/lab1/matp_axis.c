/*
 * matp_axis.c
 *
 *  Created on: 20/03/2018
 *      Author: hcn
 */

#include <stdio.h>
#include "xtime_l.h"

volatile int *memA;   // matA N1xN2
volatile int *memB, *memTB;   // matB N2xN3
volatile int *memC, *memTC;   // matC N1xN3

#define N1 3
#define N2 3
#define N3 3

#define MEMA(I,J) (memA[(I)*N2+(J)])
#define MEMB(I,J) (memB[(I)*N3+(J)])
#define MEMC(I,J) (memC[(I)*N3+(J)])
#define MEMTB(I,J) (memTB[(I)*N2+(J)])
#define MEMTC(I,J) (memTC[(I)*N1+(J)])

#define MATA_START_ADD 0x10000000
#define MATB_START_ADD (MATA_START_ADD+4*N1*N2)
#define MATC_START_ADD (MATB_START_ADD+4*N2*N3)
#define MAT_TB_START_ADD (MATC_START_ADD+4*N1*N3)
#define MAT_TC_START_ADD (MAT_TB_START_ADD+4*N2*N3)

#define CHECK_FIFO 1
#define CHECK_RESULT 1

void print_mat(int *x, int colsize, int rowsize)
{
  int i, j;

  for (i=0; i<colsize; i++) {
    for (j=0; j<rowsize; j++) {
      printf("%d ", x[i*rowsize+j]);
    }
    printf("\n");
  }
  printf("\n");
}

int main()
{
  int i, j, sum;
  XTime tStart, tEnd;

  memA = (int *)(MATA_START_ADD);
  memB = (int *)(MATB_START_ADD);
  memC = (int *)(MATC_START_ADD);
  memTB = (int *)(MAT_TB_START_ADD);
  memTC = (int *)(MAT_TC_START_ADD);

  // print_mat((int *)memA,N1,N2);
  // print_mat((int *)memB,N2,N3);

  for (i=0; i<N1; i++)
      for (j=0; j<N2; j++) {
        MEMA(i,j) = 1;
        MEMB(i,j)=1;
      }

  XTime_GetTime(&tStart); // start measuring time

  for (i=0; i<N1; i++) {
	for (j=0; j<N3; j++) {
      sum = 0;
      for (int k=0; k<N2; k++) {
    	sum += MEMA(i,k)*MEMB(k,j);
      }
      MEMC(i,j) = sum;
	} 
  }

XTime_GetTime(&tEnd); // end measuring time

print_mat((int *)memC,N1,N3);

  printf("Output took %llu clock cycles.\n", 2*(tEnd - tStart));
  printf("Output took %.2f us.\n", 1.0 * (tEnd - tStart) / (COUNTS_PER_SECOND/1000000));
  return 0;
}

