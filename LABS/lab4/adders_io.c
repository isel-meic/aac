#include "adders_io.h"

void adders_io(int in1, int in2, int *in_out1) {
#pragma HLS INTERFACE ap_vld port=in1
#pragma HLS INTERFACE ap_hs port=in_out1
#pragma HLS INTERFACE ap_ack port=in2

	*in_out1 = in1 + in2 + *in_out1;
	

}

