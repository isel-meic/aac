#include <ap_int.h>
//#include <ap_fixed.h>
#include <hls_stream.h>


struct ap_i_axis{
  int data;
  ap_uint<1> last;
};
struct ap_o_axis{
  int  data;
  ap_uint<1> last;
};

// The top-level function
void axis_fixed_macc(
      hls::stream<ap_o_axis> &strm_out,
      hls::stream<ap_i_axis> &strm_in
      )
{
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS interface axis port=strm_in
#pragma HLS INTERFACE axis port=strm_out

   struct ap_i_axis tmp;
   struct ap_o_axis tmpa;
   static int op1, op2;
   static int mult;
   static int acc;
   static ap_uint<5> vect_size;
   static int localmem[512];
   int i;

   for (i=0; i<512; i++) {
	   tmp = strm_in.read();
	   localmem[i] = tmp.data;
	   if (tmp.last == 1) break ;
   }
   vect_size = i;

   for (; ; ) {
#pragma HLS loop_flatten off
	   acc = 0.0;
	   for (i=0; i<512; i++) {
#pragma HLS pipeline
		   tmp = strm_in.read();
		   op1 = (int)(localmem[i]);
		   op2 = (int)(tmp.data);
		   mult = op1 * op2;
		   acc += mult;
		   if (i==vect_size) break;
	   }
	   tmpa.last = tmp.last;
	   tmpa.data = (int)acc;
	   strm_out.write(tmpa);
	   if (tmp.last == 1) break ;
   }
}
