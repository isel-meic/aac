/* Reads mnist image file (see http://yann.lecun.com/exdb/mnist/) 
   and prints image N in pgm format (see http://paulbourke.net/dataformats/ppm/)
   to stdout.
*/

#include <stdio.h>
#include <stdlib.h>

#ifndef _IMAGE_H
#define _IMAGE_H

#define SCALE_COEF 0.00390625

#define NIMAGES 100
#define IMAGE_HEIGTH 28
#define IMAGE_WIDTH 28

void print_pgm(unsigned char *cimgs, int im);
void image_scale2float(unsigned char *cimgs, int im, float *fim);
void print_fp_image(float *fim);

#endif
