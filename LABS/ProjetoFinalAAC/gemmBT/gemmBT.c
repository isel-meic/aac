#include "gemmBT.h"

void gemmBT(float A[INPUT_SIZE], float B[INPUT_SIZE], float C[OUTPUT_SIZE], int rowsA, int colsA, int rowsB)
{
  int i, j, k;
  int colsBT, colsB;
  colsBT = rowsB;
  colsB = colsA;
  for (i = 0; i < rowsA; i++)
  {
    for (j = 0; j < colsBT; j++)
    {
      C[i * colsBT + j] = 0.0;
      for (k = 0; k < colsA; k++)
      {
        C[i * colsBT + j] += A[i * colsA + k] * B[j * colsB + k];
      }
    }
  }
}
