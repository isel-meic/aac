#include <stdio.h>
#include "gemmBT.h"

int main()
{

  float A[] = {1, 2,
               3, 4,
               5, 6};
  float B[] = {1, 2,
               3, 4,
               5, 6};
  float C[OUTPUT_SIZE];

  FILE *fp;

  fp = fopen("out.dat", "w");

  gemmBT(A, B, C, 3, 2, 3);

  int i, j;

  for (i = 0; i < 3; i++)
  {
    for (j = 0; j < 3; j++)
    {
      fprintf(fp, "%f ", C[i * 3 + j]);
    }
    fprintf(fp, "\n");
  }
  fclose(fp);

  printf("Comparing against output data \n");
  if (system("diff -w out.dat out.gemmBT.dat"))
  {
	  printf("*******************************************\n");
	  printf("FAIL: Output DOES NOT match the golden output\n");
	  printf("*******************************************\n");
    return 1;
  }
  else
  {
	  printf("*******************************************\n");
	  printf("PASS: The output matches the golden output!\n");
	  printf("*******************************************\n");
    return 0;
  }
}
