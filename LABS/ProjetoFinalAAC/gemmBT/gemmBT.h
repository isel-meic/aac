#ifndef GEMMBT_H_
#define GEMMBT_H_

#define INPUT_SIZE 6
#define OUTPUT_SIZE 9

void gemmBT(float A[INPUT_SIZE], float B[INPUT_SIZE], float C[OUTPUT_SIZE], int rowsA, int colsA, int rowsB);

#endif
