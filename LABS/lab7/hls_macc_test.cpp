#include <stdio.h>
#include <stdlib.h>
#include "hls_macc.h"
#include <ap_cint.h>

// Internal function prototypes
float ref_macc(float a, float b, float *c, bool clr_accum);

//typedef ap_fixed<12,6> fdata;

// Program entry for C test bench which is used to validate HLS DUT
//  functionality and automatically reused by Vivado HLS cosim_design
//  for RTL verification
int main(void)
{
   const int num_tests = 32;
   const int max_accum_len = 256;
   float a, b;
   float af, bf;
   float accum_sw = 0;
   fdataOut accum_hw = 0;
   int i, j;
   int err_cnt = 0;
   float lim = 50.0;

   for (i = 0; i < num_tests; i++) {

	  int accum_len = 16;
      for (j = 0; j < accum_len; j++) {
         // Clear the accumulators at beginning of each test run
         bool clr = j == 0 ? true : false;

         a = ((float)rand()/(float)RAND_MAX) * lim;
         b = ((float)rand()/(float)RAND_MAX) * lim;

         hls_macc((fdata)a, (fdata)b, &accum_hw, clr);

         ref_macc(a, b, &accum_sw, clr);
      }
      // Check HW result vs reference result
      if (accum_hw != accum_sw) {
         printf("!!! Mismatch ");
         printf("HW returned: %f; Expected: %f; Diff = %f\n", (float)accum_hw, accum_sw,
        		 accum_sw-(float)accum_hw);
         err_cnt++;
      }
      else
    	  printf("!!! Test OK, ");
   }
   if (err_cnt)
      printf("\n!!! %d TESTS FAILED !!!\n\n", err_cnt);
   else
      printf("\n*** %d Tests Passed ***\n\n", num_tests);

   return err_cnt;
}

// Definition of reference software model of DUT
float ref_macc(float a, float b, float *accum, bool accum_clr)
{
   if (accum_clr)
	   *accum = 0;

   *accum += a*b;//acc_reg;
}
