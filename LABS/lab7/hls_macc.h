#ifndef HLS_MACC_H_
#define HLS_MACC_H_

#include <stdbool.h>
#include <ap_int.h>

typedef ap_fixed<18,7> fdata;
typedef ap_fixed<18,15> fdataOut;

//typedef short int fdata;
//typedef short int fdataOut;

void hls_macc(fdata a, fdata b, fdataOut *accum, bool accum_clr);

#endif //#ifndef HLS_MACC_H_

