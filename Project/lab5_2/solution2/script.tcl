############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project lab5_2
set_top axi_interfaces
add_files ../LABS/lab5/lab5b/axi_interfaces.c
add_files -tb ../LABS/lab5/lab5b/axi_interfaces_test.c
open_solution "solution2"
set_part {xc7z010clg400-1}
create_clock -period 4 -name default
config_sdx -optimization_level none -target none
config_export -vivado_optimization_level 2
source "./lab5_2/solution2/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog
