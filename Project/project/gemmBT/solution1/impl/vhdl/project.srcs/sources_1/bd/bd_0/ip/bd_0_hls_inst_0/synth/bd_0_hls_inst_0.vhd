-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:hls:gemmBT:1.0
-- IP Revision: 2006102007

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY bd_0_hls_inst_0 IS
  PORT (
    A_ce0 : OUT STD_LOGIC;
    B_ce0 : OUT STD_LOGIC;
    C_ce0 : OUT STD_LOGIC;
    C_we0 : OUT STD_LOGIC;
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC;
    A_address0 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    A_q0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    B_address0 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    B_q0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    C_address0 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    C_d0 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    rowsA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    colsA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    rowsB : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END bd_0_hls_inst_0;

ARCHITECTURE bd_0_hls_inst_0_arch OF bd_0_hls_inst_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF bd_0_hls_inst_0_arch: ARCHITECTURE IS "yes";
  COMPONENT gemmBT IS
    PORT (
      A_ce0 : OUT STD_LOGIC;
      B_ce0 : OUT STD_LOGIC;
      C_ce0 : OUT STD_LOGIC;
      C_we0 : OUT STD_LOGIC;
      ap_clk : IN STD_LOGIC;
      ap_rst : IN STD_LOGIC;
      ap_start : IN STD_LOGIC;
      ap_done : OUT STD_LOGIC;
      ap_idle : OUT STD_LOGIC;
      ap_ready : OUT STD_LOGIC;
      A_address0 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      A_q0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      B_address0 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      B_q0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      C_address0 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      C_d0 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      rowsA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      colsA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      rowsB : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
  END COMPONENT gemmBT;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF bd_0_hls_inst_0_arch: ARCHITECTURE IS "gemmBT,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF bd_0_hls_inst_0_arch : ARCHITECTURE IS "bd_0_hls_inst_0,gemmBT,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF bd_0_hls_inst_0_arch: ARCHITECTURE IS "bd_0_hls_inst_0,gemmBT,{x_ipProduct=Vivado 2019.1,x_ipVendor=xilinx.com,x_ipLibrary=hls,x_ipName=gemmBT,x_ipVersion=1.0,x_ipCoreRevision=2006102007,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF bd_0_hls_inst_0_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF rowsB: SIGNAL IS "XIL_INTERFACENAME rowsB, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF rowsB: SIGNAL IS "xilinx.com:signal:data:1.0 rowsB DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF colsA: SIGNAL IS "XIL_INTERFACENAME colsA, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF colsA: SIGNAL IS "xilinx.com:signal:data:1.0 colsA DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF rowsA: SIGNAL IS "XIL_INTERFACENAME rowsA, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF rowsA: SIGNAL IS "xilinx.com:signal:data:1.0 rowsA DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF C_d0: SIGNAL IS "XIL_INTERFACENAME C_d0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF C_d0: SIGNAL IS "xilinx.com:signal:data:1.0 C_d0 DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF C_address0: SIGNAL IS "XIL_INTERFACENAME C_address0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF C_address0: SIGNAL IS "xilinx.com:signal:data:1.0 C_address0 DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF B_q0: SIGNAL IS "XIL_INTERFACENAME B_q0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF B_q0: SIGNAL IS "xilinx.com:signal:data:1.0 B_q0 DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF B_address0: SIGNAL IS "XIL_INTERFACENAME B_address0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF B_address0: SIGNAL IS "xilinx.com:signal:data:1.0 B_address0 DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF A_q0: SIGNAL IS "XIL_INTERFACENAME A_q0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF A_q0: SIGNAL IS "xilinx.com:signal:data:1.0 A_q0 DATA";
  ATTRIBUTE X_INTERFACE_PARAMETER OF A_address0: SIGNAL IS "XIL_INTERFACENAME A_address0, LAYERED_METADATA undef";
  ATTRIBUTE X_INTERFACE_INFO OF A_address0: SIGNAL IS "xilinx.com:signal:data:1.0 A_address0 DATA";
  ATTRIBUTE X_INTERFACE_INFO OF ap_ready: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready";
  ATTRIBUTE X_INTERFACE_INFO OF ap_idle: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle";
  ATTRIBUTE X_INTERFACE_INFO OF ap_done: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done";
  ATTRIBUTE X_INTERFACE_INFO OF ap_start: SIGNAL IS "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_rst: SIGNAL IS "XIL_INTERFACENAME ap_rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_rst: SIGNAL IS "xilinx.com:signal:reset:1.0 ap_rst RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ap_clk: SIGNAL IS "XIL_INTERFACENAME ap_clk, ASSOCIATED_RESET ap_rst, FREQ_HZ 100000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF ap_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ap_clk CLK";
BEGIN
  U0 : gemmBT
    PORT MAP (
      A_ce0 => A_ce0,
      B_ce0 => B_ce0,
      C_ce0 => C_ce0,
      C_we0 => C_we0,
      ap_clk => ap_clk,
      ap_rst => ap_rst,
      ap_start => ap_start,
      ap_done => ap_done,
      ap_idle => ap_idle,
      ap_ready => ap_ready,
      A_address0 => A_address0,
      A_q0 => A_q0,
      B_address0 => B_address0,
      B_q0 => B_q0,
      C_address0 => C_address0,
      C_d0 => C_d0,
      rowsA => rowsA,
      colsA => colsA,
      rowsB => rowsB
    );
END bd_0_hls_inst_0_arch;
