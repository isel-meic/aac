// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Jun 10 20:09:59 2020
// Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/Susana/Desktop/Project/project/gemmBT/solution1/impl/vhdl/project.srcs/sources_1/bd/bd_0/ip/bd_0_hls_inst_0/bd_0_hls_inst_0_stub.v
// Design      : bd_0_hls_inst_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "gemmBT,Vivado 2019.1" *)
module bd_0_hls_inst_0(A_ce0, B_ce0, C_ce0, C_we0, ap_clk, ap_rst, ap_start, 
  ap_done, ap_idle, ap_ready, A_address0, A_q0, B_address0, B_q0, C_address0, C_d0, rowsA, colsA, rowsB)
/* synthesis syn_black_box black_box_pad_pin="A_ce0,B_ce0,C_ce0,C_we0,ap_clk,ap_rst,ap_start,ap_done,ap_idle,ap_ready,A_address0[2:0],A_q0[31:0],B_address0[2:0],B_q0[31:0],C_address0[3:0],C_d0[31:0],rowsA[31:0],colsA[31:0],rowsB[31:0]" */;
  output A_ce0;
  output B_ce0;
  output C_ce0;
  output C_we0;
  input ap_clk;
  input ap_rst;
  input ap_start;
  output ap_done;
  output ap_idle;
  output ap_ready;
  output [2:0]A_address0;
  input [31:0]A_q0;
  output [2:0]B_address0;
  input [31:0]B_q0;
  output [3:0]C_address0;
  output [31:0]C_d0;
  input [31:0]rowsA;
  input [31:0]colsA;
  input [31:0]rowsB;
endmodule
