-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun 10 20:09:59 2020
-- Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               c:/Users/Susana/Desktop/Project/project/gemmBT/solution1/impl/vhdl/project.srcs/sources_1/bd/bd_0/ip/bd_0_hls_inst_0/bd_0_hls_inst_0_stub.vhdl
-- Design      : bd_0_hls_inst_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bd_0_hls_inst_0 is
  Port ( 
    A_ce0 : out STD_LOGIC;
    B_ce0 : out STD_LOGIC;
    C_ce0 : out STD_LOGIC;
    C_we0 : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    A_address0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    A_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B_address0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    B_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    C_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    C_d0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rowsA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    colsA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rowsB : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end bd_0_hls_inst_0;

architecture stub of bd_0_hls_inst_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "A_ce0,B_ce0,C_ce0,C_we0,ap_clk,ap_rst,ap_start,ap_done,ap_idle,ap_ready,A_address0[2:0],A_q0[31:0],B_address0[2:0],B_q0[31:0],C_address0[3:0],C_d0[31:0],rowsA[31:0],colsA[31:0],rowsB[31:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "gemmBT,Vivado 2019.1";
begin
end;
