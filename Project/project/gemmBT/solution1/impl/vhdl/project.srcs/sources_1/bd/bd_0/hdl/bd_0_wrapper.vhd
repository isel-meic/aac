--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Wed Jun 10 20:08:08 2020
--Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
--Command     : generate_target bd_0_wrapper.bd
--Design      : bd_0_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_wrapper is
  port (
    A_address0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    A_ce0 : out STD_LOGIC;
    A_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B_address0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    B_ce0 : out STD_LOGIC;
    B_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    C_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    C_ce0 : out STD_LOGIC;
    C_d0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    C_we0 : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_ctrl_done : out STD_LOGIC;
    ap_ctrl_idle : out STD_LOGIC;
    ap_ctrl_ready : out STD_LOGIC;
    ap_ctrl_start : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    colsA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rowsA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rowsB : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end bd_0_wrapper;

architecture STRUCTURE of bd_0_wrapper is
  component bd_0 is
  port (
    A_address0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    A_ce0 : out STD_LOGIC;
    A_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    B_address0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    B_ce0 : out STD_LOGIC;
    B_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    C_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    C_ce0 : out STD_LOGIC;
    C_d0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    C_we0 : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    colsA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rowsA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rowsB : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ap_ctrl_start : in STD_LOGIC;
    ap_ctrl_done : out STD_LOGIC;
    ap_ctrl_idle : out STD_LOGIC;
    ap_ctrl_ready : out STD_LOGIC
  );
  end component bd_0;
begin
bd_0_i: component bd_0
     port map (
      A_address0(2 downto 0) => A_address0(2 downto 0),
      A_ce0 => A_ce0,
      A_q0(31 downto 0) => A_q0(31 downto 0),
      B_address0(2 downto 0) => B_address0(2 downto 0),
      B_ce0 => B_ce0,
      B_q0(31 downto 0) => B_q0(31 downto 0),
      C_address0(3 downto 0) => C_address0(3 downto 0),
      C_ce0 => C_ce0,
      C_d0(31 downto 0) => C_d0(31 downto 0),
      C_we0 => C_we0,
      ap_clk => ap_clk,
      ap_ctrl_done => ap_ctrl_done,
      ap_ctrl_idle => ap_ctrl_idle,
      ap_ctrl_ready => ap_ctrl_ready,
      ap_ctrl_start => ap_ctrl_start,
      ap_rst => ap_rst,
      colsA(31 downto 0) => colsA(31 downto 0),
      rowsA(31 downto 0) => rowsA(31 downto 0),
      rowsB(31 downto 0) => rowsB(31 downto 0)
    );
end STRUCTURE;
