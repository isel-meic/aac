$Footmark = "FPGA_Xilinx"
$Description = "by Vivado"


#=== Resource usage ===
$SLICE = "0"
$LUT = "385"
$FF = "575"
$DSP = "5"
$BRAM ="0"
$SRL ="0"
#=== Final timing ===
$TargetCP = "10.000"
$CP = "8.254"
