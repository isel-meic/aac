// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
# 1 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c"
# 1 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c" 1
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 147 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c" 2
# 1 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT.h" 1






void gemmBT(float A[6], float B[6], float C[9], int rowsA, int colsA, int rowsB);
# 1 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c" 2


#ifndef HLS_FASTSIM
#ifndef HLS_FASTSIM
#include "apatb_gemmBT.h"
#endif
# 3 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c"
int main()
{

  float A[] = {1, 2,
               3, 4,
               5, 6};
  float B[] = {1, 2,
               3, 4,
               5, 6};
  float C[9];

  
#ifndef HLS_FASTSIM
#define gemmBT AESL_WRAP_gemmBT
#endif
# 14 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c"
gemmBT(A, B, C, 3, 2, 3);
#undef gemmBT
# 14 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c"


  int i, j;

  for (i = 0; i < 3; i++)
  {
    for (j = 0; j < 3; j++)
    {
      printf("%f ", C[i * 3 + j]);
    }
    printf("\n");
  }
}
#endif
# 26 "C:/Users/Susana/Desktop/LABS/ProjetoFinalAAC/gemmBT_test.c"

