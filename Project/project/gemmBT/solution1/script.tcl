############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project gemmBT
set_top gemmBT
add_files ../../LABS/ProjetoFinalAAC/gemmBT.c
add_files -tb ../../LABS/ProjetoFinalAAC/gemmBT_test.c
add_files -tb ../../LABS/ProjetoFinalAAC/out.gemmBT.dat
open_solution "solution1"
set_part {xc7z010-clg400-1} -tool vivado
create_clock -period 10 -name default
#source "./gemmBT/solution1/directives.tcl"
csim_design
csynth_design
cosim_design -rtl vhdl
export_design -flow syn -rtl vhdl -format ip_catalog
