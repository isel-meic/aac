// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sun Jul 12 10:59:11 2020
// Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/Susana/Desktop/Project/lab6/solution1/impl/vhdl/project.srcs/sources_1/bd/bd_0/ip/bd_0_hls_inst_0/bd_0_hls_inst_0_stub.v
// Design      : bd_0_hls_inst_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "axis_fixed_macc,Vivado 2019.1" *)
module bd_0_hls_inst_0(ap_clk, ap_rst_n, strm_out_TVALID, 
  strm_out_TREADY, strm_out_TDATA, strm_out_TLAST, strm_in_TVALID, strm_in_TREADY, 
  strm_in_TDATA, strm_in_TLAST)
/* synthesis syn_black_box black_box_pad_pin="ap_clk,ap_rst_n,strm_out_TVALID,strm_out_TREADY,strm_out_TDATA[31:0],strm_out_TLAST[0:0],strm_in_TVALID,strm_in_TREADY,strm_in_TDATA[31:0],strm_in_TLAST[0:0]" */;
  input ap_clk;
  input ap_rst_n;
  output strm_out_TVALID;
  input strm_out_TREADY;
  output [31:0]strm_out_TDATA;
  output [0:0]strm_out_TLAST;
  input strm_in_TVALID;
  output strm_in_TREADY;
  input [31:0]strm_in_TDATA;
  input [0:0]strm_in_TLAST;
endmodule
