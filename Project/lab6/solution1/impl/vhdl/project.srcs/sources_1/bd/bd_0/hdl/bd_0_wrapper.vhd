--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Sun Jul 12 10:58:01 2020
--Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
--Command     : generate_target bd_0_wrapper.bd
--Design      : bd_0_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_wrapper is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    strm_in_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_tready : out STD_LOGIC;
    strm_in_tvalid : in STD_LOGIC;
    strm_out_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_out_tlast : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_out_tready : in STD_LOGIC;
    strm_out_tvalid : out STD_LOGIC
  );
end bd_0_wrapper;

architecture STRUCTURE of bd_0_wrapper is
  component bd_0 is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    strm_in_tvalid : in STD_LOGIC;
    strm_in_tready : out STD_LOGIC;
    strm_in_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    strm_out_tvalid : out STD_LOGIC;
    strm_out_tready : in STD_LOGIC;
    strm_out_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_out_tlast : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component bd_0;
begin
bd_0_i: component bd_0
     port map (
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      strm_in_tdata(31 downto 0) => strm_in_tdata(31 downto 0),
      strm_in_tlast(0) => strm_in_tlast(0),
      strm_in_tready => strm_in_tready,
      strm_in_tvalid => strm_in_tvalid,
      strm_out_tdata(31 downto 0) => strm_out_tdata(31 downto 0),
      strm_out_tlast(0) => strm_out_tlast(0),
      strm_out_tready => strm_out_tready,
      strm_out_tvalid => strm_out_tvalid
    );
end STRUCTURE;
