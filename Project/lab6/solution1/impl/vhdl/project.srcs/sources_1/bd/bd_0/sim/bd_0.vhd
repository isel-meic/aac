--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Sun Jul 12 10:58:00 2020
--Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
--Command     : generate_target bd_0.bd
--Design      : bd_0
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0 is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    strm_in_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_tready : out STD_LOGIC;
    strm_in_tvalid : in STD_LOGIC;
    strm_out_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_out_tlast : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_out_tready : in STD_LOGIC;
    strm_out_tvalid : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of bd_0 : entity is "bd_0,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=bd_0,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=1,numReposBlks=1,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=1,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of bd_0 : entity is "bd_0.hwdef";
end bd_0;

architecture STRUCTURE of bd_0 is
  component bd_0_hls_inst_0 is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    strm_out_TVALID : out STD_LOGIC;
    strm_out_TREADY : in STD_LOGIC;
    strm_out_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_TVALID : in STD_LOGIC;
    strm_in_TREADY : out STD_LOGIC;
    strm_in_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component bd_0_hls_inst_0;
  signal ap_clk_0_1 : STD_LOGIC;
  signal ap_rst_n_0_1 : STD_LOGIC;
  signal hls_inst_strm_out_TDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal hls_inst_strm_out_TLAST : STD_LOGIC_VECTOR ( 0 to 0 );
  signal hls_inst_strm_out_TREADY : STD_LOGIC;
  signal hls_inst_strm_out_TVALID : STD_LOGIC;
  signal strm_in_0_1_TDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal strm_in_0_1_TLAST : STD_LOGIC_VECTOR ( 0 to 0 );
  signal strm_in_0_1_TREADY : STD_LOGIC;
  signal strm_in_0_1_TVALID : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of ap_clk : signal is "xilinx.com:signal:clock:1.0 CLK.AP_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of ap_clk : signal is "XIL_INTERFACENAME CLK.AP_CLK, ASSOCIATED_BUSIF strm_in:strm_out, ASSOCIATED_RESET ap_rst_n, CLK_DOMAIN bd_0_ap_clk_0, FREQ_HZ 100000000.0, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 RST.AP_RST_N RST";
  attribute X_INTERFACE_PARAMETER of ap_rst_n : signal is "XIL_INTERFACENAME RST.AP_RST_N, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of strm_in_tready : signal is "xilinx.com:interface:axis:1.0 strm_in ";
  attribute X_INTERFACE_INFO of strm_in_tvalid : signal is "xilinx.com:interface:axis:1.0 strm_in ";
  attribute X_INTERFACE_INFO of strm_out_tready : signal is "xilinx.com:interface:axis:1.0 strm_out ";
  attribute X_INTERFACE_INFO of strm_out_tvalid : signal is "xilinx.com:interface:axis:1.0 strm_out ";
  attribute X_INTERFACE_INFO of strm_in_tdata : signal is "xilinx.com:interface:axis:1.0 strm_in ";
  attribute X_INTERFACE_PARAMETER of strm_in_tdata : signal is "XIL_INTERFACENAME strm_in, CLK_DOMAIN bd_0_ap_clk_0, FREQ_HZ 100000000.0, HAS_TKEEP 0, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.000, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of strm_in_tlast : signal is "xilinx.com:interface:axis:1.0 strm_in ";
  attribute X_INTERFACE_INFO of strm_out_tdata : signal is "xilinx.com:interface:axis:1.0 strm_out ";
  attribute X_INTERFACE_PARAMETER of strm_out_tdata : signal is "XIL_INTERFACENAME strm_out, CLK_DOMAIN bd_0_ap_clk_0, FREQ_HZ 100000000.0, HAS_TKEEP 0, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.000, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of strm_out_tlast : signal is "xilinx.com:interface:axis:1.0 strm_out ";
begin
  ap_clk_0_1 <= ap_clk;
  ap_rst_n_0_1 <= ap_rst_n;
  hls_inst_strm_out_TREADY <= strm_out_tready;
  strm_in_0_1_TDATA(31 downto 0) <= strm_in_tdata(31 downto 0);
  strm_in_0_1_TLAST(0) <= strm_in_tlast(0);
  strm_in_0_1_TVALID <= strm_in_tvalid;
  strm_in_tready <= strm_in_0_1_TREADY;
  strm_out_tdata(31 downto 0) <= hls_inst_strm_out_TDATA(31 downto 0);
  strm_out_tlast(0) <= hls_inst_strm_out_TLAST(0);
  strm_out_tvalid <= hls_inst_strm_out_TVALID;
hls_inst: component bd_0_hls_inst_0
     port map (
      ap_clk => ap_clk_0_1,
      ap_rst_n => ap_rst_n_0_1,
      strm_in_TDATA(31 downto 0) => strm_in_0_1_TDATA(31 downto 0),
      strm_in_TLAST(0) => strm_in_0_1_TLAST(0),
      strm_in_TREADY => strm_in_0_1_TREADY,
      strm_in_TVALID => strm_in_0_1_TVALID,
      strm_out_TDATA(31 downto 0) => hls_inst_strm_out_TDATA(31 downto 0),
      strm_out_TLAST(0) => hls_inst_strm_out_TLAST(0),
      strm_out_TREADY => hls_inst_strm_out_TREADY,
      strm_out_TVALID => hls_inst_strm_out_TVALID
    );
end STRUCTURE;
