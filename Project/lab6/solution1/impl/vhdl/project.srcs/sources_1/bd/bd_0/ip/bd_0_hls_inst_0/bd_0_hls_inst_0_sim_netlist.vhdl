-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jul 12 10:59:11 2020
-- Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/Susana/Desktop/Project/lab6/solution1/impl/vhdl/project.srcs/sources_1/bd/bd_0/ip/bd_0_hls_inst_0/bd_0_hls_inst_0_sim_netlist.vhdl
-- Design      : bd_0_hls_inst_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_axis_fixed_macc_lbkb_ram is
  port (
    localmem_q0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    d0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    WEBWE : out STD_LOGIC_VECTOR ( 0 to 0 );
    \val_assign_reg_107_reg[1]\ : out STD_LOGIC;
    \val_assign_reg_107_reg[4]\ : out STD_LOGIC;
    icmp_ln34_fu_192_p2 : out STD_LOGIC;
    icmp_ln44_fu_225_p2 : out STD_LOGIC;
    \i_op_assign_reg_153_reg[4]\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ram_reg_0 : in STD_LOGIC;
    ap_enable_reg_pp0_iter0 : in STD_LOGIC;
    ram_reg_1 : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \tmp_3_1_3_reg_142[0]_i_2\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    ram_reg_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ram_reg_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_V_data_0_sel : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_axis_fixed_macc_lbkb_ram : entity is "axis_fixed_macc_lbkb_ram";
end bd_0_hls_inst_0_axis_fixed_macc_lbkb_ram;

architecture STRUCTURE of bd_0_hls_inst_0_axis_fixed_macc_lbkb_ram is
  signal \^webwe\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal address0 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal ce0 : STD_LOGIC;
  signal \^d0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^i_op_assign_reg_153_reg[4]\ : STD_LOGIC;
  signal \^icmp_ln34_fu_192_p2\ : STD_LOGIC;
  signal \^icmp_ln44_fu_225_p2\ : STD_LOGIC;
  signal \^val_assign_reg_107_reg[1]\ : STD_LOGIC;
  signal \^val_assign_reg_107_reg[4]\ : STD_LOGIC;
  signal NLW_ram_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 14 );
  signal NLW_ram_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of ram_reg : label is "p2_d16";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of ram_reg : label is "p0_d14";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of ram_reg : label is 16384;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of ram_reg : label is "ram";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of ram_reg : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of ram_reg : label is 511;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of ram_reg : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of ram_reg : label is 17;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg : label is 511;
  attribute ram_ext_slice_begin : integer;
  attribute ram_ext_slice_begin of ram_reg : label is 18;
  attribute ram_ext_slice_end : integer;
  attribute ram_ext_slice_end of ram_reg : label is 31;
  attribute ram_offset : integer;
  attribute ram_offset of ram_reg : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg : label is 17;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of ram_reg_i_44 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of ram_reg_i_46 : label is "soft_lutpair0";
begin
  WEBWE(0) <= \^webwe\(0);
  d0(31 downto 0) <= \^d0\(31 downto 0);
  \i_op_assign_reg_153_reg[4]\ <= \^i_op_assign_reg_153_reg[4]\;
  icmp_ln34_fu_192_p2 <= \^icmp_ln34_fu_192_p2\;
  icmp_ln44_fu_225_p2 <= \^icmp_ln44_fu_225_p2\;
  \val_assign_reg_107_reg[1]\ <= \^val_assign_reg_107_reg[1]\;
  \val_assign_reg_107_reg[4]\ <= \^val_assign_reg_107_reg[4]\;
ram_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13) => '0',
      ADDRARDADDR(12 downto 4) => address0(8 downto 0),
      ADDRARDADDR(3 downto 0) => B"0000",
      ADDRBWRADDR(13) => '1',
      ADDRBWRADDR(12 downto 4) => address0(8 downto 0),
      ADDRBWRADDR(3 downto 0) => B"0000",
      CLKARDCLK => ap_clk,
      CLKBWRCLK => ap_clk,
      DIADI(15 downto 0) => \^d0\(15 downto 0),
      DIBDI(15 downto 14) => B"11",
      DIBDI(13 downto 0) => \^d0\(31 downto 18),
      DIPADIP(1 downto 0) => \^d0\(17 downto 16),
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 0) => localmem_q0(15 downto 0),
      DOBDO(15 downto 14) => NLW_ram_reg_DOBDO_UNCONNECTED(15 downto 14),
      DOBDO(13 downto 0) => localmem_q0(31 downto 18),
      DOPADOP(1 downto 0) => localmem_q0(17 downto 16),
      DOPBDOP(1 downto 0) => NLW_ram_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => ce0,
      ENBWREN => ce0,
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1) => \^webwe\(0),
      WEA(0) => \^webwe\(0),
      WEBWE(3 downto 2) => B"00",
      WEBWE(1) => \^webwe\(0),
      WEBWE(0) => \^webwe\(0)
    );
ram_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FAAAF888FAAA8888"
    )
        port map (
      I0 => Q(0),
      I1 => \^icmp_ln34_fu_192_p2\,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => Q(1),
      I4 => ram_reg_0,
      I5 => \^icmp_ln44_fu_225_p2\,
      O => ce0
    );
ram_reg_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(0),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(0),
      O => address0(0)
    );
ram_reg_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(15),
      I1 => ram_reg_3(15),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(15)
    );
ram_reg_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(14),
      I1 => ram_reg_3(14),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(14)
    );
ram_reg_i_13: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(13),
      I1 => ram_reg_3(13),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(13)
    );
ram_reg_i_14: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(12),
      I1 => ram_reg_3(12),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(12)
    );
ram_reg_i_15: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(11),
      I1 => ram_reg_3(11),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(11)
    );
ram_reg_i_16: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(10),
      I1 => ram_reg_3(10),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(10)
    );
ram_reg_i_17: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(9),
      I1 => ram_reg_3(9),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(9)
    );
ram_reg_i_18: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(8),
      I1 => ram_reg_3(8),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(8)
    );
ram_reg_i_19: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(7),
      I1 => ram_reg_3(7),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(7)
    );
ram_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(8),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(8),
      O => address0(8)
    );
ram_reg_i_20: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(6),
      I1 => ram_reg_3(6),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(6)
    );
ram_reg_i_21: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(5),
      I1 => ram_reg_3(5),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(5)
    );
ram_reg_i_22: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(4),
      I1 => ram_reg_3(4),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(4)
    );
ram_reg_i_23: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(3),
      I1 => ram_reg_3(3),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(3)
    );
ram_reg_i_24: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(2),
      I1 => ram_reg_3(2),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(2)
    );
ram_reg_i_25: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(1),
      I1 => ram_reg_3(1),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(1)
    );
ram_reg_i_26: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(0),
      I1 => ram_reg_3(0),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(0)
    );
ram_reg_i_27: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(31),
      I1 => ram_reg_3(31),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(31)
    );
ram_reg_i_28: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(30),
      I1 => ram_reg_3(30),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(30)
    );
ram_reg_i_29: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(29),
      I1 => ram_reg_3(29),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(29)
    );
ram_reg_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(7),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(7),
      O => address0(7)
    );
ram_reg_i_30: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(28),
      I1 => ram_reg_3(28),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(28)
    );
ram_reg_i_31: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(27),
      I1 => ram_reg_3(27),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(27)
    );
ram_reg_i_32: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(26),
      I1 => ram_reg_3(26),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(26)
    );
ram_reg_i_33: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(25),
      I1 => ram_reg_3(25),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(25)
    );
ram_reg_i_34: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(24),
      I1 => ram_reg_3(24),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(24)
    );
ram_reg_i_35: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(23),
      I1 => ram_reg_3(23),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(23)
    );
ram_reg_i_36: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(22),
      I1 => ram_reg_3(22),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(22)
    );
ram_reg_i_37: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(21),
      I1 => ram_reg_3(21),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(21)
    );
ram_reg_i_38: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(20),
      I1 => ram_reg_3(20),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(20)
    );
ram_reg_i_39: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(19),
      I1 => ram_reg_3(19),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(19)
    );
ram_reg_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(6),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(6),
      O => address0(6)
    );
ram_reg_i_40: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(18),
      I1 => ram_reg_3(18),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(18)
    );
ram_reg_i_41: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(17),
      I1 => ram_reg_3(17),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(17)
    );
ram_reg_i_42: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ram_reg_2(16),
      I1 => ram_reg_3(16),
      I2 => strm_in_V_data_0_sel,
      O => \^d0\(16)
    );
ram_reg_i_43: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0888"
    )
        port map (
      I0 => Q(0),
      I1 => ram_reg_0,
      I2 => \^val_assign_reg_107_reg[1]\,
      I3 => \^val_assign_reg_107_reg[4]\,
      O => \^webwe\(0)
    );
ram_reg_i_44: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \^val_assign_reg_107_reg[4]\,
      I1 => ram_reg_1(2),
      I2 => ram_reg_1(3),
      I3 => ram_reg_1(0),
      I4 => ram_reg_1(1),
      O => \^icmp_ln34_fu_192_p2\
    );
ram_reg_i_45: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \^i_op_assign_reg_153_reg[4]\,
      I1 => \tmp_3_1_3_reg_142[0]_i_2\(2),
      I2 => \tmp_3_1_3_reg_142[0]_i_2\(3),
      I3 => \tmp_3_1_3_reg_142[0]_i_2\(0),
      I4 => \tmp_3_1_3_reg_142[0]_i_2\(1),
      O => \^icmp_ln44_fu_225_p2\
    );
ram_reg_i_46: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => ram_reg_1(1),
      I1 => ram_reg_1(0),
      I2 => ram_reg_1(3),
      I3 => ram_reg_1(2),
      O => \^val_assign_reg_107_reg[1]\
    );
ram_reg_i_47: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => ram_reg_1(4),
      I1 => ram_reg_1(5),
      I2 => ram_reg_1(6),
      I3 => ram_reg_1(7),
      I4 => ram_reg_1(8),
      I5 => ram_reg_1(9),
      O => \^val_assign_reg_107_reg[4]\
    );
ram_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(5),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(5),
      O => address0(5)
    );
ram_reg_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(4),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(4),
      O => address0(4)
    );
ram_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(3),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(3),
      O => address0(3)
    );
ram_reg_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(2),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(2),
      O => address0(2)
    );
ram_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(1),
      I1 => ap_enable_reg_pp0_iter0,
      I2 => Q(1),
      I3 => ram_reg_1(1),
      O => address0(1)
    );
\strm_in_V_last_V_0_state[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \tmp_3_1_3_reg_142[0]_i_2\(4),
      I1 => \tmp_3_1_3_reg_142[0]_i_2\(5),
      I2 => \tmp_3_1_3_reg_142[0]_i_2\(6),
      I3 => \tmp_3_1_3_reg_142[0]_i_2\(7),
      I4 => \tmp_3_1_3_reg_142[0]_i_2\(8),
      I5 => \tmp_3_1_3_reg_142[0]_i_2\(9),
      O => \^i_op_assign_reg_153_reg[4]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_axis_fixed_macc_lbkb is
  port (
    localmem_q0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    d0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    we0 : out STD_LOGIC;
    \val_assign_reg_107_reg[1]\ : out STD_LOGIC;
    \val_assign_reg_107_reg[4]\ : out STD_LOGIC;
    icmp_ln34_fu_192_p2 : out STD_LOGIC;
    icmp_ln44_fu_225_p2 : out STD_LOGIC;
    \i_op_assign_reg_153_reg[4]\ : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ram_reg : in STD_LOGIC;
    ap_enable_reg_pp0_iter0 : in STD_LOGIC;
    ram_reg_0 : in STD_LOGIC_VECTOR ( 9 downto 0 );
    \tmp_3_1_3_reg_142[0]_i_2\ : in STD_LOGIC_VECTOR ( 9 downto 0 );
    ram_reg_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ram_reg_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_V_data_0_sel : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_axis_fixed_macc_lbkb : entity is "axis_fixed_macc_lbkb";
end bd_0_hls_inst_0_axis_fixed_macc_lbkb;

architecture STRUCTURE of bd_0_hls_inst_0_axis_fixed_macc_lbkb is
begin
axis_fixed_macc_lbkb_ram_U: entity work.bd_0_hls_inst_0_axis_fixed_macc_lbkb_ram
     port map (
      Q(1 downto 0) => Q(1 downto 0),
      WEBWE(0) => we0,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter0 => ap_enable_reg_pp0_iter0,
      d0(31 downto 0) => d0(31 downto 0),
      \i_op_assign_reg_153_reg[4]\ => \i_op_assign_reg_153_reg[4]\,
      icmp_ln34_fu_192_p2 => icmp_ln34_fu_192_p2,
      icmp_ln44_fu_225_p2 => icmp_ln44_fu_225_p2,
      localmem_q0(31 downto 0) => localmem_q0(31 downto 0),
      ram_reg_0 => ram_reg,
      ram_reg_1(9 downto 0) => ram_reg_0(9 downto 0),
      ram_reg_2(31 downto 0) => ram_reg_1(31 downto 0),
      ram_reg_3(31 downto 0) => ram_reg_2(31 downto 0),
      strm_in_V_data_0_sel => strm_in_V_data_0_sel,
      \tmp_3_1_3_reg_142[0]_i_2\(9 downto 0) => \tmp_3_1_3_reg_142[0]_i_2\(9 downto 0),
      \val_assign_reg_107_reg[1]\ => \val_assign_reg_107_reg[1]\,
      \val_assign_reg_107_reg[4]\ => \val_assign_reg_107_reg[4]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_axis_fixed_macc is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    strm_out_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_out_TVALID : out STD_LOGIC;
    strm_out_TREADY : in STD_LOGIC;
    strm_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_TVALID : in STD_LOGIC;
    strm_in_TREADY : out STD_LOGIC;
    strm_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_axis_fixed_macc : entity is "axis_fixed_macc";
end bd_0_hls_inst_0_axis_fixed_macc;

architecture STRUCTURE of bd_0_hls_inst_0_axis_fixed_macc is
  signal acc : STD_LOGIC;
  signal acc0 : STD_LOGIC;
  signal \acc[11]_i_2_n_2\ : STD_LOGIC;
  signal \acc[11]_i_3_n_2\ : STD_LOGIC;
  signal \acc[11]_i_4_n_2\ : STD_LOGIC;
  signal \acc[11]_i_5_n_2\ : STD_LOGIC;
  signal \acc[15]_i_2_n_2\ : STD_LOGIC;
  signal \acc[15]_i_3_n_2\ : STD_LOGIC;
  signal \acc[15]_i_4_n_2\ : STD_LOGIC;
  signal \acc[15]_i_5_n_2\ : STD_LOGIC;
  signal \acc[19]_i_2_n_2\ : STD_LOGIC;
  signal \acc[19]_i_3_n_2\ : STD_LOGIC;
  signal \acc[19]_i_4_n_2\ : STD_LOGIC;
  signal \acc[19]_i_5_n_2\ : STD_LOGIC;
  signal \acc[19]_i_7_n_2\ : STD_LOGIC;
  signal \acc[19]_i_8_n_2\ : STD_LOGIC;
  signal \acc[19]_i_9_n_2\ : STD_LOGIC;
  signal \acc[23]_i_10_n_2\ : STD_LOGIC;
  signal \acc[23]_i_2_n_2\ : STD_LOGIC;
  signal \acc[23]_i_3_n_2\ : STD_LOGIC;
  signal \acc[23]_i_4_n_2\ : STD_LOGIC;
  signal \acc[23]_i_5_n_2\ : STD_LOGIC;
  signal \acc[23]_i_7_n_2\ : STD_LOGIC;
  signal \acc[23]_i_8_n_2\ : STD_LOGIC;
  signal \acc[23]_i_9_n_2\ : STD_LOGIC;
  signal \acc[27]_i_10_n_2\ : STD_LOGIC;
  signal \acc[27]_i_2_n_2\ : STD_LOGIC;
  signal \acc[27]_i_3_n_2\ : STD_LOGIC;
  signal \acc[27]_i_4_n_2\ : STD_LOGIC;
  signal \acc[27]_i_5_n_2\ : STD_LOGIC;
  signal \acc[27]_i_7_n_2\ : STD_LOGIC;
  signal \acc[27]_i_8_n_2\ : STD_LOGIC;
  signal \acc[27]_i_9_n_2\ : STD_LOGIC;
  signal \acc[31]_i_10_n_2\ : STD_LOGIC;
  signal \acc[31]_i_11_n_2\ : STD_LOGIC;
  signal \acc[31]_i_12_n_2\ : STD_LOGIC;
  signal \acc[31]_i_4_n_2\ : STD_LOGIC;
  signal \acc[31]_i_5_n_2\ : STD_LOGIC;
  signal \acc[31]_i_6_n_2\ : STD_LOGIC;
  signal \acc[31]_i_7_n_2\ : STD_LOGIC;
  signal \acc[31]_i_9_n_2\ : STD_LOGIC;
  signal \acc[3]_i_2_n_2\ : STD_LOGIC;
  signal \acc[3]_i_3_n_2\ : STD_LOGIC;
  signal \acc[3]_i_4_n_2\ : STD_LOGIC;
  signal \acc[3]_i_5_n_2\ : STD_LOGIC;
  signal \acc[7]_i_2_n_2\ : STD_LOGIC;
  signal \acc[7]_i_3_n_2\ : STD_LOGIC;
  signal \acc[7]_i_4_n_2\ : STD_LOGIC;
  signal \acc[7]_i_5_n_2\ : STD_LOGIC;
  signal \acc_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \acc_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \acc_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \acc_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \acc_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \acc_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \acc_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \acc_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \acc_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \acc_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \acc_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \acc_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \acc_reg[19]_i_6_n_2\ : STD_LOGIC;
  signal \acc_reg[19]_i_6_n_3\ : STD_LOGIC;
  signal \acc_reg[19]_i_6_n_4\ : STD_LOGIC;
  signal \acc_reg[19]_i_6_n_5\ : STD_LOGIC;
  signal \acc_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \acc_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \acc_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \acc_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \acc_reg[23]_i_6_n_2\ : STD_LOGIC;
  signal \acc_reg[23]_i_6_n_3\ : STD_LOGIC;
  signal \acc_reg[23]_i_6_n_4\ : STD_LOGIC;
  signal \acc_reg[23]_i_6_n_5\ : STD_LOGIC;
  signal \acc_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \acc_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \acc_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \acc_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \acc_reg[27]_i_6_n_2\ : STD_LOGIC;
  signal \acc_reg[27]_i_6_n_3\ : STD_LOGIC;
  signal \acc_reg[27]_i_6_n_4\ : STD_LOGIC;
  signal \acc_reg[27]_i_6_n_5\ : STD_LOGIC;
  signal \acc_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \acc_reg[31]_i_3_n_4\ : STD_LOGIC;
  signal \acc_reg[31]_i_3_n_5\ : STD_LOGIC;
  signal \acc_reg[31]_i_8_n_3\ : STD_LOGIC;
  signal \acc_reg[31]_i_8_n_4\ : STD_LOGIC;
  signal \acc_reg[31]_i_8_n_5\ : STD_LOGIC;
  signal \acc_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \acc_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \acc_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \acc_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \acc_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \acc_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \acc_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \acc_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \acc_reg_n_2_[0]\ : STD_LOGIC;
  signal \acc_reg_n_2_[10]\ : STD_LOGIC;
  signal \acc_reg_n_2_[11]\ : STD_LOGIC;
  signal \acc_reg_n_2_[12]\ : STD_LOGIC;
  signal \acc_reg_n_2_[13]\ : STD_LOGIC;
  signal \acc_reg_n_2_[14]\ : STD_LOGIC;
  signal \acc_reg_n_2_[15]\ : STD_LOGIC;
  signal \acc_reg_n_2_[16]\ : STD_LOGIC;
  signal \acc_reg_n_2_[17]\ : STD_LOGIC;
  signal \acc_reg_n_2_[18]\ : STD_LOGIC;
  signal \acc_reg_n_2_[19]\ : STD_LOGIC;
  signal \acc_reg_n_2_[1]\ : STD_LOGIC;
  signal \acc_reg_n_2_[20]\ : STD_LOGIC;
  signal \acc_reg_n_2_[21]\ : STD_LOGIC;
  signal \acc_reg_n_2_[22]\ : STD_LOGIC;
  signal \acc_reg_n_2_[23]\ : STD_LOGIC;
  signal \acc_reg_n_2_[24]\ : STD_LOGIC;
  signal \acc_reg_n_2_[25]\ : STD_LOGIC;
  signal \acc_reg_n_2_[26]\ : STD_LOGIC;
  signal \acc_reg_n_2_[27]\ : STD_LOGIC;
  signal \acc_reg_n_2_[28]\ : STD_LOGIC;
  signal \acc_reg_n_2_[29]\ : STD_LOGIC;
  signal \acc_reg_n_2_[2]\ : STD_LOGIC;
  signal \acc_reg_n_2_[30]\ : STD_LOGIC;
  signal \acc_reg_n_2_[31]\ : STD_LOGIC;
  signal \acc_reg_n_2_[3]\ : STD_LOGIC;
  signal \acc_reg_n_2_[4]\ : STD_LOGIC;
  signal \acc_reg_n_2_[5]\ : STD_LOGIC;
  signal \acc_reg_n_2_[6]\ : STD_LOGIC;
  signal \acc_reg_n_2_[7]\ : STD_LOGIC;
  signal \acc_reg_n_2_[8]\ : STD_LOGIC;
  signal \acc_reg_n_2_[9]\ : STD_LOGIC;
  signal add_ln50_fu_260_p2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \ap_CS_fsm[0]_i_2_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_2_n_2\ : STD_LOGIC;
  signal \ap_CS_fsm[5]_i_2_n_2\ : STD_LOGIC;
  signal ap_CS_fsm_pp0_stage0 : STD_LOGIC;
  signal ap_CS_fsm_state1 : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state3 : STD_LOGIC;
  signal ap_CS_fsm_state8 : STD_LOGIC;
  signal ap_CS_fsm_state9 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal ap_NS_fsm1 : STD_LOGIC;
  signal ap_NS_fsm143_out : STD_LOGIC;
  signal ap_block_pp0_stage0_subdone : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter0_i_2_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter1_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter2_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter3_i_1_n_2 : STD_LOGIC;
  signal ap_enable_reg_pp0_iter3_reg_n_2 : STD_LOGIC;
  signal d0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal grp_fu_182_p1 : STD_LOGIC;
  signal i_1_fu_231_p2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_fu_198_p2 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal i_op_assign_reg_153 : STD_LOGIC;
  signal i_op_assign_reg_1530 : STD_LOGIC;
  signal \i_op_assign_reg_153[9]_i_5_n_2\ : STD_LOGIC;
  signal \i_op_assign_reg_153[9]_i_6_n_2\ : STD_LOGIC;
  signal \i_op_assign_reg_153[9]_i_7_n_2\ : STD_LOGIC;
  signal \i_op_assign_reg_153[9]_i_8_n_2\ : STD_LOGIC;
  signal \i_op_assign_reg_153[9]_i_9_n_2\ : STD_LOGIC;
  signal i_op_assign_reg_153_reg : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \i_op_assign_reg_153_reg[9]_i_4_n_3\ : STD_LOGIC;
  signal \i_op_assign_reg_153_reg[9]_i_4_n_4\ : STD_LOGIC;
  signal \i_op_assign_reg_153_reg[9]_i_4_n_5\ : STD_LOGIC;
  signal icmp_ln34_fu_192_p2 : STD_LOGIC;
  signal icmp_ln44_fu_225_p2 : STD_LOGIC;
  signal \icmp_ln44_reg_282[0]_i_1_n_2\ : STD_LOGIC;
  signal icmp_ln44_reg_282_pp0_iter1_reg : STD_LOGIC;
  signal \icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal icmp_ln44_reg_282_pp0_iter2_reg : STD_LOGIC;
  signal \icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \icmp_ln44_reg_282_reg_n_2_[0]\ : STD_LOGIC;
  signal icmp_ln879_fu_250_p2 : STD_LOGIC;
  signal icmp_ln879_reg_307 : STD_LOGIC;
  signal \icmp_ln879_reg_307[0]_i_1_n_2\ : STD_LOGIC;
  signal localmem_U_n_67 : STD_LOGIC;
  signal localmem_U_n_68 : STD_LOGIC;
  signal localmem_U_n_71 : STD_LOGIC;
  signal localmem_load_reg_3110 : STD_LOGIC;
  signal localmem_q0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \mul_ln49_fu_256_p2__0_n_100\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_101\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_102\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_103\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_104\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_105\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_106\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_107\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_108\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_109\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_110\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_111\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_112\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_113\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_114\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_115\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_116\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_117\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_118\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_119\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_120\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_121\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_122\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_123\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_124\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_125\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_126\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_127\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_128\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_129\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_130\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_131\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_132\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_133\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_134\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_135\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_136\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_137\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_138\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_139\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_140\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_141\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_142\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_143\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_144\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_145\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_146\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_147\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_148\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_149\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_150\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_151\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_152\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_153\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_154\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_155\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_60\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_61\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_62\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_63\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_64\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_65\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_66\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_67\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_68\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_69\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_70\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_71\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_72\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_73\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_74\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_75\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_76\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_77\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_78\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_79\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_80\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_81\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_82\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_83\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_84\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_85\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_86\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_87\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_88\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_89\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_90\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_91\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_92\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_93\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_94\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_95\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_96\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_97\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_98\ : STD_LOGIC;
  signal \mul_ln49_fu_256_p2__0_n_99\ : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_100 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_101 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_102 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_103 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_104 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_105 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_106 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_107 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_108 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_109 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_110 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_111 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_112 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_113 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_114 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_115 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_116 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_117 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_118 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_119 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_120 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_121 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_122 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_123 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_124 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_125 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_126 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_127 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_128 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_129 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_130 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_131 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_132 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_133 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_134 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_135 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_136 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_137 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_138 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_139 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_140 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_141 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_142 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_143 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_144 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_145 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_146 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_147 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_148 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_149 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_150 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_151 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_152 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_153 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_154 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_155 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_60 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_61 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_62 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_63 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_64 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_65 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_66 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_67 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_68 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_69 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_70 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_71 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_72 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_73 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_74 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_75 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_76 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_77 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_78 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_79 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_80 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_81 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_82 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_83 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_84 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_85 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_86 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_87 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_88 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_89 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_90 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_91 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_92 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_93 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_94 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_95 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_96 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_97 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_98 : STD_LOGIC;
  signal mul_ln49_fu_256_p2_n_99 : STD_LOGIC;
  signal mul_ln49_reg_3160 : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[0]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[10]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[11]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[12]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[13]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[14]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[15]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[16]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[1]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[2]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[3]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[4]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[5]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[6]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[7]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[8]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg[9]__0_n_2\ : STD_LOGIC;
  signal \mul_ln49_reg_316_reg__1\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal mul_ln49_reg_316_reg_n_100 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_101 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_102 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_103 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_104 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_105 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_106 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_107 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_60 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_61 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_62 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_63 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_64 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_65 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_66 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_67 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_68 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_69 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_70 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_71 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_72 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_73 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_74 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_75 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_76 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_77 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_78 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_79 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_80 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_81 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_82 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_83 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_84 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_85 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_86 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_87 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_88 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_89 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_90 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_91 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_92 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_93 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_94 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_95 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_96 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_97 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_98 : STD_LOGIC;
  signal mul_ln49_reg_316_reg_n_99 : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal \^strm_in_tready\ : STD_LOGIC;
  signal strm_in_V_data_0_ack_in : STD_LOGIC;
  signal strm_in_V_data_0_load_A : STD_LOGIC;
  signal strm_in_V_data_0_load_B : STD_LOGIC;
  signal strm_in_V_data_0_payload_A : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal strm_in_V_data_0_payload_B : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal strm_in_V_data_0_sel : STD_LOGIC;
  signal strm_in_V_data_0_sel2 : STD_LOGIC;
  signal strm_in_V_data_0_sel_rd_i_1_n_2 : STD_LOGIC;
  signal strm_in_V_data_0_sel_wr : STD_LOGIC;
  signal strm_in_V_data_0_sel_wr_i_1_n_2 : STD_LOGIC;
  signal strm_in_V_data_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \strm_in_V_data_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \strm_in_V_data_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal strm_in_V_last_V_0_payload_A : STD_LOGIC;
  signal \strm_in_V_last_V_0_payload_A[0]_i_1_n_2\ : STD_LOGIC;
  signal strm_in_V_last_V_0_payload_B : STD_LOGIC;
  signal \strm_in_V_last_V_0_payload_B[0]_i_1_n_2\ : STD_LOGIC;
  signal strm_in_V_last_V_0_sel : STD_LOGIC;
  signal strm_in_V_last_V_0_sel_rd_i_1_n_2 : STD_LOGIC;
  signal strm_in_V_last_V_0_sel_wr : STD_LOGIC;
  signal strm_in_V_last_V_0_sel_wr_i_1_n_2 : STD_LOGIC;
  signal strm_in_V_last_V_0_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \strm_in_V_last_V_0_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \strm_in_V_last_V_0_state[1]_i_3_n_2\ : STD_LOGIC;
  signal \strm_in_V_last_V_0_state[1]_i_4_n_2\ : STD_LOGIC;
  signal \strm_in_V_last_V_0_state_reg_n_2_[0]\ : STD_LOGIC;
  signal \^strm_out_tvalid\ : STD_LOGIC;
  signal strm_out_V_data_1_ack_in : STD_LOGIC;
  signal strm_out_V_data_1_load_A : STD_LOGIC;
  signal strm_out_V_data_1_load_B : STD_LOGIC;
  signal strm_out_V_data_1_payload_A : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal strm_out_V_data_1_payload_B : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal strm_out_V_data_1_sel : STD_LOGIC;
  signal strm_out_V_data_1_sel_rd_i_1_n_2 : STD_LOGIC;
  signal strm_out_V_data_1_sel_wr : STD_LOGIC;
  signal strm_out_V_data_1_sel_wr_i_1_n_2 : STD_LOGIC;
  signal strm_out_V_data_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \strm_out_V_data_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \strm_out_V_data_1_state_reg_n_2_[0]\ : STD_LOGIC;
  signal strm_out_V_last_V_1_ack_in : STD_LOGIC;
  signal strm_out_V_last_V_1_payload_A : STD_LOGIC;
  signal \strm_out_V_last_V_1_payload_A[0]_i_1_n_2\ : STD_LOGIC;
  signal strm_out_V_last_V_1_payload_B : STD_LOGIC;
  signal \strm_out_V_last_V_1_payload_B[0]_i_1_n_2\ : STD_LOGIC;
  signal strm_out_V_last_V_1_sel : STD_LOGIC;
  signal strm_out_V_last_V_1_sel_rd_i_1_n_2 : STD_LOGIC;
  signal strm_out_V_last_V_1_sel_wr : STD_LOGIC;
  signal strm_out_V_last_V_1_sel_wr_i_1_n_2 : STD_LOGIC;
  signal strm_out_V_last_V_1_state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \strm_out_V_last_V_1_state[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_3_1_2_reg_129[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_3_1_2_reg_129_reg_n_2_[0]\ : STD_LOGIC;
  signal tmp_3_1_3_reg_142 : STD_LOGIC;
  signal \tmp_3_1_3_reg_142[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmp_3_1_3_reg_142[0]_i_2_n_2\ : STD_LOGIC;
  signal tmp_last_V_1_reg_296 : STD_LOGIC;
  signal \tmp_last_V_1_reg_296[0]_i_1_n_2\ : STD_LOGIC;
  signal tmpa_last_V_reg_1641 : STD_LOGIC;
  signal \tmpa_last_V_reg_164[0]_i_1_n_2\ : STD_LOGIC;
  signal \tmpa_last_V_reg_164[0]_i_2_n_2\ : STD_LOGIC;
  signal \tmpa_last_V_reg_164[0]_i_3_n_2\ : STD_LOGIC;
  signal \tmpa_last_V_reg_164[0]_i_4_n_2\ : STD_LOGIC;
  signal \tmpa_last_V_reg_164_reg_n_2_[0]\ : STD_LOGIC;
  signal val_assign_reg_107 : STD_LOGIC;
  signal val_assign_reg_1070 : STD_LOGIC;
  signal \val_assign_reg_107[9]_i_5_n_2\ : STD_LOGIC;
  signal val_assign_reg_107_reg : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \val_assign_reg_107_reg__0\ : STD_LOGIC_VECTOR ( 9 downto 5 );
  signal vect_size_V : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal we0 : STD_LOGIC;
  signal \NLW_acc_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_acc_reg[31]_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_i_op_assign_reg_153_reg[9]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_mul_ln49_fu_256_p2_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_fu_256_p2_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_fu_256_p2_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_fu_256_p2_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_fu_256_p2_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_fu_256_p2_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_fu_256_p2_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_mul_ln49_fu_256_p2_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_mul_ln49_fu_256_p2_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_mul_ln49_fu_256_p2__0_CARRYCASCOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_mul_ln49_fu_256_p2__0_MULTSIGNOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_mul_ln49_fu_256_p2__0_OVERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_mul_ln49_fu_256_p2__0_PATTERNBDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_mul_ln49_fu_256_p2__0_PATTERNDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_mul_ln49_fu_256_p2__0_UNDERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_mul_ln49_fu_256_p2__0_ACOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal \NLW_mul_ln49_fu_256_p2__0_BCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \NLW_mul_ln49_fu_256_p2__0_CARRYOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_mul_ln49_reg_316_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_reg_316_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_reg_316_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_reg_316_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_reg_316_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_reg_316_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_mul_ln49_reg_316_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_mul_ln49_reg_316_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_mul_ln49_reg_316_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_mul_ln49_reg_316_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ap_CS_fsm[4]_i_2\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \ap_CS_fsm[5]_i_1\ : label is "soft_lutpair8";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[4]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[5]\ : label is "none";
  attribute SOFT_HLUTNM of ap_enable_reg_pp0_iter0_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of ap_enable_reg_pp0_iter1_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of ap_enable_reg_pp0_iter2_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[0]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[1]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[3]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[4]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[6]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[7]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[8]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \i_op_assign_reg_153[9]_i_3\ : label is "soft_lutpair7";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of mul_ln49_fu_256_p2 : label is "{SYNTH-10 {cell *THIS*} {string 15x18 4}}";
  attribute METHODOLOGY_DRC_VIOS of \mul_ln49_fu_256_p2__0\ : label is "{SYNTH-10 {cell *THIS*} {string 18x18 4}}";
  attribute METHODOLOGY_DRC_VIOS of mul_ln49_reg_316_reg : label is "{SYNTH-10 {cell *THIS*} {string 18x15 4}}";
  attribute SOFT_HLUTNM of strm_in_V_data_0_sel_wr_i_1 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of strm_in_V_last_V_0_sel_wr_i_1 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \strm_in_V_last_V_0_state[1]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \strm_in_V_last_V_0_state[1]_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \strm_out_TDATA[0]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \strm_out_TDATA[10]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \strm_out_TDATA[11]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \strm_out_TDATA[12]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \strm_out_TDATA[13]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \strm_out_TDATA[14]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \strm_out_TDATA[15]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \strm_out_TDATA[16]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \strm_out_TDATA[17]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \strm_out_TDATA[18]_INST_0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \strm_out_TDATA[19]_INST_0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \strm_out_TDATA[1]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \strm_out_TDATA[20]_INST_0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \strm_out_TDATA[21]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \strm_out_TDATA[22]_INST_0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \strm_out_TDATA[23]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \strm_out_TDATA[24]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \strm_out_TDATA[25]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \strm_out_TDATA[26]_INST_0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \strm_out_TDATA[27]_INST_0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \strm_out_TDATA[28]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \strm_out_TDATA[29]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \strm_out_TDATA[2]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \strm_out_TDATA[30]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \strm_out_TDATA[31]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \strm_out_TDATA[3]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \strm_out_TDATA[4]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \strm_out_TDATA[5]_INST_0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \strm_out_TDATA[6]_INST_0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \strm_out_TDATA[7]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \strm_out_TDATA[8]_INST_0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \strm_out_TDATA[9]_INST_0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \strm_out_TLAST[0]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of strm_out_V_data_1_sel_rd_i_1 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of strm_out_V_data_1_sel_wr_i_1 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \strm_out_V_data_1_state[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of strm_out_V_last_V_1_sel_rd_i_1 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \tmp_3_1_2_reg_129[0]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tmpa_last_V_reg_164[0]_i_4\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \val_assign_reg_107[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \val_assign_reg_107[2]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \val_assign_reg_107[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \val_assign_reg_107[4]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \val_assign_reg_107[6]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \val_assign_reg_107[7]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \val_assign_reg_107[8]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \val_assign_reg_107[9]_i_3\ : label is "soft_lutpair6";
begin
  strm_in_TREADY <= \^strm_in_tready\;
  strm_out_TVALID <= \^strm_out_tvalid\;
\acc[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[11]\,
      I1 => \mul_ln49_reg_316_reg[11]__0_n_2\,
      O => \acc[11]_i_2_n_2\
    );
\acc[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[10]\,
      I1 => \mul_ln49_reg_316_reg[10]__0_n_2\,
      O => \acc[11]_i_3_n_2\
    );
\acc[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[9]\,
      I1 => \mul_ln49_reg_316_reg[9]__0_n_2\,
      O => \acc[11]_i_4_n_2\
    );
\acc[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[8]\,
      I1 => \mul_ln49_reg_316_reg[8]__0_n_2\,
      O => \acc[11]_i_5_n_2\
    );
\acc[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[15]\,
      I1 => \mul_ln49_reg_316_reg[15]__0_n_2\,
      O => \acc[15]_i_2_n_2\
    );
\acc[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[14]\,
      I1 => \mul_ln49_reg_316_reg[14]__0_n_2\,
      O => \acc[15]_i_3_n_2\
    );
\acc[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[13]\,
      I1 => \mul_ln49_reg_316_reg[13]__0_n_2\,
      O => \acc[15]_i_4_n_2\
    );
\acc[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[12]\,
      I1 => \mul_ln49_reg_316_reg[12]__0_n_2\,
      O => \acc[15]_i_5_n_2\
    );
\acc[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[19]\,
      I1 => \mul_ln49_reg_316_reg__1\(19),
      O => \acc[19]_i_2_n_2\
    );
\acc[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[18]\,
      I1 => \mul_ln49_reg_316_reg__1\(18),
      O => \acc[19]_i_3_n_2\
    );
\acc[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[17]\,
      I1 => \mul_ln49_reg_316_reg__1\(17),
      O => \acc[19]_i_4_n_2\
    );
\acc[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[16]\,
      I1 => \mul_ln49_reg_316_reg__1\(16),
      O => \acc[19]_i_5_n_2\
    );
\acc[19]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_105,
      I1 => mul_ln49_fu_256_p2_n_105,
      O => \acc[19]_i_7_n_2\
    );
\acc[19]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_106,
      I1 => mul_ln49_fu_256_p2_n_106,
      O => \acc[19]_i_8_n_2\
    );
\acc[19]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_107,
      I1 => mul_ln49_fu_256_p2_n_107,
      O => \acc[19]_i_9_n_2\
    );
\acc[23]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_104,
      I1 => mul_ln49_fu_256_p2_n_104,
      O => \acc[23]_i_10_n_2\
    );
\acc[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[23]\,
      I1 => \mul_ln49_reg_316_reg__1\(23),
      O => \acc[23]_i_2_n_2\
    );
\acc[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[22]\,
      I1 => \mul_ln49_reg_316_reg__1\(22),
      O => \acc[23]_i_3_n_2\
    );
\acc[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[21]\,
      I1 => \mul_ln49_reg_316_reg__1\(21),
      O => \acc[23]_i_4_n_2\
    );
\acc[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[20]\,
      I1 => \mul_ln49_reg_316_reg__1\(20),
      O => \acc[23]_i_5_n_2\
    );
\acc[23]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_101,
      I1 => mul_ln49_fu_256_p2_n_101,
      O => \acc[23]_i_7_n_2\
    );
\acc[23]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_102,
      I1 => mul_ln49_fu_256_p2_n_102,
      O => \acc[23]_i_8_n_2\
    );
\acc[23]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_103,
      I1 => mul_ln49_fu_256_p2_n_103,
      O => \acc[23]_i_9_n_2\
    );
\acc[27]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_100,
      I1 => mul_ln49_fu_256_p2_n_100,
      O => \acc[27]_i_10_n_2\
    );
\acc[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[27]\,
      I1 => \mul_ln49_reg_316_reg__1\(27),
      O => \acc[27]_i_2_n_2\
    );
\acc[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[26]\,
      I1 => \mul_ln49_reg_316_reg__1\(26),
      O => \acc[27]_i_3_n_2\
    );
\acc[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[25]\,
      I1 => \mul_ln49_reg_316_reg__1\(25),
      O => \acc[27]_i_4_n_2\
    );
\acc[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[24]\,
      I1 => \mul_ln49_reg_316_reg__1\(24),
      O => \acc[27]_i_5_n_2\
    );
\acc[27]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_97,
      I1 => mul_ln49_fu_256_p2_n_97,
      O => \acc[27]_i_7_n_2\
    );
\acc[27]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_98,
      I1 => mul_ln49_fu_256_p2_n_98,
      O => \acc[27]_i_8_n_2\
    );
\acc[27]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_99,
      I1 => mul_ln49_fu_256_p2_n_99,
      O => \acc[27]_i_9_n_2\
    );
\acc[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAFFFF00000000"
    )
        port map (
      I0 => icmp_ln44_reg_282_pp0_iter2_reg,
      I1 => icmp_ln44_fu_225_p2,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => ap_enable_reg_pp0_iter3_reg_n_2,
      I5 => ap_CS_fsm_state3,
      O => acc
    );
\acc[31]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_94,
      I1 => mul_ln49_fu_256_p2_n_94,
      O => \acc[31]_i_10_n_2\
    );
\acc[31]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_95,
      I1 => mul_ln49_fu_256_p2_n_95,
      O => \acc[31]_i_11_n_2\
    );
\acc[31]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_96,
      I1 => mul_ln49_fu_256_p2_n_96,
      O => \acc[31]_i_12_n_2\
    );
\acc[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55450000"
    )
        port map (
      I0 => icmp_ln44_reg_282_pp0_iter2_reg,
      I1 => icmp_ln44_fu_225_p2,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => ap_enable_reg_pp0_iter3_reg_n_2,
      O => acc0
    );
\acc[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[31]\,
      I1 => \mul_ln49_reg_316_reg__1\(31),
      O => \acc[31]_i_4_n_2\
    );
\acc[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[30]\,
      I1 => \mul_ln49_reg_316_reg__1\(30),
      O => \acc[31]_i_5_n_2\
    );
\acc[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[29]\,
      I1 => \mul_ln49_reg_316_reg__1\(29),
      O => \acc[31]_i_6_n_2\
    );
\acc[31]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[28]\,
      I1 => \mul_ln49_reg_316_reg__1\(28),
      O => \acc[31]_i_7_n_2\
    );
\acc[31]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln49_reg_316_reg_n_93,
      I1 => mul_ln49_fu_256_p2_n_93,
      O => \acc[31]_i_9_n_2\
    );
\acc[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[3]\,
      I1 => \mul_ln49_reg_316_reg[3]__0_n_2\,
      O => \acc[3]_i_2_n_2\
    );
\acc[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[2]\,
      I1 => \mul_ln49_reg_316_reg[2]__0_n_2\,
      O => \acc[3]_i_3_n_2\
    );
\acc[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[1]\,
      I1 => \mul_ln49_reg_316_reg[1]__0_n_2\,
      O => \acc[3]_i_4_n_2\
    );
\acc[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[0]\,
      I1 => \mul_ln49_reg_316_reg[0]__0_n_2\,
      O => \acc[3]_i_5_n_2\
    );
\acc[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[7]\,
      I1 => \mul_ln49_reg_316_reg[7]__0_n_2\,
      O => \acc[7]_i_2_n_2\
    );
\acc[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[6]\,
      I1 => \mul_ln49_reg_316_reg[6]__0_n_2\,
      O => \acc[7]_i_3_n_2\
    );
\acc[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[5]\,
      I1 => \mul_ln49_reg_316_reg[5]__0_n_2\,
      O => \acc[7]_i_4_n_2\
    );
\acc[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \acc_reg_n_2_[4]\,
      I1 => \mul_ln49_reg_316_reg[4]__0_n_2\,
      O => \acc[7]_i_5_n_2\
    );
\acc_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(0),
      Q => \acc_reg_n_2_[0]\,
      R => acc
    );
\acc_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(10),
      Q => \acc_reg_n_2_[10]\,
      R => acc
    );
\acc_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(11),
      Q => \acc_reg_n_2_[11]\,
      R => acc
    );
\acc_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[7]_i_1_n_2\,
      CO(3) => \acc_reg[11]_i_1_n_2\,
      CO(2) => \acc_reg[11]_i_1_n_3\,
      CO(1) => \acc_reg[11]_i_1_n_4\,
      CO(0) => \acc_reg[11]_i_1_n_5\,
      CYINIT => '0',
      DI(3) => \acc_reg_n_2_[11]\,
      DI(2) => \acc_reg_n_2_[10]\,
      DI(1) => \acc_reg_n_2_[9]\,
      DI(0) => \acc_reg_n_2_[8]\,
      O(3 downto 0) => add_ln50_fu_260_p2(11 downto 8),
      S(3) => \acc[11]_i_2_n_2\,
      S(2) => \acc[11]_i_3_n_2\,
      S(1) => \acc[11]_i_4_n_2\,
      S(0) => \acc[11]_i_5_n_2\
    );
\acc_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(12),
      Q => \acc_reg_n_2_[12]\,
      R => acc
    );
\acc_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(13),
      Q => \acc_reg_n_2_[13]\,
      R => acc
    );
\acc_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(14),
      Q => \acc_reg_n_2_[14]\,
      R => acc
    );
\acc_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(15),
      Q => \acc_reg_n_2_[15]\,
      R => acc
    );
\acc_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[11]_i_1_n_2\,
      CO(3) => \acc_reg[15]_i_1_n_2\,
      CO(2) => \acc_reg[15]_i_1_n_3\,
      CO(1) => \acc_reg[15]_i_1_n_4\,
      CO(0) => \acc_reg[15]_i_1_n_5\,
      CYINIT => '0',
      DI(3) => \acc_reg_n_2_[15]\,
      DI(2) => \acc_reg_n_2_[14]\,
      DI(1) => \acc_reg_n_2_[13]\,
      DI(0) => \acc_reg_n_2_[12]\,
      O(3 downto 0) => add_ln50_fu_260_p2(15 downto 12),
      S(3) => \acc[15]_i_2_n_2\,
      S(2) => \acc[15]_i_3_n_2\,
      S(1) => \acc[15]_i_4_n_2\,
      S(0) => \acc[15]_i_5_n_2\
    );
\acc_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(16),
      Q => \acc_reg_n_2_[16]\,
      R => acc
    );
\acc_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(17),
      Q => \acc_reg_n_2_[17]\,
      R => acc
    );
\acc_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(18),
      Q => \acc_reg_n_2_[18]\,
      R => acc
    );
\acc_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(19),
      Q => \acc_reg_n_2_[19]\,
      R => acc
    );
\acc_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[15]_i_1_n_2\,
      CO(3) => \acc_reg[19]_i_1_n_2\,
      CO(2) => \acc_reg[19]_i_1_n_3\,
      CO(1) => \acc_reg[19]_i_1_n_4\,
      CO(0) => \acc_reg[19]_i_1_n_5\,
      CYINIT => '0',
      DI(3) => \acc_reg_n_2_[19]\,
      DI(2) => \acc_reg_n_2_[18]\,
      DI(1) => \acc_reg_n_2_[17]\,
      DI(0) => \acc_reg_n_2_[16]\,
      O(3 downto 0) => add_ln50_fu_260_p2(19 downto 16),
      S(3) => \acc[19]_i_2_n_2\,
      S(2) => \acc[19]_i_3_n_2\,
      S(1) => \acc[19]_i_4_n_2\,
      S(0) => \acc[19]_i_5_n_2\
    );
\acc_reg[19]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \acc_reg[19]_i_6_n_2\,
      CO(2) => \acc_reg[19]_i_6_n_3\,
      CO(1) => \acc_reg[19]_i_6_n_4\,
      CO(0) => \acc_reg[19]_i_6_n_5\,
      CYINIT => '0',
      DI(3) => mul_ln49_reg_316_reg_n_105,
      DI(2) => mul_ln49_reg_316_reg_n_106,
      DI(1) => mul_ln49_reg_316_reg_n_107,
      DI(0) => '0',
      O(3 downto 0) => \mul_ln49_reg_316_reg__1\(19 downto 16),
      S(3) => \acc[19]_i_7_n_2\,
      S(2) => \acc[19]_i_8_n_2\,
      S(1) => \acc[19]_i_9_n_2\,
      S(0) => \mul_ln49_reg_316_reg[16]__0_n_2\
    );
\acc_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(1),
      Q => \acc_reg_n_2_[1]\,
      R => acc
    );
\acc_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(20),
      Q => \acc_reg_n_2_[20]\,
      R => acc
    );
\acc_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(21),
      Q => \acc_reg_n_2_[21]\,
      R => acc
    );
\acc_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(22),
      Q => \acc_reg_n_2_[22]\,
      R => acc
    );
\acc_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(23),
      Q => \acc_reg_n_2_[23]\,
      R => acc
    );
\acc_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[19]_i_1_n_2\,
      CO(3) => \acc_reg[23]_i_1_n_2\,
      CO(2) => \acc_reg[23]_i_1_n_3\,
      CO(1) => \acc_reg[23]_i_1_n_4\,
      CO(0) => \acc_reg[23]_i_1_n_5\,
      CYINIT => '0',
      DI(3) => \acc_reg_n_2_[23]\,
      DI(2) => \acc_reg_n_2_[22]\,
      DI(1) => \acc_reg_n_2_[21]\,
      DI(0) => \acc_reg_n_2_[20]\,
      O(3 downto 0) => add_ln50_fu_260_p2(23 downto 20),
      S(3) => \acc[23]_i_2_n_2\,
      S(2) => \acc[23]_i_3_n_2\,
      S(1) => \acc[23]_i_4_n_2\,
      S(0) => \acc[23]_i_5_n_2\
    );
\acc_reg[23]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[19]_i_6_n_2\,
      CO(3) => \acc_reg[23]_i_6_n_2\,
      CO(2) => \acc_reg[23]_i_6_n_3\,
      CO(1) => \acc_reg[23]_i_6_n_4\,
      CO(0) => \acc_reg[23]_i_6_n_5\,
      CYINIT => '0',
      DI(3) => mul_ln49_reg_316_reg_n_101,
      DI(2) => mul_ln49_reg_316_reg_n_102,
      DI(1) => mul_ln49_reg_316_reg_n_103,
      DI(0) => mul_ln49_reg_316_reg_n_104,
      O(3 downto 0) => \mul_ln49_reg_316_reg__1\(23 downto 20),
      S(3) => \acc[23]_i_7_n_2\,
      S(2) => \acc[23]_i_8_n_2\,
      S(1) => \acc[23]_i_9_n_2\,
      S(0) => \acc[23]_i_10_n_2\
    );
\acc_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(24),
      Q => \acc_reg_n_2_[24]\,
      R => acc
    );
\acc_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(25),
      Q => \acc_reg_n_2_[25]\,
      R => acc
    );
\acc_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(26),
      Q => \acc_reg_n_2_[26]\,
      R => acc
    );
\acc_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(27),
      Q => \acc_reg_n_2_[27]\,
      R => acc
    );
\acc_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[23]_i_1_n_2\,
      CO(3) => \acc_reg[27]_i_1_n_2\,
      CO(2) => \acc_reg[27]_i_1_n_3\,
      CO(1) => \acc_reg[27]_i_1_n_4\,
      CO(0) => \acc_reg[27]_i_1_n_5\,
      CYINIT => '0',
      DI(3) => \acc_reg_n_2_[27]\,
      DI(2) => \acc_reg_n_2_[26]\,
      DI(1) => \acc_reg_n_2_[25]\,
      DI(0) => \acc_reg_n_2_[24]\,
      O(3 downto 0) => add_ln50_fu_260_p2(27 downto 24),
      S(3) => \acc[27]_i_2_n_2\,
      S(2) => \acc[27]_i_3_n_2\,
      S(1) => \acc[27]_i_4_n_2\,
      S(0) => \acc[27]_i_5_n_2\
    );
\acc_reg[27]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[23]_i_6_n_2\,
      CO(3) => \acc_reg[27]_i_6_n_2\,
      CO(2) => \acc_reg[27]_i_6_n_3\,
      CO(1) => \acc_reg[27]_i_6_n_4\,
      CO(0) => \acc_reg[27]_i_6_n_5\,
      CYINIT => '0',
      DI(3) => mul_ln49_reg_316_reg_n_97,
      DI(2) => mul_ln49_reg_316_reg_n_98,
      DI(1) => mul_ln49_reg_316_reg_n_99,
      DI(0) => mul_ln49_reg_316_reg_n_100,
      O(3 downto 0) => \mul_ln49_reg_316_reg__1\(27 downto 24),
      S(3) => \acc[27]_i_7_n_2\,
      S(2) => \acc[27]_i_8_n_2\,
      S(1) => \acc[27]_i_9_n_2\,
      S(0) => \acc[27]_i_10_n_2\
    );
\acc_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(28),
      Q => \acc_reg_n_2_[28]\,
      R => acc
    );
\acc_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(29),
      Q => \acc_reg_n_2_[29]\,
      R => acc
    );
\acc_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(2),
      Q => \acc_reg_n_2_[2]\,
      R => acc
    );
\acc_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(30),
      Q => \acc_reg_n_2_[30]\,
      R => acc
    );
\acc_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(31),
      Q => \acc_reg_n_2_[31]\,
      R => acc
    );
\acc_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[27]_i_1_n_2\,
      CO(3) => \NLW_acc_reg[31]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \acc_reg[31]_i_3_n_3\,
      CO(1) => \acc_reg[31]_i_3_n_4\,
      CO(0) => \acc_reg[31]_i_3_n_5\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \acc_reg_n_2_[30]\,
      DI(1) => \acc_reg_n_2_[29]\,
      DI(0) => \acc_reg_n_2_[28]\,
      O(3 downto 0) => add_ln50_fu_260_p2(31 downto 28),
      S(3) => \acc[31]_i_4_n_2\,
      S(2) => \acc[31]_i_5_n_2\,
      S(1) => \acc[31]_i_6_n_2\,
      S(0) => \acc[31]_i_7_n_2\
    );
\acc_reg[31]_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[27]_i_6_n_2\,
      CO(3) => \NLW_acc_reg[31]_i_8_CO_UNCONNECTED\(3),
      CO(2) => \acc_reg[31]_i_8_n_3\,
      CO(1) => \acc_reg[31]_i_8_n_4\,
      CO(0) => \acc_reg[31]_i_8_n_5\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => mul_ln49_reg_316_reg_n_94,
      DI(1) => mul_ln49_reg_316_reg_n_95,
      DI(0) => mul_ln49_reg_316_reg_n_96,
      O(3 downto 0) => \mul_ln49_reg_316_reg__1\(31 downto 28),
      S(3) => \acc[31]_i_9_n_2\,
      S(2) => \acc[31]_i_10_n_2\,
      S(1) => \acc[31]_i_11_n_2\,
      S(0) => \acc[31]_i_12_n_2\
    );
\acc_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(3),
      Q => \acc_reg_n_2_[3]\,
      R => acc
    );
\acc_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \acc_reg[3]_i_1_n_2\,
      CO(2) => \acc_reg[3]_i_1_n_3\,
      CO(1) => \acc_reg[3]_i_1_n_4\,
      CO(0) => \acc_reg[3]_i_1_n_5\,
      CYINIT => '0',
      DI(3) => \acc_reg_n_2_[3]\,
      DI(2) => \acc_reg_n_2_[2]\,
      DI(1) => \acc_reg_n_2_[1]\,
      DI(0) => \acc_reg_n_2_[0]\,
      O(3 downto 0) => add_ln50_fu_260_p2(3 downto 0),
      S(3) => \acc[3]_i_2_n_2\,
      S(2) => \acc[3]_i_3_n_2\,
      S(1) => \acc[3]_i_4_n_2\,
      S(0) => \acc[3]_i_5_n_2\
    );
\acc_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(4),
      Q => \acc_reg_n_2_[4]\,
      R => acc
    );
\acc_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(5),
      Q => \acc_reg_n_2_[5]\,
      R => acc
    );
\acc_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(6),
      Q => \acc_reg_n_2_[6]\,
      R => acc
    );
\acc_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(7),
      Q => \acc_reg_n_2_[7]\,
      R => acc
    );
\acc_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_reg[3]_i_1_n_2\,
      CO(3) => \acc_reg[7]_i_1_n_2\,
      CO(2) => \acc_reg[7]_i_1_n_3\,
      CO(1) => \acc_reg[7]_i_1_n_4\,
      CO(0) => \acc_reg[7]_i_1_n_5\,
      CYINIT => '0',
      DI(3) => \acc_reg_n_2_[7]\,
      DI(2) => \acc_reg_n_2_[6]\,
      DI(1) => \acc_reg_n_2_[5]\,
      DI(0) => \acc_reg_n_2_[4]\,
      O(3 downto 0) => add_ln50_fu_260_p2(7 downto 4),
      S(3) => \acc[7]_i_2_n_2\,
      S(2) => \acc[7]_i_3_n_2\,
      S(1) => \acc[7]_i_4_n_2\,
      S(0) => \acc[7]_i_5_n_2\
    );
\acc_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(8),
      Q => \acc_reg_n_2_[8]\,
      R => acc
    );
\acc_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => acc0,
      D => add_ln50_fu_260_p2(9),
      Q => \acc_reg_n_2_[9]\,
      R => acc
    );
\ap_CS_fsm[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_state9,
      I1 => \ap_CS_fsm[0]_i_2_n_2\,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3FFF2F2FFFFFFFFF"
    )
        port map (
      I0 => \strm_out_V_data_1_state_reg_n_2_[0]\,
      I1 => strm_out_TREADY,
      I2 => strm_out_V_data_1_ack_in,
      I3 => strm_out_V_last_V_1_ack_in,
      I4 => \^strm_out_tvalid\,
      I5 => \tmpa_last_V_reg_164_reg_n_2_[0]\,
      O => \ap_CS_fsm[0]_i_2_n_2\
    );
\ap_CS_fsm[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => \ap_CS_fsm[2]_i_2_n_2\,
      I2 => ap_CS_fsm_state1,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04FF0404"
    )
        port map (
      I0 => \tmpa_last_V_reg_164_reg_n_2_[0]\,
      I1 => ap_CS_fsm_state9,
      I2 => \ap_CS_fsm[5]_i_2_n_2\,
      I3 => \ap_CS_fsm[2]_i_2_n_2\,
      I4 => ap_CS_fsm_state2,
      O => ap_NS_fsm(2)
    );
\ap_CS_fsm[2]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"37773FFF"
    )
        port map (
      I0 => grp_fu_182_p1,
      I1 => ap_CS_fsm_state2,
      I2 => localmem_U_n_67,
      I3 => localmem_U_n_68,
      I4 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      O => \ap_CS_fsm[2]_i_2_n_2\
    );
\ap_CS_fsm[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FAEAFAFA"
    )
        port map (
      I0 => ap_CS_fsm_state3,
      I1 => ap_enable_reg_pp0_iter2,
      I2 => ap_CS_fsm_pp0_stage0,
      I3 => ap_block_pp0_stage0_subdone,
      I4 => ap_enable_reg_pp0_iter3_reg_n_2,
      O => ap_NS_fsm(3)
    );
\ap_CS_fsm[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040FFFF00400040"
    )
        port map (
      I0 => ap_block_pp0_stage0_subdone,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => ap_enable_reg_pp0_iter3_reg_n_2,
      I3 => ap_enable_reg_pp0_iter2,
      I4 => strm_out_V_data_1_ack_in,
      I5 => ap_CS_fsm_state8,
      O => ap_NS_fsm(4)
    );
\ap_CS_fsm[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0444"
    )
        port map (
      I0 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => \strm_in_V_last_V_0_state[1]_i_4_n_2\,
      I3 => localmem_U_n_71,
      O => ap_block_pp0_stage0_subdone
    );
\ap_CS_fsm[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => ap_CS_fsm_state9,
      I1 => \ap_CS_fsm[5]_i_2_n_2\,
      I2 => ap_CS_fsm_state8,
      I3 => strm_out_V_data_1_ack_in,
      O => ap_NS_fsm(5)
    );
\ap_CS_fsm[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2FFF2FAF"
    )
        port map (
      I0 => \^strm_out_tvalid\,
      I1 => strm_out_V_last_V_1_ack_in,
      I2 => strm_out_V_data_1_ack_in,
      I3 => strm_out_TREADY,
      I4 => \strm_out_V_data_1_state_reg_n_2_[0]\,
      O => \ap_CS_fsm[5]_i_2_n_2\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => ap_CS_fsm_state1,
      S => reset
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => reset
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(2),
      Q => ap_CS_fsm_state3,
      R => reset
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(3),
      Q => ap_CS_fsm_pp0_stage0,
      R => reset
    );
\ap_CS_fsm_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(4),
      Q => ap_CS_fsm_state8,
      R => reset
    );
\ap_CS_fsm_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(5),
      Q => ap_CS_fsm_state9,
      R => reset
    );
ap_enable_reg_pp0_iter0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A800A800A800A8A8"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_CS_fsm_state3,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => ap_enable_reg_pp0_iter0_i_2_n_2,
      I4 => icmp_ln879_fu_250_p2,
      I5 => icmp_ln44_fu_225_p2,
      O => ap_enable_reg_pp0_iter0_i_1_n_2
    );
ap_enable_reg_pp0_iter0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0070FFFF"
    )
        port map (
      I0 => localmem_U_n_71,
      I1 => \strm_in_V_last_V_0_state[1]_i_4_n_2\,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => ap_CS_fsm_pp0_stage0,
      O => ap_enable_reg_pp0_iter0_i_2_n_2
    );
ap_enable_reg_pp0_iter0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter0_i_1_n_2,
      Q => ap_enable_reg_pp0_iter0,
      R => '0'
    );
ap_enable_reg_pp0_iter1_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCC8"
    )
        port map (
      I0 => icmp_ln44_fu_225_p2,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => ap_enable_reg_pp0_iter1,
      O => ap_enable_reg_pp0_iter1_i_1_n_2
    );
ap_enable_reg_pp0_iter1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter1_i_1_n_2,
      Q => ap_enable_reg_pp0_iter1,
      R => reset
    );
ap_enable_reg_pp0_iter2_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AABAAA8A"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter1,
      I1 => icmp_ln44_fu_225_p2,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => ap_enable_reg_pp0_iter2,
      O => ap_enable_reg_pp0_iter2_i_1_n_2
    );
ap_enable_reg_pp0_iter2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter2_i_1_n_2,
      Q => ap_enable_reg_pp0_iter2,
      R => reset
    );
ap_enable_reg_pp0_iter3_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00A088A0"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_enable_reg_pp0_iter3_reg_n_2,
      I2 => ap_enable_reg_pp0_iter2,
      I3 => ap_block_pp0_stage0_subdone,
      I4 => ap_CS_fsm_state3,
      O => ap_enable_reg_pp0_iter3_i_1_n_2
    );
ap_enable_reg_pp0_iter3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_enable_reg_pp0_iter3_i_1_n_2,
      Q => ap_enable_reg_pp0_iter3_reg_n_2,
      R => '0'
    );
\i_op_assign_reg_153[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(0),
      O => i_1_fu_231_p2(0)
    );
\i_op_assign_reg_153[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(0),
      I1 => i_op_assign_reg_153_reg(1),
      O => i_1_fu_231_p2(1)
    );
\i_op_assign_reg_153[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(0),
      I1 => i_op_assign_reg_153_reg(1),
      I2 => i_op_assign_reg_153_reg(2),
      O => i_1_fu_231_p2(2)
    );
\i_op_assign_reg_153[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(2),
      I1 => i_op_assign_reg_153_reg(1),
      I2 => i_op_assign_reg_153_reg(0),
      I3 => i_op_assign_reg_153_reg(3),
      O => i_1_fu_231_p2(3)
    );
\i_op_assign_reg_153[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(3),
      I1 => i_op_assign_reg_153_reg(0),
      I2 => i_op_assign_reg_153_reg(1),
      I3 => i_op_assign_reg_153_reg(2),
      I4 => i_op_assign_reg_153_reg(4),
      O => i_1_fu_231_p2(4)
    );
\i_op_assign_reg_153[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(2),
      I1 => i_op_assign_reg_153_reg(1),
      I2 => i_op_assign_reg_153_reg(0),
      I3 => i_op_assign_reg_153_reg(3),
      I4 => i_op_assign_reg_153_reg(4),
      I5 => i_op_assign_reg_153_reg(5),
      O => i_1_fu_231_p2(5)
    );
\i_op_assign_reg_153[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i_op_assign_reg_153[9]_i_5_n_2\,
      I1 => i_op_assign_reg_153_reg(6),
      O => i_1_fu_231_p2(6)
    );
\i_op_assign_reg_153[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \i_op_assign_reg_153[9]_i_5_n_2\,
      I1 => i_op_assign_reg_153_reg(6),
      I2 => i_op_assign_reg_153_reg(7),
      O => i_1_fu_231_p2(7)
    );
\i_op_assign_reg_153[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F708"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(7),
      I1 => i_op_assign_reg_153_reg(6),
      I2 => \i_op_assign_reg_153[9]_i_5_n_2\,
      I3 => i_op_assign_reg_153_reg(8),
      O => i_1_fu_231_p2(8)
    );
\i_op_assign_reg_153[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00000000"
    )
        port map (
      I0 => icmp_ln879_fu_250_p2,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => ap_CS_fsm_pp0_stage0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => icmp_ln44_fu_225_p2,
      I5 => ap_CS_fsm_state3,
      O => i_op_assign_reg_153
    );
\i_op_assign_reg_153[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00004000"
    )
        port map (
      I0 => icmp_ln879_fu_250_p2,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => ap_CS_fsm_pp0_stage0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => icmp_ln44_fu_225_p2,
      O => i_op_assign_reg_1530
    );
\i_op_assign_reg_153[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(8),
      I1 => \i_op_assign_reg_153[9]_i_5_n_2\,
      I2 => i_op_assign_reg_153_reg(6),
      I3 => i_op_assign_reg_153_reg(7),
      I4 => i_op_assign_reg_153_reg(9),
      O => i_1_fu_231_p2(9)
    );
\i_op_assign_reg_153[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(2),
      I1 => i_op_assign_reg_153_reg(1),
      I2 => i_op_assign_reg_153_reg(0),
      I3 => i_op_assign_reg_153_reg(3),
      I4 => i_op_assign_reg_153_reg(4),
      I5 => i_op_assign_reg_153_reg(5),
      O => \i_op_assign_reg_153[9]_i_5_n_2\
    );
\i_op_assign_reg_153[9]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(9),
      O => \i_op_assign_reg_153[9]_i_6_n_2\
    );
\i_op_assign_reg_153[9]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(8),
      I1 => i_op_assign_reg_153_reg(7),
      I2 => i_op_assign_reg_153_reg(6),
      O => \i_op_assign_reg_153[9]_i_7_n_2\
    );
\i_op_assign_reg_153[9]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"41000041"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(5),
      I1 => vect_size_V(4),
      I2 => i_op_assign_reg_153_reg(4),
      I3 => i_op_assign_reg_153_reg(3),
      I4 => vect_size_V(3),
      O => \i_op_assign_reg_153[9]_i_8_n_2\
    );
\i_op_assign_reg_153[9]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(0),
      I1 => vect_size_V(0),
      I2 => vect_size_V(2),
      I3 => i_op_assign_reg_153_reg(2),
      I4 => i_op_assign_reg_153_reg(1),
      I5 => vect_size_V(1),
      O => \i_op_assign_reg_153[9]_i_9_n_2\
    );
\i_op_assign_reg_153_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(0),
      Q => i_op_assign_reg_153_reg(0),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(1),
      Q => i_op_assign_reg_153_reg(1),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(2),
      Q => i_op_assign_reg_153_reg(2),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(3),
      Q => i_op_assign_reg_153_reg(3),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(4),
      Q => i_op_assign_reg_153_reg(4),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(5),
      Q => i_op_assign_reg_153_reg(5),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(6),
      Q => i_op_assign_reg_153_reg(6),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(7),
      Q => i_op_assign_reg_153_reg(7),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(8),
      Q => i_op_assign_reg_153_reg(8),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => i_op_assign_reg_1530,
      D => i_1_fu_231_p2(9),
      Q => i_op_assign_reg_153_reg(9),
      R => i_op_assign_reg_153
    );
\i_op_assign_reg_153_reg[9]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => icmp_ln879_fu_250_p2,
      CO(2) => \i_op_assign_reg_153_reg[9]_i_4_n_3\,
      CO(1) => \i_op_assign_reg_153_reg[9]_i_4_n_4\,
      CO(0) => \i_op_assign_reg_153_reg[9]_i_4_n_5\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_i_op_assign_reg_153_reg[9]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3) => \i_op_assign_reg_153[9]_i_6_n_2\,
      S(2) => \i_op_assign_reg_153[9]_i_7_n_2\,
      S(1) => \i_op_assign_reg_153[9]_i_8_n_2\,
      S(0) => \i_op_assign_reg_153[9]_i_9_n_2\
    );
\icmp_ln44_reg_282[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF75AA00"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => icmp_ln44_fu_225_p2,
      I4 => \icmp_ln44_reg_282_reg_n_2_[0]\,
      O => \icmp_ln44_reg_282[0]_i_1_n_2\
    );
\icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBFBB88888088"
    )
        port map (
      I0 => \icmp_ln44_reg_282_reg_n_2_[0]\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => icmp_ln44_fu_225_p2,
      I5 => icmp_ln44_reg_282_pp0_iter1_reg,
      O => \icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1_n_2\
    );
\icmp_ln44_reg_282_pp0_iter1_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1_n_2\,
      Q => icmp_ln44_reg_282_pp0_iter1_reg,
      R => '0'
    );
\icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AABAAA8A"
    )
        port map (
      I0 => icmp_ln44_reg_282_pp0_iter1_reg,
      I1 => icmp_ln44_fu_225_p2,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => icmp_ln44_reg_282_pp0_iter2_reg,
      O => \icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1_n_2\
    );
\icmp_ln44_reg_282_pp0_iter2_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1_n_2\,
      Q => icmp_ln44_reg_282_pp0_iter2_reg,
      R => '0'
    );
\icmp_ln44_reg_282_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \icmp_ln44_reg_282[0]_i_1_n_2\,
      Q => \icmp_ln44_reg_282_reg_n_2_[0]\,
      R => '0'
    );
\icmp_ln879_reg_307[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFBB00008088"
    )
        port map (
      I0 => icmp_ln879_fu_250_p2,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => icmp_ln44_fu_225_p2,
      I5 => icmp_ln879_reg_307,
      O => \icmp_ln879_reg_307[0]_i_1_n_2\
    );
\icmp_ln879_reg_307_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \icmp_ln879_reg_307[0]_i_1_n_2\,
      Q => icmp_ln879_reg_307,
      R => '0'
    );
localmem_U: entity work.bd_0_hls_inst_0_axis_fixed_macc_lbkb
     port map (
      Q(1) => ap_CS_fsm_pp0_stage0,
      Q(0) => ap_CS_fsm_state2,
      ap_clk => ap_clk,
      ap_enable_reg_pp0_iter0 => ap_enable_reg_pp0_iter0,
      d0(31 downto 0) => d0(31 downto 0),
      \i_op_assign_reg_153_reg[4]\ => localmem_U_n_71,
      icmp_ln34_fu_192_p2 => icmp_ln34_fu_192_p2,
      icmp_ln44_fu_225_p2 => icmp_ln44_fu_225_p2,
      localmem_q0(31 downto 0) => localmem_q0(31 downto 0),
      ram_reg => \strm_in_V_data_0_state_reg_n_2_[0]\,
      ram_reg_0(9 downto 5) => \val_assign_reg_107_reg__0\(9 downto 5),
      ram_reg_0(4 downto 0) => val_assign_reg_107_reg(4 downto 0),
      ram_reg_1(31 downto 0) => strm_in_V_data_0_payload_B(31 downto 0),
      ram_reg_2(31 downto 0) => strm_in_V_data_0_payload_A(31 downto 0),
      strm_in_V_data_0_sel => strm_in_V_data_0_sel,
      \tmp_3_1_3_reg_142[0]_i_2\(9 downto 0) => i_op_assign_reg_153_reg(9 downto 0),
      \val_assign_reg_107_reg[1]\ => localmem_U_n_67,
      \val_assign_reg_107_reg[4]\ => localmem_U_n_68,
      we0 => we0
    );
mul_ln49_fu_256_p2: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 2,
      BREG => 2,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => localmem_q0(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_mul_ln49_fu_256_p2_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => d0(31),
      B(16) => d0(31),
      B(15) => d0(31),
      B(14 downto 0) => d0(31 downto 17),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_mul_ln49_fu_256_p2_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_mul_ln49_fu_256_p2_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_mul_ln49_fu_256_p2_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => localmem_load_reg_3110,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => strm_in_V_data_0_sel2,
      CEB2 => tmpa_last_V_reg_1641,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => mul_ln49_reg_3160,
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_mul_ln49_fu_256_p2_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_mul_ln49_fu_256_p2_OVERFLOW_UNCONNECTED,
      P(47) => mul_ln49_fu_256_p2_n_60,
      P(46) => mul_ln49_fu_256_p2_n_61,
      P(45) => mul_ln49_fu_256_p2_n_62,
      P(44) => mul_ln49_fu_256_p2_n_63,
      P(43) => mul_ln49_fu_256_p2_n_64,
      P(42) => mul_ln49_fu_256_p2_n_65,
      P(41) => mul_ln49_fu_256_p2_n_66,
      P(40) => mul_ln49_fu_256_p2_n_67,
      P(39) => mul_ln49_fu_256_p2_n_68,
      P(38) => mul_ln49_fu_256_p2_n_69,
      P(37) => mul_ln49_fu_256_p2_n_70,
      P(36) => mul_ln49_fu_256_p2_n_71,
      P(35) => mul_ln49_fu_256_p2_n_72,
      P(34) => mul_ln49_fu_256_p2_n_73,
      P(33) => mul_ln49_fu_256_p2_n_74,
      P(32) => mul_ln49_fu_256_p2_n_75,
      P(31) => mul_ln49_fu_256_p2_n_76,
      P(30) => mul_ln49_fu_256_p2_n_77,
      P(29) => mul_ln49_fu_256_p2_n_78,
      P(28) => mul_ln49_fu_256_p2_n_79,
      P(27) => mul_ln49_fu_256_p2_n_80,
      P(26) => mul_ln49_fu_256_p2_n_81,
      P(25) => mul_ln49_fu_256_p2_n_82,
      P(24) => mul_ln49_fu_256_p2_n_83,
      P(23) => mul_ln49_fu_256_p2_n_84,
      P(22) => mul_ln49_fu_256_p2_n_85,
      P(21) => mul_ln49_fu_256_p2_n_86,
      P(20) => mul_ln49_fu_256_p2_n_87,
      P(19) => mul_ln49_fu_256_p2_n_88,
      P(18) => mul_ln49_fu_256_p2_n_89,
      P(17) => mul_ln49_fu_256_p2_n_90,
      P(16) => mul_ln49_fu_256_p2_n_91,
      P(15) => mul_ln49_fu_256_p2_n_92,
      P(14) => mul_ln49_fu_256_p2_n_93,
      P(13) => mul_ln49_fu_256_p2_n_94,
      P(12) => mul_ln49_fu_256_p2_n_95,
      P(11) => mul_ln49_fu_256_p2_n_96,
      P(10) => mul_ln49_fu_256_p2_n_97,
      P(9) => mul_ln49_fu_256_p2_n_98,
      P(8) => mul_ln49_fu_256_p2_n_99,
      P(7) => mul_ln49_fu_256_p2_n_100,
      P(6) => mul_ln49_fu_256_p2_n_101,
      P(5) => mul_ln49_fu_256_p2_n_102,
      P(4) => mul_ln49_fu_256_p2_n_103,
      P(3) => mul_ln49_fu_256_p2_n_104,
      P(2) => mul_ln49_fu_256_p2_n_105,
      P(1) => mul_ln49_fu_256_p2_n_106,
      P(0) => mul_ln49_fu_256_p2_n_107,
      PATTERNBDETECT => NLW_mul_ln49_fu_256_p2_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_mul_ln49_fu_256_p2_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => mul_ln49_fu_256_p2_n_108,
      PCOUT(46) => mul_ln49_fu_256_p2_n_109,
      PCOUT(45) => mul_ln49_fu_256_p2_n_110,
      PCOUT(44) => mul_ln49_fu_256_p2_n_111,
      PCOUT(43) => mul_ln49_fu_256_p2_n_112,
      PCOUT(42) => mul_ln49_fu_256_p2_n_113,
      PCOUT(41) => mul_ln49_fu_256_p2_n_114,
      PCOUT(40) => mul_ln49_fu_256_p2_n_115,
      PCOUT(39) => mul_ln49_fu_256_p2_n_116,
      PCOUT(38) => mul_ln49_fu_256_p2_n_117,
      PCOUT(37) => mul_ln49_fu_256_p2_n_118,
      PCOUT(36) => mul_ln49_fu_256_p2_n_119,
      PCOUT(35) => mul_ln49_fu_256_p2_n_120,
      PCOUT(34) => mul_ln49_fu_256_p2_n_121,
      PCOUT(33) => mul_ln49_fu_256_p2_n_122,
      PCOUT(32) => mul_ln49_fu_256_p2_n_123,
      PCOUT(31) => mul_ln49_fu_256_p2_n_124,
      PCOUT(30) => mul_ln49_fu_256_p2_n_125,
      PCOUT(29) => mul_ln49_fu_256_p2_n_126,
      PCOUT(28) => mul_ln49_fu_256_p2_n_127,
      PCOUT(27) => mul_ln49_fu_256_p2_n_128,
      PCOUT(26) => mul_ln49_fu_256_p2_n_129,
      PCOUT(25) => mul_ln49_fu_256_p2_n_130,
      PCOUT(24) => mul_ln49_fu_256_p2_n_131,
      PCOUT(23) => mul_ln49_fu_256_p2_n_132,
      PCOUT(22) => mul_ln49_fu_256_p2_n_133,
      PCOUT(21) => mul_ln49_fu_256_p2_n_134,
      PCOUT(20) => mul_ln49_fu_256_p2_n_135,
      PCOUT(19) => mul_ln49_fu_256_p2_n_136,
      PCOUT(18) => mul_ln49_fu_256_p2_n_137,
      PCOUT(17) => mul_ln49_fu_256_p2_n_138,
      PCOUT(16) => mul_ln49_fu_256_p2_n_139,
      PCOUT(15) => mul_ln49_fu_256_p2_n_140,
      PCOUT(14) => mul_ln49_fu_256_p2_n_141,
      PCOUT(13) => mul_ln49_fu_256_p2_n_142,
      PCOUT(12) => mul_ln49_fu_256_p2_n_143,
      PCOUT(11) => mul_ln49_fu_256_p2_n_144,
      PCOUT(10) => mul_ln49_fu_256_p2_n_145,
      PCOUT(9) => mul_ln49_fu_256_p2_n_146,
      PCOUT(8) => mul_ln49_fu_256_p2_n_147,
      PCOUT(7) => mul_ln49_fu_256_p2_n_148,
      PCOUT(6) => mul_ln49_fu_256_p2_n_149,
      PCOUT(5) => mul_ln49_fu_256_p2_n_150,
      PCOUT(4) => mul_ln49_fu_256_p2_n_151,
      PCOUT(3) => mul_ln49_fu_256_p2_n_152,
      PCOUT(2) => mul_ln49_fu_256_p2_n_153,
      PCOUT(1) => mul_ln49_fu_256_p2_n_154,
      PCOUT(0) => mul_ln49_fu_256_p2_n_155,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_mul_ln49_fu_256_p2_UNDERFLOW_UNCONNECTED
    );
\mul_ln49_fu_256_p2__0\: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => d0(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => \NLW_mul_ln49_fu_256_p2__0_ACOUT_UNCONNECTED\(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => '0',
      B(16 downto 0) => localmem_q0(16 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => \NLW_mul_ln49_fu_256_p2__0_BCOUT_UNCONNECTED\(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => \NLW_mul_ln49_fu_256_p2__0_CARRYCASCOUT_UNCONNECTED\,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => \NLW_mul_ln49_fu_256_p2__0_CARRYOUT_UNCONNECTED\(3 downto 0),
      CEA1 => strm_in_V_data_0_sel2,
      CEA2 => tmpa_last_V_reg_1641,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => localmem_load_reg_3110,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => \NLW_mul_ln49_fu_256_p2__0_MULTSIGNOUT_UNCONNECTED\,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => \NLW_mul_ln49_fu_256_p2__0_OVERFLOW_UNCONNECTED\,
      P(47) => \mul_ln49_fu_256_p2__0_n_60\,
      P(46) => \mul_ln49_fu_256_p2__0_n_61\,
      P(45) => \mul_ln49_fu_256_p2__0_n_62\,
      P(44) => \mul_ln49_fu_256_p2__0_n_63\,
      P(43) => \mul_ln49_fu_256_p2__0_n_64\,
      P(42) => \mul_ln49_fu_256_p2__0_n_65\,
      P(41) => \mul_ln49_fu_256_p2__0_n_66\,
      P(40) => \mul_ln49_fu_256_p2__0_n_67\,
      P(39) => \mul_ln49_fu_256_p2__0_n_68\,
      P(38) => \mul_ln49_fu_256_p2__0_n_69\,
      P(37) => \mul_ln49_fu_256_p2__0_n_70\,
      P(36) => \mul_ln49_fu_256_p2__0_n_71\,
      P(35) => \mul_ln49_fu_256_p2__0_n_72\,
      P(34) => \mul_ln49_fu_256_p2__0_n_73\,
      P(33) => \mul_ln49_fu_256_p2__0_n_74\,
      P(32) => \mul_ln49_fu_256_p2__0_n_75\,
      P(31) => \mul_ln49_fu_256_p2__0_n_76\,
      P(30) => \mul_ln49_fu_256_p2__0_n_77\,
      P(29) => \mul_ln49_fu_256_p2__0_n_78\,
      P(28) => \mul_ln49_fu_256_p2__0_n_79\,
      P(27) => \mul_ln49_fu_256_p2__0_n_80\,
      P(26) => \mul_ln49_fu_256_p2__0_n_81\,
      P(25) => \mul_ln49_fu_256_p2__0_n_82\,
      P(24) => \mul_ln49_fu_256_p2__0_n_83\,
      P(23) => \mul_ln49_fu_256_p2__0_n_84\,
      P(22) => \mul_ln49_fu_256_p2__0_n_85\,
      P(21) => \mul_ln49_fu_256_p2__0_n_86\,
      P(20) => \mul_ln49_fu_256_p2__0_n_87\,
      P(19) => \mul_ln49_fu_256_p2__0_n_88\,
      P(18) => \mul_ln49_fu_256_p2__0_n_89\,
      P(17) => \mul_ln49_fu_256_p2__0_n_90\,
      P(16) => \mul_ln49_fu_256_p2__0_n_91\,
      P(15) => \mul_ln49_fu_256_p2__0_n_92\,
      P(14) => \mul_ln49_fu_256_p2__0_n_93\,
      P(13) => \mul_ln49_fu_256_p2__0_n_94\,
      P(12) => \mul_ln49_fu_256_p2__0_n_95\,
      P(11) => \mul_ln49_fu_256_p2__0_n_96\,
      P(10) => \mul_ln49_fu_256_p2__0_n_97\,
      P(9) => \mul_ln49_fu_256_p2__0_n_98\,
      P(8) => \mul_ln49_fu_256_p2__0_n_99\,
      P(7) => \mul_ln49_fu_256_p2__0_n_100\,
      P(6) => \mul_ln49_fu_256_p2__0_n_101\,
      P(5) => \mul_ln49_fu_256_p2__0_n_102\,
      P(4) => \mul_ln49_fu_256_p2__0_n_103\,
      P(3) => \mul_ln49_fu_256_p2__0_n_104\,
      P(2) => \mul_ln49_fu_256_p2__0_n_105\,
      P(1) => \mul_ln49_fu_256_p2__0_n_106\,
      P(0) => \mul_ln49_fu_256_p2__0_n_107\,
      PATTERNBDETECT => \NLW_mul_ln49_fu_256_p2__0_PATTERNBDETECT_UNCONNECTED\,
      PATTERNDETECT => \NLW_mul_ln49_fu_256_p2__0_PATTERNDETECT_UNCONNECTED\,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => \mul_ln49_fu_256_p2__0_n_108\,
      PCOUT(46) => \mul_ln49_fu_256_p2__0_n_109\,
      PCOUT(45) => \mul_ln49_fu_256_p2__0_n_110\,
      PCOUT(44) => \mul_ln49_fu_256_p2__0_n_111\,
      PCOUT(43) => \mul_ln49_fu_256_p2__0_n_112\,
      PCOUT(42) => \mul_ln49_fu_256_p2__0_n_113\,
      PCOUT(41) => \mul_ln49_fu_256_p2__0_n_114\,
      PCOUT(40) => \mul_ln49_fu_256_p2__0_n_115\,
      PCOUT(39) => \mul_ln49_fu_256_p2__0_n_116\,
      PCOUT(38) => \mul_ln49_fu_256_p2__0_n_117\,
      PCOUT(37) => \mul_ln49_fu_256_p2__0_n_118\,
      PCOUT(36) => \mul_ln49_fu_256_p2__0_n_119\,
      PCOUT(35) => \mul_ln49_fu_256_p2__0_n_120\,
      PCOUT(34) => \mul_ln49_fu_256_p2__0_n_121\,
      PCOUT(33) => \mul_ln49_fu_256_p2__0_n_122\,
      PCOUT(32) => \mul_ln49_fu_256_p2__0_n_123\,
      PCOUT(31) => \mul_ln49_fu_256_p2__0_n_124\,
      PCOUT(30) => \mul_ln49_fu_256_p2__0_n_125\,
      PCOUT(29) => \mul_ln49_fu_256_p2__0_n_126\,
      PCOUT(28) => \mul_ln49_fu_256_p2__0_n_127\,
      PCOUT(27) => \mul_ln49_fu_256_p2__0_n_128\,
      PCOUT(26) => \mul_ln49_fu_256_p2__0_n_129\,
      PCOUT(25) => \mul_ln49_fu_256_p2__0_n_130\,
      PCOUT(24) => \mul_ln49_fu_256_p2__0_n_131\,
      PCOUT(23) => \mul_ln49_fu_256_p2__0_n_132\,
      PCOUT(22) => \mul_ln49_fu_256_p2__0_n_133\,
      PCOUT(21) => \mul_ln49_fu_256_p2__0_n_134\,
      PCOUT(20) => \mul_ln49_fu_256_p2__0_n_135\,
      PCOUT(19) => \mul_ln49_fu_256_p2__0_n_136\,
      PCOUT(18) => \mul_ln49_fu_256_p2__0_n_137\,
      PCOUT(17) => \mul_ln49_fu_256_p2__0_n_138\,
      PCOUT(16) => \mul_ln49_fu_256_p2__0_n_139\,
      PCOUT(15) => \mul_ln49_fu_256_p2__0_n_140\,
      PCOUT(14) => \mul_ln49_fu_256_p2__0_n_141\,
      PCOUT(13) => \mul_ln49_fu_256_p2__0_n_142\,
      PCOUT(12) => \mul_ln49_fu_256_p2__0_n_143\,
      PCOUT(11) => \mul_ln49_fu_256_p2__0_n_144\,
      PCOUT(10) => \mul_ln49_fu_256_p2__0_n_145\,
      PCOUT(9) => \mul_ln49_fu_256_p2__0_n_146\,
      PCOUT(8) => \mul_ln49_fu_256_p2__0_n_147\,
      PCOUT(7) => \mul_ln49_fu_256_p2__0_n_148\,
      PCOUT(6) => \mul_ln49_fu_256_p2__0_n_149\,
      PCOUT(5) => \mul_ln49_fu_256_p2__0_n_150\,
      PCOUT(4) => \mul_ln49_fu_256_p2__0_n_151\,
      PCOUT(3) => \mul_ln49_fu_256_p2__0_n_152\,
      PCOUT(2) => \mul_ln49_fu_256_p2__0_n_153\,
      PCOUT(1) => \mul_ln49_fu_256_p2__0_n_154\,
      PCOUT(0) => \mul_ln49_fu_256_p2__0_n_155\,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => \NLW_mul_ln49_fu_256_p2__0_UNDERFLOW_UNCONNECTED\
    );
mul_ln49_fu_256_p2_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FB0000000000"
    )
        port map (
      I0 => icmp_ln44_fu_225_p2,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => ap_CS_fsm_pp0_stage0,
      I4 => \icmp_ln44_reg_282_reg_n_2_[0]\,
      I5 => ap_enable_reg_pp0_iter1,
      O => localmem_load_reg_3110
    );
mul_ln49_fu_256_p2_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => icmp_ln44_fu_225_p2,
      I1 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I2 => ap_CS_fsm_pp0_stage0,
      I3 => ap_enable_reg_pp0_iter0,
      O => strm_in_V_data_0_sel2
    );
mul_ln49_fu_256_p2_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA8A"
    )
        port map (
      I0 => ap_CS_fsm_pp0_stage0,
      I1 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => icmp_ln44_fu_225_p2,
      O => tmpa_last_V_reg_1641
    );
mul_ln49_fu_256_p2_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5545"
    )
        port map (
      I0 => icmp_ln44_reg_282_pp0_iter1_reg,
      I1 => icmp_ln44_fu_225_p2,
      I2 => ap_enable_reg_pp0_iter0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      O => mul_ln49_reg_3160
    );
mul_ln49_reg_316_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 2,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 2,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => d0(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_mul_ln49_reg_316_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => localmem_q0(31),
      B(16) => localmem_q0(31),
      B(15) => localmem_q0(31),
      B(14 downto 0) => localmem_q0(31 downto 17),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_mul_ln49_reg_316_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_mul_ln49_reg_316_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_mul_ln49_reg_316_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => strm_in_V_data_0_sel2,
      CEA2 => tmpa_last_V_reg_1641,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => localmem_load_reg_3110,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => mul_ln49_reg_3160,
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_mul_ln49_reg_316_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => NLW_mul_ln49_reg_316_reg_OVERFLOW_UNCONNECTED,
      P(47) => mul_ln49_reg_316_reg_n_60,
      P(46) => mul_ln49_reg_316_reg_n_61,
      P(45) => mul_ln49_reg_316_reg_n_62,
      P(44) => mul_ln49_reg_316_reg_n_63,
      P(43) => mul_ln49_reg_316_reg_n_64,
      P(42) => mul_ln49_reg_316_reg_n_65,
      P(41) => mul_ln49_reg_316_reg_n_66,
      P(40) => mul_ln49_reg_316_reg_n_67,
      P(39) => mul_ln49_reg_316_reg_n_68,
      P(38) => mul_ln49_reg_316_reg_n_69,
      P(37) => mul_ln49_reg_316_reg_n_70,
      P(36) => mul_ln49_reg_316_reg_n_71,
      P(35) => mul_ln49_reg_316_reg_n_72,
      P(34) => mul_ln49_reg_316_reg_n_73,
      P(33) => mul_ln49_reg_316_reg_n_74,
      P(32) => mul_ln49_reg_316_reg_n_75,
      P(31) => mul_ln49_reg_316_reg_n_76,
      P(30) => mul_ln49_reg_316_reg_n_77,
      P(29) => mul_ln49_reg_316_reg_n_78,
      P(28) => mul_ln49_reg_316_reg_n_79,
      P(27) => mul_ln49_reg_316_reg_n_80,
      P(26) => mul_ln49_reg_316_reg_n_81,
      P(25) => mul_ln49_reg_316_reg_n_82,
      P(24) => mul_ln49_reg_316_reg_n_83,
      P(23) => mul_ln49_reg_316_reg_n_84,
      P(22) => mul_ln49_reg_316_reg_n_85,
      P(21) => mul_ln49_reg_316_reg_n_86,
      P(20) => mul_ln49_reg_316_reg_n_87,
      P(19) => mul_ln49_reg_316_reg_n_88,
      P(18) => mul_ln49_reg_316_reg_n_89,
      P(17) => mul_ln49_reg_316_reg_n_90,
      P(16) => mul_ln49_reg_316_reg_n_91,
      P(15) => mul_ln49_reg_316_reg_n_92,
      P(14) => mul_ln49_reg_316_reg_n_93,
      P(13) => mul_ln49_reg_316_reg_n_94,
      P(12) => mul_ln49_reg_316_reg_n_95,
      P(11) => mul_ln49_reg_316_reg_n_96,
      P(10) => mul_ln49_reg_316_reg_n_97,
      P(9) => mul_ln49_reg_316_reg_n_98,
      P(8) => mul_ln49_reg_316_reg_n_99,
      P(7) => mul_ln49_reg_316_reg_n_100,
      P(6) => mul_ln49_reg_316_reg_n_101,
      P(5) => mul_ln49_reg_316_reg_n_102,
      P(4) => mul_ln49_reg_316_reg_n_103,
      P(3) => mul_ln49_reg_316_reg_n_104,
      P(2) => mul_ln49_reg_316_reg_n_105,
      P(1) => mul_ln49_reg_316_reg_n_106,
      P(0) => mul_ln49_reg_316_reg_n_107,
      PATTERNBDETECT => NLW_mul_ln49_reg_316_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_mul_ln49_reg_316_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47) => \mul_ln49_fu_256_p2__0_n_108\,
      PCIN(46) => \mul_ln49_fu_256_p2__0_n_109\,
      PCIN(45) => \mul_ln49_fu_256_p2__0_n_110\,
      PCIN(44) => \mul_ln49_fu_256_p2__0_n_111\,
      PCIN(43) => \mul_ln49_fu_256_p2__0_n_112\,
      PCIN(42) => \mul_ln49_fu_256_p2__0_n_113\,
      PCIN(41) => \mul_ln49_fu_256_p2__0_n_114\,
      PCIN(40) => \mul_ln49_fu_256_p2__0_n_115\,
      PCIN(39) => \mul_ln49_fu_256_p2__0_n_116\,
      PCIN(38) => \mul_ln49_fu_256_p2__0_n_117\,
      PCIN(37) => \mul_ln49_fu_256_p2__0_n_118\,
      PCIN(36) => \mul_ln49_fu_256_p2__0_n_119\,
      PCIN(35) => \mul_ln49_fu_256_p2__0_n_120\,
      PCIN(34) => \mul_ln49_fu_256_p2__0_n_121\,
      PCIN(33) => \mul_ln49_fu_256_p2__0_n_122\,
      PCIN(32) => \mul_ln49_fu_256_p2__0_n_123\,
      PCIN(31) => \mul_ln49_fu_256_p2__0_n_124\,
      PCIN(30) => \mul_ln49_fu_256_p2__0_n_125\,
      PCIN(29) => \mul_ln49_fu_256_p2__0_n_126\,
      PCIN(28) => \mul_ln49_fu_256_p2__0_n_127\,
      PCIN(27) => \mul_ln49_fu_256_p2__0_n_128\,
      PCIN(26) => \mul_ln49_fu_256_p2__0_n_129\,
      PCIN(25) => \mul_ln49_fu_256_p2__0_n_130\,
      PCIN(24) => \mul_ln49_fu_256_p2__0_n_131\,
      PCIN(23) => \mul_ln49_fu_256_p2__0_n_132\,
      PCIN(22) => \mul_ln49_fu_256_p2__0_n_133\,
      PCIN(21) => \mul_ln49_fu_256_p2__0_n_134\,
      PCIN(20) => \mul_ln49_fu_256_p2__0_n_135\,
      PCIN(19) => \mul_ln49_fu_256_p2__0_n_136\,
      PCIN(18) => \mul_ln49_fu_256_p2__0_n_137\,
      PCIN(17) => \mul_ln49_fu_256_p2__0_n_138\,
      PCIN(16) => \mul_ln49_fu_256_p2__0_n_139\,
      PCIN(15) => \mul_ln49_fu_256_p2__0_n_140\,
      PCIN(14) => \mul_ln49_fu_256_p2__0_n_141\,
      PCIN(13) => \mul_ln49_fu_256_p2__0_n_142\,
      PCIN(12) => \mul_ln49_fu_256_p2__0_n_143\,
      PCIN(11) => \mul_ln49_fu_256_p2__0_n_144\,
      PCIN(10) => \mul_ln49_fu_256_p2__0_n_145\,
      PCIN(9) => \mul_ln49_fu_256_p2__0_n_146\,
      PCIN(8) => \mul_ln49_fu_256_p2__0_n_147\,
      PCIN(7) => \mul_ln49_fu_256_p2__0_n_148\,
      PCIN(6) => \mul_ln49_fu_256_p2__0_n_149\,
      PCIN(5) => \mul_ln49_fu_256_p2__0_n_150\,
      PCIN(4) => \mul_ln49_fu_256_p2__0_n_151\,
      PCIN(3) => \mul_ln49_fu_256_p2__0_n_152\,
      PCIN(2) => \mul_ln49_fu_256_p2__0_n_153\,
      PCIN(1) => \mul_ln49_fu_256_p2__0_n_154\,
      PCIN(0) => \mul_ln49_fu_256_p2__0_n_155\,
      PCOUT(47 downto 0) => NLW_mul_ln49_reg_316_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_mul_ln49_reg_316_reg_UNDERFLOW_UNCONNECTED
    );
\mul_ln49_reg_316_reg[0]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_107\,
      Q => \mul_ln49_reg_316_reg[0]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[10]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_97\,
      Q => \mul_ln49_reg_316_reg[10]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[11]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_96\,
      Q => \mul_ln49_reg_316_reg[11]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[12]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_95\,
      Q => \mul_ln49_reg_316_reg[12]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[13]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_94\,
      Q => \mul_ln49_reg_316_reg[13]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[14]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_93\,
      Q => \mul_ln49_reg_316_reg[14]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[15]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_92\,
      Q => \mul_ln49_reg_316_reg[15]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[16]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_91\,
      Q => \mul_ln49_reg_316_reg[16]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[1]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_106\,
      Q => \mul_ln49_reg_316_reg[1]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[2]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_105\,
      Q => \mul_ln49_reg_316_reg[2]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[3]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_104\,
      Q => \mul_ln49_reg_316_reg[3]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[4]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_103\,
      Q => \mul_ln49_reg_316_reg[4]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[5]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_102\,
      Q => \mul_ln49_reg_316_reg[5]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[6]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_101\,
      Q => \mul_ln49_reg_316_reg[6]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[7]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_100\,
      Q => \mul_ln49_reg_316_reg[7]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[8]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_99\,
      Q => \mul_ln49_reg_316_reg[8]__0_n_2\,
      R => '0'
    );
\mul_ln49_reg_316_reg[9]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => mul_ln49_reg_3160,
      D => \mul_ln49_fu_256_p2__0_n_98\,
      Q => \mul_ln49_reg_316_reg[9]__0_n_2\,
      R => '0'
    );
\strm_in_V_data_0_payload_A[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I1 => strm_in_V_data_0_ack_in,
      I2 => strm_in_V_data_0_sel_wr,
      O => strm_in_V_data_0_load_A
    );
\strm_in_V_data_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(0),
      Q => strm_in_V_data_0_payload_A(0),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(10),
      Q => strm_in_V_data_0_payload_A(10),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(11),
      Q => strm_in_V_data_0_payload_A(11),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(12),
      Q => strm_in_V_data_0_payload_A(12),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(13),
      Q => strm_in_V_data_0_payload_A(13),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(14),
      Q => strm_in_V_data_0_payload_A(14),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(15),
      Q => strm_in_V_data_0_payload_A(15),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(16),
      Q => strm_in_V_data_0_payload_A(16),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(17),
      Q => strm_in_V_data_0_payload_A(17),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(18),
      Q => strm_in_V_data_0_payload_A(18),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(19),
      Q => strm_in_V_data_0_payload_A(19),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(1),
      Q => strm_in_V_data_0_payload_A(1),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(20),
      Q => strm_in_V_data_0_payload_A(20),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(21),
      Q => strm_in_V_data_0_payload_A(21),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(22),
      Q => strm_in_V_data_0_payload_A(22),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(23),
      Q => strm_in_V_data_0_payload_A(23),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(24),
      Q => strm_in_V_data_0_payload_A(24),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(25),
      Q => strm_in_V_data_0_payload_A(25),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(26),
      Q => strm_in_V_data_0_payload_A(26),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(27),
      Q => strm_in_V_data_0_payload_A(27),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(28),
      Q => strm_in_V_data_0_payload_A(28),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(29),
      Q => strm_in_V_data_0_payload_A(29),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(2),
      Q => strm_in_V_data_0_payload_A(2),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(30),
      Q => strm_in_V_data_0_payload_A(30),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(31),
      Q => strm_in_V_data_0_payload_A(31),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(3),
      Q => strm_in_V_data_0_payload_A(3),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(4),
      Q => strm_in_V_data_0_payload_A(4),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(5),
      Q => strm_in_V_data_0_payload_A(5),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(6),
      Q => strm_in_V_data_0_payload_A(6),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(7),
      Q => strm_in_V_data_0_payload_A(7),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(8),
      Q => strm_in_V_data_0_payload_A(8),
      R => '0'
    );
\strm_in_V_data_0_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_A,
      D => strm_in_TDATA(9),
      Q => strm_in_V_data_0_payload_A(9),
      R => '0'
    );
\strm_in_V_data_0_payload_B[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I1 => strm_in_V_data_0_ack_in,
      I2 => strm_in_V_data_0_sel_wr,
      O => strm_in_V_data_0_load_B
    );
\strm_in_V_data_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(0),
      Q => strm_in_V_data_0_payload_B(0),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(10),
      Q => strm_in_V_data_0_payload_B(10),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(11),
      Q => strm_in_V_data_0_payload_B(11),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(12),
      Q => strm_in_V_data_0_payload_B(12),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(13),
      Q => strm_in_V_data_0_payload_B(13),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(14),
      Q => strm_in_V_data_0_payload_B(14),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(15),
      Q => strm_in_V_data_0_payload_B(15),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(16),
      Q => strm_in_V_data_0_payload_B(16),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(17),
      Q => strm_in_V_data_0_payload_B(17),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(18),
      Q => strm_in_V_data_0_payload_B(18),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(19),
      Q => strm_in_V_data_0_payload_B(19),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(1),
      Q => strm_in_V_data_0_payload_B(1),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(20),
      Q => strm_in_V_data_0_payload_B(20),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(21),
      Q => strm_in_V_data_0_payload_B(21),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(22),
      Q => strm_in_V_data_0_payload_B(22),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(23),
      Q => strm_in_V_data_0_payload_B(23),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(24),
      Q => strm_in_V_data_0_payload_B(24),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(25),
      Q => strm_in_V_data_0_payload_B(25),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(26),
      Q => strm_in_V_data_0_payload_B(26),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(27),
      Q => strm_in_V_data_0_payload_B(27),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(28),
      Q => strm_in_V_data_0_payload_B(28),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(29),
      Q => strm_in_V_data_0_payload_B(29),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(2),
      Q => strm_in_V_data_0_payload_B(2),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(30),
      Q => strm_in_V_data_0_payload_B(30),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(31),
      Q => strm_in_V_data_0_payload_B(31),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(3),
      Q => strm_in_V_data_0_payload_B(3),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(4),
      Q => strm_in_V_data_0_payload_B(4),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(5),
      Q => strm_in_V_data_0_payload_B(5),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(6),
      Q => strm_in_V_data_0_payload_B(6),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(7),
      Q => strm_in_V_data_0_payload_B(7),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(8),
      Q => strm_in_V_data_0_payload_B(8),
      R => '0'
    );
\strm_in_V_data_0_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_in_V_data_0_load_B,
      D => strm_in_TDATA(9),
      Q => strm_in_V_data_0_payload_B(9),
      R => '0'
    );
strm_in_V_data_0_sel_rd_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F5F5F5FB0A0A0A0"
    )
        port map (
      I0 => we0,
      I1 => icmp_ln44_fu_225_p2,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => ap_CS_fsm_pp0_stage0,
      I4 => ap_enable_reg_pp0_iter0,
      I5 => strm_in_V_data_0_sel,
      O => strm_in_V_data_0_sel_rd_i_1_n_2
    );
strm_in_V_data_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_in_V_data_0_sel_rd_i_1_n_2,
      Q => strm_in_V_data_0_sel,
      R => reset
    );
strm_in_V_data_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => strm_in_TVALID,
      I1 => strm_in_V_data_0_ack_in,
      I2 => strm_in_V_data_0_sel_wr,
      O => strm_in_V_data_0_sel_wr_i_1_n_2
    );
strm_in_V_data_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_in_V_data_0_sel_wr_i_1_n_2,
      Q => strm_in_V_data_0_sel_wr,
      R => reset
    );
\strm_in_V_data_0_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8A8A8A820A02020"
    )
        port map (
      I0 => ap_rst_n,
      I1 => strm_in_V_data_0_ack_in,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => we0,
      I4 => \strm_in_V_last_V_0_state[1]_i_3_n_2\,
      I5 => strm_in_TVALID,
      O => \strm_in_V_data_0_state[0]_i_1_n_2\
    );
\strm_in_V_data_0_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5DFF5DFFFFFF5DFF"
    )
        port map (
      I0 => \strm_in_V_last_V_0_state[1]_i_3_n_2\,
      I1 => ap_CS_fsm_state2,
      I2 => icmp_ln34_fu_192_p2,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => strm_in_V_data_0_ack_in,
      I5 => strm_in_TVALID,
      O => strm_in_V_data_0_state(1)
    );
\strm_in_V_data_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \strm_in_V_data_0_state[0]_i_1_n_2\,
      Q => \strm_in_V_data_0_state_reg_n_2_[0]\,
      R => '0'
    );
\strm_in_V_data_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_in_V_data_0_state(1),
      Q => strm_in_V_data_0_ack_in,
      R => reset
    );
\strm_in_V_last_V_0_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => strm_in_TLAST(0),
      I1 => \strm_in_V_last_V_0_state_reg_n_2_[0]\,
      I2 => \^strm_in_tready\,
      I3 => strm_in_V_last_V_0_sel_wr,
      I4 => strm_in_V_last_V_0_payload_A,
      O => \strm_in_V_last_V_0_payload_A[0]_i_1_n_2\
    );
\strm_in_V_last_V_0_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \strm_in_V_last_V_0_payload_A[0]_i_1_n_2\,
      Q => strm_in_V_last_V_0_payload_A,
      R => '0'
    );
\strm_in_V_last_V_0_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => strm_in_TLAST(0),
      I1 => \strm_in_V_last_V_0_state_reg_n_2_[0]\,
      I2 => \^strm_in_tready\,
      I3 => strm_in_V_last_V_0_sel_wr,
      I4 => strm_in_V_last_V_0_payload_B,
      O => \strm_in_V_last_V_0_payload_B[0]_i_1_n_2\
    );
\strm_in_V_last_V_0_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \strm_in_V_last_V_0_payload_B[0]_i_1_n_2\,
      Q => strm_in_V_last_V_0_payload_B,
      R => '0'
    );
strm_in_V_last_V_0_sel_rd_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BF00FFFF40FF0000"
    )
        port map (
      I0 => icmp_ln34_fu_192_p2,
      I1 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I2 => ap_CS_fsm_state2,
      I3 => \strm_in_V_last_V_0_state[1]_i_3_n_2\,
      I4 => \strm_in_V_last_V_0_state_reg_n_2_[0]\,
      I5 => strm_in_V_last_V_0_sel,
      O => strm_in_V_last_V_0_sel_rd_i_1_n_2
    );
strm_in_V_last_V_0_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_in_V_last_V_0_sel_rd_i_1_n_2,
      Q => strm_in_V_last_V_0_sel,
      R => reset
    );
strm_in_V_last_V_0_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => strm_in_TVALID,
      I1 => \^strm_in_tready\,
      I2 => strm_in_V_last_V_0_sel_wr,
      O => strm_in_V_last_V_0_sel_wr_i_1_n_2
    );
strm_in_V_last_V_0_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_in_V_last_V_0_sel_wr_i_1_n_2,
      Q => strm_in_V_last_V_0_sel_wr,
      R => reset
    );
\strm_in_V_last_V_0_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAA0020AA0000"
    )
        port map (
      I0 => ap_rst_n,
      I1 => we0,
      I2 => \strm_in_V_last_V_0_state[1]_i_3_n_2\,
      I3 => \^strm_in_tready\,
      I4 => \strm_in_V_last_V_0_state_reg_n_2_[0]\,
      I5 => strm_in_TVALID,
      O => \strm_in_V_last_V_0_state[0]_i_1_n_2\
    );
\strm_in_V_last_V_0_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ap_rst_n,
      O => reset
    );
\strm_in_V_last_V_0_state[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF4FFFFF"
    )
        port map (
      I0 => strm_in_TVALID,
      I1 => \^strm_in_tready\,
      I2 => \strm_in_V_last_V_0_state_reg_n_2_[0]\,
      I3 => we0,
      I4 => \strm_in_V_last_V_0_state[1]_i_3_n_2\,
      O => strm_in_V_last_V_0_state(1)
    );
\strm_in_V_last_V_0_state[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF7F7F7F"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => \strm_in_V_last_V_0_state[1]_i_4_n_2\,
      I4 => localmem_U_n_71,
      O => \strm_in_V_last_V_0_state[1]_i_3_n_2\
    );
\strm_in_V_last_V_0_state[1]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => i_op_assign_reg_153_reg(1),
      I1 => i_op_assign_reg_153_reg(0),
      I2 => i_op_assign_reg_153_reg(3),
      I3 => i_op_assign_reg_153_reg(2),
      O => \strm_in_V_last_V_0_state[1]_i_4_n_2\
    );
\strm_in_V_last_V_0_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \strm_in_V_last_V_0_state[0]_i_1_n_2\,
      Q => \strm_in_V_last_V_0_state_reg_n_2_[0]\,
      R => '0'
    );
\strm_in_V_last_V_0_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_in_V_last_V_0_state(1),
      Q => \^strm_in_tready\,
      R => reset
    );
\strm_out_TDATA[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(0),
      I1 => strm_out_V_data_1_payload_A(0),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(0)
    );
\strm_out_TDATA[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(10),
      I1 => strm_out_V_data_1_payload_A(10),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(10)
    );
\strm_out_TDATA[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(11),
      I1 => strm_out_V_data_1_payload_A(11),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(11)
    );
\strm_out_TDATA[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(12),
      I1 => strm_out_V_data_1_payload_A(12),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(12)
    );
\strm_out_TDATA[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(13),
      I1 => strm_out_V_data_1_payload_A(13),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(13)
    );
\strm_out_TDATA[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(14),
      I1 => strm_out_V_data_1_payload_A(14),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(14)
    );
\strm_out_TDATA[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(15),
      I1 => strm_out_V_data_1_payload_A(15),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(15)
    );
\strm_out_TDATA[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(16),
      I1 => strm_out_V_data_1_payload_A(16),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(16)
    );
\strm_out_TDATA[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(17),
      I1 => strm_out_V_data_1_payload_A(17),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(17)
    );
\strm_out_TDATA[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(18),
      I1 => strm_out_V_data_1_payload_A(18),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(18)
    );
\strm_out_TDATA[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(19),
      I1 => strm_out_V_data_1_payload_A(19),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(19)
    );
\strm_out_TDATA[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(1),
      I1 => strm_out_V_data_1_payload_A(1),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(1)
    );
\strm_out_TDATA[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(20),
      I1 => strm_out_V_data_1_payload_A(20),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(20)
    );
\strm_out_TDATA[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(21),
      I1 => strm_out_V_data_1_payload_A(21),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(21)
    );
\strm_out_TDATA[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(22),
      I1 => strm_out_V_data_1_payload_A(22),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(22)
    );
\strm_out_TDATA[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(23),
      I1 => strm_out_V_data_1_payload_A(23),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(23)
    );
\strm_out_TDATA[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(24),
      I1 => strm_out_V_data_1_payload_A(24),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(24)
    );
\strm_out_TDATA[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(25),
      I1 => strm_out_V_data_1_payload_A(25),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(25)
    );
\strm_out_TDATA[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(26),
      I1 => strm_out_V_data_1_payload_A(26),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(26)
    );
\strm_out_TDATA[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(27),
      I1 => strm_out_V_data_1_payload_A(27),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(27)
    );
\strm_out_TDATA[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(28),
      I1 => strm_out_V_data_1_payload_A(28),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(28)
    );
\strm_out_TDATA[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(29),
      I1 => strm_out_V_data_1_payload_A(29),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(29)
    );
\strm_out_TDATA[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(2),
      I1 => strm_out_V_data_1_payload_A(2),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(2)
    );
\strm_out_TDATA[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(30),
      I1 => strm_out_V_data_1_payload_A(30),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(30)
    );
\strm_out_TDATA[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(31),
      I1 => strm_out_V_data_1_payload_A(31),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(31)
    );
\strm_out_TDATA[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(3),
      I1 => strm_out_V_data_1_payload_A(3),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(3)
    );
\strm_out_TDATA[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(4),
      I1 => strm_out_V_data_1_payload_A(4),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(4)
    );
\strm_out_TDATA[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(5),
      I1 => strm_out_V_data_1_payload_A(5),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(5)
    );
\strm_out_TDATA[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(6),
      I1 => strm_out_V_data_1_payload_A(6),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(6)
    );
\strm_out_TDATA[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(7),
      I1 => strm_out_V_data_1_payload_A(7),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(7)
    );
\strm_out_TDATA[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(8),
      I1 => strm_out_V_data_1_payload_A(8),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(8)
    );
\strm_out_TDATA[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => strm_out_V_data_1_payload_B(9),
      I1 => strm_out_V_data_1_payload_A(9),
      I2 => strm_out_V_data_1_sel,
      O => strm_out_TDATA(9)
    );
\strm_out_TLAST[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => strm_out_V_last_V_1_payload_B,
      I1 => strm_out_V_last_V_1_sel,
      I2 => strm_out_V_last_V_1_payload_A,
      O => strm_out_TLAST(0)
    );
\strm_out_V_data_1_payload_A[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0D"
    )
        port map (
      I0 => \strm_out_V_data_1_state_reg_n_2_[0]\,
      I1 => strm_out_V_data_1_ack_in,
      I2 => strm_out_V_data_1_sel_wr,
      O => strm_out_V_data_1_load_A
    );
\strm_out_V_data_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[0]\,
      Q => strm_out_V_data_1_payload_A(0),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[10]\,
      Q => strm_out_V_data_1_payload_A(10),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[11]\,
      Q => strm_out_V_data_1_payload_A(11),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[12]\,
      Q => strm_out_V_data_1_payload_A(12),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[13]\,
      Q => strm_out_V_data_1_payload_A(13),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[14]\,
      Q => strm_out_V_data_1_payload_A(14),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[15]\,
      Q => strm_out_V_data_1_payload_A(15),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[16]\,
      Q => strm_out_V_data_1_payload_A(16),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[17]\,
      Q => strm_out_V_data_1_payload_A(17),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[18]\,
      Q => strm_out_V_data_1_payload_A(18),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[19]\,
      Q => strm_out_V_data_1_payload_A(19),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[1]\,
      Q => strm_out_V_data_1_payload_A(1),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[20]\,
      Q => strm_out_V_data_1_payload_A(20),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[21]\,
      Q => strm_out_V_data_1_payload_A(21),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[22]\,
      Q => strm_out_V_data_1_payload_A(22),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[23]\,
      Q => strm_out_V_data_1_payload_A(23),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[24]\,
      Q => strm_out_V_data_1_payload_A(24),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[25]\,
      Q => strm_out_V_data_1_payload_A(25),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[26]\,
      Q => strm_out_V_data_1_payload_A(26),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[27]\,
      Q => strm_out_V_data_1_payload_A(27),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[28]\,
      Q => strm_out_V_data_1_payload_A(28),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[29]\,
      Q => strm_out_V_data_1_payload_A(29),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[2]\,
      Q => strm_out_V_data_1_payload_A(2),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[30]\,
      Q => strm_out_V_data_1_payload_A(30),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[31]\,
      Q => strm_out_V_data_1_payload_A(31),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[3]\,
      Q => strm_out_V_data_1_payload_A(3),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[4]\,
      Q => strm_out_V_data_1_payload_A(4),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[5]\,
      Q => strm_out_V_data_1_payload_A(5),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[6]\,
      Q => strm_out_V_data_1_payload_A(6),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[7]\,
      Q => strm_out_V_data_1_payload_A(7),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[8]\,
      Q => strm_out_V_data_1_payload_A(8),
      R => '0'
    );
\strm_out_V_data_1_payload_A_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_A,
      D => \acc_reg_n_2_[9]\,
      Q => strm_out_V_data_1_payload_A(9),
      R => '0'
    );
\strm_out_V_data_1_payload_B[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \strm_out_V_data_1_state_reg_n_2_[0]\,
      I1 => strm_out_V_data_1_ack_in,
      I2 => strm_out_V_data_1_sel_wr,
      O => strm_out_V_data_1_load_B
    );
\strm_out_V_data_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[0]\,
      Q => strm_out_V_data_1_payload_B(0),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[10]\,
      Q => strm_out_V_data_1_payload_B(10),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[11]\,
      Q => strm_out_V_data_1_payload_B(11),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[12]\,
      Q => strm_out_V_data_1_payload_B(12),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[13]\,
      Q => strm_out_V_data_1_payload_B(13),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[14]\,
      Q => strm_out_V_data_1_payload_B(14),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[15]\,
      Q => strm_out_V_data_1_payload_B(15),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[16]\,
      Q => strm_out_V_data_1_payload_B(16),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[17]\,
      Q => strm_out_V_data_1_payload_B(17),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[18]\,
      Q => strm_out_V_data_1_payload_B(18),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[19]\,
      Q => strm_out_V_data_1_payload_B(19),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[1]\,
      Q => strm_out_V_data_1_payload_B(1),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[20]\,
      Q => strm_out_V_data_1_payload_B(20),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[21]\,
      Q => strm_out_V_data_1_payload_B(21),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[22]\,
      Q => strm_out_V_data_1_payload_B(22),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[23]\,
      Q => strm_out_V_data_1_payload_B(23),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[24]\,
      Q => strm_out_V_data_1_payload_B(24),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[25]\,
      Q => strm_out_V_data_1_payload_B(25),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[26]\,
      Q => strm_out_V_data_1_payload_B(26),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[27]\,
      Q => strm_out_V_data_1_payload_B(27),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[28]\,
      Q => strm_out_V_data_1_payload_B(28),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[29]\,
      Q => strm_out_V_data_1_payload_B(29),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[2]\,
      Q => strm_out_V_data_1_payload_B(2),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[30]\,
      Q => strm_out_V_data_1_payload_B(30),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[31]\,
      Q => strm_out_V_data_1_payload_B(31),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[3]\,
      Q => strm_out_V_data_1_payload_B(3),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[4]\,
      Q => strm_out_V_data_1_payload_B(4),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[5]\,
      Q => strm_out_V_data_1_payload_B(5),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[6]\,
      Q => strm_out_V_data_1_payload_B(6),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[7]\,
      Q => strm_out_V_data_1_payload_B(7),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[8]\,
      Q => strm_out_V_data_1_payload_B(8),
      R => '0'
    );
\strm_out_V_data_1_payload_B_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => strm_out_V_data_1_load_B,
      D => \acc_reg_n_2_[9]\,
      Q => strm_out_V_data_1_payload_B(9),
      R => '0'
    );
strm_out_V_data_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => strm_out_TREADY,
      I1 => \strm_out_V_data_1_state_reg_n_2_[0]\,
      I2 => strm_out_V_data_1_sel,
      O => strm_out_V_data_1_sel_rd_i_1_n_2
    );
strm_out_V_data_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_out_V_data_1_sel_rd_i_1_n_2,
      Q => strm_out_V_data_1_sel,
      R => reset
    );
strm_out_V_data_1_sel_wr_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => strm_out_V_data_1_ack_in,
      I1 => ap_CS_fsm_state8,
      I2 => strm_out_V_data_1_sel_wr,
      O => strm_out_V_data_1_sel_wr_i_1_n_2
    );
strm_out_V_data_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_out_V_data_1_sel_wr_i_1_n_2,
      Q => strm_out_V_data_1_sel_wr,
      R => reset
    );
\strm_out_V_data_1_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8A820A0"
    )
        port map (
      I0 => ap_rst_n,
      I1 => strm_out_V_data_1_ack_in,
      I2 => \strm_out_V_data_1_state_reg_n_2_[0]\,
      I3 => strm_out_TREADY,
      I4 => ap_CS_fsm_state8,
      O => \strm_out_V_data_1_state[0]_i_1_n_2\
    );
\strm_out_V_data_1_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF2F"
    )
        port map (
      I0 => strm_out_V_data_1_ack_in,
      I1 => ap_CS_fsm_state8,
      I2 => \strm_out_V_data_1_state_reg_n_2_[0]\,
      I3 => strm_out_TREADY,
      O => strm_out_V_data_1_state(1)
    );
\strm_out_V_data_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \strm_out_V_data_1_state[0]_i_1_n_2\,
      Q => \strm_out_V_data_1_state_reg_n_2_[0]\,
      R => '0'
    );
\strm_out_V_data_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_out_V_data_1_state(1),
      Q => strm_out_V_data_1_ack_in,
      R => reset
    );
\strm_out_V_last_V_1_payload_A[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAE00A2"
    )
        port map (
      I0 => \tmpa_last_V_reg_164_reg_n_2_[0]\,
      I1 => \^strm_out_tvalid\,
      I2 => strm_out_V_last_V_1_ack_in,
      I3 => strm_out_V_last_V_1_sel_wr,
      I4 => strm_out_V_last_V_1_payload_A,
      O => \strm_out_V_last_V_1_payload_A[0]_i_1_n_2\
    );
\strm_out_V_last_V_1_payload_A_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \strm_out_V_last_V_1_payload_A[0]_i_1_n_2\,
      Q => strm_out_V_last_V_1_payload_A,
      R => '0'
    );
\strm_out_V_last_V_1_payload_B[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AEFFA200"
    )
        port map (
      I0 => \tmpa_last_V_reg_164_reg_n_2_[0]\,
      I1 => \^strm_out_tvalid\,
      I2 => strm_out_V_last_V_1_ack_in,
      I3 => strm_out_V_last_V_1_sel_wr,
      I4 => strm_out_V_last_V_1_payload_B,
      O => \strm_out_V_last_V_1_payload_B[0]_i_1_n_2\
    );
\strm_out_V_last_V_1_payload_B_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \strm_out_V_last_V_1_payload_B[0]_i_1_n_2\,
      Q => strm_out_V_last_V_1_payload_B,
      R => '0'
    );
strm_out_V_last_V_1_sel_rd_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => strm_out_TREADY,
      I1 => \^strm_out_tvalid\,
      I2 => strm_out_V_last_V_1_sel,
      O => strm_out_V_last_V_1_sel_rd_i_1_n_2
    );
strm_out_V_last_V_1_sel_rd_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_out_V_last_V_1_sel_rd_i_1_n_2,
      Q => strm_out_V_last_V_1_sel,
      R => reset
    );
strm_out_V_last_V_1_sel_wr_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => strm_out_V_data_1_ack_in,
      I1 => ap_CS_fsm_state8,
      I2 => strm_out_V_last_V_1_ack_in,
      I3 => strm_out_V_last_V_1_sel_wr,
      O => strm_out_V_last_V_1_sel_wr_i_1_n_2
    );
strm_out_V_last_V_1_sel_wr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_out_V_last_V_1_sel_wr_i_1_n_2,
      Q => strm_out_V_last_V_1_sel_wr,
      R => reset
    );
\strm_out_V_last_V_1_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80AAAAAA80008000"
    )
        port map (
      I0 => ap_rst_n,
      I1 => ap_CS_fsm_state8,
      I2 => strm_out_V_data_1_ack_in,
      I3 => strm_out_V_last_V_1_ack_in,
      I4 => strm_out_TREADY,
      I5 => \^strm_out_tvalid\,
      O => \strm_out_V_last_V_1_state[0]_i_1_n_2\
    );
\strm_out_V_last_V_1_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF70FF"
    )
        port map (
      I0 => strm_out_V_data_1_ack_in,
      I1 => ap_CS_fsm_state8,
      I2 => strm_out_V_last_V_1_ack_in,
      I3 => \^strm_out_tvalid\,
      I4 => strm_out_TREADY,
      O => strm_out_V_last_V_1_state(1)
    );
\strm_out_V_last_V_1_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \strm_out_V_last_V_1_state[0]_i_1_n_2\,
      Q => \^strm_out_tvalid\,
      R => '0'
    );
\strm_out_V_last_V_1_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => strm_out_V_last_V_1_state(1),
      Q => strm_out_V_last_V_1_ack_in,
      R => reset
    );
\tmp_3_1_2_reg_129[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0555055504000000"
    )
        port map (
      I0 => ap_NS_fsm1,
      I1 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I2 => icmp_ln34_fu_192_p2,
      I3 => ap_CS_fsm_state2,
      I4 => grp_fu_182_p1,
      I5 => \tmp_3_1_2_reg_129_reg_n_2_[0]\,
      O => \tmp_3_1_2_reg_129[0]_i_1_n_2\
    );
\tmp_3_1_2_reg_129[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \tmpa_last_V_reg_164_reg_n_2_[0]\,
      I1 => ap_CS_fsm_state9,
      I2 => \ap_CS_fsm[5]_i_2_n_2\,
      O => ap_NS_fsm1
    );
\tmp_3_1_2_reg_129_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_3_1_2_reg_129[0]_i_1_n_2\,
      Q => \tmp_3_1_2_reg_129_reg_n_2_[0]\,
      R => '0'
    );
\tmp_3_1_3_reg_142[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFAFCCA0A0A0CC"
    )
        port map (
      I0 => \tmp_3_1_2_reg_129_reg_n_2_[0]\,
      I1 => tmp_last_V_1_reg_296,
      I2 => ap_CS_fsm_state3,
      I3 => icmp_ln879_reg_307,
      I4 => \tmp_3_1_3_reg_142[0]_i_2_n_2\,
      I5 => tmp_3_1_3_reg_142,
      O => \tmp_3_1_3_reg_142[0]_i_1_n_2\
    );
\tmp_3_1_3_reg_142[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBFBBBFBBBFBB"
    )
        port map (
      I0 => \tmpa_last_V_reg_164[0]_i_3_n_2\,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I3 => ap_enable_reg_pp0_iter0,
      I4 => \strm_in_V_last_V_0_state[1]_i_4_n_2\,
      I5 => localmem_U_n_71,
      O => \tmp_3_1_3_reg_142[0]_i_2_n_2\
    );
\tmp_3_1_3_reg_142_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_3_1_3_reg_142[0]_i_1_n_2\,
      Q => tmp_3_1_3_reg_142,
      R => '0'
    );
\tmp_last_V_1_reg_296[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAEAAAAAAA2AAA"
    )
        port map (
      I0 => tmp_last_V_1_reg_296,
      I1 => ap_enable_reg_pp0_iter0,
      I2 => ap_CS_fsm_pp0_stage0,
      I3 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I4 => icmp_ln44_fu_225_p2,
      I5 => grp_fu_182_p1,
      O => \tmp_last_V_1_reg_296[0]_i_1_n_2\
    );
\tmp_last_V_1_reg_296_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_last_V_1_reg_296[0]_i_1_n_2\,
      Q => tmp_last_V_1_reg_296,
      R => '0'
    );
\tmpa_last_V_reg_164[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAFEFFAAAA0200"
    )
        port map (
      I0 => \tmpa_last_V_reg_164[0]_i_2_n_2\,
      I1 => \tmpa_last_V_reg_164[0]_i_3_n_2\,
      I2 => ap_enable_reg_pp0_iter0_i_2_n_2,
      I3 => icmp_ln879_reg_307,
      I4 => \tmpa_last_V_reg_164[0]_i_4_n_2\,
      I5 => \tmpa_last_V_reg_164_reg_n_2_[0]\,
      O => \tmpa_last_V_reg_164[0]_i_1_n_2\
    );
\tmpa_last_V_reg_164[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFABFFFF00A80000"
    )
        port map (
      I0 => tmp_last_V_1_reg_296,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => icmp_ln879_reg_307,
      I3 => \icmp_ln44_reg_282_reg_n_2_[0]\,
      I4 => ap_enable_reg_pp0_iter1,
      I5 => tmp_3_1_3_reg_142,
      O => \tmpa_last_V_reg_164[0]_i_2_n_2\
    );
\tmpa_last_V_reg_164[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \icmp_ln44_reg_282_reg_n_2_[0]\,
      I1 => ap_enable_reg_pp0_iter1,
      O => \tmpa_last_V_reg_164[0]_i_3_n_2\
    );
\tmpa_last_V_reg_164[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => ap_enable_reg_pp0_iter0,
      I1 => ap_CS_fsm_pp0_stage0,
      I2 => \strm_in_V_last_V_0_state[1]_i_4_n_2\,
      I3 => localmem_U_n_71,
      O => \tmpa_last_V_reg_164[0]_i_4_n_2\
    );
\tmpa_last_V_reg_164_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmpa_last_V_reg_164[0]_i_1_n_2\,
      Q => \tmpa_last_V_reg_164_reg_n_2_[0]\,
      R => '0'
    );
\val_assign_reg_107[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => val_assign_reg_107_reg(0),
      O => i_fu_198_p2(0)
    );
\val_assign_reg_107[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => val_assign_reg_107_reg(0),
      I1 => val_assign_reg_107_reg(1),
      O => i_fu_198_p2(1)
    );
\val_assign_reg_107[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => val_assign_reg_107_reg(0),
      I1 => val_assign_reg_107_reg(1),
      I2 => val_assign_reg_107_reg(2),
      O => i_fu_198_p2(2)
    );
\val_assign_reg_107[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => val_assign_reg_107_reg(2),
      I1 => val_assign_reg_107_reg(1),
      I2 => val_assign_reg_107_reg(0),
      I3 => val_assign_reg_107_reg(3),
      O => i_fu_198_p2(3)
    );
\val_assign_reg_107[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => val_assign_reg_107_reg(3),
      I1 => val_assign_reg_107_reg(0),
      I2 => val_assign_reg_107_reg(1),
      I3 => val_assign_reg_107_reg(2),
      I4 => val_assign_reg_107_reg(4),
      O => i_fu_198_p2(4)
    );
\val_assign_reg_107[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => val_assign_reg_107_reg(2),
      I1 => val_assign_reg_107_reg(1),
      I2 => val_assign_reg_107_reg(0),
      I3 => val_assign_reg_107_reg(3),
      I4 => val_assign_reg_107_reg(4),
      I5 => \val_assign_reg_107_reg__0\(5),
      O => i_fu_198_p2(5)
    );
\val_assign_reg_107[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \val_assign_reg_107[9]_i_5_n_2\,
      I1 => \val_assign_reg_107_reg__0\(6),
      O => i_fu_198_p2(6)
    );
\val_assign_reg_107[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \val_assign_reg_107[9]_i_5_n_2\,
      I1 => \val_assign_reg_107_reg__0\(6),
      I2 => \val_assign_reg_107_reg__0\(7),
      O => i_fu_198_p2(7)
    );
\val_assign_reg_107[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F708"
    )
        port map (
      I0 => \val_assign_reg_107_reg__0\(7),
      I1 => \val_assign_reg_107_reg__0\(6),
      I2 => \val_assign_reg_107[9]_i_5_n_2\,
      I3 => \val_assign_reg_107_reg__0\(8),
      O => i_fu_198_p2(8)
    );
\val_assign_reg_107[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFDF0000"
    )
        port map (
      I0 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I1 => icmp_ln34_fu_192_p2,
      I2 => ap_CS_fsm_state2,
      I3 => grp_fu_182_p1,
      I4 => ap_CS_fsm_state1,
      O => val_assign_reg_107
    );
\val_assign_reg_107[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002020200020"
    )
        port map (
      I0 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I1 => icmp_ln34_fu_192_p2,
      I2 => ap_CS_fsm_state2,
      I3 => strm_in_V_last_V_0_payload_A,
      I4 => strm_in_V_last_V_0_sel,
      I5 => strm_in_V_last_V_0_payload_B,
      O => val_assign_reg_1070
    );
\val_assign_reg_107[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => \val_assign_reg_107_reg__0\(8),
      I1 => \val_assign_reg_107[9]_i_5_n_2\,
      I2 => \val_assign_reg_107_reg__0\(6),
      I3 => \val_assign_reg_107_reg__0\(7),
      I4 => \val_assign_reg_107_reg__0\(9),
      O => i_fu_198_p2(9)
    );
\val_assign_reg_107[9]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => strm_in_V_last_V_0_payload_B,
      I1 => strm_in_V_last_V_0_sel,
      I2 => strm_in_V_last_V_0_payload_A,
      O => grp_fu_182_p1
    );
\val_assign_reg_107[9]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => val_assign_reg_107_reg(2),
      I1 => val_assign_reg_107_reg(1),
      I2 => val_assign_reg_107_reg(0),
      I3 => val_assign_reg_107_reg(3),
      I4 => val_assign_reg_107_reg(4),
      I5 => \val_assign_reg_107_reg__0\(5),
      O => \val_assign_reg_107[9]_i_5_n_2\
    );
\val_assign_reg_107_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(0),
      Q => val_assign_reg_107_reg(0),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(1),
      Q => val_assign_reg_107_reg(1),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(2),
      Q => val_assign_reg_107_reg(2),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(3),
      Q => val_assign_reg_107_reg(3),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(4),
      Q => val_assign_reg_107_reg(4),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(5),
      Q => \val_assign_reg_107_reg__0\(5),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(6),
      Q => \val_assign_reg_107_reg__0\(6),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(7),
      Q => \val_assign_reg_107_reg__0\(7),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(8),
      Q => \val_assign_reg_107_reg__0\(8),
      R => val_assign_reg_107
    );
\val_assign_reg_107_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => val_assign_reg_1070,
      D => i_fu_198_p2(9),
      Q => \val_assign_reg_107_reg__0\(9),
      R => val_assign_reg_107
    );
\vect_size_V[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E0E0E0C0C0C0E0C0"
    )
        port map (
      I0 => \strm_in_V_data_0_state_reg_n_2_[0]\,
      I1 => icmp_ln34_fu_192_p2,
      I2 => ap_CS_fsm_state2,
      I3 => strm_in_V_last_V_0_payload_A,
      I4 => strm_in_V_last_V_0_sel,
      I5 => strm_in_V_last_V_0_payload_B,
      O => ap_NS_fsm143_out
    );
\vect_size_V_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_NS_fsm143_out,
      D => val_assign_reg_107_reg(0),
      Q => vect_size_V(0),
      R => '0'
    );
\vect_size_V_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_NS_fsm143_out,
      D => val_assign_reg_107_reg(1),
      Q => vect_size_V(1),
      R => '0'
    );
\vect_size_V_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_NS_fsm143_out,
      D => val_assign_reg_107_reg(2),
      Q => vect_size_V(2),
      R => '0'
    );
\vect_size_V_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_NS_fsm143_out,
      D => val_assign_reg_107_reg(3),
      Q => vect_size_V(3),
      R => '0'
    );
\vect_size_V_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => ap_NS_fsm143_out,
      D => val_assign_reg_107_reg(4),
      Q => vect_size_V(4),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0 is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst_n : in STD_LOGIC;
    strm_out_TVALID : out STD_LOGIC;
    strm_out_TREADY : in STD_LOGIC;
    strm_out_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_out_TLAST : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_TVALID : in STD_LOGIC;
    strm_in_TREADY : out STD_LOGIC;
    strm_in_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    strm_in_TLAST : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of bd_0_hls_inst_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of bd_0_hls_inst_0 : entity is "bd_0_hls_inst_0,axis_fixed_macc,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of bd_0_hls_inst_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of bd_0_hls_inst_0 : entity is "HLS";
  attribute x_core_info : string;
  attribute x_core_info of bd_0_hls_inst_0 : entity is "axis_fixed_macc,Vivado 2019.1";
end bd_0_hls_inst_0;

architecture STRUCTURE of bd_0_hls_inst_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF strm_out:strm_in, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0";
  attribute x_interface_info of ap_rst_n : signal is "xilinx.com:signal:reset:1.0 ap_rst_n RST";
  attribute x_interface_parameter of ap_rst_n : signal is "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of strm_in_TREADY : signal is "xilinx.com:interface:axis:1.0 strm_in TREADY";
  attribute x_interface_info of strm_in_TVALID : signal is "xilinx.com:interface:axis:1.0 strm_in TVALID";
  attribute x_interface_parameter of strm_in_TVALID : signal is "XIL_INTERFACENAME strm_in, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, LAYERED_METADATA undef, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0";
  attribute x_interface_info of strm_out_TREADY : signal is "xilinx.com:interface:axis:1.0 strm_out TREADY";
  attribute x_interface_info of strm_out_TVALID : signal is "xilinx.com:interface:axis:1.0 strm_out TVALID";
  attribute x_interface_parameter of strm_out_TVALID : signal is "XIL_INTERFACENAME strm_out, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0";
  attribute x_interface_info of strm_in_TDATA : signal is "xilinx.com:interface:axis:1.0 strm_in TDATA";
  attribute x_interface_info of strm_in_TLAST : signal is "xilinx.com:interface:axis:1.0 strm_in TLAST";
  attribute x_interface_info of strm_out_TDATA : signal is "xilinx.com:interface:axis:1.0 strm_out TDATA";
  attribute x_interface_info of strm_out_TLAST : signal is "xilinx.com:interface:axis:1.0 strm_out TLAST";
begin
U0: entity work.bd_0_hls_inst_0_axis_fixed_macc
     port map (
      ap_clk => ap_clk,
      ap_rst_n => ap_rst_n,
      strm_in_TDATA(31 downto 0) => strm_in_TDATA(31 downto 0),
      strm_in_TLAST(0) => strm_in_TLAST(0),
      strm_in_TREADY => strm_in_TREADY,
      strm_in_TVALID => strm_in_TVALID,
      strm_out_TDATA(31 downto 0) => strm_out_TDATA(31 downto 0),
      strm_out_TLAST(0) => strm_out_TLAST(0),
      strm_out_TREADY => strm_out_TREADY,
      strm_out_TVALID => strm_out_TVALID
    );
end STRUCTURE;
