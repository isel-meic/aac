$Footmark = "FPGA_Xilinx"
$Description = "by Vivado"


#=== Resource usage ===
$SLICE = "0"
$LUT = "187"
$FF = "240"
$DSP = "3"
$BRAM ="1"
$SRL ="0"
#=== Final timing ===
$TargetCP = "10.000"
$CP = "5.690"
