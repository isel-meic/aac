// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sun Jul 12 10:59:10 2020
// Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ bd_0_hls_inst_0_sim_netlist.v
// Design      : bd_0_hls_inst_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_fixed_macc
   (ap_clk,
    ap_rst_n,
    strm_out_TDATA,
    strm_out_TVALID,
    strm_out_TREADY,
    strm_out_TLAST,
    strm_in_TDATA,
    strm_in_TVALID,
    strm_in_TREADY,
    strm_in_TLAST);
  input ap_clk;
  input ap_rst_n;
  output [31:0]strm_out_TDATA;
  output strm_out_TVALID;
  input strm_out_TREADY;
  output [0:0]strm_out_TLAST;
  input [31:0]strm_in_TDATA;
  input strm_in_TVALID;
  output strm_in_TREADY;
  input [0:0]strm_in_TLAST;

  wire acc;
  wire acc0;
  wire \acc[11]_i_2_n_2 ;
  wire \acc[11]_i_3_n_2 ;
  wire \acc[11]_i_4_n_2 ;
  wire \acc[11]_i_5_n_2 ;
  wire \acc[15]_i_2_n_2 ;
  wire \acc[15]_i_3_n_2 ;
  wire \acc[15]_i_4_n_2 ;
  wire \acc[15]_i_5_n_2 ;
  wire \acc[19]_i_2_n_2 ;
  wire \acc[19]_i_3_n_2 ;
  wire \acc[19]_i_4_n_2 ;
  wire \acc[19]_i_5_n_2 ;
  wire \acc[19]_i_7_n_2 ;
  wire \acc[19]_i_8_n_2 ;
  wire \acc[19]_i_9_n_2 ;
  wire \acc[23]_i_10_n_2 ;
  wire \acc[23]_i_2_n_2 ;
  wire \acc[23]_i_3_n_2 ;
  wire \acc[23]_i_4_n_2 ;
  wire \acc[23]_i_5_n_2 ;
  wire \acc[23]_i_7_n_2 ;
  wire \acc[23]_i_8_n_2 ;
  wire \acc[23]_i_9_n_2 ;
  wire \acc[27]_i_10_n_2 ;
  wire \acc[27]_i_2_n_2 ;
  wire \acc[27]_i_3_n_2 ;
  wire \acc[27]_i_4_n_2 ;
  wire \acc[27]_i_5_n_2 ;
  wire \acc[27]_i_7_n_2 ;
  wire \acc[27]_i_8_n_2 ;
  wire \acc[27]_i_9_n_2 ;
  wire \acc[31]_i_10_n_2 ;
  wire \acc[31]_i_11_n_2 ;
  wire \acc[31]_i_12_n_2 ;
  wire \acc[31]_i_4_n_2 ;
  wire \acc[31]_i_5_n_2 ;
  wire \acc[31]_i_6_n_2 ;
  wire \acc[31]_i_7_n_2 ;
  wire \acc[31]_i_9_n_2 ;
  wire \acc[3]_i_2_n_2 ;
  wire \acc[3]_i_3_n_2 ;
  wire \acc[3]_i_4_n_2 ;
  wire \acc[3]_i_5_n_2 ;
  wire \acc[7]_i_2_n_2 ;
  wire \acc[7]_i_3_n_2 ;
  wire \acc[7]_i_4_n_2 ;
  wire \acc[7]_i_5_n_2 ;
  wire \acc_reg[11]_i_1_n_2 ;
  wire \acc_reg[11]_i_1_n_3 ;
  wire \acc_reg[11]_i_1_n_4 ;
  wire \acc_reg[11]_i_1_n_5 ;
  wire \acc_reg[15]_i_1_n_2 ;
  wire \acc_reg[15]_i_1_n_3 ;
  wire \acc_reg[15]_i_1_n_4 ;
  wire \acc_reg[15]_i_1_n_5 ;
  wire \acc_reg[19]_i_1_n_2 ;
  wire \acc_reg[19]_i_1_n_3 ;
  wire \acc_reg[19]_i_1_n_4 ;
  wire \acc_reg[19]_i_1_n_5 ;
  wire \acc_reg[19]_i_6_n_2 ;
  wire \acc_reg[19]_i_6_n_3 ;
  wire \acc_reg[19]_i_6_n_4 ;
  wire \acc_reg[19]_i_6_n_5 ;
  wire \acc_reg[23]_i_1_n_2 ;
  wire \acc_reg[23]_i_1_n_3 ;
  wire \acc_reg[23]_i_1_n_4 ;
  wire \acc_reg[23]_i_1_n_5 ;
  wire \acc_reg[23]_i_6_n_2 ;
  wire \acc_reg[23]_i_6_n_3 ;
  wire \acc_reg[23]_i_6_n_4 ;
  wire \acc_reg[23]_i_6_n_5 ;
  wire \acc_reg[27]_i_1_n_2 ;
  wire \acc_reg[27]_i_1_n_3 ;
  wire \acc_reg[27]_i_1_n_4 ;
  wire \acc_reg[27]_i_1_n_5 ;
  wire \acc_reg[27]_i_6_n_2 ;
  wire \acc_reg[27]_i_6_n_3 ;
  wire \acc_reg[27]_i_6_n_4 ;
  wire \acc_reg[27]_i_6_n_5 ;
  wire \acc_reg[31]_i_3_n_3 ;
  wire \acc_reg[31]_i_3_n_4 ;
  wire \acc_reg[31]_i_3_n_5 ;
  wire \acc_reg[31]_i_8_n_3 ;
  wire \acc_reg[31]_i_8_n_4 ;
  wire \acc_reg[31]_i_8_n_5 ;
  wire \acc_reg[3]_i_1_n_2 ;
  wire \acc_reg[3]_i_1_n_3 ;
  wire \acc_reg[3]_i_1_n_4 ;
  wire \acc_reg[3]_i_1_n_5 ;
  wire \acc_reg[7]_i_1_n_2 ;
  wire \acc_reg[7]_i_1_n_3 ;
  wire \acc_reg[7]_i_1_n_4 ;
  wire \acc_reg[7]_i_1_n_5 ;
  wire \acc_reg_n_2_[0] ;
  wire \acc_reg_n_2_[10] ;
  wire \acc_reg_n_2_[11] ;
  wire \acc_reg_n_2_[12] ;
  wire \acc_reg_n_2_[13] ;
  wire \acc_reg_n_2_[14] ;
  wire \acc_reg_n_2_[15] ;
  wire \acc_reg_n_2_[16] ;
  wire \acc_reg_n_2_[17] ;
  wire \acc_reg_n_2_[18] ;
  wire \acc_reg_n_2_[19] ;
  wire \acc_reg_n_2_[1] ;
  wire \acc_reg_n_2_[20] ;
  wire \acc_reg_n_2_[21] ;
  wire \acc_reg_n_2_[22] ;
  wire \acc_reg_n_2_[23] ;
  wire \acc_reg_n_2_[24] ;
  wire \acc_reg_n_2_[25] ;
  wire \acc_reg_n_2_[26] ;
  wire \acc_reg_n_2_[27] ;
  wire \acc_reg_n_2_[28] ;
  wire \acc_reg_n_2_[29] ;
  wire \acc_reg_n_2_[2] ;
  wire \acc_reg_n_2_[30] ;
  wire \acc_reg_n_2_[31] ;
  wire \acc_reg_n_2_[3] ;
  wire \acc_reg_n_2_[4] ;
  wire \acc_reg_n_2_[5] ;
  wire \acc_reg_n_2_[6] ;
  wire \acc_reg_n_2_[7] ;
  wire \acc_reg_n_2_[8] ;
  wire \acc_reg_n_2_[9] ;
  wire [31:0]add_ln50_fu_260_p2;
  wire \ap_CS_fsm[0]_i_2_n_2 ;
  wire \ap_CS_fsm[2]_i_2_n_2 ;
  wire \ap_CS_fsm[5]_i_2_n_2 ;
  wire ap_CS_fsm_pp0_stage0;
  wire ap_CS_fsm_state1;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state3;
  wire ap_CS_fsm_state8;
  wire ap_CS_fsm_state9;
  wire [5:0]ap_NS_fsm;
  wire ap_NS_fsm1;
  wire ap_NS_fsm143_out;
  wire ap_block_pp0_stage0_subdone;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ap_enable_reg_pp0_iter0_i_1_n_2;
  wire ap_enable_reg_pp0_iter0_i_2_n_2;
  wire ap_enable_reg_pp0_iter1;
  wire ap_enable_reg_pp0_iter1_i_1_n_2;
  wire ap_enable_reg_pp0_iter2;
  wire ap_enable_reg_pp0_iter2_i_1_n_2;
  wire ap_enable_reg_pp0_iter3_i_1_n_2;
  wire ap_enable_reg_pp0_iter3_reg_n_2;
  wire ap_rst_n;
  wire [31:0]d0;
  wire grp_fu_182_p1;
  wire [9:0]i_1_fu_231_p2;
  wire [9:0]i_fu_198_p2;
  wire i_op_assign_reg_153;
  wire i_op_assign_reg_1530;
  wire \i_op_assign_reg_153[9]_i_5_n_2 ;
  wire \i_op_assign_reg_153[9]_i_6_n_2 ;
  wire \i_op_assign_reg_153[9]_i_7_n_2 ;
  wire \i_op_assign_reg_153[9]_i_8_n_2 ;
  wire \i_op_assign_reg_153[9]_i_9_n_2 ;
  wire [9:0]i_op_assign_reg_153_reg;
  wire \i_op_assign_reg_153_reg[9]_i_4_n_3 ;
  wire \i_op_assign_reg_153_reg[9]_i_4_n_4 ;
  wire \i_op_assign_reg_153_reg[9]_i_4_n_5 ;
  wire icmp_ln34_fu_192_p2;
  wire icmp_ln44_fu_225_p2;
  wire \icmp_ln44_reg_282[0]_i_1_n_2 ;
  wire icmp_ln44_reg_282_pp0_iter1_reg;
  wire \icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1_n_2 ;
  wire icmp_ln44_reg_282_pp0_iter2_reg;
  wire \icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1_n_2 ;
  wire \icmp_ln44_reg_282_reg_n_2_[0] ;
  wire icmp_ln879_fu_250_p2;
  wire icmp_ln879_reg_307;
  wire \icmp_ln879_reg_307[0]_i_1_n_2 ;
  wire localmem_U_n_67;
  wire localmem_U_n_68;
  wire localmem_U_n_71;
  wire localmem_load_reg_3110;
  wire [31:0]localmem_q0;
  wire mul_ln49_fu_256_p2__0_n_100;
  wire mul_ln49_fu_256_p2__0_n_101;
  wire mul_ln49_fu_256_p2__0_n_102;
  wire mul_ln49_fu_256_p2__0_n_103;
  wire mul_ln49_fu_256_p2__0_n_104;
  wire mul_ln49_fu_256_p2__0_n_105;
  wire mul_ln49_fu_256_p2__0_n_106;
  wire mul_ln49_fu_256_p2__0_n_107;
  wire mul_ln49_fu_256_p2__0_n_108;
  wire mul_ln49_fu_256_p2__0_n_109;
  wire mul_ln49_fu_256_p2__0_n_110;
  wire mul_ln49_fu_256_p2__0_n_111;
  wire mul_ln49_fu_256_p2__0_n_112;
  wire mul_ln49_fu_256_p2__0_n_113;
  wire mul_ln49_fu_256_p2__0_n_114;
  wire mul_ln49_fu_256_p2__0_n_115;
  wire mul_ln49_fu_256_p2__0_n_116;
  wire mul_ln49_fu_256_p2__0_n_117;
  wire mul_ln49_fu_256_p2__0_n_118;
  wire mul_ln49_fu_256_p2__0_n_119;
  wire mul_ln49_fu_256_p2__0_n_120;
  wire mul_ln49_fu_256_p2__0_n_121;
  wire mul_ln49_fu_256_p2__0_n_122;
  wire mul_ln49_fu_256_p2__0_n_123;
  wire mul_ln49_fu_256_p2__0_n_124;
  wire mul_ln49_fu_256_p2__0_n_125;
  wire mul_ln49_fu_256_p2__0_n_126;
  wire mul_ln49_fu_256_p2__0_n_127;
  wire mul_ln49_fu_256_p2__0_n_128;
  wire mul_ln49_fu_256_p2__0_n_129;
  wire mul_ln49_fu_256_p2__0_n_130;
  wire mul_ln49_fu_256_p2__0_n_131;
  wire mul_ln49_fu_256_p2__0_n_132;
  wire mul_ln49_fu_256_p2__0_n_133;
  wire mul_ln49_fu_256_p2__0_n_134;
  wire mul_ln49_fu_256_p2__0_n_135;
  wire mul_ln49_fu_256_p2__0_n_136;
  wire mul_ln49_fu_256_p2__0_n_137;
  wire mul_ln49_fu_256_p2__0_n_138;
  wire mul_ln49_fu_256_p2__0_n_139;
  wire mul_ln49_fu_256_p2__0_n_140;
  wire mul_ln49_fu_256_p2__0_n_141;
  wire mul_ln49_fu_256_p2__0_n_142;
  wire mul_ln49_fu_256_p2__0_n_143;
  wire mul_ln49_fu_256_p2__0_n_144;
  wire mul_ln49_fu_256_p2__0_n_145;
  wire mul_ln49_fu_256_p2__0_n_146;
  wire mul_ln49_fu_256_p2__0_n_147;
  wire mul_ln49_fu_256_p2__0_n_148;
  wire mul_ln49_fu_256_p2__0_n_149;
  wire mul_ln49_fu_256_p2__0_n_150;
  wire mul_ln49_fu_256_p2__0_n_151;
  wire mul_ln49_fu_256_p2__0_n_152;
  wire mul_ln49_fu_256_p2__0_n_153;
  wire mul_ln49_fu_256_p2__0_n_154;
  wire mul_ln49_fu_256_p2__0_n_155;
  wire mul_ln49_fu_256_p2__0_n_60;
  wire mul_ln49_fu_256_p2__0_n_61;
  wire mul_ln49_fu_256_p2__0_n_62;
  wire mul_ln49_fu_256_p2__0_n_63;
  wire mul_ln49_fu_256_p2__0_n_64;
  wire mul_ln49_fu_256_p2__0_n_65;
  wire mul_ln49_fu_256_p2__0_n_66;
  wire mul_ln49_fu_256_p2__0_n_67;
  wire mul_ln49_fu_256_p2__0_n_68;
  wire mul_ln49_fu_256_p2__0_n_69;
  wire mul_ln49_fu_256_p2__0_n_70;
  wire mul_ln49_fu_256_p2__0_n_71;
  wire mul_ln49_fu_256_p2__0_n_72;
  wire mul_ln49_fu_256_p2__0_n_73;
  wire mul_ln49_fu_256_p2__0_n_74;
  wire mul_ln49_fu_256_p2__0_n_75;
  wire mul_ln49_fu_256_p2__0_n_76;
  wire mul_ln49_fu_256_p2__0_n_77;
  wire mul_ln49_fu_256_p2__0_n_78;
  wire mul_ln49_fu_256_p2__0_n_79;
  wire mul_ln49_fu_256_p2__0_n_80;
  wire mul_ln49_fu_256_p2__0_n_81;
  wire mul_ln49_fu_256_p2__0_n_82;
  wire mul_ln49_fu_256_p2__0_n_83;
  wire mul_ln49_fu_256_p2__0_n_84;
  wire mul_ln49_fu_256_p2__0_n_85;
  wire mul_ln49_fu_256_p2__0_n_86;
  wire mul_ln49_fu_256_p2__0_n_87;
  wire mul_ln49_fu_256_p2__0_n_88;
  wire mul_ln49_fu_256_p2__0_n_89;
  wire mul_ln49_fu_256_p2__0_n_90;
  wire mul_ln49_fu_256_p2__0_n_91;
  wire mul_ln49_fu_256_p2__0_n_92;
  wire mul_ln49_fu_256_p2__0_n_93;
  wire mul_ln49_fu_256_p2__0_n_94;
  wire mul_ln49_fu_256_p2__0_n_95;
  wire mul_ln49_fu_256_p2__0_n_96;
  wire mul_ln49_fu_256_p2__0_n_97;
  wire mul_ln49_fu_256_p2__0_n_98;
  wire mul_ln49_fu_256_p2__0_n_99;
  wire mul_ln49_fu_256_p2_n_100;
  wire mul_ln49_fu_256_p2_n_101;
  wire mul_ln49_fu_256_p2_n_102;
  wire mul_ln49_fu_256_p2_n_103;
  wire mul_ln49_fu_256_p2_n_104;
  wire mul_ln49_fu_256_p2_n_105;
  wire mul_ln49_fu_256_p2_n_106;
  wire mul_ln49_fu_256_p2_n_107;
  wire mul_ln49_fu_256_p2_n_108;
  wire mul_ln49_fu_256_p2_n_109;
  wire mul_ln49_fu_256_p2_n_110;
  wire mul_ln49_fu_256_p2_n_111;
  wire mul_ln49_fu_256_p2_n_112;
  wire mul_ln49_fu_256_p2_n_113;
  wire mul_ln49_fu_256_p2_n_114;
  wire mul_ln49_fu_256_p2_n_115;
  wire mul_ln49_fu_256_p2_n_116;
  wire mul_ln49_fu_256_p2_n_117;
  wire mul_ln49_fu_256_p2_n_118;
  wire mul_ln49_fu_256_p2_n_119;
  wire mul_ln49_fu_256_p2_n_120;
  wire mul_ln49_fu_256_p2_n_121;
  wire mul_ln49_fu_256_p2_n_122;
  wire mul_ln49_fu_256_p2_n_123;
  wire mul_ln49_fu_256_p2_n_124;
  wire mul_ln49_fu_256_p2_n_125;
  wire mul_ln49_fu_256_p2_n_126;
  wire mul_ln49_fu_256_p2_n_127;
  wire mul_ln49_fu_256_p2_n_128;
  wire mul_ln49_fu_256_p2_n_129;
  wire mul_ln49_fu_256_p2_n_130;
  wire mul_ln49_fu_256_p2_n_131;
  wire mul_ln49_fu_256_p2_n_132;
  wire mul_ln49_fu_256_p2_n_133;
  wire mul_ln49_fu_256_p2_n_134;
  wire mul_ln49_fu_256_p2_n_135;
  wire mul_ln49_fu_256_p2_n_136;
  wire mul_ln49_fu_256_p2_n_137;
  wire mul_ln49_fu_256_p2_n_138;
  wire mul_ln49_fu_256_p2_n_139;
  wire mul_ln49_fu_256_p2_n_140;
  wire mul_ln49_fu_256_p2_n_141;
  wire mul_ln49_fu_256_p2_n_142;
  wire mul_ln49_fu_256_p2_n_143;
  wire mul_ln49_fu_256_p2_n_144;
  wire mul_ln49_fu_256_p2_n_145;
  wire mul_ln49_fu_256_p2_n_146;
  wire mul_ln49_fu_256_p2_n_147;
  wire mul_ln49_fu_256_p2_n_148;
  wire mul_ln49_fu_256_p2_n_149;
  wire mul_ln49_fu_256_p2_n_150;
  wire mul_ln49_fu_256_p2_n_151;
  wire mul_ln49_fu_256_p2_n_152;
  wire mul_ln49_fu_256_p2_n_153;
  wire mul_ln49_fu_256_p2_n_154;
  wire mul_ln49_fu_256_p2_n_155;
  wire mul_ln49_fu_256_p2_n_60;
  wire mul_ln49_fu_256_p2_n_61;
  wire mul_ln49_fu_256_p2_n_62;
  wire mul_ln49_fu_256_p2_n_63;
  wire mul_ln49_fu_256_p2_n_64;
  wire mul_ln49_fu_256_p2_n_65;
  wire mul_ln49_fu_256_p2_n_66;
  wire mul_ln49_fu_256_p2_n_67;
  wire mul_ln49_fu_256_p2_n_68;
  wire mul_ln49_fu_256_p2_n_69;
  wire mul_ln49_fu_256_p2_n_70;
  wire mul_ln49_fu_256_p2_n_71;
  wire mul_ln49_fu_256_p2_n_72;
  wire mul_ln49_fu_256_p2_n_73;
  wire mul_ln49_fu_256_p2_n_74;
  wire mul_ln49_fu_256_p2_n_75;
  wire mul_ln49_fu_256_p2_n_76;
  wire mul_ln49_fu_256_p2_n_77;
  wire mul_ln49_fu_256_p2_n_78;
  wire mul_ln49_fu_256_p2_n_79;
  wire mul_ln49_fu_256_p2_n_80;
  wire mul_ln49_fu_256_p2_n_81;
  wire mul_ln49_fu_256_p2_n_82;
  wire mul_ln49_fu_256_p2_n_83;
  wire mul_ln49_fu_256_p2_n_84;
  wire mul_ln49_fu_256_p2_n_85;
  wire mul_ln49_fu_256_p2_n_86;
  wire mul_ln49_fu_256_p2_n_87;
  wire mul_ln49_fu_256_p2_n_88;
  wire mul_ln49_fu_256_p2_n_89;
  wire mul_ln49_fu_256_p2_n_90;
  wire mul_ln49_fu_256_p2_n_91;
  wire mul_ln49_fu_256_p2_n_92;
  wire mul_ln49_fu_256_p2_n_93;
  wire mul_ln49_fu_256_p2_n_94;
  wire mul_ln49_fu_256_p2_n_95;
  wire mul_ln49_fu_256_p2_n_96;
  wire mul_ln49_fu_256_p2_n_97;
  wire mul_ln49_fu_256_p2_n_98;
  wire mul_ln49_fu_256_p2_n_99;
  wire mul_ln49_reg_3160;
  wire \mul_ln49_reg_316_reg[0]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[10]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[11]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[12]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[13]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[14]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[15]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[16]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[1]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[2]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[3]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[4]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[5]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[6]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[7]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[8]__0_n_2 ;
  wire \mul_ln49_reg_316_reg[9]__0_n_2 ;
  wire [31:16]mul_ln49_reg_316_reg__1;
  wire mul_ln49_reg_316_reg_n_100;
  wire mul_ln49_reg_316_reg_n_101;
  wire mul_ln49_reg_316_reg_n_102;
  wire mul_ln49_reg_316_reg_n_103;
  wire mul_ln49_reg_316_reg_n_104;
  wire mul_ln49_reg_316_reg_n_105;
  wire mul_ln49_reg_316_reg_n_106;
  wire mul_ln49_reg_316_reg_n_107;
  wire mul_ln49_reg_316_reg_n_60;
  wire mul_ln49_reg_316_reg_n_61;
  wire mul_ln49_reg_316_reg_n_62;
  wire mul_ln49_reg_316_reg_n_63;
  wire mul_ln49_reg_316_reg_n_64;
  wire mul_ln49_reg_316_reg_n_65;
  wire mul_ln49_reg_316_reg_n_66;
  wire mul_ln49_reg_316_reg_n_67;
  wire mul_ln49_reg_316_reg_n_68;
  wire mul_ln49_reg_316_reg_n_69;
  wire mul_ln49_reg_316_reg_n_70;
  wire mul_ln49_reg_316_reg_n_71;
  wire mul_ln49_reg_316_reg_n_72;
  wire mul_ln49_reg_316_reg_n_73;
  wire mul_ln49_reg_316_reg_n_74;
  wire mul_ln49_reg_316_reg_n_75;
  wire mul_ln49_reg_316_reg_n_76;
  wire mul_ln49_reg_316_reg_n_77;
  wire mul_ln49_reg_316_reg_n_78;
  wire mul_ln49_reg_316_reg_n_79;
  wire mul_ln49_reg_316_reg_n_80;
  wire mul_ln49_reg_316_reg_n_81;
  wire mul_ln49_reg_316_reg_n_82;
  wire mul_ln49_reg_316_reg_n_83;
  wire mul_ln49_reg_316_reg_n_84;
  wire mul_ln49_reg_316_reg_n_85;
  wire mul_ln49_reg_316_reg_n_86;
  wire mul_ln49_reg_316_reg_n_87;
  wire mul_ln49_reg_316_reg_n_88;
  wire mul_ln49_reg_316_reg_n_89;
  wire mul_ln49_reg_316_reg_n_90;
  wire mul_ln49_reg_316_reg_n_91;
  wire mul_ln49_reg_316_reg_n_92;
  wire mul_ln49_reg_316_reg_n_93;
  wire mul_ln49_reg_316_reg_n_94;
  wire mul_ln49_reg_316_reg_n_95;
  wire mul_ln49_reg_316_reg_n_96;
  wire mul_ln49_reg_316_reg_n_97;
  wire mul_ln49_reg_316_reg_n_98;
  wire mul_ln49_reg_316_reg_n_99;
  wire reset;
  wire [31:0]strm_in_TDATA;
  wire [0:0]strm_in_TLAST;
  wire strm_in_TREADY;
  wire strm_in_TVALID;
  wire strm_in_V_data_0_ack_in;
  wire strm_in_V_data_0_load_A;
  wire strm_in_V_data_0_load_B;
  wire [31:0]strm_in_V_data_0_payload_A;
  wire [31:0]strm_in_V_data_0_payload_B;
  wire strm_in_V_data_0_sel;
  wire strm_in_V_data_0_sel2;
  wire strm_in_V_data_0_sel_rd_i_1_n_2;
  wire strm_in_V_data_0_sel_wr;
  wire strm_in_V_data_0_sel_wr_i_1_n_2;
  wire [1:1]strm_in_V_data_0_state;
  wire \strm_in_V_data_0_state[0]_i_1_n_2 ;
  wire \strm_in_V_data_0_state_reg_n_2_[0] ;
  wire strm_in_V_last_V_0_payload_A;
  wire \strm_in_V_last_V_0_payload_A[0]_i_1_n_2 ;
  wire strm_in_V_last_V_0_payload_B;
  wire \strm_in_V_last_V_0_payload_B[0]_i_1_n_2 ;
  wire strm_in_V_last_V_0_sel;
  wire strm_in_V_last_V_0_sel_rd_i_1_n_2;
  wire strm_in_V_last_V_0_sel_wr;
  wire strm_in_V_last_V_0_sel_wr_i_1_n_2;
  wire [1:1]strm_in_V_last_V_0_state;
  wire \strm_in_V_last_V_0_state[0]_i_1_n_2 ;
  wire \strm_in_V_last_V_0_state[1]_i_3_n_2 ;
  wire \strm_in_V_last_V_0_state[1]_i_4_n_2 ;
  wire \strm_in_V_last_V_0_state_reg_n_2_[0] ;
  wire [31:0]strm_out_TDATA;
  wire [0:0]strm_out_TLAST;
  wire strm_out_TREADY;
  wire strm_out_TVALID;
  wire strm_out_V_data_1_ack_in;
  wire strm_out_V_data_1_load_A;
  wire strm_out_V_data_1_load_B;
  wire [31:0]strm_out_V_data_1_payload_A;
  wire [31:0]strm_out_V_data_1_payload_B;
  wire strm_out_V_data_1_sel;
  wire strm_out_V_data_1_sel_rd_i_1_n_2;
  wire strm_out_V_data_1_sel_wr;
  wire strm_out_V_data_1_sel_wr_i_1_n_2;
  wire [1:1]strm_out_V_data_1_state;
  wire \strm_out_V_data_1_state[0]_i_1_n_2 ;
  wire \strm_out_V_data_1_state_reg_n_2_[0] ;
  wire strm_out_V_last_V_1_ack_in;
  wire strm_out_V_last_V_1_payload_A;
  wire \strm_out_V_last_V_1_payload_A[0]_i_1_n_2 ;
  wire strm_out_V_last_V_1_payload_B;
  wire \strm_out_V_last_V_1_payload_B[0]_i_1_n_2 ;
  wire strm_out_V_last_V_1_sel;
  wire strm_out_V_last_V_1_sel_rd_i_1_n_2;
  wire strm_out_V_last_V_1_sel_wr;
  wire strm_out_V_last_V_1_sel_wr_i_1_n_2;
  wire [1:1]strm_out_V_last_V_1_state;
  wire \strm_out_V_last_V_1_state[0]_i_1_n_2 ;
  wire \tmp_3_1_2_reg_129[0]_i_1_n_2 ;
  wire \tmp_3_1_2_reg_129_reg_n_2_[0] ;
  wire tmp_3_1_3_reg_142;
  wire \tmp_3_1_3_reg_142[0]_i_1_n_2 ;
  wire \tmp_3_1_3_reg_142[0]_i_2_n_2 ;
  wire tmp_last_V_1_reg_296;
  wire \tmp_last_V_1_reg_296[0]_i_1_n_2 ;
  wire tmpa_last_V_reg_1641;
  wire \tmpa_last_V_reg_164[0]_i_1_n_2 ;
  wire \tmpa_last_V_reg_164[0]_i_2_n_2 ;
  wire \tmpa_last_V_reg_164[0]_i_3_n_2 ;
  wire \tmpa_last_V_reg_164[0]_i_4_n_2 ;
  wire \tmpa_last_V_reg_164_reg_n_2_[0] ;
  wire val_assign_reg_107;
  wire val_assign_reg_1070;
  wire \val_assign_reg_107[9]_i_5_n_2 ;
  wire [4:0]val_assign_reg_107_reg;
  wire [9:5]val_assign_reg_107_reg__0;
  wire [4:0]vect_size_V;
  wire we0;
  wire [3:3]\NLW_acc_reg[31]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_acc_reg[31]_i_8_CO_UNCONNECTED ;
  wire [3:0]\NLW_i_op_assign_reg_153_reg[9]_i_4_O_UNCONNECTED ;
  wire NLW_mul_ln49_fu_256_p2_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln49_fu_256_p2_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln49_fu_256_p2_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln49_fu_256_p2_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2__0_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln49_fu_256_p2__0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln49_fu_256_p2__0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln49_fu_256_p2__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln49_fu_256_p2__0_CARRYOUT_UNCONNECTED;
  wire NLW_mul_ln49_reg_316_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_mul_ln49_reg_316_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_mul_ln49_reg_316_reg_OVERFLOW_UNCONNECTED;
  wire NLW_mul_ln49_reg_316_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_mul_ln49_reg_316_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_mul_ln49_reg_316_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_mul_ln49_reg_316_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_mul_ln49_reg_316_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_mul_ln49_reg_316_reg_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_mul_ln49_reg_316_reg_PCOUT_UNCONNECTED;

  LUT2 #(
    .INIT(4'h6)) 
    \acc[11]_i_2 
       (.I0(\acc_reg_n_2_[11] ),
        .I1(\mul_ln49_reg_316_reg[11]__0_n_2 ),
        .O(\acc[11]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[11]_i_3 
       (.I0(\acc_reg_n_2_[10] ),
        .I1(\mul_ln49_reg_316_reg[10]__0_n_2 ),
        .O(\acc[11]_i_3_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[11]_i_4 
       (.I0(\acc_reg_n_2_[9] ),
        .I1(\mul_ln49_reg_316_reg[9]__0_n_2 ),
        .O(\acc[11]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[11]_i_5 
       (.I0(\acc_reg_n_2_[8] ),
        .I1(\mul_ln49_reg_316_reg[8]__0_n_2 ),
        .O(\acc[11]_i_5_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[15]_i_2 
       (.I0(\acc_reg_n_2_[15] ),
        .I1(\mul_ln49_reg_316_reg[15]__0_n_2 ),
        .O(\acc[15]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[15]_i_3 
       (.I0(\acc_reg_n_2_[14] ),
        .I1(\mul_ln49_reg_316_reg[14]__0_n_2 ),
        .O(\acc[15]_i_3_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[15]_i_4 
       (.I0(\acc_reg_n_2_[13] ),
        .I1(\mul_ln49_reg_316_reg[13]__0_n_2 ),
        .O(\acc[15]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[15]_i_5 
       (.I0(\acc_reg_n_2_[12] ),
        .I1(\mul_ln49_reg_316_reg[12]__0_n_2 ),
        .O(\acc[15]_i_5_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[19]_i_2 
       (.I0(\acc_reg_n_2_[19] ),
        .I1(mul_ln49_reg_316_reg__1[19]),
        .O(\acc[19]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[19]_i_3 
       (.I0(\acc_reg_n_2_[18] ),
        .I1(mul_ln49_reg_316_reg__1[18]),
        .O(\acc[19]_i_3_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[19]_i_4 
       (.I0(\acc_reg_n_2_[17] ),
        .I1(mul_ln49_reg_316_reg__1[17]),
        .O(\acc[19]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[19]_i_5 
       (.I0(\acc_reg_n_2_[16] ),
        .I1(mul_ln49_reg_316_reg__1[16]),
        .O(\acc[19]_i_5_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[19]_i_7 
       (.I0(mul_ln49_reg_316_reg_n_105),
        .I1(mul_ln49_fu_256_p2_n_105),
        .O(\acc[19]_i_7_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[19]_i_8 
       (.I0(mul_ln49_reg_316_reg_n_106),
        .I1(mul_ln49_fu_256_p2_n_106),
        .O(\acc[19]_i_8_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[19]_i_9 
       (.I0(mul_ln49_reg_316_reg_n_107),
        .I1(mul_ln49_fu_256_p2_n_107),
        .O(\acc[19]_i_9_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_10 
       (.I0(mul_ln49_reg_316_reg_n_104),
        .I1(mul_ln49_fu_256_p2_n_104),
        .O(\acc[23]_i_10_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_2 
       (.I0(\acc_reg_n_2_[23] ),
        .I1(mul_ln49_reg_316_reg__1[23]),
        .O(\acc[23]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_3 
       (.I0(\acc_reg_n_2_[22] ),
        .I1(mul_ln49_reg_316_reg__1[22]),
        .O(\acc[23]_i_3_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_4 
       (.I0(\acc_reg_n_2_[21] ),
        .I1(mul_ln49_reg_316_reg__1[21]),
        .O(\acc[23]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_5 
       (.I0(\acc_reg_n_2_[20] ),
        .I1(mul_ln49_reg_316_reg__1[20]),
        .O(\acc[23]_i_5_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_7 
       (.I0(mul_ln49_reg_316_reg_n_101),
        .I1(mul_ln49_fu_256_p2_n_101),
        .O(\acc[23]_i_7_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_8 
       (.I0(mul_ln49_reg_316_reg_n_102),
        .I1(mul_ln49_fu_256_p2_n_102),
        .O(\acc[23]_i_8_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[23]_i_9 
       (.I0(mul_ln49_reg_316_reg_n_103),
        .I1(mul_ln49_fu_256_p2_n_103),
        .O(\acc[23]_i_9_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_10 
       (.I0(mul_ln49_reg_316_reg_n_100),
        .I1(mul_ln49_fu_256_p2_n_100),
        .O(\acc[27]_i_10_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_2 
       (.I0(\acc_reg_n_2_[27] ),
        .I1(mul_ln49_reg_316_reg__1[27]),
        .O(\acc[27]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_3 
       (.I0(\acc_reg_n_2_[26] ),
        .I1(mul_ln49_reg_316_reg__1[26]),
        .O(\acc[27]_i_3_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_4 
       (.I0(\acc_reg_n_2_[25] ),
        .I1(mul_ln49_reg_316_reg__1[25]),
        .O(\acc[27]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_5 
       (.I0(\acc_reg_n_2_[24] ),
        .I1(mul_ln49_reg_316_reg__1[24]),
        .O(\acc[27]_i_5_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_7 
       (.I0(mul_ln49_reg_316_reg_n_97),
        .I1(mul_ln49_fu_256_p2_n_97),
        .O(\acc[27]_i_7_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_8 
       (.I0(mul_ln49_reg_316_reg_n_98),
        .I1(mul_ln49_fu_256_p2_n_98),
        .O(\acc[27]_i_8_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[27]_i_9 
       (.I0(mul_ln49_reg_316_reg_n_99),
        .I1(mul_ln49_fu_256_p2_n_99),
        .O(\acc[27]_i_9_n_2 ));
  LUT6 #(
    .INIT(64'hAABAFFFF00000000)) 
    \acc[31]_i_1 
       (.I0(icmp_ln44_reg_282_pp0_iter2_reg),
        .I1(icmp_ln44_fu_225_p2),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(ap_enable_reg_pp0_iter3_reg_n_2),
        .I5(ap_CS_fsm_state3),
        .O(acc));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_10 
       (.I0(mul_ln49_reg_316_reg_n_94),
        .I1(mul_ln49_fu_256_p2_n_94),
        .O(\acc[31]_i_10_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_11 
       (.I0(mul_ln49_reg_316_reg_n_95),
        .I1(mul_ln49_fu_256_p2_n_95),
        .O(\acc[31]_i_11_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_12 
       (.I0(mul_ln49_reg_316_reg_n_96),
        .I1(mul_ln49_fu_256_p2_n_96),
        .O(\acc[31]_i_12_n_2 ));
  LUT5 #(
    .INIT(32'h55450000)) 
    \acc[31]_i_2 
       (.I0(icmp_ln44_reg_282_pp0_iter2_reg),
        .I1(icmp_ln44_fu_225_p2),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(ap_enable_reg_pp0_iter3_reg_n_2),
        .O(acc0));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_4 
       (.I0(\acc_reg_n_2_[31] ),
        .I1(mul_ln49_reg_316_reg__1[31]),
        .O(\acc[31]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_5 
       (.I0(\acc_reg_n_2_[30] ),
        .I1(mul_ln49_reg_316_reg__1[30]),
        .O(\acc[31]_i_5_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_6 
       (.I0(\acc_reg_n_2_[29] ),
        .I1(mul_ln49_reg_316_reg__1[29]),
        .O(\acc[31]_i_6_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_7 
       (.I0(\acc_reg_n_2_[28] ),
        .I1(mul_ln49_reg_316_reg__1[28]),
        .O(\acc[31]_i_7_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[31]_i_9 
       (.I0(mul_ln49_reg_316_reg_n_93),
        .I1(mul_ln49_fu_256_p2_n_93),
        .O(\acc[31]_i_9_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[3]_i_2 
       (.I0(\acc_reg_n_2_[3] ),
        .I1(\mul_ln49_reg_316_reg[3]__0_n_2 ),
        .O(\acc[3]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[3]_i_3 
       (.I0(\acc_reg_n_2_[2] ),
        .I1(\mul_ln49_reg_316_reg[2]__0_n_2 ),
        .O(\acc[3]_i_3_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[3]_i_4 
       (.I0(\acc_reg_n_2_[1] ),
        .I1(\mul_ln49_reg_316_reg[1]__0_n_2 ),
        .O(\acc[3]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[3]_i_5 
       (.I0(\acc_reg_n_2_[0] ),
        .I1(\mul_ln49_reg_316_reg[0]__0_n_2 ),
        .O(\acc[3]_i_5_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[7]_i_2 
       (.I0(\acc_reg_n_2_[7] ),
        .I1(\mul_ln49_reg_316_reg[7]__0_n_2 ),
        .O(\acc[7]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[7]_i_3 
       (.I0(\acc_reg_n_2_[6] ),
        .I1(\mul_ln49_reg_316_reg[6]__0_n_2 ),
        .O(\acc[7]_i_3_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[7]_i_4 
       (.I0(\acc_reg_n_2_[5] ),
        .I1(\mul_ln49_reg_316_reg[5]__0_n_2 ),
        .O(\acc[7]_i_4_n_2 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc[7]_i_5 
       (.I0(\acc_reg_n_2_[4] ),
        .I1(\mul_ln49_reg_316_reg[4]__0_n_2 ),
        .O(\acc[7]_i_5_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[0] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[0]),
        .Q(\acc_reg_n_2_[0] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[10] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[10]),
        .Q(\acc_reg_n_2_[10] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[11] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[11]),
        .Q(\acc_reg_n_2_[11] ),
        .R(acc));
  CARRY4 \acc_reg[11]_i_1 
       (.CI(\acc_reg[7]_i_1_n_2 ),
        .CO({\acc_reg[11]_i_1_n_2 ,\acc_reg[11]_i_1_n_3 ,\acc_reg[11]_i_1_n_4 ,\acc_reg[11]_i_1_n_5 }),
        .CYINIT(1'b0),
        .DI({\acc_reg_n_2_[11] ,\acc_reg_n_2_[10] ,\acc_reg_n_2_[9] ,\acc_reg_n_2_[8] }),
        .O(add_ln50_fu_260_p2[11:8]),
        .S({\acc[11]_i_2_n_2 ,\acc[11]_i_3_n_2 ,\acc[11]_i_4_n_2 ,\acc[11]_i_5_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[12] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[12]),
        .Q(\acc_reg_n_2_[12] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[13] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[13]),
        .Q(\acc_reg_n_2_[13] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[14] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[14]),
        .Q(\acc_reg_n_2_[14] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[15] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[15]),
        .Q(\acc_reg_n_2_[15] ),
        .R(acc));
  CARRY4 \acc_reg[15]_i_1 
       (.CI(\acc_reg[11]_i_1_n_2 ),
        .CO({\acc_reg[15]_i_1_n_2 ,\acc_reg[15]_i_1_n_3 ,\acc_reg[15]_i_1_n_4 ,\acc_reg[15]_i_1_n_5 }),
        .CYINIT(1'b0),
        .DI({\acc_reg_n_2_[15] ,\acc_reg_n_2_[14] ,\acc_reg_n_2_[13] ,\acc_reg_n_2_[12] }),
        .O(add_ln50_fu_260_p2[15:12]),
        .S({\acc[15]_i_2_n_2 ,\acc[15]_i_3_n_2 ,\acc[15]_i_4_n_2 ,\acc[15]_i_5_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[16] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[16]),
        .Q(\acc_reg_n_2_[16] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[17] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[17]),
        .Q(\acc_reg_n_2_[17] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[18] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[18]),
        .Q(\acc_reg_n_2_[18] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[19] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[19]),
        .Q(\acc_reg_n_2_[19] ),
        .R(acc));
  CARRY4 \acc_reg[19]_i_1 
       (.CI(\acc_reg[15]_i_1_n_2 ),
        .CO({\acc_reg[19]_i_1_n_2 ,\acc_reg[19]_i_1_n_3 ,\acc_reg[19]_i_1_n_4 ,\acc_reg[19]_i_1_n_5 }),
        .CYINIT(1'b0),
        .DI({\acc_reg_n_2_[19] ,\acc_reg_n_2_[18] ,\acc_reg_n_2_[17] ,\acc_reg_n_2_[16] }),
        .O(add_ln50_fu_260_p2[19:16]),
        .S({\acc[19]_i_2_n_2 ,\acc[19]_i_3_n_2 ,\acc[19]_i_4_n_2 ,\acc[19]_i_5_n_2 }));
  CARRY4 \acc_reg[19]_i_6 
       (.CI(1'b0),
        .CO({\acc_reg[19]_i_6_n_2 ,\acc_reg[19]_i_6_n_3 ,\acc_reg[19]_i_6_n_4 ,\acc_reg[19]_i_6_n_5 }),
        .CYINIT(1'b0),
        .DI({mul_ln49_reg_316_reg_n_105,mul_ln49_reg_316_reg_n_106,mul_ln49_reg_316_reg_n_107,1'b0}),
        .O(mul_ln49_reg_316_reg__1[19:16]),
        .S({\acc[19]_i_7_n_2 ,\acc[19]_i_8_n_2 ,\acc[19]_i_9_n_2 ,\mul_ln49_reg_316_reg[16]__0_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[1] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[1]),
        .Q(\acc_reg_n_2_[1] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[20] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[20]),
        .Q(\acc_reg_n_2_[20] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[21] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[21]),
        .Q(\acc_reg_n_2_[21] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[22] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[22]),
        .Q(\acc_reg_n_2_[22] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[23] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[23]),
        .Q(\acc_reg_n_2_[23] ),
        .R(acc));
  CARRY4 \acc_reg[23]_i_1 
       (.CI(\acc_reg[19]_i_1_n_2 ),
        .CO({\acc_reg[23]_i_1_n_2 ,\acc_reg[23]_i_1_n_3 ,\acc_reg[23]_i_1_n_4 ,\acc_reg[23]_i_1_n_5 }),
        .CYINIT(1'b0),
        .DI({\acc_reg_n_2_[23] ,\acc_reg_n_2_[22] ,\acc_reg_n_2_[21] ,\acc_reg_n_2_[20] }),
        .O(add_ln50_fu_260_p2[23:20]),
        .S({\acc[23]_i_2_n_2 ,\acc[23]_i_3_n_2 ,\acc[23]_i_4_n_2 ,\acc[23]_i_5_n_2 }));
  CARRY4 \acc_reg[23]_i_6 
       (.CI(\acc_reg[19]_i_6_n_2 ),
        .CO({\acc_reg[23]_i_6_n_2 ,\acc_reg[23]_i_6_n_3 ,\acc_reg[23]_i_6_n_4 ,\acc_reg[23]_i_6_n_5 }),
        .CYINIT(1'b0),
        .DI({mul_ln49_reg_316_reg_n_101,mul_ln49_reg_316_reg_n_102,mul_ln49_reg_316_reg_n_103,mul_ln49_reg_316_reg_n_104}),
        .O(mul_ln49_reg_316_reg__1[23:20]),
        .S({\acc[23]_i_7_n_2 ,\acc[23]_i_8_n_2 ,\acc[23]_i_9_n_2 ,\acc[23]_i_10_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[24] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[24]),
        .Q(\acc_reg_n_2_[24] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[25] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[25]),
        .Q(\acc_reg_n_2_[25] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[26] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[26]),
        .Q(\acc_reg_n_2_[26] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[27] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[27]),
        .Q(\acc_reg_n_2_[27] ),
        .R(acc));
  CARRY4 \acc_reg[27]_i_1 
       (.CI(\acc_reg[23]_i_1_n_2 ),
        .CO({\acc_reg[27]_i_1_n_2 ,\acc_reg[27]_i_1_n_3 ,\acc_reg[27]_i_1_n_4 ,\acc_reg[27]_i_1_n_5 }),
        .CYINIT(1'b0),
        .DI({\acc_reg_n_2_[27] ,\acc_reg_n_2_[26] ,\acc_reg_n_2_[25] ,\acc_reg_n_2_[24] }),
        .O(add_ln50_fu_260_p2[27:24]),
        .S({\acc[27]_i_2_n_2 ,\acc[27]_i_3_n_2 ,\acc[27]_i_4_n_2 ,\acc[27]_i_5_n_2 }));
  CARRY4 \acc_reg[27]_i_6 
       (.CI(\acc_reg[23]_i_6_n_2 ),
        .CO({\acc_reg[27]_i_6_n_2 ,\acc_reg[27]_i_6_n_3 ,\acc_reg[27]_i_6_n_4 ,\acc_reg[27]_i_6_n_5 }),
        .CYINIT(1'b0),
        .DI({mul_ln49_reg_316_reg_n_97,mul_ln49_reg_316_reg_n_98,mul_ln49_reg_316_reg_n_99,mul_ln49_reg_316_reg_n_100}),
        .O(mul_ln49_reg_316_reg__1[27:24]),
        .S({\acc[27]_i_7_n_2 ,\acc[27]_i_8_n_2 ,\acc[27]_i_9_n_2 ,\acc[27]_i_10_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[28] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[28]),
        .Q(\acc_reg_n_2_[28] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[29] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[29]),
        .Q(\acc_reg_n_2_[29] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[2] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[2]),
        .Q(\acc_reg_n_2_[2] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[30] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[30]),
        .Q(\acc_reg_n_2_[30] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[31] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[31]),
        .Q(\acc_reg_n_2_[31] ),
        .R(acc));
  CARRY4 \acc_reg[31]_i_3 
       (.CI(\acc_reg[27]_i_1_n_2 ),
        .CO({\NLW_acc_reg[31]_i_3_CO_UNCONNECTED [3],\acc_reg[31]_i_3_n_3 ,\acc_reg[31]_i_3_n_4 ,\acc_reg[31]_i_3_n_5 }),
        .CYINIT(1'b0),
        .DI({1'b0,\acc_reg_n_2_[30] ,\acc_reg_n_2_[29] ,\acc_reg_n_2_[28] }),
        .O(add_ln50_fu_260_p2[31:28]),
        .S({\acc[31]_i_4_n_2 ,\acc[31]_i_5_n_2 ,\acc[31]_i_6_n_2 ,\acc[31]_i_7_n_2 }));
  CARRY4 \acc_reg[31]_i_8 
       (.CI(\acc_reg[27]_i_6_n_2 ),
        .CO({\NLW_acc_reg[31]_i_8_CO_UNCONNECTED [3],\acc_reg[31]_i_8_n_3 ,\acc_reg[31]_i_8_n_4 ,\acc_reg[31]_i_8_n_5 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln49_reg_316_reg_n_94,mul_ln49_reg_316_reg_n_95,mul_ln49_reg_316_reg_n_96}),
        .O(mul_ln49_reg_316_reg__1[31:28]),
        .S({\acc[31]_i_9_n_2 ,\acc[31]_i_10_n_2 ,\acc[31]_i_11_n_2 ,\acc[31]_i_12_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[3] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[3]),
        .Q(\acc_reg_n_2_[3] ),
        .R(acc));
  CARRY4 \acc_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\acc_reg[3]_i_1_n_2 ,\acc_reg[3]_i_1_n_3 ,\acc_reg[3]_i_1_n_4 ,\acc_reg[3]_i_1_n_5 }),
        .CYINIT(1'b0),
        .DI({\acc_reg_n_2_[3] ,\acc_reg_n_2_[2] ,\acc_reg_n_2_[1] ,\acc_reg_n_2_[0] }),
        .O(add_ln50_fu_260_p2[3:0]),
        .S({\acc[3]_i_2_n_2 ,\acc[3]_i_3_n_2 ,\acc[3]_i_4_n_2 ,\acc[3]_i_5_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[4] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[4]),
        .Q(\acc_reg_n_2_[4] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[5] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[5]),
        .Q(\acc_reg_n_2_[5] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[6] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[6]),
        .Q(\acc_reg_n_2_[6] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[7] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[7]),
        .Q(\acc_reg_n_2_[7] ),
        .R(acc));
  CARRY4 \acc_reg[7]_i_1 
       (.CI(\acc_reg[3]_i_1_n_2 ),
        .CO({\acc_reg[7]_i_1_n_2 ,\acc_reg[7]_i_1_n_3 ,\acc_reg[7]_i_1_n_4 ,\acc_reg[7]_i_1_n_5 }),
        .CYINIT(1'b0),
        .DI({\acc_reg_n_2_[7] ,\acc_reg_n_2_[6] ,\acc_reg_n_2_[5] ,\acc_reg_n_2_[4] }),
        .O(add_ln50_fu_260_p2[7:4]),
        .S({\acc[7]_i_2_n_2 ,\acc[7]_i_3_n_2 ,\acc[7]_i_4_n_2 ,\acc[7]_i_5_n_2 }));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[8] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[8]),
        .Q(\acc_reg_n_2_[8] ),
        .R(acc));
  FDRE #(
    .INIT(1'b0)) 
    \acc_reg[9] 
       (.C(ap_clk),
        .CE(acc0),
        .D(add_ln50_fu_260_p2[9]),
        .Q(\acc_reg_n_2_[9] ),
        .R(acc));
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(ap_CS_fsm_state9),
        .I1(\ap_CS_fsm[0]_i_2_n_2 ),
        .O(ap_NS_fsm[0]));
  LUT6 #(
    .INIT(64'h3FFF2F2FFFFFFFFF)) 
    \ap_CS_fsm[0]_i_2 
       (.I0(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .I1(strm_out_TREADY),
        .I2(strm_out_V_data_1_ack_in),
        .I3(strm_out_V_last_V_1_ack_in),
        .I4(strm_out_TVALID),
        .I5(\tmpa_last_V_reg_164_reg_n_2_[0] ),
        .O(\ap_CS_fsm[0]_i_2_n_2 ));
  LUT3 #(
    .INIT(8'hF8)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(\ap_CS_fsm[2]_i_2_n_2 ),
        .I2(ap_CS_fsm_state1),
        .O(ap_NS_fsm[1]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h04FF0404)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(\tmpa_last_V_reg_164_reg_n_2_[0] ),
        .I1(ap_CS_fsm_state9),
        .I2(\ap_CS_fsm[5]_i_2_n_2 ),
        .I3(\ap_CS_fsm[2]_i_2_n_2 ),
        .I4(ap_CS_fsm_state2),
        .O(ap_NS_fsm[2]));
  LUT5 #(
    .INIT(32'h37773FFF)) 
    \ap_CS_fsm[2]_i_2 
       (.I0(grp_fu_182_p1),
        .I1(ap_CS_fsm_state2),
        .I2(localmem_U_n_67),
        .I3(localmem_U_n_68),
        .I4(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .O(\ap_CS_fsm[2]_i_2_n_2 ));
  LUT5 #(
    .INIT(32'hFAEAFAFA)) 
    \ap_CS_fsm[3]_i_1 
       (.I0(ap_CS_fsm_state3),
        .I1(ap_enable_reg_pp0_iter2),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(ap_block_pp0_stage0_subdone),
        .I4(ap_enable_reg_pp0_iter3_reg_n_2),
        .O(ap_NS_fsm[3]));
  LUT6 #(
    .INIT(64'h0040FFFF00400040)) 
    \ap_CS_fsm[4]_i_1 
       (.I0(ap_block_pp0_stage0_subdone),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(ap_enable_reg_pp0_iter3_reg_n_2),
        .I3(ap_enable_reg_pp0_iter2),
        .I4(strm_out_V_data_1_ack_in),
        .I5(ap_CS_fsm_state8),
        .O(ap_NS_fsm[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0444)) 
    \ap_CS_fsm[4]_i_2 
       (.I0(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(\strm_in_V_last_V_0_state[1]_i_4_n_2 ),
        .I3(localmem_U_n_71),
        .O(ap_block_pp0_stage0_subdone));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \ap_CS_fsm[5]_i_1 
       (.I0(ap_CS_fsm_state9),
        .I1(\ap_CS_fsm[5]_i_2_n_2 ),
        .I2(ap_CS_fsm_state8),
        .I3(strm_out_V_data_1_ack_in),
        .O(ap_NS_fsm[5]));
  LUT5 #(
    .INIT(32'h2FFF2FAF)) 
    \ap_CS_fsm[5]_i_2 
       (.I0(strm_out_TVALID),
        .I1(strm_out_V_last_V_1_ack_in),
        .I2(strm_out_V_data_1_ack_in),
        .I3(strm_out_TREADY),
        .I4(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .O(\ap_CS_fsm[5]_i_2_n_2 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(ap_CS_fsm_state1),
        .S(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[2]),
        .Q(ap_CS_fsm_state3),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[3]),
        .Q(ap_CS_fsm_pp0_stage0),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[4]),
        .Q(ap_CS_fsm_state8),
        .R(reset));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[5]),
        .Q(ap_CS_fsm_state9),
        .R(reset));
  LUT6 #(
    .INIT(64'hA800A800A800A8A8)) 
    ap_enable_reg_pp0_iter0_i_1
       (.I0(ap_rst_n),
        .I1(ap_CS_fsm_state3),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(ap_enable_reg_pp0_iter0_i_2_n_2),
        .I4(icmp_ln879_fu_250_p2),
        .I5(icmp_ln44_fu_225_p2),
        .O(ap_enable_reg_pp0_iter0_i_1_n_2));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h0070FFFF)) 
    ap_enable_reg_pp0_iter0_i_2
       (.I0(localmem_U_n_71),
        .I1(\strm_in_V_last_V_0_state[1]_i_4_n_2 ),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(ap_CS_fsm_pp0_stage0),
        .O(ap_enable_reg_pp0_iter0_i_2_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter0_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter0_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hCCC8)) 
    ap_enable_reg_pp0_iter1_i_1
       (.I0(icmp_ln44_fu_225_p2),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(ap_enable_reg_pp0_iter1),
        .O(ap_enable_reg_pp0_iter1_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter1_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter1_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter1),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAABAAA8A)) 
    ap_enable_reg_pp0_iter2_i_1
       (.I0(ap_enable_reg_pp0_iter1),
        .I1(icmp_ln44_fu_225_p2),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(ap_enable_reg_pp0_iter2),
        .O(ap_enable_reg_pp0_iter2_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter2_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter2_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter2),
        .R(reset));
  LUT5 #(
    .INIT(32'h00A088A0)) 
    ap_enable_reg_pp0_iter3_i_1
       (.I0(ap_rst_n),
        .I1(ap_enable_reg_pp0_iter3_reg_n_2),
        .I2(ap_enable_reg_pp0_iter2),
        .I3(ap_block_pp0_stage0_subdone),
        .I4(ap_CS_fsm_state3),
        .O(ap_enable_reg_pp0_iter3_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    ap_enable_reg_pp0_iter3_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_enable_reg_pp0_iter3_i_1_n_2),
        .Q(ap_enable_reg_pp0_iter3_reg_n_2),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_op_assign_reg_153[0]_i_1 
       (.I0(i_op_assign_reg_153_reg[0]),
        .O(i_1_fu_231_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \i_op_assign_reg_153[1]_i_1 
       (.I0(i_op_assign_reg_153_reg[0]),
        .I1(i_op_assign_reg_153_reg[1]),
        .O(i_1_fu_231_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \i_op_assign_reg_153[2]_i_1 
       (.I0(i_op_assign_reg_153_reg[0]),
        .I1(i_op_assign_reg_153_reg[1]),
        .I2(i_op_assign_reg_153_reg[2]),
        .O(i_1_fu_231_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \i_op_assign_reg_153[3]_i_1 
       (.I0(i_op_assign_reg_153_reg[2]),
        .I1(i_op_assign_reg_153_reg[1]),
        .I2(i_op_assign_reg_153_reg[0]),
        .I3(i_op_assign_reg_153_reg[3]),
        .O(i_1_fu_231_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \i_op_assign_reg_153[4]_i_1 
       (.I0(i_op_assign_reg_153_reg[3]),
        .I1(i_op_assign_reg_153_reg[0]),
        .I2(i_op_assign_reg_153_reg[1]),
        .I3(i_op_assign_reg_153_reg[2]),
        .I4(i_op_assign_reg_153_reg[4]),
        .O(i_1_fu_231_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \i_op_assign_reg_153[5]_i_1 
       (.I0(i_op_assign_reg_153_reg[2]),
        .I1(i_op_assign_reg_153_reg[1]),
        .I2(i_op_assign_reg_153_reg[0]),
        .I3(i_op_assign_reg_153_reg[3]),
        .I4(i_op_assign_reg_153_reg[4]),
        .I5(i_op_assign_reg_153_reg[5]),
        .O(i_1_fu_231_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \i_op_assign_reg_153[6]_i_1 
       (.I0(\i_op_assign_reg_153[9]_i_5_n_2 ),
        .I1(i_op_assign_reg_153_reg[6]),
        .O(i_1_fu_231_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \i_op_assign_reg_153[7]_i_1 
       (.I0(\i_op_assign_reg_153[9]_i_5_n_2 ),
        .I1(i_op_assign_reg_153_reg[6]),
        .I2(i_op_assign_reg_153_reg[7]),
        .O(i_1_fu_231_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \i_op_assign_reg_153[8]_i_1 
       (.I0(i_op_assign_reg_153_reg[7]),
        .I1(i_op_assign_reg_153_reg[6]),
        .I2(\i_op_assign_reg_153[9]_i_5_n_2 ),
        .I3(i_op_assign_reg_153_reg[8]),
        .O(i_1_fu_231_p2[8]));
  LUT6 #(
    .INIT(64'hFFFFBFFF00000000)) 
    \i_op_assign_reg_153[9]_i_1 
       (.I0(icmp_ln879_fu_250_p2),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(icmp_ln44_fu_225_p2),
        .I5(ap_CS_fsm_state3),
        .O(i_op_assign_reg_153));
  LUT5 #(
    .INIT(32'h00004000)) 
    \i_op_assign_reg_153[9]_i_2 
       (.I0(icmp_ln879_fu_250_p2),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(icmp_ln44_fu_225_p2),
        .O(i_op_assign_reg_1530));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    \i_op_assign_reg_153[9]_i_3 
       (.I0(i_op_assign_reg_153_reg[8]),
        .I1(\i_op_assign_reg_153[9]_i_5_n_2 ),
        .I2(i_op_assign_reg_153_reg[6]),
        .I3(i_op_assign_reg_153_reg[7]),
        .I4(i_op_assign_reg_153_reg[9]),
        .O(i_1_fu_231_p2[9]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \i_op_assign_reg_153[9]_i_5 
       (.I0(i_op_assign_reg_153_reg[2]),
        .I1(i_op_assign_reg_153_reg[1]),
        .I2(i_op_assign_reg_153_reg[0]),
        .I3(i_op_assign_reg_153_reg[3]),
        .I4(i_op_assign_reg_153_reg[4]),
        .I5(i_op_assign_reg_153_reg[5]),
        .O(\i_op_assign_reg_153[9]_i_5_n_2 ));
  LUT1 #(
    .INIT(2'h1)) 
    \i_op_assign_reg_153[9]_i_6 
       (.I0(i_op_assign_reg_153_reg[9]),
        .O(\i_op_assign_reg_153[9]_i_6_n_2 ));
  LUT3 #(
    .INIT(8'h01)) 
    \i_op_assign_reg_153[9]_i_7 
       (.I0(i_op_assign_reg_153_reg[8]),
        .I1(i_op_assign_reg_153_reg[7]),
        .I2(i_op_assign_reg_153_reg[6]),
        .O(\i_op_assign_reg_153[9]_i_7_n_2 ));
  LUT5 #(
    .INIT(32'h41000041)) 
    \i_op_assign_reg_153[9]_i_8 
       (.I0(i_op_assign_reg_153_reg[5]),
        .I1(vect_size_V[4]),
        .I2(i_op_assign_reg_153_reg[4]),
        .I3(i_op_assign_reg_153_reg[3]),
        .I4(vect_size_V[3]),
        .O(\i_op_assign_reg_153[9]_i_8_n_2 ));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    \i_op_assign_reg_153[9]_i_9 
       (.I0(i_op_assign_reg_153_reg[0]),
        .I1(vect_size_V[0]),
        .I2(vect_size_V[2]),
        .I3(i_op_assign_reg_153_reg[2]),
        .I4(i_op_assign_reg_153_reg[1]),
        .I5(vect_size_V[1]),
        .O(\i_op_assign_reg_153[9]_i_9_n_2 ));
  FDRE \i_op_assign_reg_153_reg[0] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[0]),
        .Q(i_op_assign_reg_153_reg[0]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[1] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[1]),
        .Q(i_op_assign_reg_153_reg[1]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[2] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[2]),
        .Q(i_op_assign_reg_153_reg[2]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[3] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[3]),
        .Q(i_op_assign_reg_153_reg[3]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[4] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[4]),
        .Q(i_op_assign_reg_153_reg[4]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[5] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[5]),
        .Q(i_op_assign_reg_153_reg[5]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[6] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[6]),
        .Q(i_op_assign_reg_153_reg[6]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[7] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[7]),
        .Q(i_op_assign_reg_153_reg[7]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[8] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[8]),
        .Q(i_op_assign_reg_153_reg[8]),
        .R(i_op_assign_reg_153));
  FDRE \i_op_assign_reg_153_reg[9] 
       (.C(ap_clk),
        .CE(i_op_assign_reg_1530),
        .D(i_1_fu_231_p2[9]),
        .Q(i_op_assign_reg_153_reg[9]),
        .R(i_op_assign_reg_153));
  CARRY4 \i_op_assign_reg_153_reg[9]_i_4 
       (.CI(1'b0),
        .CO({icmp_ln879_fu_250_p2,\i_op_assign_reg_153_reg[9]_i_4_n_3 ,\i_op_assign_reg_153_reg[9]_i_4_n_4 ,\i_op_assign_reg_153_reg[9]_i_4_n_5 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_i_op_assign_reg_153_reg[9]_i_4_O_UNCONNECTED [3:0]),
        .S({\i_op_assign_reg_153[9]_i_6_n_2 ,\i_op_assign_reg_153[9]_i_7_n_2 ,\i_op_assign_reg_153[9]_i_8_n_2 ,\i_op_assign_reg_153[9]_i_9_n_2 }));
  LUT5 #(
    .INIT(32'hFF75AA00)) 
    \icmp_ln44_reg_282[0]_i_1 
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(icmp_ln44_fu_225_p2),
        .I4(\icmp_ln44_reg_282_reg_n_2_[0] ),
        .O(\icmp_ln44_reg_282[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hBBBBBFBB88888088)) 
    \icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1 
       (.I0(\icmp_ln44_reg_282_reg_n_2_[0] ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(icmp_ln44_fu_225_p2),
        .I5(icmp_ln44_reg_282_pp0_iter1_reg),
        .O(\icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1_n_2 ));
  FDRE \icmp_ln44_reg_282_pp0_iter1_reg_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\icmp_ln44_reg_282_pp0_iter1_reg[0]_i_1_n_2 ),
        .Q(icmp_ln44_reg_282_pp0_iter1_reg),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAABAAA8A)) 
    \icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1 
       (.I0(icmp_ln44_reg_282_pp0_iter1_reg),
        .I1(icmp_ln44_fu_225_p2),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(icmp_ln44_reg_282_pp0_iter2_reg),
        .O(\icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1_n_2 ));
  FDRE \icmp_ln44_reg_282_pp0_iter2_reg_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\icmp_ln44_reg_282_pp0_iter2_reg[0]_i_1_n_2 ),
        .Q(icmp_ln44_reg_282_pp0_iter2_reg),
        .R(1'b0));
  FDRE \icmp_ln44_reg_282_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\icmp_ln44_reg_282[0]_i_1_n_2 ),
        .Q(\icmp_ln44_reg_282_reg_n_2_[0] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFBFBB00008088)) 
    \icmp_ln879_reg_307[0]_i_1 
       (.I0(icmp_ln879_fu_250_p2),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(icmp_ln44_fu_225_p2),
        .I5(icmp_ln879_reg_307),
        .O(\icmp_ln879_reg_307[0]_i_1_n_2 ));
  FDRE \icmp_ln879_reg_307_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\icmp_ln879_reg_307[0]_i_1_n_2 ),
        .Q(icmp_ln879_reg_307),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_fixed_macc_lbkb localmem_U
       (.Q({ap_CS_fsm_pp0_stage0,ap_CS_fsm_state2}),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter0(ap_enable_reg_pp0_iter0),
        .d0(d0),
        .\i_op_assign_reg_153_reg[4] (localmem_U_n_71),
        .icmp_ln34_fu_192_p2(icmp_ln34_fu_192_p2),
        .icmp_ln44_fu_225_p2(icmp_ln44_fu_225_p2),
        .localmem_q0(localmem_q0),
        .ram_reg(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .ram_reg_0({val_assign_reg_107_reg__0,val_assign_reg_107_reg}),
        .ram_reg_1(strm_in_V_data_0_payload_B),
        .ram_reg_2(strm_in_V_data_0_payload_A),
        .strm_in_V_data_0_sel(strm_in_V_data_0_sel),
        .\tmp_3_1_3_reg_142[0]_i_2 (i_op_assign_reg_153_reg),
        .\val_assign_reg_107_reg[1] (localmem_U_n_67),
        .\val_assign_reg_107_reg[4] (localmem_U_n_68),
        .we0(we0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 15x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(2),
    .BREG(2),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln49_fu_256_p2
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,localmem_q0[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln49_fu_256_p2_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({d0[31],d0[31],d0[31],d0[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln49_fu_256_p2_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln49_fu_256_p2_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln49_fu_256_p2_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(localmem_load_reg_3110),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(strm_in_V_data_0_sel2),
        .CEB2(tmpa_last_V_reg_1641),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(mul_ln49_reg_3160),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln49_fu_256_p2_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln49_fu_256_p2_OVERFLOW_UNCONNECTED),
        .P({mul_ln49_fu_256_p2_n_60,mul_ln49_fu_256_p2_n_61,mul_ln49_fu_256_p2_n_62,mul_ln49_fu_256_p2_n_63,mul_ln49_fu_256_p2_n_64,mul_ln49_fu_256_p2_n_65,mul_ln49_fu_256_p2_n_66,mul_ln49_fu_256_p2_n_67,mul_ln49_fu_256_p2_n_68,mul_ln49_fu_256_p2_n_69,mul_ln49_fu_256_p2_n_70,mul_ln49_fu_256_p2_n_71,mul_ln49_fu_256_p2_n_72,mul_ln49_fu_256_p2_n_73,mul_ln49_fu_256_p2_n_74,mul_ln49_fu_256_p2_n_75,mul_ln49_fu_256_p2_n_76,mul_ln49_fu_256_p2_n_77,mul_ln49_fu_256_p2_n_78,mul_ln49_fu_256_p2_n_79,mul_ln49_fu_256_p2_n_80,mul_ln49_fu_256_p2_n_81,mul_ln49_fu_256_p2_n_82,mul_ln49_fu_256_p2_n_83,mul_ln49_fu_256_p2_n_84,mul_ln49_fu_256_p2_n_85,mul_ln49_fu_256_p2_n_86,mul_ln49_fu_256_p2_n_87,mul_ln49_fu_256_p2_n_88,mul_ln49_fu_256_p2_n_89,mul_ln49_fu_256_p2_n_90,mul_ln49_fu_256_p2_n_91,mul_ln49_fu_256_p2_n_92,mul_ln49_fu_256_p2_n_93,mul_ln49_fu_256_p2_n_94,mul_ln49_fu_256_p2_n_95,mul_ln49_fu_256_p2_n_96,mul_ln49_fu_256_p2_n_97,mul_ln49_fu_256_p2_n_98,mul_ln49_fu_256_p2_n_99,mul_ln49_fu_256_p2_n_100,mul_ln49_fu_256_p2_n_101,mul_ln49_fu_256_p2_n_102,mul_ln49_fu_256_p2_n_103,mul_ln49_fu_256_p2_n_104,mul_ln49_fu_256_p2_n_105,mul_ln49_fu_256_p2_n_106,mul_ln49_fu_256_p2_n_107}),
        .PATTERNBDETECT(NLW_mul_ln49_fu_256_p2_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln49_fu_256_p2_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln49_fu_256_p2_n_108,mul_ln49_fu_256_p2_n_109,mul_ln49_fu_256_p2_n_110,mul_ln49_fu_256_p2_n_111,mul_ln49_fu_256_p2_n_112,mul_ln49_fu_256_p2_n_113,mul_ln49_fu_256_p2_n_114,mul_ln49_fu_256_p2_n_115,mul_ln49_fu_256_p2_n_116,mul_ln49_fu_256_p2_n_117,mul_ln49_fu_256_p2_n_118,mul_ln49_fu_256_p2_n_119,mul_ln49_fu_256_p2_n_120,mul_ln49_fu_256_p2_n_121,mul_ln49_fu_256_p2_n_122,mul_ln49_fu_256_p2_n_123,mul_ln49_fu_256_p2_n_124,mul_ln49_fu_256_p2_n_125,mul_ln49_fu_256_p2_n_126,mul_ln49_fu_256_p2_n_127,mul_ln49_fu_256_p2_n_128,mul_ln49_fu_256_p2_n_129,mul_ln49_fu_256_p2_n_130,mul_ln49_fu_256_p2_n_131,mul_ln49_fu_256_p2_n_132,mul_ln49_fu_256_p2_n_133,mul_ln49_fu_256_p2_n_134,mul_ln49_fu_256_p2_n_135,mul_ln49_fu_256_p2_n_136,mul_ln49_fu_256_p2_n_137,mul_ln49_fu_256_p2_n_138,mul_ln49_fu_256_p2_n_139,mul_ln49_fu_256_p2_n_140,mul_ln49_fu_256_p2_n_141,mul_ln49_fu_256_p2_n_142,mul_ln49_fu_256_p2_n_143,mul_ln49_fu_256_p2_n_144,mul_ln49_fu_256_p2_n_145,mul_ln49_fu_256_p2_n_146,mul_ln49_fu_256_p2_n_147,mul_ln49_fu_256_p2_n_148,mul_ln49_fu_256_p2_n_149,mul_ln49_fu_256_p2_n_150,mul_ln49_fu_256_p2_n_151,mul_ln49_fu_256_p2_n_152,mul_ln49_fu_256_p2_n_153,mul_ln49_fu_256_p2_n_154,mul_ln49_fu_256_p2_n_155}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln49_fu_256_p2_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln49_fu_256_p2__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,d0[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln49_fu_256_p2__0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,localmem_q0[16:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln49_fu_256_p2__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln49_fu_256_p2__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln49_fu_256_p2__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(strm_in_V_data_0_sel2),
        .CEA2(tmpa_last_V_reg_1641),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(localmem_load_reg_3110),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln49_fu_256_p2__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln49_fu_256_p2__0_OVERFLOW_UNCONNECTED),
        .P({mul_ln49_fu_256_p2__0_n_60,mul_ln49_fu_256_p2__0_n_61,mul_ln49_fu_256_p2__0_n_62,mul_ln49_fu_256_p2__0_n_63,mul_ln49_fu_256_p2__0_n_64,mul_ln49_fu_256_p2__0_n_65,mul_ln49_fu_256_p2__0_n_66,mul_ln49_fu_256_p2__0_n_67,mul_ln49_fu_256_p2__0_n_68,mul_ln49_fu_256_p2__0_n_69,mul_ln49_fu_256_p2__0_n_70,mul_ln49_fu_256_p2__0_n_71,mul_ln49_fu_256_p2__0_n_72,mul_ln49_fu_256_p2__0_n_73,mul_ln49_fu_256_p2__0_n_74,mul_ln49_fu_256_p2__0_n_75,mul_ln49_fu_256_p2__0_n_76,mul_ln49_fu_256_p2__0_n_77,mul_ln49_fu_256_p2__0_n_78,mul_ln49_fu_256_p2__0_n_79,mul_ln49_fu_256_p2__0_n_80,mul_ln49_fu_256_p2__0_n_81,mul_ln49_fu_256_p2__0_n_82,mul_ln49_fu_256_p2__0_n_83,mul_ln49_fu_256_p2__0_n_84,mul_ln49_fu_256_p2__0_n_85,mul_ln49_fu_256_p2__0_n_86,mul_ln49_fu_256_p2__0_n_87,mul_ln49_fu_256_p2__0_n_88,mul_ln49_fu_256_p2__0_n_89,mul_ln49_fu_256_p2__0_n_90,mul_ln49_fu_256_p2__0_n_91,mul_ln49_fu_256_p2__0_n_92,mul_ln49_fu_256_p2__0_n_93,mul_ln49_fu_256_p2__0_n_94,mul_ln49_fu_256_p2__0_n_95,mul_ln49_fu_256_p2__0_n_96,mul_ln49_fu_256_p2__0_n_97,mul_ln49_fu_256_p2__0_n_98,mul_ln49_fu_256_p2__0_n_99,mul_ln49_fu_256_p2__0_n_100,mul_ln49_fu_256_p2__0_n_101,mul_ln49_fu_256_p2__0_n_102,mul_ln49_fu_256_p2__0_n_103,mul_ln49_fu_256_p2__0_n_104,mul_ln49_fu_256_p2__0_n_105,mul_ln49_fu_256_p2__0_n_106,mul_ln49_fu_256_p2__0_n_107}),
        .PATTERNBDETECT(NLW_mul_ln49_fu_256_p2__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln49_fu_256_p2__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({mul_ln49_fu_256_p2__0_n_108,mul_ln49_fu_256_p2__0_n_109,mul_ln49_fu_256_p2__0_n_110,mul_ln49_fu_256_p2__0_n_111,mul_ln49_fu_256_p2__0_n_112,mul_ln49_fu_256_p2__0_n_113,mul_ln49_fu_256_p2__0_n_114,mul_ln49_fu_256_p2__0_n_115,mul_ln49_fu_256_p2__0_n_116,mul_ln49_fu_256_p2__0_n_117,mul_ln49_fu_256_p2__0_n_118,mul_ln49_fu_256_p2__0_n_119,mul_ln49_fu_256_p2__0_n_120,mul_ln49_fu_256_p2__0_n_121,mul_ln49_fu_256_p2__0_n_122,mul_ln49_fu_256_p2__0_n_123,mul_ln49_fu_256_p2__0_n_124,mul_ln49_fu_256_p2__0_n_125,mul_ln49_fu_256_p2__0_n_126,mul_ln49_fu_256_p2__0_n_127,mul_ln49_fu_256_p2__0_n_128,mul_ln49_fu_256_p2__0_n_129,mul_ln49_fu_256_p2__0_n_130,mul_ln49_fu_256_p2__0_n_131,mul_ln49_fu_256_p2__0_n_132,mul_ln49_fu_256_p2__0_n_133,mul_ln49_fu_256_p2__0_n_134,mul_ln49_fu_256_p2__0_n_135,mul_ln49_fu_256_p2__0_n_136,mul_ln49_fu_256_p2__0_n_137,mul_ln49_fu_256_p2__0_n_138,mul_ln49_fu_256_p2__0_n_139,mul_ln49_fu_256_p2__0_n_140,mul_ln49_fu_256_p2__0_n_141,mul_ln49_fu_256_p2__0_n_142,mul_ln49_fu_256_p2__0_n_143,mul_ln49_fu_256_p2__0_n_144,mul_ln49_fu_256_p2__0_n_145,mul_ln49_fu_256_p2__0_n_146,mul_ln49_fu_256_p2__0_n_147,mul_ln49_fu_256_p2__0_n_148,mul_ln49_fu_256_p2__0_n_149,mul_ln49_fu_256_p2__0_n_150,mul_ln49_fu_256_p2__0_n_151,mul_ln49_fu_256_p2__0_n_152,mul_ln49_fu_256_p2__0_n_153,mul_ln49_fu_256_p2__0_n_154,mul_ln49_fu_256_p2__0_n_155}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln49_fu_256_p2__0_UNDERFLOW_UNCONNECTED));
  LUT6 #(
    .INIT(64'h0000FB0000000000)) 
    mul_ln49_fu_256_p2_i_1
       (.I0(icmp_ln44_fu_225_p2),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(\icmp_ln44_reg_282_reg_n_2_[0] ),
        .I5(ap_enable_reg_pp0_iter1),
        .O(localmem_load_reg_3110));
  LUT4 #(
    .INIT(16'h4000)) 
    mul_ln49_fu_256_p2_i_2
       (.I0(icmp_ln44_fu_225_p2),
        .I1(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(ap_enable_reg_pp0_iter0),
        .O(strm_in_V_data_0_sel2));
  LUT4 #(
    .INIT(16'hAA8A)) 
    mul_ln49_fu_256_p2_i_3
       (.I0(ap_CS_fsm_pp0_stage0),
        .I1(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(icmp_ln44_fu_225_p2),
        .O(tmpa_last_V_reg_1641));
  LUT4 #(
    .INIT(16'h5545)) 
    mul_ln49_fu_256_p2_i_4
       (.I0(icmp_ln44_reg_282_pp0_iter1_reg),
        .I1(icmp_ln44_fu_225_p2),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .O(mul_ln49_reg_3160));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x15 4}}" *) 
  DSP48E1 #(
    .ACASCREG(2),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(2),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    mul_ln49_reg_316_reg
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,d0[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_mul_ln49_reg_316_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({localmem_q0[31],localmem_q0[31],localmem_q0[31],localmem_q0[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_mul_ln49_reg_316_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_mul_ln49_reg_316_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_mul_ln49_reg_316_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(strm_in_V_data_0_sel2),
        .CEA2(tmpa_last_V_reg_1641),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(localmem_load_reg_3110),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(mul_ln49_reg_3160),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_mul_ln49_reg_316_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_mul_ln49_reg_316_reg_OVERFLOW_UNCONNECTED),
        .P({mul_ln49_reg_316_reg_n_60,mul_ln49_reg_316_reg_n_61,mul_ln49_reg_316_reg_n_62,mul_ln49_reg_316_reg_n_63,mul_ln49_reg_316_reg_n_64,mul_ln49_reg_316_reg_n_65,mul_ln49_reg_316_reg_n_66,mul_ln49_reg_316_reg_n_67,mul_ln49_reg_316_reg_n_68,mul_ln49_reg_316_reg_n_69,mul_ln49_reg_316_reg_n_70,mul_ln49_reg_316_reg_n_71,mul_ln49_reg_316_reg_n_72,mul_ln49_reg_316_reg_n_73,mul_ln49_reg_316_reg_n_74,mul_ln49_reg_316_reg_n_75,mul_ln49_reg_316_reg_n_76,mul_ln49_reg_316_reg_n_77,mul_ln49_reg_316_reg_n_78,mul_ln49_reg_316_reg_n_79,mul_ln49_reg_316_reg_n_80,mul_ln49_reg_316_reg_n_81,mul_ln49_reg_316_reg_n_82,mul_ln49_reg_316_reg_n_83,mul_ln49_reg_316_reg_n_84,mul_ln49_reg_316_reg_n_85,mul_ln49_reg_316_reg_n_86,mul_ln49_reg_316_reg_n_87,mul_ln49_reg_316_reg_n_88,mul_ln49_reg_316_reg_n_89,mul_ln49_reg_316_reg_n_90,mul_ln49_reg_316_reg_n_91,mul_ln49_reg_316_reg_n_92,mul_ln49_reg_316_reg_n_93,mul_ln49_reg_316_reg_n_94,mul_ln49_reg_316_reg_n_95,mul_ln49_reg_316_reg_n_96,mul_ln49_reg_316_reg_n_97,mul_ln49_reg_316_reg_n_98,mul_ln49_reg_316_reg_n_99,mul_ln49_reg_316_reg_n_100,mul_ln49_reg_316_reg_n_101,mul_ln49_reg_316_reg_n_102,mul_ln49_reg_316_reg_n_103,mul_ln49_reg_316_reg_n_104,mul_ln49_reg_316_reg_n_105,mul_ln49_reg_316_reg_n_106,mul_ln49_reg_316_reg_n_107}),
        .PATTERNBDETECT(NLW_mul_ln49_reg_316_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_mul_ln49_reg_316_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({mul_ln49_fu_256_p2__0_n_108,mul_ln49_fu_256_p2__0_n_109,mul_ln49_fu_256_p2__0_n_110,mul_ln49_fu_256_p2__0_n_111,mul_ln49_fu_256_p2__0_n_112,mul_ln49_fu_256_p2__0_n_113,mul_ln49_fu_256_p2__0_n_114,mul_ln49_fu_256_p2__0_n_115,mul_ln49_fu_256_p2__0_n_116,mul_ln49_fu_256_p2__0_n_117,mul_ln49_fu_256_p2__0_n_118,mul_ln49_fu_256_p2__0_n_119,mul_ln49_fu_256_p2__0_n_120,mul_ln49_fu_256_p2__0_n_121,mul_ln49_fu_256_p2__0_n_122,mul_ln49_fu_256_p2__0_n_123,mul_ln49_fu_256_p2__0_n_124,mul_ln49_fu_256_p2__0_n_125,mul_ln49_fu_256_p2__0_n_126,mul_ln49_fu_256_p2__0_n_127,mul_ln49_fu_256_p2__0_n_128,mul_ln49_fu_256_p2__0_n_129,mul_ln49_fu_256_p2__0_n_130,mul_ln49_fu_256_p2__0_n_131,mul_ln49_fu_256_p2__0_n_132,mul_ln49_fu_256_p2__0_n_133,mul_ln49_fu_256_p2__0_n_134,mul_ln49_fu_256_p2__0_n_135,mul_ln49_fu_256_p2__0_n_136,mul_ln49_fu_256_p2__0_n_137,mul_ln49_fu_256_p2__0_n_138,mul_ln49_fu_256_p2__0_n_139,mul_ln49_fu_256_p2__0_n_140,mul_ln49_fu_256_p2__0_n_141,mul_ln49_fu_256_p2__0_n_142,mul_ln49_fu_256_p2__0_n_143,mul_ln49_fu_256_p2__0_n_144,mul_ln49_fu_256_p2__0_n_145,mul_ln49_fu_256_p2__0_n_146,mul_ln49_fu_256_p2__0_n_147,mul_ln49_fu_256_p2__0_n_148,mul_ln49_fu_256_p2__0_n_149,mul_ln49_fu_256_p2__0_n_150,mul_ln49_fu_256_p2__0_n_151,mul_ln49_fu_256_p2__0_n_152,mul_ln49_fu_256_p2__0_n_153,mul_ln49_fu_256_p2__0_n_154,mul_ln49_fu_256_p2__0_n_155}),
        .PCOUT(NLW_mul_ln49_reg_316_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_mul_ln49_reg_316_reg_UNDERFLOW_UNCONNECTED));
  FDRE \mul_ln49_reg_316_reg[0]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_107),
        .Q(\mul_ln49_reg_316_reg[0]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[10]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_97),
        .Q(\mul_ln49_reg_316_reg[10]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[11]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_96),
        .Q(\mul_ln49_reg_316_reg[11]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[12]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_95),
        .Q(\mul_ln49_reg_316_reg[12]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[13]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_94),
        .Q(\mul_ln49_reg_316_reg[13]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[14]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_93),
        .Q(\mul_ln49_reg_316_reg[14]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[15]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_92),
        .Q(\mul_ln49_reg_316_reg[15]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[16]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_91),
        .Q(\mul_ln49_reg_316_reg[16]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[1]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_106),
        .Q(\mul_ln49_reg_316_reg[1]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[2]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_105),
        .Q(\mul_ln49_reg_316_reg[2]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[3]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_104),
        .Q(\mul_ln49_reg_316_reg[3]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[4]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_103),
        .Q(\mul_ln49_reg_316_reg[4]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[5]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_102),
        .Q(\mul_ln49_reg_316_reg[5]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[6]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_101),
        .Q(\mul_ln49_reg_316_reg[6]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[7]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_100),
        .Q(\mul_ln49_reg_316_reg[7]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[8]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_99),
        .Q(\mul_ln49_reg_316_reg[8]__0_n_2 ),
        .R(1'b0));
  FDRE \mul_ln49_reg_316_reg[9]__0 
       (.C(ap_clk),
        .CE(mul_ln49_reg_3160),
        .D(mul_ln49_fu_256_p2__0_n_98),
        .Q(\mul_ln49_reg_316_reg[9]__0_n_2 ),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h0D)) 
    \strm_in_V_data_0_payload_A[31]_i_1 
       (.I0(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I1(strm_in_V_data_0_ack_in),
        .I2(strm_in_V_data_0_sel_wr),
        .O(strm_in_V_data_0_load_A));
  FDRE \strm_in_V_data_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[0]),
        .Q(strm_in_V_data_0_payload_A[0]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[10]),
        .Q(strm_in_V_data_0_payload_A[10]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[11]),
        .Q(strm_in_V_data_0_payload_A[11]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[12]),
        .Q(strm_in_V_data_0_payload_A[12]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[13]),
        .Q(strm_in_V_data_0_payload_A[13]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[14]),
        .Q(strm_in_V_data_0_payload_A[14]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[15]),
        .Q(strm_in_V_data_0_payload_A[15]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[16]),
        .Q(strm_in_V_data_0_payload_A[16]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[17]),
        .Q(strm_in_V_data_0_payload_A[17]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[18]),
        .Q(strm_in_V_data_0_payload_A[18]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[19]),
        .Q(strm_in_V_data_0_payload_A[19]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[1]),
        .Q(strm_in_V_data_0_payload_A[1]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[20]),
        .Q(strm_in_V_data_0_payload_A[20]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[21]),
        .Q(strm_in_V_data_0_payload_A[21]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[22]),
        .Q(strm_in_V_data_0_payload_A[22]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[23]),
        .Q(strm_in_V_data_0_payload_A[23]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[24]),
        .Q(strm_in_V_data_0_payload_A[24]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[25]),
        .Q(strm_in_V_data_0_payload_A[25]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[26]),
        .Q(strm_in_V_data_0_payload_A[26]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[27]),
        .Q(strm_in_V_data_0_payload_A[27]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[28]),
        .Q(strm_in_V_data_0_payload_A[28]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[29]),
        .Q(strm_in_V_data_0_payload_A[29]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[2]),
        .Q(strm_in_V_data_0_payload_A[2]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[30]),
        .Q(strm_in_V_data_0_payload_A[30]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[31]),
        .Q(strm_in_V_data_0_payload_A[31]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[3]),
        .Q(strm_in_V_data_0_payload_A[3]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[4]),
        .Q(strm_in_V_data_0_payload_A[4]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[5]),
        .Q(strm_in_V_data_0_payload_A[5]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[6]),
        .Q(strm_in_V_data_0_payload_A[6]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[7]),
        .Q(strm_in_V_data_0_payload_A[7]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[8]),
        .Q(strm_in_V_data_0_payload_A[8]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_A),
        .D(strm_in_TDATA[9]),
        .Q(strm_in_V_data_0_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \strm_in_V_data_0_payload_B[31]_i_1 
       (.I0(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I1(strm_in_V_data_0_ack_in),
        .I2(strm_in_V_data_0_sel_wr),
        .O(strm_in_V_data_0_load_B));
  FDRE \strm_in_V_data_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[0]),
        .Q(strm_in_V_data_0_payload_B[0]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[10]),
        .Q(strm_in_V_data_0_payload_B[10]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[11]),
        .Q(strm_in_V_data_0_payload_B[11]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[12]),
        .Q(strm_in_V_data_0_payload_B[12]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[13]),
        .Q(strm_in_V_data_0_payload_B[13]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[14]),
        .Q(strm_in_V_data_0_payload_B[14]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[15]),
        .Q(strm_in_V_data_0_payload_B[15]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[16]),
        .Q(strm_in_V_data_0_payload_B[16]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[17]),
        .Q(strm_in_V_data_0_payload_B[17]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[18]),
        .Q(strm_in_V_data_0_payload_B[18]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[19]),
        .Q(strm_in_V_data_0_payload_B[19]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[1]),
        .Q(strm_in_V_data_0_payload_B[1]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[20]),
        .Q(strm_in_V_data_0_payload_B[20]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[21]),
        .Q(strm_in_V_data_0_payload_B[21]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[22]),
        .Q(strm_in_V_data_0_payload_B[22]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[23]),
        .Q(strm_in_V_data_0_payload_B[23]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[24]),
        .Q(strm_in_V_data_0_payload_B[24]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[25]),
        .Q(strm_in_V_data_0_payload_B[25]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[26]),
        .Q(strm_in_V_data_0_payload_B[26]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[27]),
        .Q(strm_in_V_data_0_payload_B[27]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[28]),
        .Q(strm_in_V_data_0_payload_B[28]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[29]),
        .Q(strm_in_V_data_0_payload_B[29]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[2]),
        .Q(strm_in_V_data_0_payload_B[2]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[30]),
        .Q(strm_in_V_data_0_payload_B[30]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[31]),
        .Q(strm_in_V_data_0_payload_B[31]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[3]),
        .Q(strm_in_V_data_0_payload_B[3]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[4]),
        .Q(strm_in_V_data_0_payload_B[4]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[5]),
        .Q(strm_in_V_data_0_payload_B[5]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[6]),
        .Q(strm_in_V_data_0_payload_B[6]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[7]),
        .Q(strm_in_V_data_0_payload_B[7]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[8]),
        .Q(strm_in_V_data_0_payload_B[8]),
        .R(1'b0));
  FDRE \strm_in_V_data_0_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(strm_in_V_data_0_load_B),
        .D(strm_in_TDATA[9]),
        .Q(strm_in_V_data_0_payload_B[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h4F5F5F5FB0A0A0A0)) 
    strm_in_V_data_0_sel_rd_i_1
       (.I0(we0),
        .I1(icmp_ln44_fu_225_p2),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(ap_CS_fsm_pp0_stage0),
        .I4(ap_enable_reg_pp0_iter0),
        .I5(strm_in_V_data_0_sel),
        .O(strm_in_V_data_0_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_in_V_data_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_in_V_data_0_sel_rd_i_1_n_2),
        .Q(strm_in_V_data_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h78)) 
    strm_in_V_data_0_sel_wr_i_1
       (.I0(strm_in_TVALID),
        .I1(strm_in_V_data_0_ack_in),
        .I2(strm_in_V_data_0_sel_wr),
        .O(strm_in_V_data_0_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_in_V_data_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_in_V_data_0_sel_wr_i_1_n_2),
        .Q(strm_in_V_data_0_sel_wr),
        .R(reset));
  LUT6 #(
    .INIT(64'hA8A8A8A820A02020)) 
    \strm_in_V_data_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(strm_in_V_data_0_ack_in),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(we0),
        .I4(\strm_in_V_last_V_0_state[1]_i_3_n_2 ),
        .I5(strm_in_TVALID),
        .O(\strm_in_V_data_0_state[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'h5DFF5DFFFFFF5DFF)) 
    \strm_in_V_data_0_state[1]_i_1 
       (.I0(\strm_in_V_last_V_0_state[1]_i_3_n_2 ),
        .I1(ap_CS_fsm_state2),
        .I2(icmp_ln34_fu_192_p2),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(strm_in_V_data_0_ack_in),
        .I5(strm_in_TVALID),
        .O(strm_in_V_data_0_state));
  FDRE #(
    .INIT(1'b0)) 
    \strm_in_V_data_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_in_V_data_0_state[0]_i_1_n_2 ),
        .Q(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \strm_in_V_data_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_in_V_data_0_state),
        .Q(strm_in_V_data_0_ack_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \strm_in_V_last_V_0_payload_A[0]_i_1 
       (.I0(strm_in_TLAST),
        .I1(\strm_in_V_last_V_0_state_reg_n_2_[0] ),
        .I2(strm_in_TREADY),
        .I3(strm_in_V_last_V_0_sel_wr),
        .I4(strm_in_V_last_V_0_payload_A),
        .O(\strm_in_V_last_V_0_payload_A[0]_i_1_n_2 ));
  FDRE \strm_in_V_last_V_0_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_in_V_last_V_0_payload_A[0]_i_1_n_2 ),
        .Q(strm_in_V_last_V_0_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \strm_in_V_last_V_0_payload_B[0]_i_1 
       (.I0(strm_in_TLAST),
        .I1(\strm_in_V_last_V_0_state_reg_n_2_[0] ),
        .I2(strm_in_TREADY),
        .I3(strm_in_V_last_V_0_sel_wr),
        .I4(strm_in_V_last_V_0_payload_B),
        .O(\strm_in_V_last_V_0_payload_B[0]_i_1_n_2 ));
  FDRE \strm_in_V_last_V_0_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_in_V_last_V_0_payload_B[0]_i_1_n_2 ),
        .Q(strm_in_V_last_V_0_payload_B),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBF00FFFF40FF0000)) 
    strm_in_V_last_V_0_sel_rd_i_1
       (.I0(icmp_ln34_fu_192_p2),
        .I1(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I2(ap_CS_fsm_state2),
        .I3(\strm_in_V_last_V_0_state[1]_i_3_n_2 ),
        .I4(\strm_in_V_last_V_0_state_reg_n_2_[0] ),
        .I5(strm_in_V_last_V_0_sel),
        .O(strm_in_V_last_V_0_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_in_V_last_V_0_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_in_V_last_V_0_sel_rd_i_1_n_2),
        .Q(strm_in_V_last_V_0_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h78)) 
    strm_in_V_last_V_0_sel_wr_i_1
       (.I0(strm_in_TVALID),
        .I1(strm_in_TREADY),
        .I2(strm_in_V_last_V_0_sel_wr),
        .O(strm_in_V_last_V_0_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_in_V_last_V_0_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_in_V_last_V_0_sel_wr_i_1_n_2),
        .Q(strm_in_V_last_V_0_sel_wr),
        .R(reset));
  LUT6 #(
    .INIT(64'hAAAAAA0020AA0000)) 
    \strm_in_V_last_V_0_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(we0),
        .I2(\strm_in_V_last_V_0_state[1]_i_3_n_2 ),
        .I3(strm_in_TREADY),
        .I4(\strm_in_V_last_V_0_state_reg_n_2_[0] ),
        .I5(strm_in_TVALID),
        .O(\strm_in_V_last_V_0_state[0]_i_1_n_2 ));
  LUT1 #(
    .INIT(2'h1)) 
    \strm_in_V_last_V_0_state[1]_i_1 
       (.I0(ap_rst_n),
        .O(reset));
  LUT5 #(
    .INIT(32'hFF4FFFFF)) 
    \strm_in_V_last_V_0_state[1]_i_2 
       (.I0(strm_in_TVALID),
        .I1(strm_in_TREADY),
        .I2(\strm_in_V_last_V_0_state_reg_n_2_[0] ),
        .I3(we0),
        .I4(\strm_in_V_last_V_0_state[1]_i_3_n_2 ),
        .O(strm_in_V_last_V_0_state));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFF7F7F7F)) 
    \strm_in_V_last_V_0_state[1]_i_3 
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(\strm_in_V_last_V_0_state[1]_i_4_n_2 ),
        .I4(localmem_U_n_71),
        .O(\strm_in_V_last_V_0_state[1]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \strm_in_V_last_V_0_state[1]_i_4 
       (.I0(i_op_assign_reg_153_reg[1]),
        .I1(i_op_assign_reg_153_reg[0]),
        .I2(i_op_assign_reg_153_reg[3]),
        .I3(i_op_assign_reg_153_reg[2]),
        .O(\strm_in_V_last_V_0_state[1]_i_4_n_2 ));
  FDRE #(
    .INIT(1'b0)) 
    \strm_in_V_last_V_0_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_in_V_last_V_0_state[0]_i_1_n_2 ),
        .Q(\strm_in_V_last_V_0_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \strm_in_V_last_V_0_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_in_V_last_V_0_state),
        .Q(strm_in_TREADY),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[0]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[0]),
        .I1(strm_out_V_data_1_payload_A[0]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[10]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[10]),
        .I1(strm_out_V_data_1_payload_A[10]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[11]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[11]),
        .I1(strm_out_V_data_1_payload_A[11]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[12]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[12]),
        .I1(strm_out_V_data_1_payload_A[12]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[13]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[13]),
        .I1(strm_out_V_data_1_payload_A[13]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[14]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[14]),
        .I1(strm_out_V_data_1_payload_A[14]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[15]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[15]),
        .I1(strm_out_V_data_1_payload_A[15]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[16]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[16]),
        .I1(strm_out_V_data_1_payload_A[16]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[16]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[17]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[17]),
        .I1(strm_out_V_data_1_payload_A[17]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[17]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[18]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[18]),
        .I1(strm_out_V_data_1_payload_A[18]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[18]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[19]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[19]),
        .I1(strm_out_V_data_1_payload_A[19]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[19]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[1]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[1]),
        .I1(strm_out_V_data_1_payload_A[1]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[20]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[20]),
        .I1(strm_out_V_data_1_payload_A[20]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[20]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[21]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[21]),
        .I1(strm_out_V_data_1_payload_A[21]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[21]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[22]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[22]),
        .I1(strm_out_V_data_1_payload_A[22]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[22]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[23]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[23]),
        .I1(strm_out_V_data_1_payload_A[23]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[23]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[24]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[24]),
        .I1(strm_out_V_data_1_payload_A[24]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[24]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[25]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[25]),
        .I1(strm_out_V_data_1_payload_A[25]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[25]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[26]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[26]),
        .I1(strm_out_V_data_1_payload_A[26]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[26]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[27]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[27]),
        .I1(strm_out_V_data_1_payload_A[27]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[27]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[28]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[28]),
        .I1(strm_out_V_data_1_payload_A[28]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[28]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[29]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[29]),
        .I1(strm_out_V_data_1_payload_A[29]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[29]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[2]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[2]),
        .I1(strm_out_V_data_1_payload_A[2]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[30]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[30]),
        .I1(strm_out_V_data_1_payload_A[30]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[30]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[31]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[31]),
        .I1(strm_out_V_data_1_payload_A[31]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[31]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[3]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[3]),
        .I1(strm_out_V_data_1_payload_A[3]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[4]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[4]),
        .I1(strm_out_V_data_1_payload_A[4]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[5]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[5]),
        .I1(strm_out_V_data_1_payload_A[5]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[6]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[6]),
        .I1(strm_out_V_data_1_payload_A[6]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[7]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[7]),
        .I1(strm_out_V_data_1_payload_A[7]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[8]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[8]),
        .I1(strm_out_V_data_1_payload_A[8]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \strm_out_TDATA[9]_INST_0 
       (.I0(strm_out_V_data_1_payload_B[9]),
        .I1(strm_out_V_data_1_payload_A[9]),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_TDATA[9]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \strm_out_TLAST[0]_INST_0 
       (.I0(strm_out_V_last_V_1_payload_B),
        .I1(strm_out_V_last_V_1_sel),
        .I2(strm_out_V_last_V_1_payload_A),
        .O(strm_out_TLAST));
  LUT3 #(
    .INIT(8'h0D)) 
    \strm_out_V_data_1_payload_A[31]_i_1 
       (.I0(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .I1(strm_out_V_data_1_ack_in),
        .I2(strm_out_V_data_1_sel_wr),
        .O(strm_out_V_data_1_load_A));
  FDRE \strm_out_V_data_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[0] ),
        .Q(strm_out_V_data_1_payload_A[0]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[10] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[10] ),
        .Q(strm_out_V_data_1_payload_A[10]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[11] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[11] ),
        .Q(strm_out_V_data_1_payload_A[11]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[12] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[12] ),
        .Q(strm_out_V_data_1_payload_A[12]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[13] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[13] ),
        .Q(strm_out_V_data_1_payload_A[13]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[14] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[14] ),
        .Q(strm_out_V_data_1_payload_A[14]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[15] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[15] ),
        .Q(strm_out_V_data_1_payload_A[15]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[16] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[16] ),
        .Q(strm_out_V_data_1_payload_A[16]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[17] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[17] ),
        .Q(strm_out_V_data_1_payload_A[17]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[18] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[18] ),
        .Q(strm_out_V_data_1_payload_A[18]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[19] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[19] ),
        .Q(strm_out_V_data_1_payload_A[19]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[1] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[1] ),
        .Q(strm_out_V_data_1_payload_A[1]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[20] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[20] ),
        .Q(strm_out_V_data_1_payload_A[20]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[21] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[21] ),
        .Q(strm_out_V_data_1_payload_A[21]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[22] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[22] ),
        .Q(strm_out_V_data_1_payload_A[22]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[23] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[23] ),
        .Q(strm_out_V_data_1_payload_A[23]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[24] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[24] ),
        .Q(strm_out_V_data_1_payload_A[24]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[25] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[25] ),
        .Q(strm_out_V_data_1_payload_A[25]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[26] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[26] ),
        .Q(strm_out_V_data_1_payload_A[26]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[27] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[27] ),
        .Q(strm_out_V_data_1_payload_A[27]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[28] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[28] ),
        .Q(strm_out_V_data_1_payload_A[28]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[29] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[29] ),
        .Q(strm_out_V_data_1_payload_A[29]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[2] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[2] ),
        .Q(strm_out_V_data_1_payload_A[2]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[30] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[30] ),
        .Q(strm_out_V_data_1_payload_A[30]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[31] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[31] ),
        .Q(strm_out_V_data_1_payload_A[31]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[3] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[3] ),
        .Q(strm_out_V_data_1_payload_A[3]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[4] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[4] ),
        .Q(strm_out_V_data_1_payload_A[4]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[5] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[5] ),
        .Q(strm_out_V_data_1_payload_A[5]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[6] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[6] ),
        .Q(strm_out_V_data_1_payload_A[6]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[7] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[7] ),
        .Q(strm_out_V_data_1_payload_A[7]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[8] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[8] ),
        .Q(strm_out_V_data_1_payload_A[8]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_A_reg[9] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_A),
        .D(\acc_reg_n_2_[9] ),
        .Q(strm_out_V_data_1_payload_A[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hD0)) 
    \strm_out_V_data_1_payload_B[31]_i_1 
       (.I0(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .I1(strm_out_V_data_1_ack_in),
        .I2(strm_out_V_data_1_sel_wr),
        .O(strm_out_V_data_1_load_B));
  FDRE \strm_out_V_data_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[0] ),
        .Q(strm_out_V_data_1_payload_B[0]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[10] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[10] ),
        .Q(strm_out_V_data_1_payload_B[10]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[11] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[11] ),
        .Q(strm_out_V_data_1_payload_B[11]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[12] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[12] ),
        .Q(strm_out_V_data_1_payload_B[12]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[13] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[13] ),
        .Q(strm_out_V_data_1_payload_B[13]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[14] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[14] ),
        .Q(strm_out_V_data_1_payload_B[14]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[15] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[15] ),
        .Q(strm_out_V_data_1_payload_B[15]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[16] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[16] ),
        .Q(strm_out_V_data_1_payload_B[16]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[17] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[17] ),
        .Q(strm_out_V_data_1_payload_B[17]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[18] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[18] ),
        .Q(strm_out_V_data_1_payload_B[18]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[19] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[19] ),
        .Q(strm_out_V_data_1_payload_B[19]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[1] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[1] ),
        .Q(strm_out_V_data_1_payload_B[1]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[20] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[20] ),
        .Q(strm_out_V_data_1_payload_B[20]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[21] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[21] ),
        .Q(strm_out_V_data_1_payload_B[21]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[22] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[22] ),
        .Q(strm_out_V_data_1_payload_B[22]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[23] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[23] ),
        .Q(strm_out_V_data_1_payload_B[23]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[24] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[24] ),
        .Q(strm_out_V_data_1_payload_B[24]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[25] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[25] ),
        .Q(strm_out_V_data_1_payload_B[25]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[26] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[26] ),
        .Q(strm_out_V_data_1_payload_B[26]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[27] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[27] ),
        .Q(strm_out_V_data_1_payload_B[27]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[28] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[28] ),
        .Q(strm_out_V_data_1_payload_B[28]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[29] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[29] ),
        .Q(strm_out_V_data_1_payload_B[29]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[2] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[2] ),
        .Q(strm_out_V_data_1_payload_B[2]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[30] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[30] ),
        .Q(strm_out_V_data_1_payload_B[30]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[31] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[31] ),
        .Q(strm_out_V_data_1_payload_B[31]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[3] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[3] ),
        .Q(strm_out_V_data_1_payload_B[3]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[4] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[4] ),
        .Q(strm_out_V_data_1_payload_B[4]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[5] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[5] ),
        .Q(strm_out_V_data_1_payload_B[5]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[6] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[6] ),
        .Q(strm_out_V_data_1_payload_B[6]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[7] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[7] ),
        .Q(strm_out_V_data_1_payload_B[7]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[8] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[8] ),
        .Q(strm_out_V_data_1_payload_B[8]),
        .R(1'b0));
  FDRE \strm_out_V_data_1_payload_B_reg[9] 
       (.C(ap_clk),
        .CE(strm_out_V_data_1_load_B),
        .D(\acc_reg_n_2_[9] ),
        .Q(strm_out_V_data_1_payload_B[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    strm_out_V_data_1_sel_rd_i_1
       (.I0(strm_out_TREADY),
        .I1(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .I2(strm_out_V_data_1_sel),
        .O(strm_out_V_data_1_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_out_V_data_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_out_V_data_1_sel_rd_i_1_n_2),
        .Q(strm_out_V_data_1_sel),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h78)) 
    strm_out_V_data_1_sel_wr_i_1
       (.I0(strm_out_V_data_1_ack_in),
        .I1(ap_CS_fsm_state8),
        .I2(strm_out_V_data_1_sel_wr),
        .O(strm_out_V_data_1_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_out_V_data_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_out_V_data_1_sel_wr_i_1_n_2),
        .Q(strm_out_V_data_1_sel_wr),
        .R(reset));
  LUT5 #(
    .INIT(32'hA8A820A0)) 
    \strm_out_V_data_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(strm_out_V_data_1_ack_in),
        .I2(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .I3(strm_out_TREADY),
        .I4(ap_CS_fsm_state8),
        .O(\strm_out_V_data_1_state[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hFF2F)) 
    \strm_out_V_data_1_state[1]_i_1 
       (.I0(strm_out_V_data_1_ack_in),
        .I1(ap_CS_fsm_state8),
        .I2(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .I3(strm_out_TREADY),
        .O(strm_out_V_data_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \strm_out_V_data_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_out_V_data_1_state[0]_i_1_n_2 ),
        .Q(\strm_out_V_data_1_state_reg_n_2_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \strm_out_V_data_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_out_V_data_1_state),
        .Q(strm_out_V_data_1_ack_in),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFAE00A2)) 
    \strm_out_V_last_V_1_payload_A[0]_i_1 
       (.I0(\tmpa_last_V_reg_164_reg_n_2_[0] ),
        .I1(strm_out_TVALID),
        .I2(strm_out_V_last_V_1_ack_in),
        .I3(strm_out_V_last_V_1_sel_wr),
        .I4(strm_out_V_last_V_1_payload_A),
        .O(\strm_out_V_last_V_1_payload_A[0]_i_1_n_2 ));
  FDRE \strm_out_V_last_V_1_payload_A_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_out_V_last_V_1_payload_A[0]_i_1_n_2 ),
        .Q(strm_out_V_last_V_1_payload_A),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAEFFA200)) 
    \strm_out_V_last_V_1_payload_B[0]_i_1 
       (.I0(\tmpa_last_V_reg_164_reg_n_2_[0] ),
        .I1(strm_out_TVALID),
        .I2(strm_out_V_last_V_1_ack_in),
        .I3(strm_out_V_last_V_1_sel_wr),
        .I4(strm_out_V_last_V_1_payload_B),
        .O(\strm_out_V_last_V_1_payload_B[0]_i_1_n_2 ));
  FDRE \strm_out_V_last_V_1_payload_B_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_out_V_last_V_1_payload_B[0]_i_1_n_2 ),
        .Q(strm_out_V_last_V_1_payload_B),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    strm_out_V_last_V_1_sel_rd_i_1
       (.I0(strm_out_TREADY),
        .I1(strm_out_TVALID),
        .I2(strm_out_V_last_V_1_sel),
        .O(strm_out_V_last_V_1_sel_rd_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_out_V_last_V_1_sel_rd_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_out_V_last_V_1_sel_rd_i_1_n_2),
        .Q(strm_out_V_last_V_1_sel),
        .R(reset));
  LUT4 #(
    .INIT(16'h7F80)) 
    strm_out_V_last_V_1_sel_wr_i_1
       (.I0(strm_out_V_data_1_ack_in),
        .I1(ap_CS_fsm_state8),
        .I2(strm_out_V_last_V_1_ack_in),
        .I3(strm_out_V_last_V_1_sel_wr),
        .O(strm_out_V_last_V_1_sel_wr_i_1_n_2));
  FDRE #(
    .INIT(1'b0)) 
    strm_out_V_last_V_1_sel_wr_reg
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_out_V_last_V_1_sel_wr_i_1_n_2),
        .Q(strm_out_V_last_V_1_sel_wr),
        .R(reset));
  LUT6 #(
    .INIT(64'h80AAAAAA80008000)) 
    \strm_out_V_last_V_1_state[0]_i_1 
       (.I0(ap_rst_n),
        .I1(ap_CS_fsm_state8),
        .I2(strm_out_V_data_1_ack_in),
        .I3(strm_out_V_last_V_1_ack_in),
        .I4(strm_out_TREADY),
        .I5(strm_out_TVALID),
        .O(\strm_out_V_last_V_1_state[0]_i_1_n_2 ));
  LUT5 #(
    .INIT(32'hFFFF70FF)) 
    \strm_out_V_last_V_1_state[1]_i_1 
       (.I0(strm_out_V_data_1_ack_in),
        .I1(ap_CS_fsm_state8),
        .I2(strm_out_V_last_V_1_ack_in),
        .I3(strm_out_TVALID),
        .I4(strm_out_TREADY),
        .O(strm_out_V_last_V_1_state));
  FDRE #(
    .INIT(1'b0)) 
    \strm_out_V_last_V_1_state_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\strm_out_V_last_V_1_state[0]_i_1_n_2 ),
        .Q(strm_out_TVALID),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \strm_out_V_last_V_1_state_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(strm_out_V_last_V_1_state),
        .Q(strm_out_V_last_V_1_ack_in),
        .R(reset));
  LUT6 #(
    .INIT(64'h0555055504000000)) 
    \tmp_3_1_2_reg_129[0]_i_1 
       (.I0(ap_NS_fsm1),
        .I1(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I2(icmp_ln34_fu_192_p2),
        .I3(ap_CS_fsm_state2),
        .I4(grp_fu_182_p1),
        .I5(\tmp_3_1_2_reg_129_reg_n_2_[0] ),
        .O(\tmp_3_1_2_reg_129[0]_i_1_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \tmp_3_1_2_reg_129[0]_i_2 
       (.I0(\tmpa_last_V_reg_164_reg_n_2_[0] ),
        .I1(ap_CS_fsm_state9),
        .I2(\ap_CS_fsm[5]_i_2_n_2 ),
        .O(ap_NS_fsm1));
  FDRE \tmp_3_1_2_reg_129_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_3_1_2_reg_129[0]_i_1_n_2 ),
        .Q(\tmp_3_1_2_reg_129_reg_n_2_[0] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAFAFAFCCA0A0A0CC)) 
    \tmp_3_1_3_reg_142[0]_i_1 
       (.I0(\tmp_3_1_2_reg_129_reg_n_2_[0] ),
        .I1(tmp_last_V_1_reg_296),
        .I2(ap_CS_fsm_state3),
        .I3(icmp_ln879_reg_307),
        .I4(\tmp_3_1_3_reg_142[0]_i_2_n_2 ),
        .I5(tmp_3_1_3_reg_142),
        .O(\tmp_3_1_3_reg_142[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hBBBBBFBBBFBBBFBB)) 
    \tmp_3_1_3_reg_142[0]_i_2 
       (.I0(\tmpa_last_V_reg_164[0]_i_3_n_2 ),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I3(ap_enable_reg_pp0_iter0),
        .I4(\strm_in_V_last_V_0_state[1]_i_4_n_2 ),
        .I5(localmem_U_n_71),
        .O(\tmp_3_1_3_reg_142[0]_i_2_n_2 ));
  FDRE \tmp_3_1_3_reg_142_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_3_1_3_reg_142[0]_i_1_n_2 ),
        .Q(tmp_3_1_3_reg_142),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAEAAAAAAA2AAA)) 
    \tmp_last_V_1_reg_296[0]_i_1 
       (.I0(tmp_last_V_1_reg_296),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(ap_CS_fsm_pp0_stage0),
        .I3(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I4(icmp_ln44_fu_225_p2),
        .I5(grp_fu_182_p1),
        .O(\tmp_last_V_1_reg_296[0]_i_1_n_2 ));
  FDRE \tmp_last_V_1_reg_296_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmp_last_V_1_reg_296[0]_i_1_n_2 ),
        .Q(tmp_last_V_1_reg_296),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAFEFFAAAA0200)) 
    \tmpa_last_V_reg_164[0]_i_1 
       (.I0(\tmpa_last_V_reg_164[0]_i_2_n_2 ),
        .I1(\tmpa_last_V_reg_164[0]_i_3_n_2 ),
        .I2(ap_enable_reg_pp0_iter0_i_2_n_2),
        .I3(icmp_ln879_reg_307),
        .I4(\tmpa_last_V_reg_164[0]_i_4_n_2 ),
        .I5(\tmpa_last_V_reg_164_reg_n_2_[0] ),
        .O(\tmpa_last_V_reg_164[0]_i_1_n_2 ));
  LUT6 #(
    .INIT(64'hFFABFFFF00A80000)) 
    \tmpa_last_V_reg_164[0]_i_2 
       (.I0(tmp_last_V_1_reg_296),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(icmp_ln879_reg_307),
        .I3(\icmp_ln44_reg_282_reg_n_2_[0] ),
        .I4(ap_enable_reg_pp0_iter1),
        .I5(tmp_3_1_3_reg_142),
        .O(\tmpa_last_V_reg_164[0]_i_2_n_2 ));
  LUT2 #(
    .INIT(4'hB)) 
    \tmpa_last_V_reg_164[0]_i_3 
       (.I0(\icmp_ln44_reg_282_reg_n_2_[0] ),
        .I1(ap_enable_reg_pp0_iter1),
        .O(\tmpa_last_V_reg_164[0]_i_3_n_2 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \tmpa_last_V_reg_164[0]_i_4 
       (.I0(ap_enable_reg_pp0_iter0),
        .I1(ap_CS_fsm_pp0_stage0),
        .I2(\strm_in_V_last_V_0_state[1]_i_4_n_2 ),
        .I3(localmem_U_n_71),
        .O(\tmpa_last_V_reg_164[0]_i_4_n_2 ));
  FDRE \tmpa_last_V_reg_164_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\tmpa_last_V_reg_164[0]_i_1_n_2 ),
        .Q(\tmpa_last_V_reg_164_reg_n_2_[0] ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \val_assign_reg_107[0]_i_1 
       (.I0(val_assign_reg_107_reg[0]),
        .O(i_fu_198_p2[0]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \val_assign_reg_107[1]_i_1 
       (.I0(val_assign_reg_107_reg[0]),
        .I1(val_assign_reg_107_reg[1]),
        .O(i_fu_198_p2[1]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \val_assign_reg_107[2]_i_1 
       (.I0(val_assign_reg_107_reg[0]),
        .I1(val_assign_reg_107_reg[1]),
        .I2(val_assign_reg_107_reg[2]),
        .O(i_fu_198_p2[2]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \val_assign_reg_107[3]_i_1 
       (.I0(val_assign_reg_107_reg[2]),
        .I1(val_assign_reg_107_reg[1]),
        .I2(val_assign_reg_107_reg[0]),
        .I3(val_assign_reg_107_reg[3]),
        .O(i_fu_198_p2[3]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \val_assign_reg_107[4]_i_1 
       (.I0(val_assign_reg_107_reg[3]),
        .I1(val_assign_reg_107_reg[0]),
        .I2(val_assign_reg_107_reg[1]),
        .I3(val_assign_reg_107_reg[2]),
        .I4(val_assign_reg_107_reg[4]),
        .O(i_fu_198_p2[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \val_assign_reg_107[5]_i_1 
       (.I0(val_assign_reg_107_reg[2]),
        .I1(val_assign_reg_107_reg[1]),
        .I2(val_assign_reg_107_reg[0]),
        .I3(val_assign_reg_107_reg[3]),
        .I4(val_assign_reg_107_reg[4]),
        .I5(val_assign_reg_107_reg__0[5]),
        .O(i_fu_198_p2[5]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \val_assign_reg_107[6]_i_1 
       (.I0(\val_assign_reg_107[9]_i_5_n_2 ),
        .I1(val_assign_reg_107_reg__0[6]),
        .O(i_fu_198_p2[6]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \val_assign_reg_107[7]_i_1 
       (.I0(\val_assign_reg_107[9]_i_5_n_2 ),
        .I1(val_assign_reg_107_reg__0[6]),
        .I2(val_assign_reg_107_reg__0[7]),
        .O(i_fu_198_p2[7]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \val_assign_reg_107[8]_i_1 
       (.I0(val_assign_reg_107_reg__0[7]),
        .I1(val_assign_reg_107_reg__0[6]),
        .I2(\val_assign_reg_107[9]_i_5_n_2 ),
        .I3(val_assign_reg_107_reg__0[8]),
        .O(i_fu_198_p2[8]));
  LUT5 #(
    .INIT(32'hFFDF0000)) 
    \val_assign_reg_107[9]_i_1 
       (.I0(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I1(icmp_ln34_fu_192_p2),
        .I2(ap_CS_fsm_state2),
        .I3(grp_fu_182_p1),
        .I4(ap_CS_fsm_state1),
        .O(val_assign_reg_107));
  LUT6 #(
    .INIT(64'h0000002020200020)) 
    \val_assign_reg_107[9]_i_2 
       (.I0(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I1(icmp_ln34_fu_192_p2),
        .I2(ap_CS_fsm_state2),
        .I3(strm_in_V_last_V_0_payload_A),
        .I4(strm_in_V_last_V_0_sel),
        .I5(strm_in_V_last_V_0_payload_B),
        .O(val_assign_reg_1070));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    \val_assign_reg_107[9]_i_3 
       (.I0(val_assign_reg_107_reg__0[8]),
        .I1(\val_assign_reg_107[9]_i_5_n_2 ),
        .I2(val_assign_reg_107_reg__0[6]),
        .I3(val_assign_reg_107_reg__0[7]),
        .I4(val_assign_reg_107_reg__0[9]),
        .O(i_fu_198_p2[9]));
  LUT3 #(
    .INIT(8'hB8)) 
    \val_assign_reg_107[9]_i_4 
       (.I0(strm_in_V_last_V_0_payload_B),
        .I1(strm_in_V_last_V_0_sel),
        .I2(strm_in_V_last_V_0_payload_A),
        .O(grp_fu_182_p1));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \val_assign_reg_107[9]_i_5 
       (.I0(val_assign_reg_107_reg[2]),
        .I1(val_assign_reg_107_reg[1]),
        .I2(val_assign_reg_107_reg[0]),
        .I3(val_assign_reg_107_reg[3]),
        .I4(val_assign_reg_107_reg[4]),
        .I5(val_assign_reg_107_reg__0[5]),
        .O(\val_assign_reg_107[9]_i_5_n_2 ));
  FDRE \val_assign_reg_107_reg[0] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[0]),
        .Q(val_assign_reg_107_reg[0]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[1] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[1]),
        .Q(val_assign_reg_107_reg[1]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[2] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[2]),
        .Q(val_assign_reg_107_reg[2]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[3] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[3]),
        .Q(val_assign_reg_107_reg[3]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[4] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[4]),
        .Q(val_assign_reg_107_reg[4]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[5] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[5]),
        .Q(val_assign_reg_107_reg__0[5]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[6] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[6]),
        .Q(val_assign_reg_107_reg__0[6]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[7] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[7]),
        .Q(val_assign_reg_107_reg__0[7]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[8] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[8]),
        .Q(val_assign_reg_107_reg__0[8]),
        .R(val_assign_reg_107));
  FDRE \val_assign_reg_107_reg[9] 
       (.C(ap_clk),
        .CE(val_assign_reg_1070),
        .D(i_fu_198_p2[9]),
        .Q(val_assign_reg_107_reg__0[9]),
        .R(val_assign_reg_107));
  LUT6 #(
    .INIT(64'hE0E0E0C0C0C0E0C0)) 
    \vect_size_V[4]_i_1 
       (.I0(\strm_in_V_data_0_state_reg_n_2_[0] ),
        .I1(icmp_ln34_fu_192_p2),
        .I2(ap_CS_fsm_state2),
        .I3(strm_in_V_last_V_0_payload_A),
        .I4(strm_in_V_last_V_0_sel),
        .I5(strm_in_V_last_V_0_payload_B),
        .O(ap_NS_fsm143_out));
  FDRE #(
    .INIT(1'b0)) 
    \vect_size_V_reg[0] 
       (.C(ap_clk),
        .CE(ap_NS_fsm143_out),
        .D(val_assign_reg_107_reg[0]),
        .Q(vect_size_V[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \vect_size_V_reg[1] 
       (.C(ap_clk),
        .CE(ap_NS_fsm143_out),
        .D(val_assign_reg_107_reg[1]),
        .Q(vect_size_V[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \vect_size_V_reg[2] 
       (.C(ap_clk),
        .CE(ap_NS_fsm143_out),
        .D(val_assign_reg_107_reg[2]),
        .Q(vect_size_V[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \vect_size_V_reg[3] 
       (.C(ap_clk),
        .CE(ap_NS_fsm143_out),
        .D(val_assign_reg_107_reg[3]),
        .Q(vect_size_V[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \vect_size_V_reg[4] 
       (.C(ap_clk),
        .CE(ap_NS_fsm143_out),
        .D(val_assign_reg_107_reg[4]),
        .Q(vect_size_V[4]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_fixed_macc_lbkb
   (localmem_q0,
    d0,
    we0,
    \val_assign_reg_107_reg[1] ,
    \val_assign_reg_107_reg[4] ,
    icmp_ln34_fu_192_p2,
    icmp_ln44_fu_225_p2,
    \i_op_assign_reg_153_reg[4] ,
    ap_clk,
    Q,
    ram_reg,
    ap_enable_reg_pp0_iter0,
    ram_reg_0,
    \tmp_3_1_3_reg_142[0]_i_2 ,
    ram_reg_1,
    ram_reg_2,
    strm_in_V_data_0_sel);
  output [31:0]localmem_q0;
  output [31:0]d0;
  output we0;
  output \val_assign_reg_107_reg[1] ;
  output \val_assign_reg_107_reg[4] ;
  output icmp_ln34_fu_192_p2;
  output icmp_ln44_fu_225_p2;
  output \i_op_assign_reg_153_reg[4] ;
  input ap_clk;
  input [1:0]Q;
  input ram_reg;
  input ap_enable_reg_pp0_iter0;
  input [9:0]ram_reg_0;
  input [9:0]\tmp_3_1_3_reg_142[0]_i_2 ;
  input [31:0]ram_reg_1;
  input [31:0]ram_reg_2;
  input strm_in_V_data_0_sel;

  wire [1:0]Q;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire [31:0]d0;
  wire \i_op_assign_reg_153_reg[4] ;
  wire icmp_ln34_fu_192_p2;
  wire icmp_ln44_fu_225_p2;
  wire [31:0]localmem_q0;
  wire ram_reg;
  wire [9:0]ram_reg_0;
  wire [31:0]ram_reg_1;
  wire [31:0]ram_reg_2;
  wire strm_in_V_data_0_sel;
  wire [9:0]\tmp_3_1_3_reg_142[0]_i_2 ;
  wire \val_assign_reg_107_reg[1] ;
  wire \val_assign_reg_107_reg[4] ;
  wire we0;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_fixed_macc_lbkb_ram axis_fixed_macc_lbkb_ram_U
       (.Q(Q),
        .WEBWE(we0),
        .ap_clk(ap_clk),
        .ap_enable_reg_pp0_iter0(ap_enable_reg_pp0_iter0),
        .d0(d0),
        .\i_op_assign_reg_153_reg[4] (\i_op_assign_reg_153_reg[4] ),
        .icmp_ln34_fu_192_p2(icmp_ln34_fu_192_p2),
        .icmp_ln44_fu_225_p2(icmp_ln44_fu_225_p2),
        .localmem_q0(localmem_q0),
        .ram_reg_0(ram_reg),
        .ram_reg_1(ram_reg_0),
        .ram_reg_2(ram_reg_1),
        .ram_reg_3(ram_reg_2),
        .strm_in_V_data_0_sel(strm_in_V_data_0_sel),
        .\tmp_3_1_3_reg_142[0]_i_2 (\tmp_3_1_3_reg_142[0]_i_2 ),
        .\val_assign_reg_107_reg[1] (\val_assign_reg_107_reg[1] ),
        .\val_assign_reg_107_reg[4] (\val_assign_reg_107_reg[4] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_fixed_macc_lbkb_ram
   (localmem_q0,
    d0,
    WEBWE,
    \val_assign_reg_107_reg[1] ,
    \val_assign_reg_107_reg[4] ,
    icmp_ln34_fu_192_p2,
    icmp_ln44_fu_225_p2,
    \i_op_assign_reg_153_reg[4] ,
    ap_clk,
    Q,
    ram_reg_0,
    ap_enable_reg_pp0_iter0,
    ram_reg_1,
    \tmp_3_1_3_reg_142[0]_i_2 ,
    ram_reg_2,
    ram_reg_3,
    strm_in_V_data_0_sel);
  output [31:0]localmem_q0;
  output [31:0]d0;
  output [0:0]WEBWE;
  output \val_assign_reg_107_reg[1] ;
  output \val_assign_reg_107_reg[4] ;
  output icmp_ln34_fu_192_p2;
  output icmp_ln44_fu_225_p2;
  output \i_op_assign_reg_153_reg[4] ;
  input ap_clk;
  input [1:0]Q;
  input ram_reg_0;
  input ap_enable_reg_pp0_iter0;
  input [9:0]ram_reg_1;
  input [9:0]\tmp_3_1_3_reg_142[0]_i_2 ;
  input [31:0]ram_reg_2;
  input [31:0]ram_reg_3;
  input strm_in_V_data_0_sel;

  wire [1:0]Q;
  wire [0:0]WEBWE;
  wire [8:0]address0;
  wire ap_clk;
  wire ap_enable_reg_pp0_iter0;
  wire ce0;
  wire [31:0]d0;
  wire \i_op_assign_reg_153_reg[4] ;
  wire icmp_ln34_fu_192_p2;
  wire icmp_ln44_fu_225_p2;
  wire [31:0]localmem_q0;
  wire ram_reg_0;
  wire [9:0]ram_reg_1;
  wire [31:0]ram_reg_2;
  wire [31:0]ram_reg_3;
  wire strm_in_V_data_0_sel;
  wire [9:0]\tmp_3_1_3_reg_142[0]_i_2 ;
  wire \val_assign_reg_107_reg[1] ;
  wire \val_assign_reg_107_reg[4] ;
  wire [15:14]NLW_ram_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_ram_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d14" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "16384" *) 
  (* RTL_RAM_NAME = "ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "511" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "17" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "511" *) 
  (* ram_ext_slice_begin = "18" *) 
  (* ram_ext_slice_end = "31" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    ram_reg
       (.ADDRARDADDR({1'b0,address0,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,address0,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(ap_clk),
        .CLKBWRCLK(ap_clk),
        .DIADI(d0[15:0]),
        .DIBDI({1'b1,1'b1,d0[31:18]}),
        .DIPADIP(d0[17:16]),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(localmem_q0[15:0]),
        .DOBDO({NLW_ram_reg_DOBDO_UNCONNECTED[15:14],localmem_q0[31:18]}),
        .DOPADOP(localmem_q0[17:16]),
        .DOPBDOP(NLW_ram_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(ce0),
        .ENBWREN(ce0),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({WEBWE,WEBWE}),
        .WEBWE({1'b0,1'b0,WEBWE,WEBWE}));
  LUT6 #(
    .INIT(64'hFAAAF888FAAA8888)) 
    ram_reg_i_1
       (.I0(Q[0]),
        .I1(icmp_ln34_fu_192_p2),
        .I2(ap_enable_reg_pp0_iter0),
        .I3(Q[1]),
        .I4(ram_reg_0),
        .I5(icmp_ln44_fu_225_p2),
        .O(ce0));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_10
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [0]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[0]),
        .O(address0[0]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_11
       (.I0(ram_reg_2[15]),
        .I1(ram_reg_3[15]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[15]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_12
       (.I0(ram_reg_2[14]),
        .I1(ram_reg_3[14]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[14]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_13
       (.I0(ram_reg_2[13]),
        .I1(ram_reg_3[13]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[13]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_14
       (.I0(ram_reg_2[12]),
        .I1(ram_reg_3[12]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[12]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_15
       (.I0(ram_reg_2[11]),
        .I1(ram_reg_3[11]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[11]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_16
       (.I0(ram_reg_2[10]),
        .I1(ram_reg_3[10]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[10]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_17
       (.I0(ram_reg_2[9]),
        .I1(ram_reg_3[9]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[9]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_18
       (.I0(ram_reg_2[8]),
        .I1(ram_reg_3[8]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[8]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_19
       (.I0(ram_reg_2[7]),
        .I1(ram_reg_3[7]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[7]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_2
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [8]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[8]),
        .O(address0[8]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_20
       (.I0(ram_reg_2[6]),
        .I1(ram_reg_3[6]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[6]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_21
       (.I0(ram_reg_2[5]),
        .I1(ram_reg_3[5]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[5]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_22
       (.I0(ram_reg_2[4]),
        .I1(ram_reg_3[4]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[4]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_23
       (.I0(ram_reg_2[3]),
        .I1(ram_reg_3[3]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[3]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_24
       (.I0(ram_reg_2[2]),
        .I1(ram_reg_3[2]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[2]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_25
       (.I0(ram_reg_2[1]),
        .I1(ram_reg_3[1]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[1]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_26
       (.I0(ram_reg_2[0]),
        .I1(ram_reg_3[0]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[0]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_27
       (.I0(ram_reg_2[31]),
        .I1(ram_reg_3[31]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[31]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_28
       (.I0(ram_reg_2[30]),
        .I1(ram_reg_3[30]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[30]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_29
       (.I0(ram_reg_2[29]),
        .I1(ram_reg_3[29]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[29]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_3
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [7]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[7]),
        .O(address0[7]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_30
       (.I0(ram_reg_2[28]),
        .I1(ram_reg_3[28]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[28]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_31
       (.I0(ram_reg_2[27]),
        .I1(ram_reg_3[27]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[27]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_32
       (.I0(ram_reg_2[26]),
        .I1(ram_reg_3[26]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[26]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_33
       (.I0(ram_reg_2[25]),
        .I1(ram_reg_3[25]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[25]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_34
       (.I0(ram_reg_2[24]),
        .I1(ram_reg_3[24]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[24]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_35
       (.I0(ram_reg_2[23]),
        .I1(ram_reg_3[23]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[23]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_36
       (.I0(ram_reg_2[22]),
        .I1(ram_reg_3[22]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[22]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_37
       (.I0(ram_reg_2[21]),
        .I1(ram_reg_3[21]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[21]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_38
       (.I0(ram_reg_2[20]),
        .I1(ram_reg_3[20]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[20]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_39
       (.I0(ram_reg_2[19]),
        .I1(ram_reg_3[19]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[19]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_4
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [6]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[6]),
        .O(address0[6]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_40
       (.I0(ram_reg_2[18]),
        .I1(ram_reg_3[18]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[18]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_41
       (.I0(ram_reg_2[17]),
        .I1(ram_reg_3[17]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[17]));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_i_42
       (.I0(ram_reg_2[16]),
        .I1(ram_reg_3[16]),
        .I2(strm_in_V_data_0_sel),
        .O(d0[16]));
  LUT4 #(
    .INIT(16'h0888)) 
    ram_reg_i_43
       (.I0(Q[0]),
        .I1(ram_reg_0),
        .I2(\val_assign_reg_107_reg[1] ),
        .I3(\val_assign_reg_107_reg[4] ),
        .O(WEBWE));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    ram_reg_i_44
       (.I0(\val_assign_reg_107_reg[4] ),
        .I1(ram_reg_1[2]),
        .I2(ram_reg_1[3]),
        .I3(ram_reg_1[0]),
        .I4(ram_reg_1[1]),
        .O(icmp_ln34_fu_192_p2));
  LUT5 #(
    .INIT(32'h00000002)) 
    ram_reg_i_45
       (.I0(\i_op_assign_reg_153_reg[4] ),
        .I1(\tmp_3_1_3_reg_142[0]_i_2 [2]),
        .I2(\tmp_3_1_3_reg_142[0]_i_2 [3]),
        .I3(\tmp_3_1_3_reg_142[0]_i_2 [0]),
        .I4(\tmp_3_1_3_reg_142[0]_i_2 [1]),
        .O(icmp_ln44_fu_225_p2));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    ram_reg_i_46
       (.I0(ram_reg_1[1]),
        .I1(ram_reg_1[0]),
        .I2(ram_reg_1[3]),
        .I3(ram_reg_1[2]),
        .O(\val_assign_reg_107_reg[1] ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    ram_reg_i_47
       (.I0(ram_reg_1[4]),
        .I1(ram_reg_1[5]),
        .I2(ram_reg_1[6]),
        .I3(ram_reg_1[7]),
        .I4(ram_reg_1[8]),
        .I5(ram_reg_1[9]),
        .O(\val_assign_reg_107_reg[4] ));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_5
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [5]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[5]),
        .O(address0[5]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_6
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [4]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[4]),
        .O(address0[4]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_7
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [3]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[3]),
        .O(address0[3]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_8
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [2]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[2]),
        .O(address0[2]));
  LUT4 #(
    .INIT(16'hBF80)) 
    ram_reg_i_9
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [1]),
        .I1(ap_enable_reg_pp0_iter0),
        .I2(Q[1]),
        .I3(ram_reg_1[1]),
        .O(address0[1]));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \strm_in_V_last_V_0_state[1]_i_5 
       (.I0(\tmp_3_1_3_reg_142[0]_i_2 [4]),
        .I1(\tmp_3_1_3_reg_142[0]_i_2 [5]),
        .I2(\tmp_3_1_3_reg_142[0]_i_2 [6]),
        .I3(\tmp_3_1_3_reg_142[0]_i_2 [7]),
        .I4(\tmp_3_1_3_reg_142[0]_i_2 [8]),
        .I5(\tmp_3_1_3_reg_142[0]_i_2 [9]),
        .O(\i_op_assign_reg_153_reg[4] ));
endmodule

(* CHECK_LICENSE_TYPE = "bd_0_hls_inst_0,axis_fixed_macc,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "HLS" *) 
(* x_core_info = "axis_fixed_macc,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ap_clk,
    ap_rst_n,
    strm_out_TVALID,
    strm_out_TREADY,
    strm_out_TDATA,
    strm_out_TLAST,
    strm_in_TVALID,
    strm_in_TREADY,
    strm_in_TDATA,
    strm_in_TLAST);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_BUSIF strm_out:strm_in, ASSOCIATED_RESET ap_rst_n, FREQ_HZ 100000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst_n RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input ap_rst_n;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_out TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME strm_out, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0" *) output strm_out_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_out TREADY" *) input strm_out_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_out TDATA" *) output [31:0]strm_out_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_out TLAST" *) output [0:0]strm_out_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_in TVALID" *) (* x_interface_parameter = "XIL_INTERFACENAME strm_in, TDATA_NUM_BYTES 4, TUSER_WIDTH 0, LAYERED_METADATA undef, TDEST_WIDTH 0, TID_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0" *) input strm_in_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_in TREADY" *) output strm_in_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_in TDATA" *) input [31:0]strm_in_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 strm_in TLAST" *) input [0:0]strm_in_TLAST;

  wire ap_clk;
  wire ap_rst_n;
  wire [31:0]strm_in_TDATA;
  wire [0:0]strm_in_TLAST;
  wire strm_in_TREADY;
  wire strm_in_TVALID;
  wire [31:0]strm_out_TDATA;
  wire [0:0]strm_out_TLAST;
  wire strm_out_TREADY;
  wire strm_out_TVALID;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axis_fixed_macc U0
       (.ap_clk(ap_clk),
        .ap_rst_n(ap_rst_n),
        .strm_in_TDATA(strm_in_TDATA),
        .strm_in_TLAST(strm_in_TLAST),
        .strm_in_TREADY(strm_in_TREADY),
        .strm_in_TVALID(strm_in_TVALID),
        .strm_out_TDATA(strm_out_TDATA),
        .strm_out_TLAST(strm_out_TLAST),
        .strm_out_TREADY(strm_out_TREADY),
        .strm_out_TVALID(strm_out_TVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
