// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Apr 18 23:26:54 2020
// Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ bd_0_hls_inst_0_stub.v
// Design      : bd_0_hls_inst_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fir,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(y_ap_vld, c_ce0, ap_clk, ap_rst, ap_start, ap_done, 
  ap_idle, ap_ready, y, c_address0, c_q0, x)
/* synthesis syn_black_box black_box_pad_pin="y_ap_vld,c_ce0,ap_clk,ap_rst,ap_start,ap_done,ap_idle,ap_ready,y[31:0],c_address0[3:0],c_q0[31:0],x[31:0]" */;
  output y_ap_vld;
  output c_ce0;
  input ap_clk;
  input ap_rst;
  input ap_start;
  output ap_done;
  output ap_idle;
  output ap_ready;
  output [31:0]y;
  output [3:0]c_address0;
  input [31:0]c_q0;
  input [31:0]x;
endmodule
