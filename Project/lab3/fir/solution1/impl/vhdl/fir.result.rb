$Footmark = "FPGA_Xilinx"
$Description = "by Vivado"


#=== Resource usage ===
$SLICE = "0"
$LUT = "192"
$FF = "135"
$DSP = "3"
$BRAM ="0"
$SRL ="0"
#=== Final timing ===
$TargetCP = "8.000"
$CP = "5.690"
