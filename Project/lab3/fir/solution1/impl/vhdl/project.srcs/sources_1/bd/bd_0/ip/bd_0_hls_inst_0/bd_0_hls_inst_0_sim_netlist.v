// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Apr 18 23:26:55 2020
// Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/Susana/Desktop/Project/lab3/fir/solution1/impl/vhdl/project.srcs/sources_1/bd/bd_0/ip/bd_0_hls_inst_0/bd_0_hls_inst_0_sim_netlist.v
// Design      : bd_0_hls_inst_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "bd_0_hls_inst_0,fir,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "HLS" *) 
(* x_core_info = "fir,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module bd_0_hls_inst_0
   (y_ap_vld,
    c_ce0,
    ap_clk,
    ap_rst,
    ap_start,
    ap_done,
    ap_idle,
    ap_ready,
    y,
    c_address0,
    c_q0,
    x);
  output y_ap_vld;
  output c_ce0;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 ap_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_clk, ASSOCIATED_RESET ap_rst, FREQ_HZ 125000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0" *) input ap_clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 ap_rst RST" *) (* x_interface_parameter = "XIL_INTERFACENAME ap_rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input ap_rst;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start" *) input ap_start;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done" *) output ap_done;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle" *) output ap_idle;
  (* x_interface_info = "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready" *) output ap_ready;
  (* x_interface_info = "xilinx.com:signal:data:1.0 y DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME y, LAYERED_METADATA undef" *) output [31:0]y;
  (* x_interface_info = "xilinx.com:signal:data:1.0 c_address0 DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME c_address0, LAYERED_METADATA undef" *) output [3:0]c_address0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 c_q0 DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME c_q0, LAYERED_METADATA undef" *) input [31:0]c_q0;
  (* x_interface_info = "xilinx.com:signal:data:1.0 x DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME x, LAYERED_METADATA undef" *) input [31:0]x;

  wire ap_clk;
  wire ap_done;
  wire ap_idle;
  wire ap_ready;
  wire ap_rst;
  wire ap_start;
  wire [3:0]c_address0;
  wire c_ce0;
  wire [31:0]c_q0;
  wire [31:0]x;
  wire [31:0]y;
  wire y_ap_vld;

  bd_0_hls_inst_0_fir U0
       (.ap_clk(ap_clk),
        .ap_done(ap_done),
        .ap_idle(ap_idle),
        .ap_ready(ap_ready),
        .ap_rst(ap_rst),
        .ap_start(ap_start),
        .c_address0(c_address0),
        .c_ce0(c_ce0),
        .c_q0(c_q0),
        .x(x),
        .y(y),
        .y_ap_vld(y_ap_vld));
endmodule

(* ORIG_REF_NAME = "fir" *) 
module bd_0_hls_inst_0_fir
   (ap_clk,
    ap_rst,
    ap_start,
    ap_done,
    ap_idle,
    ap_ready,
    y,
    y_ap_vld,
    c_address0,
    c_ce0,
    c_q0,
    x);
  input ap_clk;
  input ap_rst;
  input ap_start;
  output ap_done;
  output ap_idle;
  output ap_ready;
  output [31:0]y;
  output y_ap_vld;
  output [3:0]c_address0;
  output c_ce0;
  input [31:0]c_q0;
  input [31:0]x;

  wire acc_0_reg_91;
  wire \acc_0_reg_91[11]_i_2_n_1 ;
  wire \acc_0_reg_91[11]_i_3_n_1 ;
  wire \acc_0_reg_91[11]_i_4_n_1 ;
  wire \acc_0_reg_91[11]_i_5_n_1 ;
  wire \acc_0_reg_91[15]_i_2_n_1 ;
  wire \acc_0_reg_91[15]_i_3_n_1 ;
  wire \acc_0_reg_91[15]_i_4_n_1 ;
  wire \acc_0_reg_91[15]_i_5_n_1 ;
  wire \acc_0_reg_91[19]_i_2_n_1 ;
  wire \acc_0_reg_91[19]_i_3_n_1 ;
  wire \acc_0_reg_91[19]_i_4_n_1 ;
  wire \acc_0_reg_91[19]_i_5_n_1 ;
  wire \acc_0_reg_91[23]_i_2_n_1 ;
  wire \acc_0_reg_91[23]_i_3_n_1 ;
  wire \acc_0_reg_91[23]_i_4_n_1 ;
  wire \acc_0_reg_91[23]_i_5_n_1 ;
  wire \acc_0_reg_91[27]_i_2_n_1 ;
  wire \acc_0_reg_91[27]_i_3_n_1 ;
  wire \acc_0_reg_91[27]_i_4_n_1 ;
  wire \acc_0_reg_91[27]_i_5_n_1 ;
  wire \acc_0_reg_91[31]_i_3_n_1 ;
  wire \acc_0_reg_91[31]_i_4_n_1 ;
  wire \acc_0_reg_91[31]_i_5_n_1 ;
  wire \acc_0_reg_91[31]_i_6_n_1 ;
  wire \acc_0_reg_91[3]_i_2_n_1 ;
  wire \acc_0_reg_91[3]_i_3_n_1 ;
  wire \acc_0_reg_91[3]_i_4_n_1 ;
  wire \acc_0_reg_91[3]_i_5_n_1 ;
  wire \acc_0_reg_91[7]_i_2_n_1 ;
  wire \acc_0_reg_91[7]_i_3_n_1 ;
  wire \acc_0_reg_91[7]_i_4_n_1 ;
  wire \acc_0_reg_91[7]_i_5_n_1 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_1 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_2 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_3 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_4 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_5 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_6 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_7 ;
  wire \acc_0_reg_91_reg[11]_i_1_n_8 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_1 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_2 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_3 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_4 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_5 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_6 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_7 ;
  wire \acc_0_reg_91_reg[15]_i_1_n_8 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_1 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_2 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_3 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_4 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_5 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_6 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_7 ;
  wire \acc_0_reg_91_reg[19]_i_1_n_8 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_1 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_2 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_3 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_4 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_5 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_6 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_7 ;
  wire \acc_0_reg_91_reg[23]_i_1_n_8 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_1 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_2 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_3 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_4 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_5 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_6 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_7 ;
  wire \acc_0_reg_91_reg[27]_i_1_n_8 ;
  wire \acc_0_reg_91_reg[31]_i_2_n_2 ;
  wire \acc_0_reg_91_reg[31]_i_2_n_3 ;
  wire \acc_0_reg_91_reg[31]_i_2_n_4 ;
  wire \acc_0_reg_91_reg[31]_i_2_n_5 ;
  wire \acc_0_reg_91_reg[31]_i_2_n_6 ;
  wire \acc_0_reg_91_reg[31]_i_2_n_7 ;
  wire \acc_0_reg_91_reg[31]_i_2_n_8 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_1 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_2 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_3 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_4 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_5 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_6 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_7 ;
  wire \acc_0_reg_91_reg[3]_i_1_n_8 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_1 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_2 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_3 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_4 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_5 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_6 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_7 ;
  wire \acc_0_reg_91_reg[7]_i_1_n_8 ;
  wire \ap_CS_fsm[0]_i_2_n_1 ;
  wire \ap_CS_fsm[2]_i_1_n_1 ;
  wire \ap_CS_fsm_reg_n_1_[0] ;
  wire \ap_CS_fsm_reg_n_1_[4] ;
  wire ap_CS_fsm_state2;
  wire ap_CS_fsm_state4;
  wire ap_CS_fsm_state6;
  wire ap_CS_fsm_state7;
  wire [1:0]ap_NS_fsm;
  wire ap_clk;
  wire ap_idle;
  wire ap_rst;
  wire ap_start;
  wire [3:0]c_address0;
  wire c_ce0;
  wire [31:0]c_q0;
  wire [31:16]\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 ;
  wire fir_mul_32s_32s_3bkb_U1_n_17;
  wire fir_mul_32s_32s_3bkb_U1_n_18;
  wire fir_mul_32s_32s_3bkb_U1_n_19;
  wire fir_mul_32s_32s_3bkb_U1_n_20;
  wire fir_mul_32s_32s_3bkb_U1_n_21;
  wire fir_mul_32s_32s_3bkb_U1_n_22;
  wire fir_mul_32s_32s_3bkb_U1_n_23;
  wire fir_mul_32s_32s_3bkb_U1_n_24;
  wire fir_mul_32s_32s_3bkb_U1_n_25;
  wire fir_mul_32s_32s_3bkb_U1_n_26;
  wire fir_mul_32s_32s_3bkb_U1_n_27;
  wire fir_mul_32s_32s_3bkb_U1_n_28;
  wire fir_mul_32s_32s_3bkb_U1_n_29;
  wire fir_mul_32s_32s_3bkb_U1_n_30;
  wire fir_mul_32s_32s_3bkb_U1_n_31;
  wire fir_mul_32s_32s_3bkb_U1_n_32;
  wire \i_0_reg_104_reg_n_1_[0] ;
  wire \i_0_reg_104_reg_n_1_[1] ;
  wire \i_0_reg_104_reg_n_1_[2] ;
  wire \i_0_reg_104_reg_n_1_[3] ;
  wire [4:0]i_reg_208;
  wire \i_reg_208[0]_i_1_n_1 ;
  wire \i_reg_208[1]_i_1_n_1 ;
  wire \i_reg_208[2]_i_1_n_1 ;
  wire \i_reg_208[3]_i_1_n_1 ;
  wire \i_reg_208[4]_i_1_n_1 ;
  wire icmp_ln61_fu_145_p2;
  wire icmp_ln61_reg_189;
  wire \icmp_ln61_reg_189[0]_i_1_n_1 ;
  wire [31:0]mul_ln68_reg_218;
  wire [31:0]p_1_in;
  wire shift_reg_address01;
  wire tmp_fu_137_p30_in;
  wire [31:0]x;
  wire [31:0]y;
  wire y_ap_vld;
  wire [3:3]\NLW_acc_0_reg_91_reg[31]_i_2_CO_UNCONNECTED ;

  assign ap_done = y_ap_vld;
  assign ap_ready = y_ap_vld;
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[11]_i_2 
       (.I0(mul_ln68_reg_218[11]),
        .I1(y[11]),
        .O(\acc_0_reg_91[11]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[11]_i_3 
       (.I0(mul_ln68_reg_218[10]),
        .I1(y[10]),
        .O(\acc_0_reg_91[11]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[11]_i_4 
       (.I0(mul_ln68_reg_218[9]),
        .I1(y[9]),
        .O(\acc_0_reg_91[11]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[11]_i_5 
       (.I0(mul_ln68_reg_218[8]),
        .I1(y[8]),
        .O(\acc_0_reg_91[11]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[15]_i_2 
       (.I0(mul_ln68_reg_218[15]),
        .I1(y[15]),
        .O(\acc_0_reg_91[15]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[15]_i_3 
       (.I0(mul_ln68_reg_218[14]),
        .I1(y[14]),
        .O(\acc_0_reg_91[15]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[15]_i_4 
       (.I0(mul_ln68_reg_218[13]),
        .I1(y[13]),
        .O(\acc_0_reg_91[15]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[15]_i_5 
       (.I0(mul_ln68_reg_218[12]),
        .I1(y[12]),
        .O(\acc_0_reg_91[15]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[19]_i_2 
       (.I0(mul_ln68_reg_218[19]),
        .I1(y[19]),
        .O(\acc_0_reg_91[19]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[19]_i_3 
       (.I0(mul_ln68_reg_218[18]),
        .I1(y[18]),
        .O(\acc_0_reg_91[19]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[19]_i_4 
       (.I0(mul_ln68_reg_218[17]),
        .I1(y[17]),
        .O(\acc_0_reg_91[19]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[19]_i_5 
       (.I0(mul_ln68_reg_218[16]),
        .I1(y[16]),
        .O(\acc_0_reg_91[19]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[23]_i_2 
       (.I0(mul_ln68_reg_218[23]),
        .I1(y[23]),
        .O(\acc_0_reg_91[23]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[23]_i_3 
       (.I0(mul_ln68_reg_218[22]),
        .I1(y[22]),
        .O(\acc_0_reg_91[23]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[23]_i_4 
       (.I0(mul_ln68_reg_218[21]),
        .I1(y[21]),
        .O(\acc_0_reg_91[23]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[23]_i_5 
       (.I0(mul_ln68_reg_218[20]),
        .I1(y[20]),
        .O(\acc_0_reg_91[23]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[27]_i_2 
       (.I0(mul_ln68_reg_218[27]),
        .I1(y[27]),
        .O(\acc_0_reg_91[27]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[27]_i_3 
       (.I0(mul_ln68_reg_218[26]),
        .I1(y[26]),
        .O(\acc_0_reg_91[27]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[27]_i_4 
       (.I0(mul_ln68_reg_218[25]),
        .I1(y[25]),
        .O(\acc_0_reg_91[27]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[27]_i_5 
       (.I0(mul_ln68_reg_218[24]),
        .I1(y[24]),
        .O(\acc_0_reg_91[27]_i_5_n_1 ));
  LUT3 #(
    .INIT(8'h08)) 
    \acc_0_reg_91[31]_i_1 
       (.I0(\ap_CS_fsm_reg_n_1_[0] ),
        .I1(ap_start),
        .I2(ap_CS_fsm_state7),
        .O(acc_0_reg_91));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[31]_i_3 
       (.I0(mul_ln68_reg_218[31]),
        .I1(y[31]),
        .O(\acc_0_reg_91[31]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[31]_i_4 
       (.I0(mul_ln68_reg_218[30]),
        .I1(y[30]),
        .O(\acc_0_reg_91[31]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[31]_i_5 
       (.I0(mul_ln68_reg_218[29]),
        .I1(y[29]),
        .O(\acc_0_reg_91[31]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[31]_i_6 
       (.I0(mul_ln68_reg_218[28]),
        .I1(y[28]),
        .O(\acc_0_reg_91[31]_i_6_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[3]_i_2 
       (.I0(mul_ln68_reg_218[3]),
        .I1(y[3]),
        .O(\acc_0_reg_91[3]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[3]_i_3 
       (.I0(mul_ln68_reg_218[2]),
        .I1(y[2]),
        .O(\acc_0_reg_91[3]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[3]_i_4 
       (.I0(mul_ln68_reg_218[1]),
        .I1(y[1]),
        .O(\acc_0_reg_91[3]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[3]_i_5 
       (.I0(mul_ln68_reg_218[0]),
        .I1(y[0]),
        .O(\acc_0_reg_91[3]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[7]_i_2 
       (.I0(mul_ln68_reg_218[7]),
        .I1(y[7]),
        .O(\acc_0_reg_91[7]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[7]_i_3 
       (.I0(mul_ln68_reg_218[6]),
        .I1(y[6]),
        .O(\acc_0_reg_91[7]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[7]_i_4 
       (.I0(mul_ln68_reg_218[5]),
        .I1(y[5]),
        .O(\acc_0_reg_91[7]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \acc_0_reg_91[7]_i_5 
       (.I0(mul_ln68_reg_218[4]),
        .I1(y[4]),
        .O(\acc_0_reg_91[7]_i_5_n_1 ));
  FDRE \acc_0_reg_91_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[3]_i_1_n_8 ),
        .Q(y[0]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[11]_i_1_n_6 ),
        .Q(y[10]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[11] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[11]_i_1_n_5 ),
        .Q(y[11]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[11]_i_1 
       (.CI(\acc_0_reg_91_reg[7]_i_1_n_1 ),
        .CO({\acc_0_reg_91_reg[11]_i_1_n_1 ,\acc_0_reg_91_reg[11]_i_1_n_2 ,\acc_0_reg_91_reg[11]_i_1_n_3 ,\acc_0_reg_91_reg[11]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln68_reg_218[11:8]),
        .O({\acc_0_reg_91_reg[11]_i_1_n_5 ,\acc_0_reg_91_reg[11]_i_1_n_6 ,\acc_0_reg_91_reg[11]_i_1_n_7 ,\acc_0_reg_91_reg[11]_i_1_n_8 }),
        .S({\acc_0_reg_91[11]_i_2_n_1 ,\acc_0_reg_91[11]_i_3_n_1 ,\acc_0_reg_91[11]_i_4_n_1 ,\acc_0_reg_91[11]_i_5_n_1 }));
  FDRE \acc_0_reg_91_reg[12] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[15]_i_1_n_8 ),
        .Q(y[12]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[13] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[15]_i_1_n_7 ),
        .Q(y[13]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[14] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[15]_i_1_n_6 ),
        .Q(y[14]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[15] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[15]_i_1_n_5 ),
        .Q(y[15]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[15]_i_1 
       (.CI(\acc_0_reg_91_reg[11]_i_1_n_1 ),
        .CO({\acc_0_reg_91_reg[15]_i_1_n_1 ,\acc_0_reg_91_reg[15]_i_1_n_2 ,\acc_0_reg_91_reg[15]_i_1_n_3 ,\acc_0_reg_91_reg[15]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln68_reg_218[15:12]),
        .O({\acc_0_reg_91_reg[15]_i_1_n_5 ,\acc_0_reg_91_reg[15]_i_1_n_6 ,\acc_0_reg_91_reg[15]_i_1_n_7 ,\acc_0_reg_91_reg[15]_i_1_n_8 }),
        .S({\acc_0_reg_91[15]_i_2_n_1 ,\acc_0_reg_91[15]_i_3_n_1 ,\acc_0_reg_91[15]_i_4_n_1 ,\acc_0_reg_91[15]_i_5_n_1 }));
  FDRE \acc_0_reg_91_reg[16] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[19]_i_1_n_8 ),
        .Q(y[16]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[17] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[19]_i_1_n_7 ),
        .Q(y[17]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[18] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[19]_i_1_n_6 ),
        .Q(y[18]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[19] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[19]_i_1_n_5 ),
        .Q(y[19]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[19]_i_1 
       (.CI(\acc_0_reg_91_reg[15]_i_1_n_1 ),
        .CO({\acc_0_reg_91_reg[19]_i_1_n_1 ,\acc_0_reg_91_reg[19]_i_1_n_2 ,\acc_0_reg_91_reg[19]_i_1_n_3 ,\acc_0_reg_91_reg[19]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln68_reg_218[19:16]),
        .O({\acc_0_reg_91_reg[19]_i_1_n_5 ,\acc_0_reg_91_reg[19]_i_1_n_6 ,\acc_0_reg_91_reg[19]_i_1_n_7 ,\acc_0_reg_91_reg[19]_i_1_n_8 }),
        .S({\acc_0_reg_91[19]_i_2_n_1 ,\acc_0_reg_91[19]_i_3_n_1 ,\acc_0_reg_91[19]_i_4_n_1 ,\acc_0_reg_91[19]_i_5_n_1 }));
  FDRE \acc_0_reg_91_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[3]_i_1_n_7 ),
        .Q(y[1]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[20] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[23]_i_1_n_8 ),
        .Q(y[20]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[21] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[23]_i_1_n_7 ),
        .Q(y[21]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[22] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[23]_i_1_n_6 ),
        .Q(y[22]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[23] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[23]_i_1_n_5 ),
        .Q(y[23]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[23]_i_1 
       (.CI(\acc_0_reg_91_reg[19]_i_1_n_1 ),
        .CO({\acc_0_reg_91_reg[23]_i_1_n_1 ,\acc_0_reg_91_reg[23]_i_1_n_2 ,\acc_0_reg_91_reg[23]_i_1_n_3 ,\acc_0_reg_91_reg[23]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln68_reg_218[23:20]),
        .O({\acc_0_reg_91_reg[23]_i_1_n_5 ,\acc_0_reg_91_reg[23]_i_1_n_6 ,\acc_0_reg_91_reg[23]_i_1_n_7 ,\acc_0_reg_91_reg[23]_i_1_n_8 }),
        .S({\acc_0_reg_91[23]_i_2_n_1 ,\acc_0_reg_91[23]_i_3_n_1 ,\acc_0_reg_91[23]_i_4_n_1 ,\acc_0_reg_91[23]_i_5_n_1 }));
  FDRE \acc_0_reg_91_reg[24] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[27]_i_1_n_8 ),
        .Q(y[24]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[25] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[27]_i_1_n_7 ),
        .Q(y[25]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[26] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[27]_i_1_n_6 ),
        .Q(y[26]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[27] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[27]_i_1_n_5 ),
        .Q(y[27]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[27]_i_1 
       (.CI(\acc_0_reg_91_reg[23]_i_1_n_1 ),
        .CO({\acc_0_reg_91_reg[27]_i_1_n_1 ,\acc_0_reg_91_reg[27]_i_1_n_2 ,\acc_0_reg_91_reg[27]_i_1_n_3 ,\acc_0_reg_91_reg[27]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln68_reg_218[27:24]),
        .O({\acc_0_reg_91_reg[27]_i_1_n_5 ,\acc_0_reg_91_reg[27]_i_1_n_6 ,\acc_0_reg_91_reg[27]_i_1_n_7 ,\acc_0_reg_91_reg[27]_i_1_n_8 }),
        .S({\acc_0_reg_91[27]_i_2_n_1 ,\acc_0_reg_91[27]_i_3_n_1 ,\acc_0_reg_91[27]_i_4_n_1 ,\acc_0_reg_91[27]_i_5_n_1 }));
  FDRE \acc_0_reg_91_reg[28] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[31]_i_2_n_8 ),
        .Q(y[28]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[29] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[31]_i_2_n_7 ),
        .Q(y[29]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[3]_i_1_n_6 ),
        .Q(y[2]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[30] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[31]_i_2_n_6 ),
        .Q(y[30]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[31] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[31]_i_2_n_5 ),
        .Q(y[31]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[31]_i_2 
       (.CI(\acc_0_reg_91_reg[27]_i_1_n_1 ),
        .CO({\NLW_acc_0_reg_91_reg[31]_i_2_CO_UNCONNECTED [3],\acc_0_reg_91_reg[31]_i_2_n_2 ,\acc_0_reg_91_reg[31]_i_2_n_3 ,\acc_0_reg_91_reg[31]_i_2_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,mul_ln68_reg_218[30:28]}),
        .O({\acc_0_reg_91_reg[31]_i_2_n_5 ,\acc_0_reg_91_reg[31]_i_2_n_6 ,\acc_0_reg_91_reg[31]_i_2_n_7 ,\acc_0_reg_91_reg[31]_i_2_n_8 }),
        .S({\acc_0_reg_91[31]_i_3_n_1 ,\acc_0_reg_91[31]_i_4_n_1 ,\acc_0_reg_91[31]_i_5_n_1 ,\acc_0_reg_91[31]_i_6_n_1 }));
  FDRE \acc_0_reg_91_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[3]_i_1_n_5 ),
        .Q(y[3]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\acc_0_reg_91_reg[3]_i_1_n_1 ,\acc_0_reg_91_reg[3]_i_1_n_2 ,\acc_0_reg_91_reg[3]_i_1_n_3 ,\acc_0_reg_91_reg[3]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln68_reg_218[3:0]),
        .O({\acc_0_reg_91_reg[3]_i_1_n_5 ,\acc_0_reg_91_reg[3]_i_1_n_6 ,\acc_0_reg_91_reg[3]_i_1_n_7 ,\acc_0_reg_91_reg[3]_i_1_n_8 }),
        .S({\acc_0_reg_91[3]_i_2_n_1 ,\acc_0_reg_91[3]_i_3_n_1 ,\acc_0_reg_91[3]_i_4_n_1 ,\acc_0_reg_91[3]_i_5_n_1 }));
  FDRE \acc_0_reg_91_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[7]_i_1_n_8 ),
        .Q(y[4]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[7]_i_1_n_7 ),
        .Q(y[5]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[7]_i_1_n_6 ),
        .Q(y[6]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[7]_i_1_n_5 ),
        .Q(y[7]),
        .R(acc_0_reg_91));
  CARRY4 \acc_0_reg_91_reg[7]_i_1 
       (.CI(\acc_0_reg_91_reg[3]_i_1_n_1 ),
        .CO({\acc_0_reg_91_reg[7]_i_1_n_1 ,\acc_0_reg_91_reg[7]_i_1_n_2 ,\acc_0_reg_91_reg[7]_i_1_n_3 ,\acc_0_reg_91_reg[7]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI(mul_ln68_reg_218[7:4]),
        .O({\acc_0_reg_91_reg[7]_i_1_n_5 ,\acc_0_reg_91_reg[7]_i_1_n_6 ,\acc_0_reg_91_reg[7]_i_1_n_7 ,\acc_0_reg_91_reg[7]_i_1_n_8 }),
        .S({\acc_0_reg_91[7]_i_2_n_1 ,\acc_0_reg_91[7]_i_3_n_1 ,\acc_0_reg_91[7]_i_4_n_1 ,\acc_0_reg_91[7]_i_5_n_1 }));
  FDRE \acc_0_reg_91_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[11]_i_1_n_8 ),
        .Q(y[8]),
        .R(acc_0_reg_91));
  FDRE \acc_0_reg_91_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(\acc_0_reg_91_reg[11]_i_1_n_7 ),
        .Q(y[9]),
        .R(acc_0_reg_91));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h44444447)) 
    \ap_CS_fsm[0]_i_1 
       (.I0(ap_start),
        .I1(\ap_CS_fsm_reg_n_1_[0] ),
        .I2(c_ce0),
        .I3(ap_CS_fsm_state7),
        .I4(\ap_CS_fsm[0]_i_2_n_1 ),
        .O(ap_NS_fsm[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    \ap_CS_fsm[0]_i_2 
       (.I0(\ap_CS_fsm_reg_n_1_[4] ),
        .I1(ap_CS_fsm_state6),
        .I2(tmp_fu_137_p30_in),
        .I3(ap_CS_fsm_state2),
        .I4(ap_CS_fsm_state4),
        .O(\ap_CS_fsm[0]_i_2_n_1 ));
  LUT3 #(
    .INIT(8'hEA)) 
    \ap_CS_fsm[1]_i_1 
       (.I0(ap_CS_fsm_state7),
        .I1(ap_start),
        .I2(\ap_CS_fsm_reg_n_1_[0] ),
        .O(ap_NS_fsm[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \ap_CS_fsm[2]_i_1 
       (.I0(ap_CS_fsm_state2),
        .I1(tmp_fu_137_p30_in),
        .O(\ap_CS_fsm[2]_i_1_n_1 ));
  (* FSM_ENCODING = "none" *) 
  FDSE #(
    .INIT(1'b1)) 
    \ap_CS_fsm_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[0]),
        .Q(\ap_CS_fsm_reg_n_1_[0] ),
        .S(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[1] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_NS_fsm[1]),
        .Q(ap_CS_fsm_state2),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[2] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm[2]_i_1_n_1 ),
        .Q(c_ce0),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[3] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(c_ce0),
        .Q(ap_CS_fsm_state4),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[4] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_CS_fsm_state4),
        .Q(\ap_CS_fsm_reg_n_1_[4] ),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[5] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\ap_CS_fsm_reg_n_1_[4] ),
        .Q(ap_CS_fsm_state6),
        .R(ap_rst));
  (* FSM_ENCODING = "none" *) 
  FDRE #(
    .INIT(1'b0)) 
    \ap_CS_fsm_reg[6] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(ap_CS_fsm_state6),
        .Q(ap_CS_fsm_state7),
        .R(ap_rst));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    ap_idle_INST_0
       (.I0(\ap_CS_fsm_reg_n_1_[0] ),
        .I1(ap_start),
        .O(ap_idle));
  bd_0_hls_inst_0_fir_mul_32s_32s_3bkb fir_mul_32s_32s_3bkb_U1
       (.D({\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 ,fir_mul_32s_32s_3bkb_U1_n_17,fir_mul_32s_32s_3bkb_U1_n_18,fir_mul_32s_32s_3bkb_U1_n_19,fir_mul_32s_32s_3bkb_U1_n_20,fir_mul_32s_32s_3bkb_U1_n_21,fir_mul_32s_32s_3bkb_U1_n_22,fir_mul_32s_32s_3bkb_U1_n_23,fir_mul_32s_32s_3bkb_U1_n_24,fir_mul_32s_32s_3bkb_U1_n_25,fir_mul_32s_32s_3bkb_U1_n_26,fir_mul_32s_32s_3bkb_U1_n_27,fir_mul_32s_32s_3bkb_U1_n_28,fir_mul_32s_32s_3bkb_U1_n_29,fir_mul_32s_32s_3bkb_U1_n_30,fir_mul_32s_32s_3bkb_U1_n_31,fir_mul_32s_32s_3bkb_U1_n_32}),
        .Q({ap_CS_fsm_state4,c_ce0}),
        .ap_clk(ap_clk),
        .c_q0(c_q0),
        .icmp_ln61_reg_189(icmp_ln61_reg_189),
        .p_1_in(p_1_in),
        .shift_reg_address01(shift_reg_address01));
  FDRE \i_0_reg_104_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(i_reg_208[0]),
        .Q(\i_0_reg_104_reg_n_1_[0] ),
        .R(acc_0_reg_91));
  FDSE \i_0_reg_104_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(i_reg_208[1]),
        .Q(\i_0_reg_104_reg_n_1_[1] ),
        .S(acc_0_reg_91));
  FDRE \i_0_reg_104_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(i_reg_208[2]),
        .Q(\i_0_reg_104_reg_n_1_[2] ),
        .R(acc_0_reg_91));
  FDSE \i_0_reg_104_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(i_reg_208[3]),
        .Q(\i_0_reg_104_reg_n_1_[3] ),
        .S(acc_0_reg_91));
  FDRE \i_0_reg_104_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state7),
        .D(i_reg_208[4]),
        .Q(tmp_fu_137_p30_in),
        .R(acc_0_reg_91));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \i_reg_208[0]_i_1 
       (.I0(\i_0_reg_104_reg_n_1_[0] ),
        .O(\i_reg_208[0]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \i_reg_208[1]_i_1 
       (.I0(\i_0_reg_104_reg_n_1_[1] ),
        .I1(\i_0_reg_104_reg_n_1_[0] ),
        .O(\i_reg_208[1]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \i_reg_208[2]_i_1 
       (.I0(\i_0_reg_104_reg_n_1_[2] ),
        .I1(\i_0_reg_104_reg_n_1_[0] ),
        .I2(\i_0_reg_104_reg_n_1_[1] ),
        .O(\i_reg_208[2]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \i_reg_208[3]_i_1 
       (.I0(\i_0_reg_104_reg_n_1_[3] ),
        .I1(\i_0_reg_104_reg_n_1_[1] ),
        .I2(\i_0_reg_104_reg_n_1_[0] ),
        .I3(\i_0_reg_104_reg_n_1_[2] ),
        .O(\i_reg_208[3]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \i_reg_208[4]_i_1 
       (.I0(\i_0_reg_104_reg_n_1_[3] ),
        .I1(\i_0_reg_104_reg_n_1_[1] ),
        .I2(\i_0_reg_104_reg_n_1_[0] ),
        .I3(\i_0_reg_104_reg_n_1_[2] ),
        .I4(tmp_fu_137_p30_in),
        .O(\i_reg_208[4]_i_1_n_1 ));
  FDRE \i_reg_208_reg[0] 
       (.C(ap_clk),
        .CE(c_ce0),
        .D(\i_reg_208[0]_i_1_n_1 ),
        .Q(i_reg_208[0]),
        .R(1'b0));
  FDRE \i_reg_208_reg[1] 
       (.C(ap_clk),
        .CE(c_ce0),
        .D(\i_reg_208[1]_i_1_n_1 ),
        .Q(i_reg_208[1]),
        .R(1'b0));
  FDRE \i_reg_208_reg[2] 
       (.C(ap_clk),
        .CE(c_ce0),
        .D(\i_reg_208[2]_i_1_n_1 ),
        .Q(i_reg_208[2]),
        .R(1'b0));
  FDRE \i_reg_208_reg[3] 
       (.C(ap_clk),
        .CE(c_ce0),
        .D(\i_reg_208[3]_i_1_n_1 ),
        .Q(i_reg_208[3]),
        .R(1'b0));
  FDRE \i_reg_208_reg[4] 
       (.C(ap_clk),
        .CE(c_ce0),
        .D(\i_reg_208[4]_i_1_n_1 ),
        .Q(i_reg_208[4]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \icmp_ln61_reg_189[0]_i_1 
       (.I0(icmp_ln61_fu_145_p2),
        .I1(ap_CS_fsm_state2),
        .I2(tmp_fu_137_p30_in),
        .I3(icmp_ln61_reg_189),
        .O(\icmp_ln61_reg_189[0]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \icmp_ln61_reg_189[0]_i_2 
       (.I0(\i_0_reg_104_reg_n_1_[3] ),
        .I1(tmp_fu_137_p30_in),
        .I2(\i_0_reg_104_reg_n_1_[0] ),
        .I3(\i_0_reg_104_reg_n_1_[1] ),
        .I4(\i_0_reg_104_reg_n_1_[2] ),
        .O(icmp_ln61_fu_145_p2));
  FDRE \icmp_ln61_reg_189_reg[0] 
       (.C(ap_clk),
        .CE(1'b1),
        .D(\icmp_ln61_reg_189[0]_i_1_n_1 ),
        .Q(icmp_ln61_reg_189),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_32),
        .Q(mul_ln68_reg_218[0]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[10] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_22),
        .Q(mul_ln68_reg_218[10]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[11] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_21),
        .Q(mul_ln68_reg_218[11]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[12] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_20),
        .Q(mul_ln68_reg_218[12]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[13] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_19),
        .Q(mul_ln68_reg_218[13]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[14] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_18),
        .Q(mul_ln68_reg_218[14]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[15] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_17),
        .Q(mul_ln68_reg_218[15]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[16] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [16]),
        .Q(mul_ln68_reg_218[16]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[17] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [17]),
        .Q(mul_ln68_reg_218[17]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[18] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [18]),
        .Q(mul_ln68_reg_218[18]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[19] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [19]),
        .Q(mul_ln68_reg_218[19]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_31),
        .Q(mul_ln68_reg_218[1]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[20] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [20]),
        .Q(mul_ln68_reg_218[20]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[21] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [21]),
        .Q(mul_ln68_reg_218[21]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[22] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [22]),
        .Q(mul_ln68_reg_218[22]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[23] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [23]),
        .Q(mul_ln68_reg_218[23]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[24] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [24]),
        .Q(mul_ln68_reg_218[24]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[25] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [25]),
        .Q(mul_ln68_reg_218[25]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[26] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [26]),
        .Q(mul_ln68_reg_218[26]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[27] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [27]),
        .Q(mul_ln68_reg_218[27]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[28] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [28]),
        .Q(mul_ln68_reg_218[28]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[29] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [29]),
        .Q(mul_ln68_reg_218[29]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_30),
        .Q(mul_ln68_reg_218[2]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[30] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [30]),
        .Q(mul_ln68_reg_218[30]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[31] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(\fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1 [31]),
        .Q(mul_ln68_reg_218[31]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_29),
        .Q(mul_ln68_reg_218[3]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[4] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_28),
        .Q(mul_ln68_reg_218[4]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[5] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_27),
        .Q(mul_ln68_reg_218[5]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[6] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_26),
        .Q(mul_ln68_reg_218[6]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[7] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_25),
        .Q(mul_ln68_reg_218[7]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[8] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_24),
        .Q(mul_ln68_reg_218[8]),
        .R(1'b0));
  FDRE \mul_ln68_reg_218_reg[9] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state6),
        .D(fir_mul_32s_32s_3bkb_U1_n_23),
        .Q(mul_ln68_reg_218[9]),
        .R(1'b0));
  FDRE \sext_ln60_reg_180_reg[0] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(\i_0_reg_104_reg_n_1_[0] ),
        .Q(c_address0[0]),
        .R(1'b0));
  FDRE \sext_ln60_reg_180_reg[1] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(\i_0_reg_104_reg_n_1_[1] ),
        .Q(c_address0[1]),
        .R(1'b0));
  FDRE \sext_ln60_reg_180_reg[2] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(\i_0_reg_104_reg_n_1_[2] ),
        .Q(c_address0[2]),
        .R(1'b0));
  FDRE \sext_ln60_reg_180_reg[3] 
       (.C(ap_clk),
        .CE(ap_CS_fsm_state2),
        .D(\i_0_reg_104_reg_n_1_[3] ),
        .Q(c_address0[3]),
        .R(1'b0));
  bd_0_hls_inst_0_fir_shift_reg shift_reg_U
       (.Q({c_ce0,ap_CS_fsm_state2}),
        .ap_clk(ap_clk),
        .c_address0(c_address0),
        .icmp_ln61_reg_189(icmp_ln61_reg_189),
        .p_1_in(p_1_in),
        .\q0_reg[0] ({tmp_fu_137_p30_in,\i_0_reg_104_reg_n_1_[3] ,\i_0_reg_104_reg_n_1_[2] ,\i_0_reg_104_reg_n_1_[1] ,\i_0_reg_104_reg_n_1_[0] }),
        .shift_reg_address01(shift_reg_address01),
        .x(x));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h8)) 
    y_ap_vld_INST_0
       (.I0(tmp_fu_137_p30_in),
        .I1(ap_CS_fsm_state2),
        .O(y_ap_vld));
endmodule

(* ORIG_REF_NAME = "fir_mul_32s_32s_3bkb" *) 
module bd_0_hls_inst_0_fir_mul_32s_32s_3bkb
   (D,
    Q,
    ap_clk,
    c_q0,
    p_1_in,
    icmp_ln61_reg_189,
    shift_reg_address01);
  output [31:0]D;
  input [1:0]Q;
  input ap_clk;
  input [31:0]c_q0;
  input [31:0]p_1_in;
  input icmp_ln61_reg_189;
  input shift_reg_address01;

  wire [31:0]D;
  wire [1:0]Q;
  wire ap_clk;
  wire [31:0]c_q0;
  wire icmp_ln61_reg_189;
  wire [31:0]p_1_in;
  wire shift_reg_address01;

  bd_0_hls_inst_0_fir_mul_32s_32s_3bkb_MulnS_0 fir_mul_32s_32s_3bkb_MulnS_0_U
       (.D(D),
        .Q(Q),
        .ap_clk(ap_clk),
        .c_q0(c_q0),
        .icmp_ln61_reg_189(icmp_ln61_reg_189),
        .p_1_in(p_1_in),
        .shift_reg_address01(shift_reg_address01));
endmodule

(* ORIG_REF_NAME = "fir_mul_32s_32s_3bkb_MulnS_0" *) 
module bd_0_hls_inst_0_fir_mul_32s_32s_3bkb_MulnS_0
   (D,
    Q,
    ap_clk,
    c_q0,
    p_1_in,
    icmp_ln61_reg_189,
    shift_reg_address01);
  output [31:0]D;
  input [1:0]Q;
  input ap_clk;
  input [31:0]c_q0;
  input [31:0]p_1_in;
  input icmp_ln61_reg_189;
  input shift_reg_address01;

  wire [31:0]D;
  wire [1:0]Q;
  wire ap_clk;
  wire [31:0]c_q0;
  wire icmp_ln61_reg_189;
  wire \mul_ln68_reg_218[19]_i_2_n_1 ;
  wire \mul_ln68_reg_218[19]_i_3_n_1 ;
  wire \mul_ln68_reg_218[19]_i_4_n_1 ;
  wire \mul_ln68_reg_218[23]_i_2_n_1 ;
  wire \mul_ln68_reg_218[23]_i_3_n_1 ;
  wire \mul_ln68_reg_218[23]_i_4_n_1 ;
  wire \mul_ln68_reg_218[23]_i_5_n_1 ;
  wire \mul_ln68_reg_218[27]_i_2_n_1 ;
  wire \mul_ln68_reg_218[27]_i_3_n_1 ;
  wire \mul_ln68_reg_218[27]_i_4_n_1 ;
  wire \mul_ln68_reg_218[27]_i_5_n_1 ;
  wire \mul_ln68_reg_218[31]_i_2_n_1 ;
  wire \mul_ln68_reg_218[31]_i_3_n_1 ;
  wire \mul_ln68_reg_218[31]_i_4_n_1 ;
  wire \mul_ln68_reg_218[31]_i_5_n_1 ;
  wire \mul_ln68_reg_218_reg[19]_i_1_n_1 ;
  wire \mul_ln68_reg_218_reg[19]_i_1_n_2 ;
  wire \mul_ln68_reg_218_reg[19]_i_1_n_3 ;
  wire \mul_ln68_reg_218_reg[19]_i_1_n_4 ;
  wire \mul_ln68_reg_218_reg[23]_i_1_n_1 ;
  wire \mul_ln68_reg_218_reg[23]_i_1_n_2 ;
  wire \mul_ln68_reg_218_reg[23]_i_1_n_3 ;
  wire \mul_ln68_reg_218_reg[23]_i_1_n_4 ;
  wire \mul_ln68_reg_218_reg[27]_i_1_n_1 ;
  wire \mul_ln68_reg_218_reg[27]_i_1_n_2 ;
  wire \mul_ln68_reg_218_reg[27]_i_1_n_3 ;
  wire \mul_ln68_reg_218_reg[27]_i_1_n_4 ;
  wire \mul_ln68_reg_218_reg[31]_i_1_n_2 ;
  wire \mul_ln68_reg_218_reg[31]_i_1_n_3 ;
  wire \mul_ln68_reg_218_reg[31]_i_1_n_4 ;
  wire [31:0]p_1_in;
  wire \p_tmp_reg[16]__0_n_1 ;
  wire p_tmp_reg_n_100;
  wire p_tmp_reg_n_101;
  wire p_tmp_reg_n_102;
  wire p_tmp_reg_n_103;
  wire p_tmp_reg_n_104;
  wire p_tmp_reg_n_105;
  wire p_tmp_reg_n_106;
  wire p_tmp_reg_n_59;
  wire p_tmp_reg_n_60;
  wire p_tmp_reg_n_61;
  wire p_tmp_reg_n_62;
  wire p_tmp_reg_n_63;
  wire p_tmp_reg_n_64;
  wire p_tmp_reg_n_65;
  wire p_tmp_reg_n_66;
  wire p_tmp_reg_n_67;
  wire p_tmp_reg_n_68;
  wire p_tmp_reg_n_69;
  wire p_tmp_reg_n_70;
  wire p_tmp_reg_n_71;
  wire p_tmp_reg_n_72;
  wire p_tmp_reg_n_73;
  wire p_tmp_reg_n_74;
  wire p_tmp_reg_n_75;
  wire p_tmp_reg_n_76;
  wire p_tmp_reg_n_77;
  wire p_tmp_reg_n_78;
  wire p_tmp_reg_n_79;
  wire p_tmp_reg_n_80;
  wire p_tmp_reg_n_81;
  wire p_tmp_reg_n_82;
  wire p_tmp_reg_n_83;
  wire p_tmp_reg_n_84;
  wire p_tmp_reg_n_85;
  wire p_tmp_reg_n_86;
  wire p_tmp_reg_n_87;
  wire p_tmp_reg_n_88;
  wire p_tmp_reg_n_89;
  wire p_tmp_reg_n_90;
  wire p_tmp_reg_n_91;
  wire p_tmp_reg_n_92;
  wire p_tmp_reg_n_93;
  wire p_tmp_reg_n_94;
  wire p_tmp_reg_n_95;
  wire p_tmp_reg_n_96;
  wire p_tmp_reg_n_97;
  wire p_tmp_reg_n_98;
  wire p_tmp_reg_n_99;
  wire shift_reg_address01;
  wire tmp_product__0_n_100;
  wire tmp_product__0_n_101;
  wire tmp_product__0_n_102;
  wire tmp_product__0_n_103;
  wire tmp_product__0_n_104;
  wire tmp_product__0_n_105;
  wire tmp_product__0_n_106;
  wire tmp_product__0_n_107;
  wire tmp_product__0_n_108;
  wire tmp_product__0_n_109;
  wire tmp_product__0_n_110;
  wire tmp_product__0_n_111;
  wire tmp_product__0_n_112;
  wire tmp_product__0_n_113;
  wire tmp_product__0_n_114;
  wire tmp_product__0_n_115;
  wire tmp_product__0_n_116;
  wire tmp_product__0_n_117;
  wire tmp_product__0_n_118;
  wire tmp_product__0_n_119;
  wire tmp_product__0_n_120;
  wire tmp_product__0_n_121;
  wire tmp_product__0_n_122;
  wire tmp_product__0_n_123;
  wire tmp_product__0_n_124;
  wire tmp_product__0_n_125;
  wire tmp_product__0_n_126;
  wire tmp_product__0_n_127;
  wire tmp_product__0_n_128;
  wire tmp_product__0_n_129;
  wire tmp_product__0_n_130;
  wire tmp_product__0_n_131;
  wire tmp_product__0_n_132;
  wire tmp_product__0_n_133;
  wire tmp_product__0_n_134;
  wire tmp_product__0_n_135;
  wire tmp_product__0_n_136;
  wire tmp_product__0_n_137;
  wire tmp_product__0_n_138;
  wire tmp_product__0_n_139;
  wire tmp_product__0_n_140;
  wire tmp_product__0_n_141;
  wire tmp_product__0_n_142;
  wire tmp_product__0_n_143;
  wire tmp_product__0_n_144;
  wire tmp_product__0_n_145;
  wire tmp_product__0_n_146;
  wire tmp_product__0_n_147;
  wire tmp_product__0_n_148;
  wire tmp_product__0_n_149;
  wire tmp_product__0_n_150;
  wire tmp_product__0_n_151;
  wire tmp_product__0_n_152;
  wire tmp_product__0_n_153;
  wire tmp_product__0_n_154;
  wire tmp_product__0_n_25;
  wire tmp_product__0_n_26;
  wire tmp_product__0_n_27;
  wire tmp_product__0_n_28;
  wire tmp_product__0_n_29;
  wire tmp_product__0_n_30;
  wire tmp_product__0_n_31;
  wire tmp_product__0_n_32;
  wire tmp_product__0_n_33;
  wire tmp_product__0_n_34;
  wire tmp_product__0_n_35;
  wire tmp_product__0_n_36;
  wire tmp_product__0_n_37;
  wire tmp_product__0_n_38;
  wire tmp_product__0_n_39;
  wire tmp_product__0_n_40;
  wire tmp_product__0_n_41;
  wire tmp_product__0_n_42;
  wire tmp_product__0_n_43;
  wire tmp_product__0_n_44;
  wire tmp_product__0_n_45;
  wire tmp_product__0_n_46;
  wire tmp_product__0_n_47;
  wire tmp_product__0_n_48;
  wire tmp_product__0_n_49;
  wire tmp_product__0_n_50;
  wire tmp_product__0_n_51;
  wire tmp_product__0_n_52;
  wire tmp_product__0_n_53;
  wire tmp_product__0_n_54;
  wire tmp_product__0_n_59;
  wire tmp_product__0_n_60;
  wire tmp_product__0_n_61;
  wire tmp_product__0_n_62;
  wire tmp_product__0_n_63;
  wire tmp_product__0_n_64;
  wire tmp_product__0_n_65;
  wire tmp_product__0_n_66;
  wire tmp_product__0_n_67;
  wire tmp_product__0_n_68;
  wire tmp_product__0_n_69;
  wire tmp_product__0_n_70;
  wire tmp_product__0_n_71;
  wire tmp_product__0_n_72;
  wire tmp_product__0_n_73;
  wire tmp_product__0_n_74;
  wire tmp_product__0_n_75;
  wire tmp_product__0_n_76;
  wire tmp_product__0_n_77;
  wire tmp_product__0_n_78;
  wire tmp_product__0_n_79;
  wire tmp_product__0_n_80;
  wire tmp_product__0_n_81;
  wire tmp_product__0_n_82;
  wire tmp_product__0_n_83;
  wire tmp_product__0_n_84;
  wire tmp_product__0_n_85;
  wire tmp_product__0_n_86;
  wire tmp_product__0_n_87;
  wire tmp_product__0_n_88;
  wire tmp_product__0_n_89;
  wire tmp_product__0_n_90;
  wire tmp_product__0_n_91;
  wire tmp_product__0_n_92;
  wire tmp_product__0_n_93;
  wire tmp_product__0_n_94;
  wire tmp_product__0_n_95;
  wire tmp_product__0_n_96;
  wire tmp_product__0_n_97;
  wire tmp_product__0_n_98;
  wire tmp_product__0_n_99;
  wire tmp_product_i_1_n_1;
  wire tmp_product_n_100;
  wire tmp_product_n_101;
  wire tmp_product_n_102;
  wire tmp_product_n_103;
  wire tmp_product_n_104;
  wire tmp_product_n_105;
  wire tmp_product_n_106;
  wire tmp_product_n_107;
  wire tmp_product_n_108;
  wire tmp_product_n_109;
  wire tmp_product_n_110;
  wire tmp_product_n_111;
  wire tmp_product_n_112;
  wire tmp_product_n_113;
  wire tmp_product_n_114;
  wire tmp_product_n_115;
  wire tmp_product_n_116;
  wire tmp_product_n_117;
  wire tmp_product_n_118;
  wire tmp_product_n_119;
  wire tmp_product_n_120;
  wire tmp_product_n_121;
  wire tmp_product_n_122;
  wire tmp_product_n_123;
  wire tmp_product_n_124;
  wire tmp_product_n_125;
  wire tmp_product_n_126;
  wire tmp_product_n_127;
  wire tmp_product_n_128;
  wire tmp_product_n_129;
  wire tmp_product_n_130;
  wire tmp_product_n_131;
  wire tmp_product_n_132;
  wire tmp_product_n_133;
  wire tmp_product_n_134;
  wire tmp_product_n_135;
  wire tmp_product_n_136;
  wire tmp_product_n_137;
  wire tmp_product_n_138;
  wire tmp_product_n_139;
  wire tmp_product_n_140;
  wire tmp_product_n_141;
  wire tmp_product_n_142;
  wire tmp_product_n_143;
  wire tmp_product_n_144;
  wire tmp_product_n_145;
  wire tmp_product_n_146;
  wire tmp_product_n_147;
  wire tmp_product_n_148;
  wire tmp_product_n_149;
  wire tmp_product_n_150;
  wire tmp_product_n_151;
  wire tmp_product_n_152;
  wire tmp_product_n_153;
  wire tmp_product_n_154;
  wire tmp_product_n_59;
  wire tmp_product_n_60;
  wire tmp_product_n_61;
  wire tmp_product_n_62;
  wire tmp_product_n_63;
  wire tmp_product_n_64;
  wire tmp_product_n_65;
  wire tmp_product_n_66;
  wire tmp_product_n_67;
  wire tmp_product_n_68;
  wire tmp_product_n_69;
  wire tmp_product_n_70;
  wire tmp_product_n_71;
  wire tmp_product_n_72;
  wire tmp_product_n_73;
  wire tmp_product_n_74;
  wire tmp_product_n_75;
  wire tmp_product_n_76;
  wire tmp_product_n_77;
  wire tmp_product_n_78;
  wire tmp_product_n_79;
  wire tmp_product_n_80;
  wire tmp_product_n_81;
  wire tmp_product_n_82;
  wire tmp_product_n_83;
  wire tmp_product_n_84;
  wire tmp_product_n_85;
  wire tmp_product_n_86;
  wire tmp_product_n_87;
  wire tmp_product_n_88;
  wire tmp_product_n_89;
  wire tmp_product_n_90;
  wire tmp_product_n_91;
  wire tmp_product_n_92;
  wire tmp_product_n_93;
  wire tmp_product_n_94;
  wire tmp_product_n_95;
  wire tmp_product_n_96;
  wire tmp_product_n_97;
  wire tmp_product_n_98;
  wire tmp_product_n_99;
  wire [3:3]\NLW_mul_ln68_reg_218_reg[31]_i_1_CO_UNCONNECTED ;
  wire NLW_p_tmp_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_p_tmp_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_p_tmp_reg_OVERFLOW_UNCONNECTED;
  wire NLW_p_tmp_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_p_tmp_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_p_tmp_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_p_tmp_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_p_tmp_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_p_tmp_reg_CARRYOUT_UNCONNECTED;
  wire [47:0]NLW_p_tmp_reg_PCOUT_UNCONNECTED;
  wire NLW_tmp_product_CARRYCASCOUT_UNCONNECTED;
  wire NLW_tmp_product_MULTSIGNOUT_UNCONNECTED;
  wire NLW_tmp_product_OVERFLOW_UNCONNECTED;
  wire NLW_tmp_product_PATTERNBDETECT_UNCONNECTED;
  wire NLW_tmp_product_PATTERNDETECT_UNCONNECTED;
  wire NLW_tmp_product_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_tmp_product_ACOUT_UNCONNECTED;
  wire [17:0]NLW_tmp_product_BCOUT_UNCONNECTED;
  wire [3:0]NLW_tmp_product_CARRYOUT_UNCONNECTED;
  wire NLW_tmp_product__0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_tmp_product__0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_tmp_product__0_OVERFLOW_UNCONNECTED;
  wire NLW_tmp_product__0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_tmp_product__0_PATTERNDETECT_UNCONNECTED;
  wire NLW_tmp_product__0_UNDERFLOW_UNCONNECTED;
  wire [17:0]NLW_tmp_product__0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_tmp_product__0_CARRYOUT_UNCONNECTED;

  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[19]_i_2 
       (.I0(p_tmp_reg_n_104),
        .I1(tmp_product_n_104),
        .O(\mul_ln68_reg_218[19]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[19]_i_3 
       (.I0(p_tmp_reg_n_105),
        .I1(tmp_product_n_105),
        .O(\mul_ln68_reg_218[19]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[19]_i_4 
       (.I0(p_tmp_reg_n_106),
        .I1(tmp_product_n_106),
        .O(\mul_ln68_reg_218[19]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[23]_i_2 
       (.I0(p_tmp_reg_n_100),
        .I1(tmp_product_n_100),
        .O(\mul_ln68_reg_218[23]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[23]_i_3 
       (.I0(p_tmp_reg_n_101),
        .I1(tmp_product_n_101),
        .O(\mul_ln68_reg_218[23]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[23]_i_4 
       (.I0(p_tmp_reg_n_102),
        .I1(tmp_product_n_102),
        .O(\mul_ln68_reg_218[23]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[23]_i_5 
       (.I0(p_tmp_reg_n_103),
        .I1(tmp_product_n_103),
        .O(\mul_ln68_reg_218[23]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[27]_i_2 
       (.I0(p_tmp_reg_n_96),
        .I1(tmp_product_n_96),
        .O(\mul_ln68_reg_218[27]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[27]_i_3 
       (.I0(p_tmp_reg_n_97),
        .I1(tmp_product_n_97),
        .O(\mul_ln68_reg_218[27]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[27]_i_4 
       (.I0(p_tmp_reg_n_98),
        .I1(tmp_product_n_98),
        .O(\mul_ln68_reg_218[27]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[27]_i_5 
       (.I0(p_tmp_reg_n_99),
        .I1(tmp_product_n_99),
        .O(\mul_ln68_reg_218[27]_i_5_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[31]_i_2 
       (.I0(p_tmp_reg_n_92),
        .I1(tmp_product_n_92),
        .O(\mul_ln68_reg_218[31]_i_2_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[31]_i_3 
       (.I0(p_tmp_reg_n_93),
        .I1(tmp_product_n_93),
        .O(\mul_ln68_reg_218[31]_i_3_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[31]_i_4 
       (.I0(p_tmp_reg_n_94),
        .I1(tmp_product_n_94),
        .O(\mul_ln68_reg_218[31]_i_4_n_1 ));
  LUT2 #(
    .INIT(4'h6)) 
    \mul_ln68_reg_218[31]_i_5 
       (.I0(p_tmp_reg_n_95),
        .I1(tmp_product_n_95),
        .O(\mul_ln68_reg_218[31]_i_5_n_1 ));
  CARRY4 \mul_ln68_reg_218_reg[19]_i_1 
       (.CI(1'b0),
        .CO({\mul_ln68_reg_218_reg[19]_i_1_n_1 ,\mul_ln68_reg_218_reg[19]_i_1_n_2 ,\mul_ln68_reg_218_reg[19]_i_1_n_3 ,\mul_ln68_reg_218_reg[19]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({p_tmp_reg_n_104,p_tmp_reg_n_105,p_tmp_reg_n_106,1'b0}),
        .O(D[19:16]),
        .S({\mul_ln68_reg_218[19]_i_2_n_1 ,\mul_ln68_reg_218[19]_i_3_n_1 ,\mul_ln68_reg_218[19]_i_4_n_1 ,\p_tmp_reg[16]__0_n_1 }));
  CARRY4 \mul_ln68_reg_218_reg[23]_i_1 
       (.CI(\mul_ln68_reg_218_reg[19]_i_1_n_1 ),
        .CO({\mul_ln68_reg_218_reg[23]_i_1_n_1 ,\mul_ln68_reg_218_reg[23]_i_1_n_2 ,\mul_ln68_reg_218_reg[23]_i_1_n_3 ,\mul_ln68_reg_218_reg[23]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({p_tmp_reg_n_100,p_tmp_reg_n_101,p_tmp_reg_n_102,p_tmp_reg_n_103}),
        .O(D[23:20]),
        .S({\mul_ln68_reg_218[23]_i_2_n_1 ,\mul_ln68_reg_218[23]_i_3_n_1 ,\mul_ln68_reg_218[23]_i_4_n_1 ,\mul_ln68_reg_218[23]_i_5_n_1 }));
  CARRY4 \mul_ln68_reg_218_reg[27]_i_1 
       (.CI(\mul_ln68_reg_218_reg[23]_i_1_n_1 ),
        .CO({\mul_ln68_reg_218_reg[27]_i_1_n_1 ,\mul_ln68_reg_218_reg[27]_i_1_n_2 ,\mul_ln68_reg_218_reg[27]_i_1_n_3 ,\mul_ln68_reg_218_reg[27]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({p_tmp_reg_n_96,p_tmp_reg_n_97,p_tmp_reg_n_98,p_tmp_reg_n_99}),
        .O(D[27:24]),
        .S({\mul_ln68_reg_218[27]_i_2_n_1 ,\mul_ln68_reg_218[27]_i_3_n_1 ,\mul_ln68_reg_218[27]_i_4_n_1 ,\mul_ln68_reg_218[27]_i_5_n_1 }));
  CARRY4 \mul_ln68_reg_218_reg[31]_i_1 
       (.CI(\mul_ln68_reg_218_reg[27]_i_1_n_1 ),
        .CO({\NLW_mul_ln68_reg_218_reg[31]_i_1_CO_UNCONNECTED [3],\mul_ln68_reg_218_reg[31]_i_1_n_2 ,\mul_ln68_reg_218_reg[31]_i_1_n_3 ,\mul_ln68_reg_218_reg[31]_i_1_n_4 }),
        .CYINIT(1'b0),
        .DI({1'b0,p_tmp_reg_n_93,p_tmp_reg_n_94,p_tmp_reg_n_95}),
        .O(D[31:28]),
        .S({\mul_ln68_reg_218[31]_i_2_n_1 ,\mul_ln68_reg_218[31]_i_3_n_1 ,\mul_ln68_reg_218[31]_i_4_n_1 ,\mul_ln68_reg_218[31]_i_5_n_1 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x15 4}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("CASCADE"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    p_tmp_reg
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACIN({tmp_product__0_n_25,tmp_product__0_n_26,tmp_product__0_n_27,tmp_product__0_n_28,tmp_product__0_n_29,tmp_product__0_n_30,tmp_product__0_n_31,tmp_product__0_n_32,tmp_product__0_n_33,tmp_product__0_n_34,tmp_product__0_n_35,tmp_product__0_n_36,tmp_product__0_n_37,tmp_product__0_n_38,tmp_product__0_n_39,tmp_product__0_n_40,tmp_product__0_n_41,tmp_product__0_n_42,tmp_product__0_n_43,tmp_product__0_n_44,tmp_product__0_n_45,tmp_product__0_n_46,tmp_product__0_n_47,tmp_product__0_n_48,tmp_product__0_n_49,tmp_product__0_n_50,tmp_product__0_n_51,tmp_product__0_n_52,tmp_product__0_n_53,tmp_product__0_n_54}),
        .ACOUT(NLW_p_tmp_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({p_1_in[31],p_1_in[31],p_1_in[31],p_1_in[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_p_tmp_reg_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_p_tmp_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_p_tmp_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(tmp_product_i_1_n_1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b1),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_p_tmp_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_p_tmp_reg_OVERFLOW_UNCONNECTED),
        .P({p_tmp_reg_n_59,p_tmp_reg_n_60,p_tmp_reg_n_61,p_tmp_reg_n_62,p_tmp_reg_n_63,p_tmp_reg_n_64,p_tmp_reg_n_65,p_tmp_reg_n_66,p_tmp_reg_n_67,p_tmp_reg_n_68,p_tmp_reg_n_69,p_tmp_reg_n_70,p_tmp_reg_n_71,p_tmp_reg_n_72,p_tmp_reg_n_73,p_tmp_reg_n_74,p_tmp_reg_n_75,p_tmp_reg_n_76,p_tmp_reg_n_77,p_tmp_reg_n_78,p_tmp_reg_n_79,p_tmp_reg_n_80,p_tmp_reg_n_81,p_tmp_reg_n_82,p_tmp_reg_n_83,p_tmp_reg_n_84,p_tmp_reg_n_85,p_tmp_reg_n_86,p_tmp_reg_n_87,p_tmp_reg_n_88,p_tmp_reg_n_89,p_tmp_reg_n_90,p_tmp_reg_n_91,p_tmp_reg_n_92,p_tmp_reg_n_93,p_tmp_reg_n_94,p_tmp_reg_n_95,p_tmp_reg_n_96,p_tmp_reg_n_97,p_tmp_reg_n_98,p_tmp_reg_n_99,p_tmp_reg_n_100,p_tmp_reg_n_101,p_tmp_reg_n_102,p_tmp_reg_n_103,p_tmp_reg_n_104,p_tmp_reg_n_105,p_tmp_reg_n_106}),
        .PATTERNBDETECT(NLW_p_tmp_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_p_tmp_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({tmp_product__0_n_107,tmp_product__0_n_108,tmp_product__0_n_109,tmp_product__0_n_110,tmp_product__0_n_111,tmp_product__0_n_112,tmp_product__0_n_113,tmp_product__0_n_114,tmp_product__0_n_115,tmp_product__0_n_116,tmp_product__0_n_117,tmp_product__0_n_118,tmp_product__0_n_119,tmp_product__0_n_120,tmp_product__0_n_121,tmp_product__0_n_122,tmp_product__0_n_123,tmp_product__0_n_124,tmp_product__0_n_125,tmp_product__0_n_126,tmp_product__0_n_127,tmp_product__0_n_128,tmp_product__0_n_129,tmp_product__0_n_130,tmp_product__0_n_131,tmp_product__0_n_132,tmp_product__0_n_133,tmp_product__0_n_134,tmp_product__0_n_135,tmp_product__0_n_136,tmp_product__0_n_137,tmp_product__0_n_138,tmp_product__0_n_139,tmp_product__0_n_140,tmp_product__0_n_141,tmp_product__0_n_142,tmp_product__0_n_143,tmp_product__0_n_144,tmp_product__0_n_145,tmp_product__0_n_146,tmp_product__0_n_147,tmp_product__0_n_148,tmp_product__0_n_149,tmp_product__0_n_150,tmp_product__0_n_151,tmp_product__0_n_152,tmp_product__0_n_153,tmp_product__0_n_154}),
        .PCOUT(NLW_p_tmp_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_p_tmp_reg_UNDERFLOW_UNCONNECTED));
  FDRE \p_tmp_reg[0]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_106),
        .Q(D[0]),
        .R(1'b0));
  FDRE \p_tmp_reg[10]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_96),
        .Q(D[10]),
        .R(1'b0));
  FDRE \p_tmp_reg[11]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_95),
        .Q(D[11]),
        .R(1'b0));
  FDRE \p_tmp_reg[12]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_94),
        .Q(D[12]),
        .R(1'b0));
  FDRE \p_tmp_reg[13]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_93),
        .Q(D[13]),
        .R(1'b0));
  FDRE \p_tmp_reg[14]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_92),
        .Q(D[14]),
        .R(1'b0));
  FDRE \p_tmp_reg[15]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_91),
        .Q(D[15]),
        .R(1'b0));
  FDRE \p_tmp_reg[16]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_90),
        .Q(\p_tmp_reg[16]__0_n_1 ),
        .R(1'b0));
  FDRE \p_tmp_reg[1]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_105),
        .Q(D[1]),
        .R(1'b0));
  FDRE \p_tmp_reg[2]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_104),
        .Q(D[2]),
        .R(1'b0));
  FDRE \p_tmp_reg[3]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_103),
        .Q(D[3]),
        .R(1'b0));
  FDRE \p_tmp_reg[4]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_102),
        .Q(D[4]),
        .R(1'b0));
  FDRE \p_tmp_reg[5]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_101),
        .Q(D[5]),
        .R(1'b0));
  FDRE \p_tmp_reg[6]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_100),
        .Q(D[6]),
        .R(1'b0));
  FDRE \p_tmp_reg[7]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_99),
        .Q(D[7]),
        .R(1'b0));
  FDRE \p_tmp_reg[8]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_98),
        .Q(D[8]),
        .R(1'b0));
  FDRE \p_tmp_reg[9]__0 
       (.C(ap_clk),
        .CE(1'b1),
        .D(tmp_product__0_n_97),
        .Q(D[9]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 15x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    tmp_product
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p_1_in[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_tmp_product_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({c_q0[31],c_q0[31],c_q0[31],c_q0[31:17]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_tmp_product_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_tmp_product_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_tmp_product_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(tmp_product_i_1_n_1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(Q[1]),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b1),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_tmp_product_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_tmp_product_OVERFLOW_UNCONNECTED),
        .P({tmp_product_n_59,tmp_product_n_60,tmp_product_n_61,tmp_product_n_62,tmp_product_n_63,tmp_product_n_64,tmp_product_n_65,tmp_product_n_66,tmp_product_n_67,tmp_product_n_68,tmp_product_n_69,tmp_product_n_70,tmp_product_n_71,tmp_product_n_72,tmp_product_n_73,tmp_product_n_74,tmp_product_n_75,tmp_product_n_76,tmp_product_n_77,tmp_product_n_78,tmp_product_n_79,tmp_product_n_80,tmp_product_n_81,tmp_product_n_82,tmp_product_n_83,tmp_product_n_84,tmp_product_n_85,tmp_product_n_86,tmp_product_n_87,tmp_product_n_88,tmp_product_n_89,tmp_product_n_90,tmp_product_n_91,tmp_product_n_92,tmp_product_n_93,tmp_product_n_94,tmp_product_n_95,tmp_product_n_96,tmp_product_n_97,tmp_product_n_98,tmp_product_n_99,tmp_product_n_100,tmp_product_n_101,tmp_product_n_102,tmp_product_n_103,tmp_product_n_104,tmp_product_n_105,tmp_product_n_106}),
        .PATTERNBDETECT(NLW_tmp_product_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_tmp_product_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({tmp_product_n_107,tmp_product_n_108,tmp_product_n_109,tmp_product_n_110,tmp_product_n_111,tmp_product_n_112,tmp_product_n_113,tmp_product_n_114,tmp_product_n_115,tmp_product_n_116,tmp_product_n_117,tmp_product_n_118,tmp_product_n_119,tmp_product_n_120,tmp_product_n_121,tmp_product_n_122,tmp_product_n_123,tmp_product_n_124,tmp_product_n_125,tmp_product_n_126,tmp_product_n_127,tmp_product_n_128,tmp_product_n_129,tmp_product_n_130,tmp_product_n_131,tmp_product_n_132,tmp_product_n_133,tmp_product_n_134,tmp_product_n_135,tmp_product_n_136,tmp_product_n_137,tmp_product_n_138,tmp_product_n_139,tmp_product_n_140,tmp_product_n_141,tmp_product_n_142,tmp_product_n_143,tmp_product_n_144,tmp_product_n_145,tmp_product_n_146,tmp_product_n_147,tmp_product_n_148,tmp_product_n_149,tmp_product_n_150,tmp_product_n_151,tmp_product_n_152,tmp_product_n_153,tmp_product_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_tmp_product_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-10 {cell *THIS*} {string 18x18 4}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    tmp_product__0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,c_q0[16:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT({tmp_product__0_n_25,tmp_product__0_n_26,tmp_product__0_n_27,tmp_product__0_n_28,tmp_product__0_n_29,tmp_product__0_n_30,tmp_product__0_n_31,tmp_product__0_n_32,tmp_product__0_n_33,tmp_product__0_n_34,tmp_product__0_n_35,tmp_product__0_n_36,tmp_product__0_n_37,tmp_product__0_n_38,tmp_product__0_n_39,tmp_product__0_n_40,tmp_product__0_n_41,tmp_product__0_n_42,tmp_product__0_n_43,tmp_product__0_n_44,tmp_product__0_n_45,tmp_product__0_n_46,tmp_product__0_n_47,tmp_product__0_n_48,tmp_product__0_n_49,tmp_product__0_n_50,tmp_product__0_n_51,tmp_product__0_n_52,tmp_product__0_n_53,tmp_product__0_n_54}),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,p_1_in[16:0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_tmp_product__0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_tmp_product__0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_tmp_product__0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(Q[1]),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(tmp_product_i_1_n_1),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(ap_clk),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_tmp_product__0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_tmp_product__0_OVERFLOW_UNCONNECTED),
        .P({tmp_product__0_n_59,tmp_product__0_n_60,tmp_product__0_n_61,tmp_product__0_n_62,tmp_product__0_n_63,tmp_product__0_n_64,tmp_product__0_n_65,tmp_product__0_n_66,tmp_product__0_n_67,tmp_product__0_n_68,tmp_product__0_n_69,tmp_product__0_n_70,tmp_product__0_n_71,tmp_product__0_n_72,tmp_product__0_n_73,tmp_product__0_n_74,tmp_product__0_n_75,tmp_product__0_n_76,tmp_product__0_n_77,tmp_product__0_n_78,tmp_product__0_n_79,tmp_product__0_n_80,tmp_product__0_n_81,tmp_product__0_n_82,tmp_product__0_n_83,tmp_product__0_n_84,tmp_product__0_n_85,tmp_product__0_n_86,tmp_product__0_n_87,tmp_product__0_n_88,tmp_product__0_n_89,tmp_product__0_n_90,tmp_product__0_n_91,tmp_product__0_n_92,tmp_product__0_n_93,tmp_product__0_n_94,tmp_product__0_n_95,tmp_product__0_n_96,tmp_product__0_n_97,tmp_product__0_n_98,tmp_product__0_n_99,tmp_product__0_n_100,tmp_product__0_n_101,tmp_product__0_n_102,tmp_product__0_n_103,tmp_product__0_n_104,tmp_product__0_n_105,tmp_product__0_n_106}),
        .PATTERNBDETECT(NLW_tmp_product__0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_tmp_product__0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT({tmp_product__0_n_107,tmp_product__0_n_108,tmp_product__0_n_109,tmp_product__0_n_110,tmp_product__0_n_111,tmp_product__0_n_112,tmp_product__0_n_113,tmp_product__0_n_114,tmp_product__0_n_115,tmp_product__0_n_116,tmp_product__0_n_117,tmp_product__0_n_118,tmp_product__0_n_119,tmp_product__0_n_120,tmp_product__0_n_121,tmp_product__0_n_122,tmp_product__0_n_123,tmp_product__0_n_124,tmp_product__0_n_125,tmp_product__0_n_126,tmp_product__0_n_127,tmp_product__0_n_128,tmp_product__0_n_129,tmp_product__0_n_130,tmp_product__0_n_131,tmp_product__0_n_132,tmp_product__0_n_133,tmp_product__0_n_134,tmp_product__0_n_135,tmp_product__0_n_136,tmp_product__0_n_137,tmp_product__0_n_138,tmp_product__0_n_139,tmp_product__0_n_140,tmp_product__0_n_141,tmp_product__0_n_142,tmp_product__0_n_143,tmp_product__0_n_144,tmp_product__0_n_145,tmp_product__0_n_146,tmp_product__0_n_147,tmp_product__0_n_148,tmp_product__0_n_149,tmp_product__0_n_150,tmp_product__0_n_151,tmp_product__0_n_152,tmp_product__0_n_153,tmp_product__0_n_154}),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_tmp_product__0_UNDERFLOW_UNCONNECTED));
  LUT3 #(
    .INIT(8'hF4)) 
    tmp_product_i_1
       (.I0(icmp_ln61_reg_189),
        .I1(Q[0]),
        .I2(shift_reg_address01),
        .O(tmp_product_i_1_n_1));
endmodule

(* ORIG_REF_NAME = "fir_shift_reg" *) 
module bd_0_hls_inst_0_fir_shift_reg
   (p_1_in,
    shift_reg_address01,
    x,
    Q,
    icmp_ln61_reg_189,
    c_address0,
    \q0_reg[0] ,
    ap_clk);
  output [31:0]p_1_in;
  output shift_reg_address01;
  input [31:0]x;
  input [1:0]Q;
  input icmp_ln61_reg_189;
  input [3:0]c_address0;
  input [4:0]\q0_reg[0] ;
  input ap_clk;

  wire [1:0]Q;
  wire ap_clk;
  wire [3:0]c_address0;
  wire icmp_ln61_reg_189;
  wire [31:0]p_1_in;
  wire [4:0]\q0_reg[0] ;
  wire shift_reg_address01;
  wire [31:0]x;

  bd_0_hls_inst_0_fir_shift_reg_ram fir_shift_reg_ram_U
       (.Q(Q),
        .\ap_CS_fsm_reg[1] (shift_reg_address01),
        .ap_clk(ap_clk),
        .c_address0(c_address0),
        .icmp_ln61_reg_189(icmp_ln61_reg_189),
        .p_1_in(p_1_in),
        .\q0_reg[0]_0 (\q0_reg[0] ),
        .x(x));
endmodule

(* ORIG_REF_NAME = "fir_shift_reg_ram" *) 
module bd_0_hls_inst_0_fir_shift_reg_ram
   (p_1_in,
    \ap_CS_fsm_reg[1] ,
    x,
    Q,
    icmp_ln61_reg_189,
    c_address0,
    \q0_reg[0]_0 ,
    ap_clk);
  output [31:0]p_1_in;
  output \ap_CS_fsm_reg[1] ;
  input [31:0]x;
  input [1:0]Q;
  input icmp_ln61_reg_189;
  input [3:0]c_address0;
  input [4:0]\q0_reg[0]_0 ;
  input ap_clk;

  wire [1:0]Q;
  wire [3:0]address0;
  wire \ap_CS_fsm_reg[1] ;
  wire ap_clk;
  wire [3:0]c_address0;
  wire ce0;
  wire [31:0]d0;
  wire icmp_ln61_reg_189;
  wire p_0_in;
  wire [31:0]p_1_in;
  wire [31:0]p_1_out;
  wire [31:0]q0;
  wire \q0[0]_i_1_n_1 ;
  wire \q0[10]_i_1_n_1 ;
  wire \q0[11]_i_1_n_1 ;
  wire \q0[12]_i_1_n_1 ;
  wire \q0[13]_i_1_n_1 ;
  wire \q0[14]_i_1_n_1 ;
  wire \q0[15]_i_1_n_1 ;
  wire \q0[16]_i_1_n_1 ;
  wire \q0[17]_i_1_n_1 ;
  wire \q0[18]_i_1_n_1 ;
  wire \q0[19]_i_1_n_1 ;
  wire \q0[1]_i_1_n_1 ;
  wire \q0[20]_i_1_n_1 ;
  wire \q0[21]_i_1_n_1 ;
  wire \q0[22]_i_1_n_1 ;
  wire \q0[23]_i_1_n_1 ;
  wire \q0[24]_i_1_n_1 ;
  wire \q0[25]_i_1_n_1 ;
  wire \q0[26]_i_1_n_1 ;
  wire \q0[27]_i_1_n_1 ;
  wire \q0[28]_i_1_n_1 ;
  wire \q0[29]_i_1_n_1 ;
  wire \q0[2]_i_1_n_1 ;
  wire \q0[30]_i_1_n_1 ;
  wire \q0[31]_i_2_n_1 ;
  wire \q0[3]_i_1_n_1 ;
  wire \q0[4]_i_1_n_1 ;
  wire \q0[5]_i_1_n_1 ;
  wire \q0[6]_i_1_n_1 ;
  wire \q0[7]_i_1_n_1 ;
  wire \q0[8]_i_1_n_1 ;
  wire \q0[9]_i_1_n_1 ;
  wire [4:0]\q0_reg[0]_0 ;
  wire ram_reg_0_15_0_0_i_8_n_1;
  wire [31:0]x;

  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_1
       (.I0(q0[31]),
        .I1(x[31]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_10
       (.I0(q0[22]),
        .I1(x[22]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[22]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_11
       (.I0(q0[21]),
        .I1(x[21]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[21]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_12
       (.I0(q0[20]),
        .I1(x[20]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[20]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_13
       (.I0(q0[19]),
        .I1(x[19]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[19]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_14
       (.I0(q0[18]),
        .I1(x[18]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[18]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_15
       (.I0(q0[17]),
        .I1(x[17]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[17]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_2
       (.I0(q0[30]),
        .I1(x[30]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[30]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_3
       (.I0(q0[29]),
        .I1(x[29]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[29]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_4
       (.I0(q0[28]),
        .I1(x[28]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[28]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_5
       (.I0(q0[27]),
        .I1(x[27]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[27]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_6
       (.I0(q0[26]),
        .I1(x[26]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[26]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_7
       (.I0(q0[25]),
        .I1(x[25]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[25]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_8
       (.I0(q0[24]),
        .I1(x[24]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[24]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    p_tmp_reg_i_9
       (.I0(q0[23]),
        .I1(x[23]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[0]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[0]),
        .I4(x[0]),
        .I5(p_1_out[0]),
        .O(\q0[0]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[10]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[10]),
        .I4(x[10]),
        .I5(p_1_out[10]),
        .O(\q0[10]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[11]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[11]),
        .I4(x[11]),
        .I5(p_1_out[11]),
        .O(\q0[11]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[12]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[12]),
        .I4(x[12]),
        .I5(p_1_out[12]),
        .O(\q0[12]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[13]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[13]),
        .I4(x[13]),
        .I5(p_1_out[13]),
        .O(\q0[13]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[14]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[14]),
        .I4(x[14]),
        .I5(p_1_out[14]),
        .O(\q0[14]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[15]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[15]),
        .I4(x[15]),
        .I5(p_1_out[15]),
        .O(\q0[15]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[16]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[16]),
        .I4(x[16]),
        .I5(p_1_out[16]),
        .O(\q0[16]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[17]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[17]),
        .I4(x[17]),
        .I5(p_1_out[17]),
        .O(\q0[17]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[18]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[18]),
        .I4(x[18]),
        .I5(p_1_out[18]),
        .O(\q0[18]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[19]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[19]),
        .I4(x[19]),
        .I5(p_1_out[19]),
        .O(\q0[19]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[1]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[1]),
        .I4(x[1]),
        .I5(p_1_out[1]),
        .O(\q0[1]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[20]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[20]),
        .I4(x[20]),
        .I5(p_1_out[20]),
        .O(\q0[20]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[21]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[21]),
        .I4(x[21]),
        .I5(p_1_out[21]),
        .O(\q0[21]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[22]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[22]),
        .I4(x[22]),
        .I5(p_1_out[22]),
        .O(\q0[22]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[23]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[23]),
        .I4(x[23]),
        .I5(p_1_out[23]),
        .O(\q0[23]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[24]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[24]),
        .I4(x[24]),
        .I5(p_1_out[24]),
        .O(\q0[24]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[25]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[25]),
        .I4(x[25]),
        .I5(p_1_out[25]),
        .O(\q0[25]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[26]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[26]),
        .I4(x[26]),
        .I5(p_1_out[26]),
        .O(\q0[26]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[27]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[27]),
        .I4(x[27]),
        .I5(p_1_out[27]),
        .O(\q0[27]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[28]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[28]),
        .I4(x[28]),
        .I5(p_1_out[28]),
        .O(\q0[28]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[29]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[29]),
        .I4(x[29]),
        .I5(p_1_out[29]),
        .O(\q0[29]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[2]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[2]),
        .I4(x[2]),
        .I5(p_1_out[2]),
        .O(\q0[2]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[30]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[30]),
        .I4(x[30]),
        .I5(p_1_out[30]),
        .O(\q0[30]_i_1_n_1 ));
  LUT3 #(
    .INIT(8'hBA)) 
    \q0[31]_i_1 
       (.I0(Q[1]),
        .I1(\q0_reg[0]_0 [4]),
        .I2(Q[0]),
        .O(ce0));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[31]_i_2 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[31]),
        .I4(x[31]),
        .I5(p_1_out[31]),
        .O(\q0[31]_i_2_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[3]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[3]),
        .I4(x[3]),
        .I5(p_1_out[3]),
        .O(\q0[3]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[4]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[4]),
        .I4(x[4]),
        .I5(p_1_out[4]),
        .O(\q0[4]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[5]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[5]),
        .I4(x[5]),
        .I5(p_1_out[5]),
        .O(\q0[5]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[6]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[6]),
        .I4(x[6]),
        .I5(p_1_out[6]),
        .O(\q0[6]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[7]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[7]),
        .I4(x[7]),
        .I5(p_1_out[7]),
        .O(\q0[7]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[8]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[8]),
        .I4(x[8]),
        .I5(p_1_out[8]),
        .O(\q0[8]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'hFF3BCF0BF430C400)) 
    \q0[9]_i_1 
       (.I0(icmp_ln61_reg_189),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(q0[9]),
        .I4(x[9]),
        .I5(p_1_out[9]),
        .O(\q0[9]_i_1_n_1 ));
  FDRE \q0_reg[0] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[0]_i_1_n_1 ),
        .Q(q0[0]),
        .R(1'b0));
  FDRE \q0_reg[10] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[10]_i_1_n_1 ),
        .Q(q0[10]),
        .R(1'b0));
  FDRE \q0_reg[11] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[11]_i_1_n_1 ),
        .Q(q0[11]),
        .R(1'b0));
  FDRE \q0_reg[12] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[12]_i_1_n_1 ),
        .Q(q0[12]),
        .R(1'b0));
  FDRE \q0_reg[13] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[13]_i_1_n_1 ),
        .Q(q0[13]),
        .R(1'b0));
  FDRE \q0_reg[14] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[14]_i_1_n_1 ),
        .Q(q0[14]),
        .R(1'b0));
  FDRE \q0_reg[15] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[15]_i_1_n_1 ),
        .Q(q0[15]),
        .R(1'b0));
  FDRE \q0_reg[16] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[16]_i_1_n_1 ),
        .Q(q0[16]),
        .R(1'b0));
  FDRE \q0_reg[17] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[17]_i_1_n_1 ),
        .Q(q0[17]),
        .R(1'b0));
  FDRE \q0_reg[18] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[18]_i_1_n_1 ),
        .Q(q0[18]),
        .R(1'b0));
  FDRE \q0_reg[19] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[19]_i_1_n_1 ),
        .Q(q0[19]),
        .R(1'b0));
  FDRE \q0_reg[1] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[1]_i_1_n_1 ),
        .Q(q0[1]),
        .R(1'b0));
  FDRE \q0_reg[20] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[20]_i_1_n_1 ),
        .Q(q0[20]),
        .R(1'b0));
  FDRE \q0_reg[21] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[21]_i_1_n_1 ),
        .Q(q0[21]),
        .R(1'b0));
  FDRE \q0_reg[22] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[22]_i_1_n_1 ),
        .Q(q0[22]),
        .R(1'b0));
  FDRE \q0_reg[23] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[23]_i_1_n_1 ),
        .Q(q0[23]),
        .R(1'b0));
  FDRE \q0_reg[24] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[24]_i_1_n_1 ),
        .Q(q0[24]),
        .R(1'b0));
  FDRE \q0_reg[25] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[25]_i_1_n_1 ),
        .Q(q0[25]),
        .R(1'b0));
  FDRE \q0_reg[26] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[26]_i_1_n_1 ),
        .Q(q0[26]),
        .R(1'b0));
  FDRE \q0_reg[27] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[27]_i_1_n_1 ),
        .Q(q0[27]),
        .R(1'b0));
  FDRE \q0_reg[28] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[28]_i_1_n_1 ),
        .Q(q0[28]),
        .R(1'b0));
  FDRE \q0_reg[29] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[29]_i_1_n_1 ),
        .Q(q0[29]),
        .R(1'b0));
  FDRE \q0_reg[2] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[2]_i_1_n_1 ),
        .Q(q0[2]),
        .R(1'b0));
  FDRE \q0_reg[30] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[30]_i_1_n_1 ),
        .Q(q0[30]),
        .R(1'b0));
  FDRE \q0_reg[31] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[31]_i_2_n_1 ),
        .Q(q0[31]),
        .R(1'b0));
  FDRE \q0_reg[3] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[3]_i_1_n_1 ),
        .Q(q0[3]),
        .R(1'b0));
  FDRE \q0_reg[4] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[4]_i_1_n_1 ),
        .Q(q0[4]),
        .R(1'b0));
  FDRE \q0_reg[5] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[5]_i_1_n_1 ),
        .Q(q0[5]),
        .R(1'b0));
  FDRE \q0_reg[6] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[6]_i_1_n_1 ),
        .Q(q0[6]),
        .R(1'b0));
  FDRE \q0_reg[7] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[7]_i_1_n_1 ),
        .Q(q0[7]),
        .R(1'b0));
  FDRE \q0_reg[8] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[8]_i_1_n_1 ),
        .Q(q0[8]),
        .R(1'b0));
  FDRE \q0_reg[9] 
       (.C(ap_clk),
        .CE(ce0),
        .D(\q0[9]_i_1_n_1 ),
        .Q(q0[9]),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_0_0
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[0]),
        .O(p_1_out[0]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_0_0_i_1
       (.I0(q0[0]),
        .I1(x[0]),
        .I2(Q[1]),
        .O(d0[0]));
  LUT5 #(
    .INIT(32'hBABA00AA)) 
    ram_reg_0_15_0_0_i_2
       (.I0(Q[1]),
        .I1(\q0_reg[0]_0 [4]),
        .I2(Q[0]),
        .I3(icmp_ln61_reg_189),
        .I4(\ap_CS_fsm_reg[1] ),
        .O(p_0_in));
  LUT4 #(
    .INIT(16'h888B)) 
    ram_reg_0_15_0_0_i_3
       (.I0(c_address0[0]),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(\q0_reg[0]_0 [0]),
        .O(address0[0]));
  LUT5 #(
    .INIT(32'h8B88888B)) 
    ram_reg_0_15_0_0_i_4
       (.I0(c_address0[1]),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(\q0_reg[0]_0 [1]),
        .I4(\q0_reg[0]_0 [0]),
        .O(address0[1]));
  LUT6 #(
    .INIT(64'h8B888B888B88888B)) 
    ram_reg_0_15_0_0_i_5
       (.I0(c_address0[2]),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(\q0_reg[0]_0 [2]),
        .I4(\q0_reg[0]_0 [0]),
        .I5(\q0_reg[0]_0 [1]),
        .O(address0[2]));
  LUT6 #(
    .INIT(64'h8B888B888B88888B)) 
    ram_reg_0_15_0_0_i_6
       (.I0(c_address0[3]),
        .I1(Q[1]),
        .I2(\ap_CS_fsm_reg[1] ),
        .I3(\q0_reg[0]_0 [3]),
        .I4(ram_reg_0_15_0_0_i_8_n_1),
        .I5(\q0_reg[0]_0 [2]),
        .O(address0[3]));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    ram_reg_0_15_0_0_i_7
       (.I0(Q[0]),
        .I1(\q0_reg[0]_0 [2]),
        .I2(\q0_reg[0]_0 [1]),
        .I3(\q0_reg[0]_0 [0]),
        .I4(\q0_reg[0]_0 [4]),
        .I5(\q0_reg[0]_0 [3]),
        .O(\ap_CS_fsm_reg[1] ));
  LUT2 #(
    .INIT(4'hE)) 
    ram_reg_0_15_0_0_i_8
       (.I0(\q0_reg[0]_0 [0]),
        .I1(\q0_reg[0]_0 [1]),
        .O(ram_reg_0_15_0_0_i_8_n_1));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_10_10
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[10]),
        .O(p_1_out[10]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_10_10_i_1
       (.I0(q0[10]),
        .I1(x[10]),
        .I2(Q[1]),
        .O(d0[10]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_11_11
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[11]),
        .O(p_1_out[11]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_11_11_i_1
       (.I0(q0[11]),
        .I1(x[11]),
        .I2(Q[1]),
        .O(d0[11]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_12_12
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[12]),
        .O(p_1_out[12]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_12_12_i_1
       (.I0(q0[12]),
        .I1(x[12]),
        .I2(Q[1]),
        .O(d0[12]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_13_13
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[13]),
        .O(p_1_out[13]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_13_13_i_1
       (.I0(q0[13]),
        .I1(x[13]),
        .I2(Q[1]),
        .O(d0[13]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_14_14
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[14]),
        .O(p_1_out[14]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_14_14_i_1
       (.I0(q0[14]),
        .I1(x[14]),
        .I2(Q[1]),
        .O(d0[14]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_15_15
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[15]),
        .O(p_1_out[15]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_15_15_i_1
       (.I0(q0[15]),
        .I1(x[15]),
        .I2(Q[1]),
        .O(d0[15]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_16_16
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[16]),
        .O(p_1_out[16]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_16_16_i_1
       (.I0(q0[16]),
        .I1(x[16]),
        .I2(Q[1]),
        .O(d0[16]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_17_17
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[17]),
        .O(p_1_out[17]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_17_17_i_1
       (.I0(q0[17]),
        .I1(x[17]),
        .I2(Q[1]),
        .O(d0[17]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_18_18
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[18]),
        .O(p_1_out[18]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_18_18_i_1
       (.I0(q0[18]),
        .I1(x[18]),
        .I2(Q[1]),
        .O(d0[18]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_19_19
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[19]),
        .O(p_1_out[19]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_19_19_i_1
       (.I0(q0[19]),
        .I1(x[19]),
        .I2(Q[1]),
        .O(d0[19]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_1_1
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[1]),
        .O(p_1_out[1]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_1_1_i_1
       (.I0(q0[1]),
        .I1(x[1]),
        .I2(Q[1]),
        .O(d0[1]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_20_20
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[20]),
        .O(p_1_out[20]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_20_20_i_1
       (.I0(q0[20]),
        .I1(x[20]),
        .I2(Q[1]),
        .O(d0[20]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_21_21
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[21]),
        .O(p_1_out[21]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_21_21_i_1
       (.I0(q0[21]),
        .I1(x[21]),
        .I2(Q[1]),
        .O(d0[21]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_22_22
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[22]),
        .O(p_1_out[22]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_22_22_i_1
       (.I0(q0[22]),
        .I1(x[22]),
        .I2(Q[1]),
        .O(d0[22]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_23_23
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[23]),
        .O(p_1_out[23]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_23_23_i_1
       (.I0(q0[23]),
        .I1(x[23]),
        .I2(Q[1]),
        .O(d0[23]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_24_24
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[24]),
        .O(p_1_out[24]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_24_24_i_1
       (.I0(q0[24]),
        .I1(x[24]),
        .I2(Q[1]),
        .O(d0[24]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_25_25
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[25]),
        .O(p_1_out[25]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_25_25_i_1
       (.I0(q0[25]),
        .I1(x[25]),
        .I2(Q[1]),
        .O(d0[25]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_26_26
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[26]),
        .O(p_1_out[26]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_26_26_i_1
       (.I0(q0[26]),
        .I1(x[26]),
        .I2(Q[1]),
        .O(d0[26]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_27_27
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[27]),
        .O(p_1_out[27]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_27_27_i_1
       (.I0(q0[27]),
        .I1(x[27]),
        .I2(Q[1]),
        .O(d0[27]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_28_28
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[28]),
        .O(p_1_out[28]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_28_28_i_1
       (.I0(q0[28]),
        .I1(x[28]),
        .I2(Q[1]),
        .O(d0[28]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_29_29
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[29]),
        .O(p_1_out[29]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_29_29_i_1
       (.I0(q0[29]),
        .I1(x[29]),
        .I2(Q[1]),
        .O(d0[29]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_2_2
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[2]),
        .O(p_1_out[2]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_2_2_i_1
       (.I0(q0[2]),
        .I1(x[2]),
        .I2(Q[1]),
        .O(d0[2]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_30_30
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[30]),
        .O(p_1_out[30]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_30_30_i_1
       (.I0(q0[30]),
        .I1(x[30]),
        .I2(Q[1]),
        .O(d0[30]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_31_31
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[31]),
        .O(p_1_out[31]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_31_31_i_1
       (.I0(q0[31]),
        .I1(x[31]),
        .I2(Q[1]),
        .O(d0[31]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_3_3
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[3]),
        .O(p_1_out[3]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_3_3_i_1
       (.I0(q0[3]),
        .I1(x[3]),
        .I2(Q[1]),
        .O(d0[3]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_4_4
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[4]),
        .O(p_1_out[4]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_4_4_i_1
       (.I0(q0[4]),
        .I1(x[4]),
        .I2(Q[1]),
        .O(d0[4]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_5_5
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[5]),
        .O(p_1_out[5]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_5_5_i_1
       (.I0(q0[5]),
        .I1(x[5]),
        .I2(Q[1]),
        .O(d0[5]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_6_6
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[6]),
        .O(p_1_out[6]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_6_6_i_1
       (.I0(q0[6]),
        .I1(x[6]),
        .I2(Q[1]),
        .O(d0[6]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_7_7
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[7]),
        .O(p_1_out[7]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_7_7_i_1
       (.I0(q0[7]),
        .I1(x[7]),
        .I2(Q[1]),
        .O(d0[7]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_8_8
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[8]),
        .O(p_1_out[8]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_8_8_i_1
       (.I0(q0[8]),
        .I1(x[8]),
        .I2(Q[1]),
        .O(d0[8]));
  (* XILINX_LEGACY_PRIM = "RAM16X1S" *) 
  RAM32X1S #(
    .INIT(32'h00000000)) 
    ram_reg_0_15_9_9
       (.A0(address0[0]),
        .A1(address0[1]),
        .A2(address0[2]),
        .A3(address0[3]),
        .A4(1'b0),
        .D(d0[9]),
        .O(p_1_out[9]),
        .WCLK(ap_clk),
        .WE(p_0_in));
  LUT3 #(
    .INIT(8'hAC)) 
    ram_reg_0_15_9_9_i_1
       (.I0(q0[9]),
        .I1(x[9]),
        .I2(Q[1]),
        .O(d0[9]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_10
       (.I0(q0[8]),
        .I1(x[8]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[8]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_11
       (.I0(q0[7]),
        .I1(x[7]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[7]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_12
       (.I0(q0[6]),
        .I1(x[6]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[6]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_13
       (.I0(q0[5]),
        .I1(x[5]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[5]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_14
       (.I0(q0[4]),
        .I1(x[4]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[4]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_15
       (.I0(q0[3]),
        .I1(x[3]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[3]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_16
       (.I0(q0[2]),
        .I1(x[2]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[2]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_17
       (.I0(q0[1]),
        .I1(x[1]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[1]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_18
       (.I0(q0[0]),
        .I1(x[0]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[0]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_2
       (.I0(q0[16]),
        .I1(x[16]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[16]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_3
       (.I0(q0[15]),
        .I1(x[15]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_4
       (.I0(q0[14]),
        .I1(x[14]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[14]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_5
       (.I0(q0[13]),
        .I1(x[13]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[13]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_6
       (.I0(q0[12]),
        .I1(x[12]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[12]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_7
       (.I0(q0[11]),
        .I1(x[11]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[11]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_8
       (.I0(q0[10]),
        .I1(x[10]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[10]));
  LUT4 #(
    .INIT(16'hCCAC)) 
    tmp_product_i_9
       (.I0(q0[9]),
        .I1(x[9]),
        .I2(Q[1]),
        .I3(icmp_ln61_reg_189),
        .O(p_1_in[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
