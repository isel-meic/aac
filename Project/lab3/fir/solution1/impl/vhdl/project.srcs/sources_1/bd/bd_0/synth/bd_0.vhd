--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Sat Apr 18 23:25:44 2020
--Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
--Command     : generate_target bd_0.bd
--Design      : bd_0
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0 is
  port (
    ap_clk : in STD_LOGIC;
    ap_ctrl_done : out STD_LOGIC;
    ap_ctrl_idle : out STD_LOGIC;
    ap_ctrl_ready : out STD_LOGIC;
    ap_ctrl_start : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    c_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    c_ce0 : out STD_LOGIC;
    c_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    x : in STD_LOGIC_VECTOR ( 31 downto 0 );
    y : out STD_LOGIC_VECTOR ( 31 downto 0 );
    y_ap_vld : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of bd_0 : entity is "bd_0,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=bd_0,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=1,numReposBlks=1,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=1,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of bd_0 : entity is "bd_0.hwdef";
end bd_0;

architecture STRUCTURE of bd_0 is
  component bd_0_hls_inst_0 is
  port (
    y_ap_vld : out STD_LOGIC;
    c_ce0 : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 31 downto 0 );
    c_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    c_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    x : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component bd_0_hls_inst_0;
  signal ap_clk_0_1 : STD_LOGIC;
  signal ap_ctrl_0_1_done : STD_LOGIC;
  signal ap_ctrl_0_1_idle : STD_LOGIC;
  signal ap_ctrl_0_1_ready : STD_LOGIC;
  signal ap_ctrl_0_1_start : STD_LOGIC;
  signal ap_rst_0_1 : STD_LOGIC;
  signal c_q0_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal hls_inst_c_address0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal hls_inst_c_ce0 : STD_LOGIC;
  signal hls_inst_y : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal hls_inst_y_ap_vld : STD_LOGIC;
  signal x_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of ap_clk : signal is "xilinx.com:signal:clock:1.0 CLK.AP_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of ap_clk : signal is "XIL_INTERFACENAME CLK.AP_CLK, ASSOCIATED_RESET ap_rst, CLK_DOMAIN bd_0_ap_clk_0, FREQ_HZ 125000000.0, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of ap_ctrl_done : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ";
  attribute X_INTERFACE_INFO of ap_ctrl_idle : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ";
  attribute X_INTERFACE_INFO of ap_ctrl_ready : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ";
  attribute X_INTERFACE_INFO of ap_ctrl_start : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ";
  attribute X_INTERFACE_INFO of ap_rst : signal is "xilinx.com:signal:reset:1.0 RST.AP_RST RST";
  attribute X_INTERFACE_PARAMETER of ap_rst : signal is "XIL_INTERFACENAME RST.AP_RST, INSERT_VIP 0, POLARITY ACTIVE_HIGH";
  attribute X_INTERFACE_INFO of c_address0 : signal is "xilinx.com:signal:data:1.0 DATA.C_ADDRESS0 DATA";
  attribute X_INTERFACE_PARAMETER of c_address0 : signal is "XIL_INTERFACENAME DATA.C_ADDRESS0, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of c_q0 : signal is "xilinx.com:signal:data:1.0 DATA.C_Q0 DATA";
  attribute X_INTERFACE_PARAMETER of c_q0 : signal is "XIL_INTERFACENAME DATA.C_Q0, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of x : signal is "xilinx.com:signal:data:1.0 DATA.X DATA";
  attribute X_INTERFACE_PARAMETER of x : signal is "XIL_INTERFACENAME DATA.X, LAYERED_METADATA undef";
  attribute X_INTERFACE_INFO of y : signal is "xilinx.com:signal:data:1.0 DATA.Y DATA";
  attribute X_INTERFACE_PARAMETER of y : signal is "XIL_INTERFACENAME DATA.Y, LAYERED_METADATA undef";
begin
  ap_clk_0_1 <= ap_clk;
  ap_ctrl_0_1_start <= ap_ctrl_start;
  ap_ctrl_done <= ap_ctrl_0_1_done;
  ap_ctrl_idle <= ap_ctrl_0_1_idle;
  ap_ctrl_ready <= ap_ctrl_0_1_ready;
  ap_rst_0_1 <= ap_rst;
  c_address0(3 downto 0) <= hls_inst_c_address0(3 downto 0);
  c_ce0 <= hls_inst_c_ce0;
  c_q0_0_1(31 downto 0) <= c_q0(31 downto 0);
  x_0_1(31 downto 0) <= x(31 downto 0);
  y(31 downto 0) <= hls_inst_y(31 downto 0);
  y_ap_vld <= hls_inst_y_ap_vld;
hls_inst: component bd_0_hls_inst_0
     port map (
      ap_clk => ap_clk_0_1,
      ap_done => ap_ctrl_0_1_done,
      ap_idle => ap_ctrl_0_1_idle,
      ap_ready => ap_ctrl_0_1_ready,
      ap_rst => ap_rst_0_1,
      ap_start => ap_ctrl_0_1_start,
      c_address0(3 downto 0) => hls_inst_c_address0(3 downto 0),
      c_ce0 => hls_inst_c_ce0,
      c_q0(31 downto 0) => c_q0_0_1(31 downto 0),
      x(31 downto 0) => x_0_1(31 downto 0),
      y(31 downto 0) => hls_inst_y(31 downto 0),
      y_ap_vld => hls_inst_y_ap_vld
    );
end STRUCTURE;
