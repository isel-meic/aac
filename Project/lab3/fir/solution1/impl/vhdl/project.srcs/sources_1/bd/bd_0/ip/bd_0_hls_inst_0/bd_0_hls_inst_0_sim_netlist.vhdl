-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Apr 18 23:26:55 2020
-- Host        : DESKTOP-7T6EOU3 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/Susana/Desktop/Project/lab3/fir/solution1/impl/vhdl/project.srcs/sources_1/bd/bd_0/ip/bd_0_hls_inst_0/bd_0_hls_inst_0_sim_netlist.vhdl
-- Design      : bd_0_hls_inst_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_fir_mul_32s_32s_3bkb_MulnS_0 is
  port (
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    c_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    p_1_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    icmp_ln61_reg_189 : in STD_LOGIC;
    shift_reg_address01 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_fir_mul_32s_32s_3bkb_MulnS_0 : entity is "fir_mul_32s_32s_3bkb_MulnS_0";
end bd_0_hls_inst_0_fir_mul_32s_32s_3bkb_MulnS_0;

architecture STRUCTURE of bd_0_hls_inst_0_fir_mul_32s_32s_3bkb_MulnS_0 is
  signal \mul_ln68_reg_218[19]_i_2_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[19]_i_3_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[19]_i_4_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[23]_i_2_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[23]_i_3_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[23]_i_4_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[23]_i_5_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[27]_i_2_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[27]_i_3_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[27]_i_4_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[27]_i_5_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[31]_i_2_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[31]_i_3_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[31]_i_4_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218[31]_i_5_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \mul_ln68_reg_218_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \p_tmp_reg[16]__0_n_1\ : STD_LOGIC;
  signal p_tmp_reg_n_100 : STD_LOGIC;
  signal p_tmp_reg_n_101 : STD_LOGIC;
  signal p_tmp_reg_n_102 : STD_LOGIC;
  signal p_tmp_reg_n_103 : STD_LOGIC;
  signal p_tmp_reg_n_104 : STD_LOGIC;
  signal p_tmp_reg_n_105 : STD_LOGIC;
  signal p_tmp_reg_n_106 : STD_LOGIC;
  signal p_tmp_reg_n_59 : STD_LOGIC;
  signal p_tmp_reg_n_60 : STD_LOGIC;
  signal p_tmp_reg_n_61 : STD_LOGIC;
  signal p_tmp_reg_n_62 : STD_LOGIC;
  signal p_tmp_reg_n_63 : STD_LOGIC;
  signal p_tmp_reg_n_64 : STD_LOGIC;
  signal p_tmp_reg_n_65 : STD_LOGIC;
  signal p_tmp_reg_n_66 : STD_LOGIC;
  signal p_tmp_reg_n_67 : STD_LOGIC;
  signal p_tmp_reg_n_68 : STD_LOGIC;
  signal p_tmp_reg_n_69 : STD_LOGIC;
  signal p_tmp_reg_n_70 : STD_LOGIC;
  signal p_tmp_reg_n_71 : STD_LOGIC;
  signal p_tmp_reg_n_72 : STD_LOGIC;
  signal p_tmp_reg_n_73 : STD_LOGIC;
  signal p_tmp_reg_n_74 : STD_LOGIC;
  signal p_tmp_reg_n_75 : STD_LOGIC;
  signal p_tmp_reg_n_76 : STD_LOGIC;
  signal p_tmp_reg_n_77 : STD_LOGIC;
  signal p_tmp_reg_n_78 : STD_LOGIC;
  signal p_tmp_reg_n_79 : STD_LOGIC;
  signal p_tmp_reg_n_80 : STD_LOGIC;
  signal p_tmp_reg_n_81 : STD_LOGIC;
  signal p_tmp_reg_n_82 : STD_LOGIC;
  signal p_tmp_reg_n_83 : STD_LOGIC;
  signal p_tmp_reg_n_84 : STD_LOGIC;
  signal p_tmp_reg_n_85 : STD_LOGIC;
  signal p_tmp_reg_n_86 : STD_LOGIC;
  signal p_tmp_reg_n_87 : STD_LOGIC;
  signal p_tmp_reg_n_88 : STD_LOGIC;
  signal p_tmp_reg_n_89 : STD_LOGIC;
  signal p_tmp_reg_n_90 : STD_LOGIC;
  signal p_tmp_reg_n_91 : STD_LOGIC;
  signal p_tmp_reg_n_92 : STD_LOGIC;
  signal p_tmp_reg_n_93 : STD_LOGIC;
  signal p_tmp_reg_n_94 : STD_LOGIC;
  signal p_tmp_reg_n_95 : STD_LOGIC;
  signal p_tmp_reg_n_96 : STD_LOGIC;
  signal p_tmp_reg_n_97 : STD_LOGIC;
  signal p_tmp_reg_n_98 : STD_LOGIC;
  signal p_tmp_reg_n_99 : STD_LOGIC;
  signal \tmp_product__0_n_100\ : STD_LOGIC;
  signal \tmp_product__0_n_101\ : STD_LOGIC;
  signal \tmp_product__0_n_102\ : STD_LOGIC;
  signal \tmp_product__0_n_103\ : STD_LOGIC;
  signal \tmp_product__0_n_104\ : STD_LOGIC;
  signal \tmp_product__0_n_105\ : STD_LOGIC;
  signal \tmp_product__0_n_106\ : STD_LOGIC;
  signal \tmp_product__0_n_107\ : STD_LOGIC;
  signal \tmp_product__0_n_108\ : STD_LOGIC;
  signal \tmp_product__0_n_109\ : STD_LOGIC;
  signal \tmp_product__0_n_110\ : STD_LOGIC;
  signal \tmp_product__0_n_111\ : STD_LOGIC;
  signal \tmp_product__0_n_112\ : STD_LOGIC;
  signal \tmp_product__0_n_113\ : STD_LOGIC;
  signal \tmp_product__0_n_114\ : STD_LOGIC;
  signal \tmp_product__0_n_115\ : STD_LOGIC;
  signal \tmp_product__0_n_116\ : STD_LOGIC;
  signal \tmp_product__0_n_117\ : STD_LOGIC;
  signal \tmp_product__0_n_118\ : STD_LOGIC;
  signal \tmp_product__0_n_119\ : STD_LOGIC;
  signal \tmp_product__0_n_120\ : STD_LOGIC;
  signal \tmp_product__0_n_121\ : STD_LOGIC;
  signal \tmp_product__0_n_122\ : STD_LOGIC;
  signal \tmp_product__0_n_123\ : STD_LOGIC;
  signal \tmp_product__0_n_124\ : STD_LOGIC;
  signal \tmp_product__0_n_125\ : STD_LOGIC;
  signal \tmp_product__0_n_126\ : STD_LOGIC;
  signal \tmp_product__0_n_127\ : STD_LOGIC;
  signal \tmp_product__0_n_128\ : STD_LOGIC;
  signal \tmp_product__0_n_129\ : STD_LOGIC;
  signal \tmp_product__0_n_130\ : STD_LOGIC;
  signal \tmp_product__0_n_131\ : STD_LOGIC;
  signal \tmp_product__0_n_132\ : STD_LOGIC;
  signal \tmp_product__0_n_133\ : STD_LOGIC;
  signal \tmp_product__0_n_134\ : STD_LOGIC;
  signal \tmp_product__0_n_135\ : STD_LOGIC;
  signal \tmp_product__0_n_136\ : STD_LOGIC;
  signal \tmp_product__0_n_137\ : STD_LOGIC;
  signal \tmp_product__0_n_138\ : STD_LOGIC;
  signal \tmp_product__0_n_139\ : STD_LOGIC;
  signal \tmp_product__0_n_140\ : STD_LOGIC;
  signal \tmp_product__0_n_141\ : STD_LOGIC;
  signal \tmp_product__0_n_142\ : STD_LOGIC;
  signal \tmp_product__0_n_143\ : STD_LOGIC;
  signal \tmp_product__0_n_144\ : STD_LOGIC;
  signal \tmp_product__0_n_145\ : STD_LOGIC;
  signal \tmp_product__0_n_146\ : STD_LOGIC;
  signal \tmp_product__0_n_147\ : STD_LOGIC;
  signal \tmp_product__0_n_148\ : STD_LOGIC;
  signal \tmp_product__0_n_149\ : STD_LOGIC;
  signal \tmp_product__0_n_150\ : STD_LOGIC;
  signal \tmp_product__0_n_151\ : STD_LOGIC;
  signal \tmp_product__0_n_152\ : STD_LOGIC;
  signal \tmp_product__0_n_153\ : STD_LOGIC;
  signal \tmp_product__0_n_154\ : STD_LOGIC;
  signal \tmp_product__0_n_25\ : STD_LOGIC;
  signal \tmp_product__0_n_26\ : STD_LOGIC;
  signal \tmp_product__0_n_27\ : STD_LOGIC;
  signal \tmp_product__0_n_28\ : STD_LOGIC;
  signal \tmp_product__0_n_29\ : STD_LOGIC;
  signal \tmp_product__0_n_30\ : STD_LOGIC;
  signal \tmp_product__0_n_31\ : STD_LOGIC;
  signal \tmp_product__0_n_32\ : STD_LOGIC;
  signal \tmp_product__0_n_33\ : STD_LOGIC;
  signal \tmp_product__0_n_34\ : STD_LOGIC;
  signal \tmp_product__0_n_35\ : STD_LOGIC;
  signal \tmp_product__0_n_36\ : STD_LOGIC;
  signal \tmp_product__0_n_37\ : STD_LOGIC;
  signal \tmp_product__0_n_38\ : STD_LOGIC;
  signal \tmp_product__0_n_39\ : STD_LOGIC;
  signal \tmp_product__0_n_40\ : STD_LOGIC;
  signal \tmp_product__0_n_41\ : STD_LOGIC;
  signal \tmp_product__0_n_42\ : STD_LOGIC;
  signal \tmp_product__0_n_43\ : STD_LOGIC;
  signal \tmp_product__0_n_44\ : STD_LOGIC;
  signal \tmp_product__0_n_45\ : STD_LOGIC;
  signal \tmp_product__0_n_46\ : STD_LOGIC;
  signal \tmp_product__0_n_47\ : STD_LOGIC;
  signal \tmp_product__0_n_48\ : STD_LOGIC;
  signal \tmp_product__0_n_49\ : STD_LOGIC;
  signal \tmp_product__0_n_50\ : STD_LOGIC;
  signal \tmp_product__0_n_51\ : STD_LOGIC;
  signal \tmp_product__0_n_52\ : STD_LOGIC;
  signal \tmp_product__0_n_53\ : STD_LOGIC;
  signal \tmp_product__0_n_54\ : STD_LOGIC;
  signal \tmp_product__0_n_59\ : STD_LOGIC;
  signal \tmp_product__0_n_60\ : STD_LOGIC;
  signal \tmp_product__0_n_61\ : STD_LOGIC;
  signal \tmp_product__0_n_62\ : STD_LOGIC;
  signal \tmp_product__0_n_63\ : STD_LOGIC;
  signal \tmp_product__0_n_64\ : STD_LOGIC;
  signal \tmp_product__0_n_65\ : STD_LOGIC;
  signal \tmp_product__0_n_66\ : STD_LOGIC;
  signal \tmp_product__0_n_67\ : STD_LOGIC;
  signal \tmp_product__0_n_68\ : STD_LOGIC;
  signal \tmp_product__0_n_69\ : STD_LOGIC;
  signal \tmp_product__0_n_70\ : STD_LOGIC;
  signal \tmp_product__0_n_71\ : STD_LOGIC;
  signal \tmp_product__0_n_72\ : STD_LOGIC;
  signal \tmp_product__0_n_73\ : STD_LOGIC;
  signal \tmp_product__0_n_74\ : STD_LOGIC;
  signal \tmp_product__0_n_75\ : STD_LOGIC;
  signal \tmp_product__0_n_76\ : STD_LOGIC;
  signal \tmp_product__0_n_77\ : STD_LOGIC;
  signal \tmp_product__0_n_78\ : STD_LOGIC;
  signal \tmp_product__0_n_79\ : STD_LOGIC;
  signal \tmp_product__0_n_80\ : STD_LOGIC;
  signal \tmp_product__0_n_81\ : STD_LOGIC;
  signal \tmp_product__0_n_82\ : STD_LOGIC;
  signal \tmp_product__0_n_83\ : STD_LOGIC;
  signal \tmp_product__0_n_84\ : STD_LOGIC;
  signal \tmp_product__0_n_85\ : STD_LOGIC;
  signal \tmp_product__0_n_86\ : STD_LOGIC;
  signal \tmp_product__0_n_87\ : STD_LOGIC;
  signal \tmp_product__0_n_88\ : STD_LOGIC;
  signal \tmp_product__0_n_89\ : STD_LOGIC;
  signal \tmp_product__0_n_90\ : STD_LOGIC;
  signal \tmp_product__0_n_91\ : STD_LOGIC;
  signal \tmp_product__0_n_92\ : STD_LOGIC;
  signal \tmp_product__0_n_93\ : STD_LOGIC;
  signal \tmp_product__0_n_94\ : STD_LOGIC;
  signal \tmp_product__0_n_95\ : STD_LOGIC;
  signal \tmp_product__0_n_96\ : STD_LOGIC;
  signal \tmp_product__0_n_97\ : STD_LOGIC;
  signal \tmp_product__0_n_98\ : STD_LOGIC;
  signal \tmp_product__0_n_99\ : STD_LOGIC;
  signal tmp_product_i_1_n_1 : STD_LOGIC;
  signal tmp_product_n_100 : STD_LOGIC;
  signal tmp_product_n_101 : STD_LOGIC;
  signal tmp_product_n_102 : STD_LOGIC;
  signal tmp_product_n_103 : STD_LOGIC;
  signal tmp_product_n_104 : STD_LOGIC;
  signal tmp_product_n_105 : STD_LOGIC;
  signal tmp_product_n_106 : STD_LOGIC;
  signal tmp_product_n_107 : STD_LOGIC;
  signal tmp_product_n_108 : STD_LOGIC;
  signal tmp_product_n_109 : STD_LOGIC;
  signal tmp_product_n_110 : STD_LOGIC;
  signal tmp_product_n_111 : STD_LOGIC;
  signal tmp_product_n_112 : STD_LOGIC;
  signal tmp_product_n_113 : STD_LOGIC;
  signal tmp_product_n_114 : STD_LOGIC;
  signal tmp_product_n_115 : STD_LOGIC;
  signal tmp_product_n_116 : STD_LOGIC;
  signal tmp_product_n_117 : STD_LOGIC;
  signal tmp_product_n_118 : STD_LOGIC;
  signal tmp_product_n_119 : STD_LOGIC;
  signal tmp_product_n_120 : STD_LOGIC;
  signal tmp_product_n_121 : STD_LOGIC;
  signal tmp_product_n_122 : STD_LOGIC;
  signal tmp_product_n_123 : STD_LOGIC;
  signal tmp_product_n_124 : STD_LOGIC;
  signal tmp_product_n_125 : STD_LOGIC;
  signal tmp_product_n_126 : STD_LOGIC;
  signal tmp_product_n_127 : STD_LOGIC;
  signal tmp_product_n_128 : STD_LOGIC;
  signal tmp_product_n_129 : STD_LOGIC;
  signal tmp_product_n_130 : STD_LOGIC;
  signal tmp_product_n_131 : STD_LOGIC;
  signal tmp_product_n_132 : STD_LOGIC;
  signal tmp_product_n_133 : STD_LOGIC;
  signal tmp_product_n_134 : STD_LOGIC;
  signal tmp_product_n_135 : STD_LOGIC;
  signal tmp_product_n_136 : STD_LOGIC;
  signal tmp_product_n_137 : STD_LOGIC;
  signal tmp_product_n_138 : STD_LOGIC;
  signal tmp_product_n_139 : STD_LOGIC;
  signal tmp_product_n_140 : STD_LOGIC;
  signal tmp_product_n_141 : STD_LOGIC;
  signal tmp_product_n_142 : STD_LOGIC;
  signal tmp_product_n_143 : STD_LOGIC;
  signal tmp_product_n_144 : STD_LOGIC;
  signal tmp_product_n_145 : STD_LOGIC;
  signal tmp_product_n_146 : STD_LOGIC;
  signal tmp_product_n_147 : STD_LOGIC;
  signal tmp_product_n_148 : STD_LOGIC;
  signal tmp_product_n_149 : STD_LOGIC;
  signal tmp_product_n_150 : STD_LOGIC;
  signal tmp_product_n_151 : STD_LOGIC;
  signal tmp_product_n_152 : STD_LOGIC;
  signal tmp_product_n_153 : STD_LOGIC;
  signal tmp_product_n_154 : STD_LOGIC;
  signal tmp_product_n_59 : STD_LOGIC;
  signal tmp_product_n_60 : STD_LOGIC;
  signal tmp_product_n_61 : STD_LOGIC;
  signal tmp_product_n_62 : STD_LOGIC;
  signal tmp_product_n_63 : STD_LOGIC;
  signal tmp_product_n_64 : STD_LOGIC;
  signal tmp_product_n_65 : STD_LOGIC;
  signal tmp_product_n_66 : STD_LOGIC;
  signal tmp_product_n_67 : STD_LOGIC;
  signal tmp_product_n_68 : STD_LOGIC;
  signal tmp_product_n_69 : STD_LOGIC;
  signal tmp_product_n_70 : STD_LOGIC;
  signal tmp_product_n_71 : STD_LOGIC;
  signal tmp_product_n_72 : STD_LOGIC;
  signal tmp_product_n_73 : STD_LOGIC;
  signal tmp_product_n_74 : STD_LOGIC;
  signal tmp_product_n_75 : STD_LOGIC;
  signal tmp_product_n_76 : STD_LOGIC;
  signal tmp_product_n_77 : STD_LOGIC;
  signal tmp_product_n_78 : STD_LOGIC;
  signal tmp_product_n_79 : STD_LOGIC;
  signal tmp_product_n_80 : STD_LOGIC;
  signal tmp_product_n_81 : STD_LOGIC;
  signal tmp_product_n_82 : STD_LOGIC;
  signal tmp_product_n_83 : STD_LOGIC;
  signal tmp_product_n_84 : STD_LOGIC;
  signal tmp_product_n_85 : STD_LOGIC;
  signal tmp_product_n_86 : STD_LOGIC;
  signal tmp_product_n_87 : STD_LOGIC;
  signal tmp_product_n_88 : STD_LOGIC;
  signal tmp_product_n_89 : STD_LOGIC;
  signal tmp_product_n_90 : STD_LOGIC;
  signal tmp_product_n_91 : STD_LOGIC;
  signal tmp_product_n_92 : STD_LOGIC;
  signal tmp_product_n_93 : STD_LOGIC;
  signal tmp_product_n_94 : STD_LOGIC;
  signal tmp_product_n_95 : STD_LOGIC;
  signal tmp_product_n_96 : STD_LOGIC;
  signal tmp_product_n_97 : STD_LOGIC;
  signal tmp_product_n_98 : STD_LOGIC;
  signal tmp_product_n_99 : STD_LOGIC;
  signal \NLW_mul_ln68_reg_218_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_p_tmp_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_tmp_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_tmp_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p_tmp_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_tmp_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p_tmp_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p_tmp_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_p_tmp_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_p_tmp_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_p_tmp_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_tmp_product_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_tmp_product_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_tmp_product_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_tmp_product_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_tmp_product_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_tmp_product_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_tmp_product_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_tmp_product_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_tmp_product_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tmp_product__0_CARRYCASCOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_tmp_product__0_MULTSIGNOUT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_tmp_product__0_OVERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_tmp_product__0_PATTERNBDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_tmp_product__0_PATTERNDETECT_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_tmp_product__0_UNDERFLOW_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_tmp_product__0_BCOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal \NLW_tmp_product__0_CARRYOUT_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of p_tmp_reg : label is "{SYNTH-10 {cell *THIS*} {string 18x15 4}}";
  attribute METHODOLOGY_DRC_VIOS of tmp_product : label is "{SYNTH-10 {cell *THIS*} {string 15x18 4}}";
  attribute METHODOLOGY_DRC_VIOS of \tmp_product__0\ : label is "{SYNTH-10 {cell *THIS*} {string 18x18 4}}";
begin
\mul_ln68_reg_218[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_104,
      I1 => tmp_product_n_104,
      O => \mul_ln68_reg_218[19]_i_2_n_1\
    );
\mul_ln68_reg_218[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_105,
      I1 => tmp_product_n_105,
      O => \mul_ln68_reg_218[19]_i_3_n_1\
    );
\mul_ln68_reg_218[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_106,
      I1 => tmp_product_n_106,
      O => \mul_ln68_reg_218[19]_i_4_n_1\
    );
\mul_ln68_reg_218[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_100,
      I1 => tmp_product_n_100,
      O => \mul_ln68_reg_218[23]_i_2_n_1\
    );
\mul_ln68_reg_218[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_101,
      I1 => tmp_product_n_101,
      O => \mul_ln68_reg_218[23]_i_3_n_1\
    );
\mul_ln68_reg_218[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_102,
      I1 => tmp_product_n_102,
      O => \mul_ln68_reg_218[23]_i_4_n_1\
    );
\mul_ln68_reg_218[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_103,
      I1 => tmp_product_n_103,
      O => \mul_ln68_reg_218[23]_i_5_n_1\
    );
\mul_ln68_reg_218[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_96,
      I1 => tmp_product_n_96,
      O => \mul_ln68_reg_218[27]_i_2_n_1\
    );
\mul_ln68_reg_218[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_97,
      I1 => tmp_product_n_97,
      O => \mul_ln68_reg_218[27]_i_3_n_1\
    );
\mul_ln68_reg_218[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_98,
      I1 => tmp_product_n_98,
      O => \mul_ln68_reg_218[27]_i_4_n_1\
    );
\mul_ln68_reg_218[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_99,
      I1 => tmp_product_n_99,
      O => \mul_ln68_reg_218[27]_i_5_n_1\
    );
\mul_ln68_reg_218[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_92,
      I1 => tmp_product_n_92,
      O => \mul_ln68_reg_218[31]_i_2_n_1\
    );
\mul_ln68_reg_218[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_93,
      I1 => tmp_product_n_93,
      O => \mul_ln68_reg_218[31]_i_3_n_1\
    );
\mul_ln68_reg_218[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_94,
      I1 => tmp_product_n_94,
      O => \mul_ln68_reg_218[31]_i_4_n_1\
    );
\mul_ln68_reg_218[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_tmp_reg_n_95,
      I1 => tmp_product_n_95,
      O => \mul_ln68_reg_218[31]_i_5_n_1\
    );
\mul_ln68_reg_218_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \mul_ln68_reg_218_reg[19]_i_1_n_1\,
      CO(2) => \mul_ln68_reg_218_reg[19]_i_1_n_2\,
      CO(1) => \mul_ln68_reg_218_reg[19]_i_1_n_3\,
      CO(0) => \mul_ln68_reg_218_reg[19]_i_1_n_4\,
      CYINIT => '0',
      DI(3) => p_tmp_reg_n_104,
      DI(2) => p_tmp_reg_n_105,
      DI(1) => p_tmp_reg_n_106,
      DI(0) => '0',
      O(3 downto 0) => D(19 downto 16),
      S(3) => \mul_ln68_reg_218[19]_i_2_n_1\,
      S(2) => \mul_ln68_reg_218[19]_i_3_n_1\,
      S(1) => \mul_ln68_reg_218[19]_i_4_n_1\,
      S(0) => \p_tmp_reg[16]__0_n_1\
    );
\mul_ln68_reg_218_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \mul_ln68_reg_218_reg[19]_i_1_n_1\,
      CO(3) => \mul_ln68_reg_218_reg[23]_i_1_n_1\,
      CO(2) => \mul_ln68_reg_218_reg[23]_i_1_n_2\,
      CO(1) => \mul_ln68_reg_218_reg[23]_i_1_n_3\,
      CO(0) => \mul_ln68_reg_218_reg[23]_i_1_n_4\,
      CYINIT => '0',
      DI(3) => p_tmp_reg_n_100,
      DI(2) => p_tmp_reg_n_101,
      DI(1) => p_tmp_reg_n_102,
      DI(0) => p_tmp_reg_n_103,
      O(3 downto 0) => D(23 downto 20),
      S(3) => \mul_ln68_reg_218[23]_i_2_n_1\,
      S(2) => \mul_ln68_reg_218[23]_i_3_n_1\,
      S(1) => \mul_ln68_reg_218[23]_i_4_n_1\,
      S(0) => \mul_ln68_reg_218[23]_i_5_n_1\
    );
\mul_ln68_reg_218_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \mul_ln68_reg_218_reg[23]_i_1_n_1\,
      CO(3) => \mul_ln68_reg_218_reg[27]_i_1_n_1\,
      CO(2) => \mul_ln68_reg_218_reg[27]_i_1_n_2\,
      CO(1) => \mul_ln68_reg_218_reg[27]_i_1_n_3\,
      CO(0) => \mul_ln68_reg_218_reg[27]_i_1_n_4\,
      CYINIT => '0',
      DI(3) => p_tmp_reg_n_96,
      DI(2) => p_tmp_reg_n_97,
      DI(1) => p_tmp_reg_n_98,
      DI(0) => p_tmp_reg_n_99,
      O(3 downto 0) => D(27 downto 24),
      S(3) => \mul_ln68_reg_218[27]_i_2_n_1\,
      S(2) => \mul_ln68_reg_218[27]_i_3_n_1\,
      S(1) => \mul_ln68_reg_218[27]_i_4_n_1\,
      S(0) => \mul_ln68_reg_218[27]_i_5_n_1\
    );
\mul_ln68_reg_218_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \mul_ln68_reg_218_reg[27]_i_1_n_1\,
      CO(3) => \NLW_mul_ln68_reg_218_reg[31]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \mul_ln68_reg_218_reg[31]_i_1_n_2\,
      CO(1) => \mul_ln68_reg_218_reg[31]_i_1_n_3\,
      CO(0) => \mul_ln68_reg_218_reg[31]_i_1_n_4\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => p_tmp_reg_n_93,
      DI(1) => p_tmp_reg_n_94,
      DI(0) => p_tmp_reg_n_95,
      O(3 downto 0) => D(31 downto 28),
      S(3) => \mul_ln68_reg_218[31]_i_2_n_1\,
      S(2) => \mul_ln68_reg_218[31]_i_3_n_1\,
      S(1) => \mul_ln68_reg_218[31]_i_4_n_1\,
      S(0) => \mul_ln68_reg_218[31]_i_5_n_1\
    );
p_tmp_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "CASCADE",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 0) => B"000000000000000000000000000000",
      ACIN(29) => \tmp_product__0_n_25\,
      ACIN(28) => \tmp_product__0_n_26\,
      ACIN(27) => \tmp_product__0_n_27\,
      ACIN(26) => \tmp_product__0_n_28\,
      ACIN(25) => \tmp_product__0_n_29\,
      ACIN(24) => \tmp_product__0_n_30\,
      ACIN(23) => \tmp_product__0_n_31\,
      ACIN(22) => \tmp_product__0_n_32\,
      ACIN(21) => \tmp_product__0_n_33\,
      ACIN(20) => \tmp_product__0_n_34\,
      ACIN(19) => \tmp_product__0_n_35\,
      ACIN(18) => \tmp_product__0_n_36\,
      ACIN(17) => \tmp_product__0_n_37\,
      ACIN(16) => \tmp_product__0_n_38\,
      ACIN(15) => \tmp_product__0_n_39\,
      ACIN(14) => \tmp_product__0_n_40\,
      ACIN(13) => \tmp_product__0_n_41\,
      ACIN(12) => \tmp_product__0_n_42\,
      ACIN(11) => \tmp_product__0_n_43\,
      ACIN(10) => \tmp_product__0_n_44\,
      ACIN(9) => \tmp_product__0_n_45\,
      ACIN(8) => \tmp_product__0_n_46\,
      ACIN(7) => \tmp_product__0_n_47\,
      ACIN(6) => \tmp_product__0_n_48\,
      ACIN(5) => \tmp_product__0_n_49\,
      ACIN(4) => \tmp_product__0_n_50\,
      ACIN(3) => \tmp_product__0_n_51\,
      ACIN(2) => \tmp_product__0_n_52\,
      ACIN(1) => \tmp_product__0_n_53\,
      ACIN(0) => \tmp_product__0_n_54\,
      ACOUT(29 downto 0) => NLW_p_tmp_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => p_1_in(31),
      B(16) => p_1_in(31),
      B(15) => p_1_in(31),
      B(14 downto 0) => p_1_in(31 downto 17),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_p_tmp_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_p_tmp_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_p_tmp_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => tmp_product_i_1_n_1,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '1',
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_p_tmp_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"1010101",
      OVERFLOW => NLW_p_tmp_reg_OVERFLOW_UNCONNECTED,
      P(47) => p_tmp_reg_n_59,
      P(46) => p_tmp_reg_n_60,
      P(45) => p_tmp_reg_n_61,
      P(44) => p_tmp_reg_n_62,
      P(43) => p_tmp_reg_n_63,
      P(42) => p_tmp_reg_n_64,
      P(41) => p_tmp_reg_n_65,
      P(40) => p_tmp_reg_n_66,
      P(39) => p_tmp_reg_n_67,
      P(38) => p_tmp_reg_n_68,
      P(37) => p_tmp_reg_n_69,
      P(36) => p_tmp_reg_n_70,
      P(35) => p_tmp_reg_n_71,
      P(34) => p_tmp_reg_n_72,
      P(33) => p_tmp_reg_n_73,
      P(32) => p_tmp_reg_n_74,
      P(31) => p_tmp_reg_n_75,
      P(30) => p_tmp_reg_n_76,
      P(29) => p_tmp_reg_n_77,
      P(28) => p_tmp_reg_n_78,
      P(27) => p_tmp_reg_n_79,
      P(26) => p_tmp_reg_n_80,
      P(25) => p_tmp_reg_n_81,
      P(24) => p_tmp_reg_n_82,
      P(23) => p_tmp_reg_n_83,
      P(22) => p_tmp_reg_n_84,
      P(21) => p_tmp_reg_n_85,
      P(20) => p_tmp_reg_n_86,
      P(19) => p_tmp_reg_n_87,
      P(18) => p_tmp_reg_n_88,
      P(17) => p_tmp_reg_n_89,
      P(16) => p_tmp_reg_n_90,
      P(15) => p_tmp_reg_n_91,
      P(14) => p_tmp_reg_n_92,
      P(13) => p_tmp_reg_n_93,
      P(12) => p_tmp_reg_n_94,
      P(11) => p_tmp_reg_n_95,
      P(10) => p_tmp_reg_n_96,
      P(9) => p_tmp_reg_n_97,
      P(8) => p_tmp_reg_n_98,
      P(7) => p_tmp_reg_n_99,
      P(6) => p_tmp_reg_n_100,
      P(5) => p_tmp_reg_n_101,
      P(4) => p_tmp_reg_n_102,
      P(3) => p_tmp_reg_n_103,
      P(2) => p_tmp_reg_n_104,
      P(1) => p_tmp_reg_n_105,
      P(0) => p_tmp_reg_n_106,
      PATTERNBDETECT => NLW_p_tmp_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_p_tmp_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47) => \tmp_product__0_n_107\,
      PCIN(46) => \tmp_product__0_n_108\,
      PCIN(45) => \tmp_product__0_n_109\,
      PCIN(44) => \tmp_product__0_n_110\,
      PCIN(43) => \tmp_product__0_n_111\,
      PCIN(42) => \tmp_product__0_n_112\,
      PCIN(41) => \tmp_product__0_n_113\,
      PCIN(40) => \tmp_product__0_n_114\,
      PCIN(39) => \tmp_product__0_n_115\,
      PCIN(38) => \tmp_product__0_n_116\,
      PCIN(37) => \tmp_product__0_n_117\,
      PCIN(36) => \tmp_product__0_n_118\,
      PCIN(35) => \tmp_product__0_n_119\,
      PCIN(34) => \tmp_product__0_n_120\,
      PCIN(33) => \tmp_product__0_n_121\,
      PCIN(32) => \tmp_product__0_n_122\,
      PCIN(31) => \tmp_product__0_n_123\,
      PCIN(30) => \tmp_product__0_n_124\,
      PCIN(29) => \tmp_product__0_n_125\,
      PCIN(28) => \tmp_product__0_n_126\,
      PCIN(27) => \tmp_product__0_n_127\,
      PCIN(26) => \tmp_product__0_n_128\,
      PCIN(25) => \tmp_product__0_n_129\,
      PCIN(24) => \tmp_product__0_n_130\,
      PCIN(23) => \tmp_product__0_n_131\,
      PCIN(22) => \tmp_product__0_n_132\,
      PCIN(21) => \tmp_product__0_n_133\,
      PCIN(20) => \tmp_product__0_n_134\,
      PCIN(19) => \tmp_product__0_n_135\,
      PCIN(18) => \tmp_product__0_n_136\,
      PCIN(17) => \tmp_product__0_n_137\,
      PCIN(16) => \tmp_product__0_n_138\,
      PCIN(15) => \tmp_product__0_n_139\,
      PCIN(14) => \tmp_product__0_n_140\,
      PCIN(13) => \tmp_product__0_n_141\,
      PCIN(12) => \tmp_product__0_n_142\,
      PCIN(11) => \tmp_product__0_n_143\,
      PCIN(10) => \tmp_product__0_n_144\,
      PCIN(9) => \tmp_product__0_n_145\,
      PCIN(8) => \tmp_product__0_n_146\,
      PCIN(7) => \tmp_product__0_n_147\,
      PCIN(6) => \tmp_product__0_n_148\,
      PCIN(5) => \tmp_product__0_n_149\,
      PCIN(4) => \tmp_product__0_n_150\,
      PCIN(3) => \tmp_product__0_n_151\,
      PCIN(2) => \tmp_product__0_n_152\,
      PCIN(1) => \tmp_product__0_n_153\,
      PCIN(0) => \tmp_product__0_n_154\,
      PCOUT(47 downto 0) => NLW_p_tmp_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_p_tmp_reg_UNDERFLOW_UNCONNECTED
    );
\p_tmp_reg[0]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_106\,
      Q => D(0),
      R => '0'
    );
\p_tmp_reg[10]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_96\,
      Q => D(10),
      R => '0'
    );
\p_tmp_reg[11]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_95\,
      Q => D(11),
      R => '0'
    );
\p_tmp_reg[12]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_94\,
      Q => D(12),
      R => '0'
    );
\p_tmp_reg[13]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_93\,
      Q => D(13),
      R => '0'
    );
\p_tmp_reg[14]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_92\,
      Q => D(14),
      R => '0'
    );
\p_tmp_reg[15]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_91\,
      Q => D(15),
      R => '0'
    );
\p_tmp_reg[16]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_90\,
      Q => \p_tmp_reg[16]__0_n_1\,
      R => '0'
    );
\p_tmp_reg[1]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_105\,
      Q => D(1),
      R => '0'
    );
\p_tmp_reg[2]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_104\,
      Q => D(2),
      R => '0'
    );
\p_tmp_reg[3]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_103\,
      Q => D(3),
      R => '0'
    );
\p_tmp_reg[4]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_102\,
      Q => D(4),
      R => '0'
    );
\p_tmp_reg[5]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_101\,
      Q => D(5),
      R => '0'
    );
\p_tmp_reg[6]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_100\,
      Q => D(6),
      R => '0'
    );
\p_tmp_reg[7]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_99\,
      Q => D(7),
      R => '0'
    );
\p_tmp_reg[8]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_98\,
      Q => D(8),
      R => '0'
    );
\p_tmp_reg[9]__0\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \tmp_product__0_n_97\,
      Q => D(9),
      R => '0'
    );
tmp_product: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => p_1_in(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_tmp_product_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => c_q0(31),
      B(16) => c_q0(31),
      B(15) => c_q0(31),
      B(14 downto 0) => c_q0(31 downto 17),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_tmp_product_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_tmp_product_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_tmp_product_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => tmp_product_i_1_n_1,
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => Q(1),
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '1',
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_tmp_product_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_tmp_product_OVERFLOW_UNCONNECTED,
      P(47) => tmp_product_n_59,
      P(46) => tmp_product_n_60,
      P(45) => tmp_product_n_61,
      P(44) => tmp_product_n_62,
      P(43) => tmp_product_n_63,
      P(42) => tmp_product_n_64,
      P(41) => tmp_product_n_65,
      P(40) => tmp_product_n_66,
      P(39) => tmp_product_n_67,
      P(38) => tmp_product_n_68,
      P(37) => tmp_product_n_69,
      P(36) => tmp_product_n_70,
      P(35) => tmp_product_n_71,
      P(34) => tmp_product_n_72,
      P(33) => tmp_product_n_73,
      P(32) => tmp_product_n_74,
      P(31) => tmp_product_n_75,
      P(30) => tmp_product_n_76,
      P(29) => tmp_product_n_77,
      P(28) => tmp_product_n_78,
      P(27) => tmp_product_n_79,
      P(26) => tmp_product_n_80,
      P(25) => tmp_product_n_81,
      P(24) => tmp_product_n_82,
      P(23) => tmp_product_n_83,
      P(22) => tmp_product_n_84,
      P(21) => tmp_product_n_85,
      P(20) => tmp_product_n_86,
      P(19) => tmp_product_n_87,
      P(18) => tmp_product_n_88,
      P(17) => tmp_product_n_89,
      P(16) => tmp_product_n_90,
      P(15) => tmp_product_n_91,
      P(14) => tmp_product_n_92,
      P(13) => tmp_product_n_93,
      P(12) => tmp_product_n_94,
      P(11) => tmp_product_n_95,
      P(10) => tmp_product_n_96,
      P(9) => tmp_product_n_97,
      P(8) => tmp_product_n_98,
      P(7) => tmp_product_n_99,
      P(6) => tmp_product_n_100,
      P(5) => tmp_product_n_101,
      P(4) => tmp_product_n_102,
      P(3) => tmp_product_n_103,
      P(2) => tmp_product_n_104,
      P(1) => tmp_product_n_105,
      P(0) => tmp_product_n_106,
      PATTERNBDETECT => NLW_tmp_product_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_tmp_product_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => tmp_product_n_107,
      PCOUT(46) => tmp_product_n_108,
      PCOUT(45) => tmp_product_n_109,
      PCOUT(44) => tmp_product_n_110,
      PCOUT(43) => tmp_product_n_111,
      PCOUT(42) => tmp_product_n_112,
      PCOUT(41) => tmp_product_n_113,
      PCOUT(40) => tmp_product_n_114,
      PCOUT(39) => tmp_product_n_115,
      PCOUT(38) => tmp_product_n_116,
      PCOUT(37) => tmp_product_n_117,
      PCOUT(36) => tmp_product_n_118,
      PCOUT(35) => tmp_product_n_119,
      PCOUT(34) => tmp_product_n_120,
      PCOUT(33) => tmp_product_n_121,
      PCOUT(32) => tmp_product_n_122,
      PCOUT(31) => tmp_product_n_123,
      PCOUT(30) => tmp_product_n_124,
      PCOUT(29) => tmp_product_n_125,
      PCOUT(28) => tmp_product_n_126,
      PCOUT(27) => tmp_product_n_127,
      PCOUT(26) => tmp_product_n_128,
      PCOUT(25) => tmp_product_n_129,
      PCOUT(24) => tmp_product_n_130,
      PCOUT(23) => tmp_product_n_131,
      PCOUT(22) => tmp_product_n_132,
      PCOUT(21) => tmp_product_n_133,
      PCOUT(20) => tmp_product_n_134,
      PCOUT(19) => tmp_product_n_135,
      PCOUT(18) => tmp_product_n_136,
      PCOUT(17) => tmp_product_n_137,
      PCOUT(16) => tmp_product_n_138,
      PCOUT(15) => tmp_product_n_139,
      PCOUT(14) => tmp_product_n_140,
      PCOUT(13) => tmp_product_n_141,
      PCOUT(12) => tmp_product_n_142,
      PCOUT(11) => tmp_product_n_143,
      PCOUT(10) => tmp_product_n_144,
      PCOUT(9) => tmp_product_n_145,
      PCOUT(8) => tmp_product_n_146,
      PCOUT(7) => tmp_product_n_147,
      PCOUT(6) => tmp_product_n_148,
      PCOUT(5) => tmp_product_n_149,
      PCOUT(4) => tmp_product_n_150,
      PCOUT(3) => tmp_product_n_151,
      PCOUT(2) => tmp_product_n_152,
      PCOUT(1) => tmp_product_n_153,
      PCOUT(0) => tmp_product_n_154,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_tmp_product_UNDERFLOW_UNCONNECTED
    );
\tmp_product__0\: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 17) => B"0000000000000",
      A(16 downto 0) => c_q0(16 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29) => \tmp_product__0_n_25\,
      ACOUT(28) => \tmp_product__0_n_26\,
      ACOUT(27) => \tmp_product__0_n_27\,
      ACOUT(26) => \tmp_product__0_n_28\,
      ACOUT(25) => \tmp_product__0_n_29\,
      ACOUT(24) => \tmp_product__0_n_30\,
      ACOUT(23) => \tmp_product__0_n_31\,
      ACOUT(22) => \tmp_product__0_n_32\,
      ACOUT(21) => \tmp_product__0_n_33\,
      ACOUT(20) => \tmp_product__0_n_34\,
      ACOUT(19) => \tmp_product__0_n_35\,
      ACOUT(18) => \tmp_product__0_n_36\,
      ACOUT(17) => \tmp_product__0_n_37\,
      ACOUT(16) => \tmp_product__0_n_38\,
      ACOUT(15) => \tmp_product__0_n_39\,
      ACOUT(14) => \tmp_product__0_n_40\,
      ACOUT(13) => \tmp_product__0_n_41\,
      ACOUT(12) => \tmp_product__0_n_42\,
      ACOUT(11) => \tmp_product__0_n_43\,
      ACOUT(10) => \tmp_product__0_n_44\,
      ACOUT(9) => \tmp_product__0_n_45\,
      ACOUT(8) => \tmp_product__0_n_46\,
      ACOUT(7) => \tmp_product__0_n_47\,
      ACOUT(6) => \tmp_product__0_n_48\,
      ACOUT(5) => \tmp_product__0_n_49\,
      ACOUT(4) => \tmp_product__0_n_50\,
      ACOUT(3) => \tmp_product__0_n_51\,
      ACOUT(2) => \tmp_product__0_n_52\,
      ACOUT(1) => \tmp_product__0_n_53\,
      ACOUT(0) => \tmp_product__0_n_54\,
      ALUMODE(3 downto 0) => B"0000",
      B(17) => '0',
      B(16 downto 0) => p_1_in(16 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => \NLW_tmp_product__0_BCOUT_UNCONNECTED\(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => \NLW_tmp_product__0_CARRYCASCOUT_UNCONNECTED\,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => \NLW_tmp_product__0_CARRYOUT_UNCONNECTED\(3 downto 0),
      CEA1 => '0',
      CEA2 => Q(1),
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => tmp_product_i_1_n_1,
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => ap_clk,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => \NLW_tmp_product__0_MULTSIGNOUT_UNCONNECTED\,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => \NLW_tmp_product__0_OVERFLOW_UNCONNECTED\,
      P(47) => \tmp_product__0_n_59\,
      P(46) => \tmp_product__0_n_60\,
      P(45) => \tmp_product__0_n_61\,
      P(44) => \tmp_product__0_n_62\,
      P(43) => \tmp_product__0_n_63\,
      P(42) => \tmp_product__0_n_64\,
      P(41) => \tmp_product__0_n_65\,
      P(40) => \tmp_product__0_n_66\,
      P(39) => \tmp_product__0_n_67\,
      P(38) => \tmp_product__0_n_68\,
      P(37) => \tmp_product__0_n_69\,
      P(36) => \tmp_product__0_n_70\,
      P(35) => \tmp_product__0_n_71\,
      P(34) => \tmp_product__0_n_72\,
      P(33) => \tmp_product__0_n_73\,
      P(32) => \tmp_product__0_n_74\,
      P(31) => \tmp_product__0_n_75\,
      P(30) => \tmp_product__0_n_76\,
      P(29) => \tmp_product__0_n_77\,
      P(28) => \tmp_product__0_n_78\,
      P(27) => \tmp_product__0_n_79\,
      P(26) => \tmp_product__0_n_80\,
      P(25) => \tmp_product__0_n_81\,
      P(24) => \tmp_product__0_n_82\,
      P(23) => \tmp_product__0_n_83\,
      P(22) => \tmp_product__0_n_84\,
      P(21) => \tmp_product__0_n_85\,
      P(20) => \tmp_product__0_n_86\,
      P(19) => \tmp_product__0_n_87\,
      P(18) => \tmp_product__0_n_88\,
      P(17) => \tmp_product__0_n_89\,
      P(16) => \tmp_product__0_n_90\,
      P(15) => \tmp_product__0_n_91\,
      P(14) => \tmp_product__0_n_92\,
      P(13) => \tmp_product__0_n_93\,
      P(12) => \tmp_product__0_n_94\,
      P(11) => \tmp_product__0_n_95\,
      P(10) => \tmp_product__0_n_96\,
      P(9) => \tmp_product__0_n_97\,
      P(8) => \tmp_product__0_n_98\,
      P(7) => \tmp_product__0_n_99\,
      P(6) => \tmp_product__0_n_100\,
      P(5) => \tmp_product__0_n_101\,
      P(4) => \tmp_product__0_n_102\,
      P(3) => \tmp_product__0_n_103\,
      P(2) => \tmp_product__0_n_104\,
      P(1) => \tmp_product__0_n_105\,
      P(0) => \tmp_product__0_n_106\,
      PATTERNBDETECT => \NLW_tmp_product__0_PATTERNBDETECT_UNCONNECTED\,
      PATTERNDETECT => \NLW_tmp_product__0_PATTERNDETECT_UNCONNECTED\,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47) => \tmp_product__0_n_107\,
      PCOUT(46) => \tmp_product__0_n_108\,
      PCOUT(45) => \tmp_product__0_n_109\,
      PCOUT(44) => \tmp_product__0_n_110\,
      PCOUT(43) => \tmp_product__0_n_111\,
      PCOUT(42) => \tmp_product__0_n_112\,
      PCOUT(41) => \tmp_product__0_n_113\,
      PCOUT(40) => \tmp_product__0_n_114\,
      PCOUT(39) => \tmp_product__0_n_115\,
      PCOUT(38) => \tmp_product__0_n_116\,
      PCOUT(37) => \tmp_product__0_n_117\,
      PCOUT(36) => \tmp_product__0_n_118\,
      PCOUT(35) => \tmp_product__0_n_119\,
      PCOUT(34) => \tmp_product__0_n_120\,
      PCOUT(33) => \tmp_product__0_n_121\,
      PCOUT(32) => \tmp_product__0_n_122\,
      PCOUT(31) => \tmp_product__0_n_123\,
      PCOUT(30) => \tmp_product__0_n_124\,
      PCOUT(29) => \tmp_product__0_n_125\,
      PCOUT(28) => \tmp_product__0_n_126\,
      PCOUT(27) => \tmp_product__0_n_127\,
      PCOUT(26) => \tmp_product__0_n_128\,
      PCOUT(25) => \tmp_product__0_n_129\,
      PCOUT(24) => \tmp_product__0_n_130\,
      PCOUT(23) => \tmp_product__0_n_131\,
      PCOUT(22) => \tmp_product__0_n_132\,
      PCOUT(21) => \tmp_product__0_n_133\,
      PCOUT(20) => \tmp_product__0_n_134\,
      PCOUT(19) => \tmp_product__0_n_135\,
      PCOUT(18) => \tmp_product__0_n_136\,
      PCOUT(17) => \tmp_product__0_n_137\,
      PCOUT(16) => \tmp_product__0_n_138\,
      PCOUT(15) => \tmp_product__0_n_139\,
      PCOUT(14) => \tmp_product__0_n_140\,
      PCOUT(13) => \tmp_product__0_n_141\,
      PCOUT(12) => \tmp_product__0_n_142\,
      PCOUT(11) => \tmp_product__0_n_143\,
      PCOUT(10) => \tmp_product__0_n_144\,
      PCOUT(9) => \tmp_product__0_n_145\,
      PCOUT(8) => \tmp_product__0_n_146\,
      PCOUT(7) => \tmp_product__0_n_147\,
      PCOUT(6) => \tmp_product__0_n_148\,
      PCOUT(5) => \tmp_product__0_n_149\,
      PCOUT(4) => \tmp_product__0_n_150\,
      PCOUT(3) => \tmp_product__0_n_151\,
      PCOUT(2) => \tmp_product__0_n_152\,
      PCOUT(1) => \tmp_product__0_n_153\,
      PCOUT(0) => \tmp_product__0_n_154\,
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => \NLW_tmp_product__0_UNDERFLOW_UNCONNECTED\
    );
tmp_product_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(0),
      I2 => shift_reg_address01,
      O => tmp_product_i_1_n_1
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_fir_shift_reg_ram is
  port (
    p_1_in : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \ap_CS_fsm_reg[1]\ : out STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    icmp_ln61_reg_189 : in STD_LOGIC;
    c_address0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q0_reg[0]_0\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_fir_shift_reg_ram : entity is "fir_shift_reg_ram";
end bd_0_hls_inst_0_fir_shift_reg_ram;

architecture STRUCTURE of bd_0_hls_inst_0_fir_shift_reg_ram is
  signal address0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^ap_cs_fsm_reg[1]\ : STD_LOGIC;
  signal ce0 : STD_LOGIC;
  signal d0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal p_1_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal q0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \q0[0]_i_1_n_1\ : STD_LOGIC;
  signal \q0[10]_i_1_n_1\ : STD_LOGIC;
  signal \q0[11]_i_1_n_1\ : STD_LOGIC;
  signal \q0[12]_i_1_n_1\ : STD_LOGIC;
  signal \q0[13]_i_1_n_1\ : STD_LOGIC;
  signal \q0[14]_i_1_n_1\ : STD_LOGIC;
  signal \q0[15]_i_1_n_1\ : STD_LOGIC;
  signal \q0[16]_i_1_n_1\ : STD_LOGIC;
  signal \q0[17]_i_1_n_1\ : STD_LOGIC;
  signal \q0[18]_i_1_n_1\ : STD_LOGIC;
  signal \q0[19]_i_1_n_1\ : STD_LOGIC;
  signal \q0[1]_i_1_n_1\ : STD_LOGIC;
  signal \q0[20]_i_1_n_1\ : STD_LOGIC;
  signal \q0[21]_i_1_n_1\ : STD_LOGIC;
  signal \q0[22]_i_1_n_1\ : STD_LOGIC;
  signal \q0[23]_i_1_n_1\ : STD_LOGIC;
  signal \q0[24]_i_1_n_1\ : STD_LOGIC;
  signal \q0[25]_i_1_n_1\ : STD_LOGIC;
  signal \q0[26]_i_1_n_1\ : STD_LOGIC;
  signal \q0[27]_i_1_n_1\ : STD_LOGIC;
  signal \q0[28]_i_1_n_1\ : STD_LOGIC;
  signal \q0[29]_i_1_n_1\ : STD_LOGIC;
  signal \q0[2]_i_1_n_1\ : STD_LOGIC;
  signal \q0[30]_i_1_n_1\ : STD_LOGIC;
  signal \q0[31]_i_2_n_1\ : STD_LOGIC;
  signal \q0[3]_i_1_n_1\ : STD_LOGIC;
  signal \q0[4]_i_1_n_1\ : STD_LOGIC;
  signal \q0[5]_i_1_n_1\ : STD_LOGIC;
  signal \q0[6]_i_1_n_1\ : STD_LOGIC;
  signal \q0[7]_i_1_n_1\ : STD_LOGIC;
  signal \q0[8]_i_1_n_1\ : STD_LOGIC;
  signal \q0[9]_i_1_n_1\ : STD_LOGIC;
  signal ram_reg_0_15_0_0_i_8_n_1 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_0_0 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_10_10 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_11_11 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_12_12 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_13_13 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_14_14 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_15_15 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_16_16 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_17_17 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_18_18 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_19_19 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_1_1 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_20_20 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_21_21 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_22_22 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_23_23 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_24_24 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_25_25 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_26_26 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_27_27 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_28_28 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_29_29 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_2_2 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_30_30 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_31_31 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_3_3 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_4_4 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_5_5 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_6_6 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_7_7 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_8_8 : label is "RAM16X1S";
  attribute XILINX_LEGACY_PRIM of ram_reg_0_15_9_9 : label is "RAM16X1S";
begin
  \ap_CS_fsm_reg[1]\ <= \^ap_cs_fsm_reg[1]\;
p_tmp_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(31),
      I1 => x(31),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(31)
    );
p_tmp_reg_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(22),
      I1 => x(22),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(22)
    );
p_tmp_reg_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(21),
      I1 => x(21),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(21)
    );
p_tmp_reg_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(20),
      I1 => x(20),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(20)
    );
p_tmp_reg_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(19),
      I1 => x(19),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(19)
    );
p_tmp_reg_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(18),
      I1 => x(18),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(18)
    );
p_tmp_reg_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(17),
      I1 => x(17),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(17)
    );
p_tmp_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(30),
      I1 => x(30),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(30)
    );
p_tmp_reg_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(29),
      I1 => x(29),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(29)
    );
p_tmp_reg_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(28),
      I1 => x(28),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(28)
    );
p_tmp_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(27),
      I1 => x(27),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(27)
    );
p_tmp_reg_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(26),
      I1 => x(26),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(26)
    );
p_tmp_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(25),
      I1 => x(25),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(25)
    );
p_tmp_reg_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(24),
      I1 => x(24),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(24)
    );
p_tmp_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(23),
      I1 => x(23),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(23)
    );
\q0[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(0),
      I4 => x(0),
      I5 => p_1_out(0),
      O => \q0[0]_i_1_n_1\
    );
\q0[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(10),
      I4 => x(10),
      I5 => p_1_out(10),
      O => \q0[10]_i_1_n_1\
    );
\q0[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(11),
      I4 => x(11),
      I5 => p_1_out(11),
      O => \q0[11]_i_1_n_1\
    );
\q0[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(12),
      I4 => x(12),
      I5 => p_1_out(12),
      O => \q0[12]_i_1_n_1\
    );
\q0[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(13),
      I4 => x(13),
      I5 => p_1_out(13),
      O => \q0[13]_i_1_n_1\
    );
\q0[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(14),
      I4 => x(14),
      I5 => p_1_out(14),
      O => \q0[14]_i_1_n_1\
    );
\q0[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(15),
      I4 => x(15),
      I5 => p_1_out(15),
      O => \q0[15]_i_1_n_1\
    );
\q0[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(16),
      I4 => x(16),
      I5 => p_1_out(16),
      O => \q0[16]_i_1_n_1\
    );
\q0[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(17),
      I4 => x(17),
      I5 => p_1_out(17),
      O => \q0[17]_i_1_n_1\
    );
\q0[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(18),
      I4 => x(18),
      I5 => p_1_out(18),
      O => \q0[18]_i_1_n_1\
    );
\q0[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(19),
      I4 => x(19),
      I5 => p_1_out(19),
      O => \q0[19]_i_1_n_1\
    );
\q0[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(1),
      I4 => x(1),
      I5 => p_1_out(1),
      O => \q0[1]_i_1_n_1\
    );
\q0[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(20),
      I4 => x(20),
      I5 => p_1_out(20),
      O => \q0[20]_i_1_n_1\
    );
\q0[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(21),
      I4 => x(21),
      I5 => p_1_out(21),
      O => \q0[21]_i_1_n_1\
    );
\q0[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(22),
      I4 => x(22),
      I5 => p_1_out(22),
      O => \q0[22]_i_1_n_1\
    );
\q0[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(23),
      I4 => x(23),
      I5 => p_1_out(23),
      O => \q0[23]_i_1_n_1\
    );
\q0[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(24),
      I4 => x(24),
      I5 => p_1_out(24),
      O => \q0[24]_i_1_n_1\
    );
\q0[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(25),
      I4 => x(25),
      I5 => p_1_out(25),
      O => \q0[25]_i_1_n_1\
    );
\q0[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(26),
      I4 => x(26),
      I5 => p_1_out(26),
      O => \q0[26]_i_1_n_1\
    );
\q0[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(27),
      I4 => x(27),
      I5 => p_1_out(27),
      O => \q0[27]_i_1_n_1\
    );
\q0[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(28),
      I4 => x(28),
      I5 => p_1_out(28),
      O => \q0[28]_i_1_n_1\
    );
\q0[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(29),
      I4 => x(29),
      I5 => p_1_out(29),
      O => \q0[29]_i_1_n_1\
    );
\q0[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(2),
      I4 => x(2),
      I5 => p_1_out(2),
      O => \q0[2]_i_1_n_1\
    );
\q0[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(30),
      I4 => x(30),
      I5 => p_1_out(30),
      O => \q0[30]_i_1_n_1\
    );
\q0[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => Q(1),
      I1 => \q0_reg[0]_0\(4),
      I2 => Q(0),
      O => ce0
    );
\q0[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(31),
      I4 => x(31),
      I5 => p_1_out(31),
      O => \q0[31]_i_2_n_1\
    );
\q0[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(3),
      I4 => x(3),
      I5 => p_1_out(3),
      O => \q0[3]_i_1_n_1\
    );
\q0[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(4),
      I4 => x(4),
      I5 => p_1_out(4),
      O => \q0[4]_i_1_n_1\
    );
\q0[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(5),
      I4 => x(5),
      I5 => p_1_out(5),
      O => \q0[5]_i_1_n_1\
    );
\q0[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(6),
      I4 => x(6),
      I5 => p_1_out(6),
      O => \q0[6]_i_1_n_1\
    );
\q0[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(7),
      I4 => x(7),
      I5 => p_1_out(7),
      O => \q0[7]_i_1_n_1\
    );
\q0[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(8),
      I4 => x(8),
      I5 => p_1_out(8),
      O => \q0[8]_i_1_n_1\
    );
\q0[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3BCF0BF430C400"
    )
        port map (
      I0 => icmp_ln61_reg_189,
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => q0(9),
      I4 => x(9),
      I5 => p_1_out(9),
      O => \q0[9]_i_1_n_1\
    );
\q0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[0]_i_1_n_1\,
      Q => q0(0),
      R => '0'
    );
\q0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[10]_i_1_n_1\,
      Q => q0(10),
      R => '0'
    );
\q0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[11]_i_1_n_1\,
      Q => q0(11),
      R => '0'
    );
\q0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[12]_i_1_n_1\,
      Q => q0(12),
      R => '0'
    );
\q0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[13]_i_1_n_1\,
      Q => q0(13),
      R => '0'
    );
\q0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[14]_i_1_n_1\,
      Q => q0(14),
      R => '0'
    );
\q0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[15]_i_1_n_1\,
      Q => q0(15),
      R => '0'
    );
\q0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[16]_i_1_n_1\,
      Q => q0(16),
      R => '0'
    );
\q0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[17]_i_1_n_1\,
      Q => q0(17),
      R => '0'
    );
\q0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[18]_i_1_n_1\,
      Q => q0(18),
      R => '0'
    );
\q0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[19]_i_1_n_1\,
      Q => q0(19),
      R => '0'
    );
\q0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[1]_i_1_n_1\,
      Q => q0(1),
      R => '0'
    );
\q0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[20]_i_1_n_1\,
      Q => q0(20),
      R => '0'
    );
\q0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[21]_i_1_n_1\,
      Q => q0(21),
      R => '0'
    );
\q0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[22]_i_1_n_1\,
      Q => q0(22),
      R => '0'
    );
\q0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[23]_i_1_n_1\,
      Q => q0(23),
      R => '0'
    );
\q0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[24]_i_1_n_1\,
      Q => q0(24),
      R => '0'
    );
\q0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[25]_i_1_n_1\,
      Q => q0(25),
      R => '0'
    );
\q0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[26]_i_1_n_1\,
      Q => q0(26),
      R => '0'
    );
\q0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[27]_i_1_n_1\,
      Q => q0(27),
      R => '0'
    );
\q0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[28]_i_1_n_1\,
      Q => q0(28),
      R => '0'
    );
\q0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[29]_i_1_n_1\,
      Q => q0(29),
      R => '0'
    );
\q0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[2]_i_1_n_1\,
      Q => q0(2),
      R => '0'
    );
\q0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[30]_i_1_n_1\,
      Q => q0(30),
      R => '0'
    );
\q0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[31]_i_2_n_1\,
      Q => q0(31),
      R => '0'
    );
\q0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[3]_i_1_n_1\,
      Q => q0(3),
      R => '0'
    );
\q0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[4]_i_1_n_1\,
      Q => q0(4),
      R => '0'
    );
\q0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[5]_i_1_n_1\,
      Q => q0(5),
      R => '0'
    );
\q0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[6]_i_1_n_1\,
      Q => q0(6),
      R => '0'
    );
\q0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[7]_i_1_n_1\,
      Q => q0(7),
      R => '0'
    );
\q0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[8]_i_1_n_1\,
      Q => q0(8),
      R => '0'
    );
\q0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ce0,
      D => \q0[9]_i_1_n_1\,
      Q => q0(9),
      R => '0'
    );
ram_reg_0_15_0_0: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(0),
      O => p_1_out(0),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_0_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(0),
      I1 => x(0),
      I2 => Q(1),
      O => d0(0)
    );
ram_reg_0_15_0_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BABA00AA"
    )
        port map (
      I0 => Q(1),
      I1 => \q0_reg[0]_0\(4),
      I2 => Q(0),
      I3 => icmp_ln61_reg_189,
      I4 => \^ap_cs_fsm_reg[1]\,
      O => p_0_in
    );
ram_reg_0_15_0_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"888B"
    )
        port map (
      I0 => c_address0(0),
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => \q0_reg[0]_0\(0),
      O => address0(0)
    );
ram_reg_0_15_0_0_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8B88888B"
    )
        port map (
      I0 => c_address0(1),
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => \q0_reg[0]_0\(1),
      I4 => \q0_reg[0]_0\(0),
      O => address0(1)
    );
ram_reg_0_15_0_0_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8B888B888B88888B"
    )
        port map (
      I0 => c_address0(2),
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => \q0_reg[0]_0\(2),
      I4 => \q0_reg[0]_0\(0),
      I5 => \q0_reg[0]_0\(1),
      O => address0(2)
    );
ram_reg_0_15_0_0_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8B888B888B88888B"
    )
        port map (
      I0 => c_address0(3),
      I1 => Q(1),
      I2 => \^ap_cs_fsm_reg[1]\,
      I3 => \q0_reg[0]_0\(3),
      I4 => ram_reg_0_15_0_0_i_8_n_1,
      I5 => \q0_reg[0]_0\(2),
      O => address0(3)
    );
ram_reg_0_15_0_0_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => Q(0),
      I1 => \q0_reg[0]_0\(2),
      I2 => \q0_reg[0]_0\(1),
      I3 => \q0_reg[0]_0\(0),
      I4 => \q0_reg[0]_0\(4),
      I5 => \q0_reg[0]_0\(3),
      O => \^ap_cs_fsm_reg[1]\
    );
ram_reg_0_15_0_0_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \q0_reg[0]_0\(0),
      I1 => \q0_reg[0]_0\(1),
      O => ram_reg_0_15_0_0_i_8_n_1
    );
ram_reg_0_15_10_10: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(10),
      O => p_1_out(10),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_10_10_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(10),
      I1 => x(10),
      I2 => Q(1),
      O => d0(10)
    );
ram_reg_0_15_11_11: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(11),
      O => p_1_out(11),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_11_11_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(11),
      I1 => x(11),
      I2 => Q(1),
      O => d0(11)
    );
ram_reg_0_15_12_12: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(12),
      O => p_1_out(12),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_12_12_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(12),
      I1 => x(12),
      I2 => Q(1),
      O => d0(12)
    );
ram_reg_0_15_13_13: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(13),
      O => p_1_out(13),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_13_13_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(13),
      I1 => x(13),
      I2 => Q(1),
      O => d0(13)
    );
ram_reg_0_15_14_14: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(14),
      O => p_1_out(14),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_14_14_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(14),
      I1 => x(14),
      I2 => Q(1),
      O => d0(14)
    );
ram_reg_0_15_15_15: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(15),
      O => p_1_out(15),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_15_15_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(15),
      I1 => x(15),
      I2 => Q(1),
      O => d0(15)
    );
ram_reg_0_15_16_16: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(16),
      O => p_1_out(16),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_16_16_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(16),
      I1 => x(16),
      I2 => Q(1),
      O => d0(16)
    );
ram_reg_0_15_17_17: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(17),
      O => p_1_out(17),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_17_17_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(17),
      I1 => x(17),
      I2 => Q(1),
      O => d0(17)
    );
ram_reg_0_15_18_18: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(18),
      O => p_1_out(18),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_18_18_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(18),
      I1 => x(18),
      I2 => Q(1),
      O => d0(18)
    );
ram_reg_0_15_19_19: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(19),
      O => p_1_out(19),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_19_19_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(19),
      I1 => x(19),
      I2 => Q(1),
      O => d0(19)
    );
ram_reg_0_15_1_1: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(1),
      O => p_1_out(1),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_1_1_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(1),
      I1 => x(1),
      I2 => Q(1),
      O => d0(1)
    );
ram_reg_0_15_20_20: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(20),
      O => p_1_out(20),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_20_20_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(20),
      I1 => x(20),
      I2 => Q(1),
      O => d0(20)
    );
ram_reg_0_15_21_21: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(21),
      O => p_1_out(21),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_21_21_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(21),
      I1 => x(21),
      I2 => Q(1),
      O => d0(21)
    );
ram_reg_0_15_22_22: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(22),
      O => p_1_out(22),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_22_22_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(22),
      I1 => x(22),
      I2 => Q(1),
      O => d0(22)
    );
ram_reg_0_15_23_23: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(23),
      O => p_1_out(23),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_23_23_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(23),
      I1 => x(23),
      I2 => Q(1),
      O => d0(23)
    );
ram_reg_0_15_24_24: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(24),
      O => p_1_out(24),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_24_24_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(24),
      I1 => x(24),
      I2 => Q(1),
      O => d0(24)
    );
ram_reg_0_15_25_25: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(25),
      O => p_1_out(25),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_25_25_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(25),
      I1 => x(25),
      I2 => Q(1),
      O => d0(25)
    );
ram_reg_0_15_26_26: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(26),
      O => p_1_out(26),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_26_26_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(26),
      I1 => x(26),
      I2 => Q(1),
      O => d0(26)
    );
ram_reg_0_15_27_27: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(27),
      O => p_1_out(27),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_27_27_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(27),
      I1 => x(27),
      I2 => Q(1),
      O => d0(27)
    );
ram_reg_0_15_28_28: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(28),
      O => p_1_out(28),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_28_28_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(28),
      I1 => x(28),
      I2 => Q(1),
      O => d0(28)
    );
ram_reg_0_15_29_29: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(29),
      O => p_1_out(29),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_29_29_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(29),
      I1 => x(29),
      I2 => Q(1),
      O => d0(29)
    );
ram_reg_0_15_2_2: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(2),
      O => p_1_out(2),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_2_2_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(2),
      I1 => x(2),
      I2 => Q(1),
      O => d0(2)
    );
ram_reg_0_15_30_30: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(30),
      O => p_1_out(30),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_30_30_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(30),
      I1 => x(30),
      I2 => Q(1),
      O => d0(30)
    );
ram_reg_0_15_31_31: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(31),
      O => p_1_out(31),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_31_31_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(31),
      I1 => x(31),
      I2 => Q(1),
      O => d0(31)
    );
ram_reg_0_15_3_3: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(3),
      O => p_1_out(3),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_3_3_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(3),
      I1 => x(3),
      I2 => Q(1),
      O => d0(3)
    );
ram_reg_0_15_4_4: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(4),
      O => p_1_out(4),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_4_4_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(4),
      I1 => x(4),
      I2 => Q(1),
      O => d0(4)
    );
ram_reg_0_15_5_5: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(5),
      O => p_1_out(5),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_5_5_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(5),
      I1 => x(5),
      I2 => Q(1),
      O => d0(5)
    );
ram_reg_0_15_6_6: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(6),
      O => p_1_out(6),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_6_6_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(6),
      I1 => x(6),
      I2 => Q(1),
      O => d0(6)
    );
ram_reg_0_15_7_7: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(7),
      O => p_1_out(7),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_7_7_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(7),
      I1 => x(7),
      I2 => Q(1),
      O => d0(7)
    );
ram_reg_0_15_8_8: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(8),
      O => p_1_out(8),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_8_8_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(8),
      I1 => x(8),
      I2 => Q(1),
      O => d0(8)
    );
ram_reg_0_15_9_9: unisim.vcomponents.RAM32X1S
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => address0(0),
      A1 => address0(1),
      A2 => address0(2),
      A3 => address0(3),
      A4 => '0',
      D => d0(9),
      O => p_1_out(9),
      WCLK => ap_clk,
      WE => p_0_in
    );
ram_reg_0_15_9_9_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => q0(9),
      I1 => x(9),
      I2 => Q(1),
      O => d0(9)
    );
tmp_product_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(8),
      I1 => x(8),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(8)
    );
tmp_product_i_11: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(7),
      I1 => x(7),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(7)
    );
tmp_product_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(6),
      I1 => x(6),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(6)
    );
tmp_product_i_13: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(5),
      I1 => x(5),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(5)
    );
tmp_product_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(4),
      I1 => x(4),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(4)
    );
tmp_product_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(3),
      I1 => x(3),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(3)
    );
tmp_product_i_16: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(2),
      I1 => x(2),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(2)
    );
tmp_product_i_17: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(1),
      I1 => x(1),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(1)
    );
tmp_product_i_18: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(0),
      I1 => x(0),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(0)
    );
tmp_product_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(16),
      I1 => x(16),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(16)
    );
tmp_product_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(15),
      I1 => x(15),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(15)
    );
tmp_product_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(14),
      I1 => x(14),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(14)
    );
tmp_product_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(13),
      I1 => x(13),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(13)
    );
tmp_product_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(12),
      I1 => x(12),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(12)
    );
tmp_product_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(11),
      I1 => x(11),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(11)
    );
tmp_product_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(10),
      I1 => x(10),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(10)
    );
tmp_product_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CCAC"
    )
        port map (
      I0 => q0(9),
      I1 => x(9),
      I2 => Q(1),
      I3 => icmp_ln61_reg_189,
      O => p_1_in(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_fir_mul_32s_32s_3bkb is
  port (
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ap_clk : in STD_LOGIC;
    c_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    p_1_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    icmp_ln61_reg_189 : in STD_LOGIC;
    shift_reg_address01 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_fir_mul_32s_32s_3bkb : entity is "fir_mul_32s_32s_3bkb";
end bd_0_hls_inst_0_fir_mul_32s_32s_3bkb;

architecture STRUCTURE of bd_0_hls_inst_0_fir_mul_32s_32s_3bkb is
begin
fir_mul_32s_32s_3bkb_MulnS_0_U: entity work.bd_0_hls_inst_0_fir_mul_32s_32s_3bkb_MulnS_0
     port map (
      D(31 downto 0) => D(31 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      ap_clk => ap_clk,
      c_q0(31 downto 0) => c_q0(31 downto 0),
      icmp_ln61_reg_189 => icmp_ln61_reg_189,
      p_1_in(31 downto 0) => p_1_in(31 downto 0),
      shift_reg_address01 => shift_reg_address01
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_fir_shift_reg is
  port (
    p_1_in : out STD_LOGIC_VECTOR ( 31 downto 0 );
    shift_reg_address01 : out STD_LOGIC;
    x : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    icmp_ln61_reg_189 : in STD_LOGIC;
    c_address0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q0_reg[0]\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ap_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_fir_shift_reg : entity is "fir_shift_reg";
end bd_0_hls_inst_0_fir_shift_reg;

architecture STRUCTURE of bd_0_hls_inst_0_fir_shift_reg is
begin
fir_shift_reg_ram_U: entity work.bd_0_hls_inst_0_fir_shift_reg_ram
     port map (
      Q(1 downto 0) => Q(1 downto 0),
      \ap_CS_fsm_reg[1]\ => shift_reg_address01,
      ap_clk => ap_clk,
      c_address0(3 downto 0) => c_address0(3 downto 0),
      icmp_ln61_reg_189 => icmp_ln61_reg_189,
      p_1_in(31 downto 0) => p_1_in(31 downto 0),
      \q0_reg[0]_0\(4 downto 0) => \q0_reg[0]\(4 downto 0),
      x(31 downto 0) => x(31 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0_fir is
  port (
    ap_clk : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 31 downto 0 );
    y_ap_vld : out STD_LOGIC;
    c_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    c_ce0 : out STD_LOGIC;
    c_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    x : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of bd_0_hls_inst_0_fir : entity is "fir";
end bd_0_hls_inst_0_fir;

architecture STRUCTURE of bd_0_hls_inst_0_fir is
  signal acc_0_reg_91 : STD_LOGIC;
  signal \acc_0_reg_91[11]_i_2_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[11]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[11]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[11]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[15]_i_2_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[15]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[15]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[15]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[19]_i_2_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[19]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[19]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[19]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[23]_i_2_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[23]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[23]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[23]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[27]_i_2_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[27]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[27]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[27]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[31]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[31]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[31]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[31]_i_6_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[3]_i_2_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[3]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[3]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[3]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[7]_i_2_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[7]_i_3_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[7]_i_4_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91[7]_i_5_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[11]_i_1_n_8\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[15]_i_1_n_8\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[19]_i_1_n_8\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[23]_i_1_n_8\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[27]_i_1_n_8\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[31]_i_2_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[31]_i_2_n_8\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[3]_i_1_n_8\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \acc_0_reg_91_reg[7]_i_1_n_8\ : STD_LOGIC;
  signal \ap_CS_fsm[0]_i_2_n_1\ : STD_LOGIC;
  signal \ap_CS_fsm[2]_i_1_n_1\ : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_1_[0]\ : STD_LOGIC;
  signal \ap_CS_fsm_reg_n_1_[4]\ : STD_LOGIC;
  signal ap_CS_fsm_state2 : STD_LOGIC;
  signal ap_CS_fsm_state4 : STD_LOGIC;
  signal ap_CS_fsm_state6 : STD_LOGIC;
  signal ap_CS_fsm_state7 : STD_LOGIC;
  signal ap_NS_fsm : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^c_address0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^c_ce0\ : STD_LOGIC;
  signal \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\ : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal fir_mul_32s_32s_3bkb_U1_n_17 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_18 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_19 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_20 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_21 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_22 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_23 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_24 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_25 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_26 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_27 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_28 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_29 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_30 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_31 : STD_LOGIC;
  signal fir_mul_32s_32s_3bkb_U1_n_32 : STD_LOGIC;
  signal \i_0_reg_104_reg_n_1_[0]\ : STD_LOGIC;
  signal \i_0_reg_104_reg_n_1_[1]\ : STD_LOGIC;
  signal \i_0_reg_104_reg_n_1_[2]\ : STD_LOGIC;
  signal \i_0_reg_104_reg_n_1_[3]\ : STD_LOGIC;
  signal i_reg_208 : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \i_reg_208[0]_i_1_n_1\ : STD_LOGIC;
  signal \i_reg_208[1]_i_1_n_1\ : STD_LOGIC;
  signal \i_reg_208[2]_i_1_n_1\ : STD_LOGIC;
  signal \i_reg_208[3]_i_1_n_1\ : STD_LOGIC;
  signal \i_reg_208[4]_i_1_n_1\ : STD_LOGIC;
  signal icmp_ln61_fu_145_p2 : STD_LOGIC;
  signal icmp_ln61_reg_189 : STD_LOGIC;
  signal \icmp_ln61_reg_189[0]_i_1_n_1\ : STD_LOGIC;
  signal mul_ln68_reg_218 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal shift_reg_address01 : STD_LOGIC;
  signal tmp_fu_137_p30_in : STD_LOGIC;
  signal \^y\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^y_ap_vld\ : STD_LOGIC;
  signal \NLW_acc_0_reg_91_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ap_CS_fsm[0]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ap_CS_fsm[2]_i_1\ : label is "soft_lutpair1";
  attribute FSM_ENCODING : string;
  attribute FSM_ENCODING of \ap_CS_fsm_reg[0]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[1]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[2]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[3]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[4]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[5]\ : label is "none";
  attribute FSM_ENCODING of \ap_CS_fsm_reg[6]\ : label is "none";
  attribute SOFT_HLUTNM of ap_idle_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \i_reg_208[0]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \i_reg_208[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \i_reg_208[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \i_reg_208[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \i_reg_208[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \icmp_ln61_reg_189[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \icmp_ln61_reg_189[0]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of y_ap_vld_INST_0 : label is "soft_lutpair3";
begin
  ap_done <= \^y_ap_vld\;
  ap_ready <= \^y_ap_vld\;
  c_address0(3 downto 0) <= \^c_address0\(3 downto 0);
  c_ce0 <= \^c_ce0\;
  y(31 downto 0) <= \^y\(31 downto 0);
  y_ap_vld <= \^y_ap_vld\;
\acc_0_reg_91[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(11),
      I1 => \^y\(11),
      O => \acc_0_reg_91[11]_i_2_n_1\
    );
\acc_0_reg_91[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(10),
      I1 => \^y\(10),
      O => \acc_0_reg_91[11]_i_3_n_1\
    );
\acc_0_reg_91[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(9),
      I1 => \^y\(9),
      O => \acc_0_reg_91[11]_i_4_n_1\
    );
\acc_0_reg_91[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(8),
      I1 => \^y\(8),
      O => \acc_0_reg_91[11]_i_5_n_1\
    );
\acc_0_reg_91[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(15),
      I1 => \^y\(15),
      O => \acc_0_reg_91[15]_i_2_n_1\
    );
\acc_0_reg_91[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(14),
      I1 => \^y\(14),
      O => \acc_0_reg_91[15]_i_3_n_1\
    );
\acc_0_reg_91[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(13),
      I1 => \^y\(13),
      O => \acc_0_reg_91[15]_i_4_n_1\
    );
\acc_0_reg_91[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(12),
      I1 => \^y\(12),
      O => \acc_0_reg_91[15]_i_5_n_1\
    );
\acc_0_reg_91[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(19),
      I1 => \^y\(19),
      O => \acc_0_reg_91[19]_i_2_n_1\
    );
\acc_0_reg_91[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(18),
      I1 => \^y\(18),
      O => \acc_0_reg_91[19]_i_3_n_1\
    );
\acc_0_reg_91[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(17),
      I1 => \^y\(17),
      O => \acc_0_reg_91[19]_i_4_n_1\
    );
\acc_0_reg_91[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(16),
      I1 => \^y\(16),
      O => \acc_0_reg_91[19]_i_5_n_1\
    );
\acc_0_reg_91[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(23),
      I1 => \^y\(23),
      O => \acc_0_reg_91[23]_i_2_n_1\
    );
\acc_0_reg_91[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(22),
      I1 => \^y\(22),
      O => \acc_0_reg_91[23]_i_3_n_1\
    );
\acc_0_reg_91[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(21),
      I1 => \^y\(21),
      O => \acc_0_reg_91[23]_i_4_n_1\
    );
\acc_0_reg_91[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(20),
      I1 => \^y\(20),
      O => \acc_0_reg_91[23]_i_5_n_1\
    );
\acc_0_reg_91[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(27),
      I1 => \^y\(27),
      O => \acc_0_reg_91[27]_i_2_n_1\
    );
\acc_0_reg_91[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(26),
      I1 => \^y\(26),
      O => \acc_0_reg_91[27]_i_3_n_1\
    );
\acc_0_reg_91[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(25),
      I1 => \^y\(25),
      O => \acc_0_reg_91[27]_i_4_n_1\
    );
\acc_0_reg_91[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(24),
      I1 => \^y\(24),
      O => \acc_0_reg_91[27]_i_5_n_1\
    );
\acc_0_reg_91[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \ap_CS_fsm_reg_n_1_[0]\,
      I1 => ap_start,
      I2 => ap_CS_fsm_state7,
      O => acc_0_reg_91
    );
\acc_0_reg_91[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(31),
      I1 => \^y\(31),
      O => \acc_0_reg_91[31]_i_3_n_1\
    );
\acc_0_reg_91[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(30),
      I1 => \^y\(30),
      O => \acc_0_reg_91[31]_i_4_n_1\
    );
\acc_0_reg_91[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(29),
      I1 => \^y\(29),
      O => \acc_0_reg_91[31]_i_5_n_1\
    );
\acc_0_reg_91[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(28),
      I1 => \^y\(28),
      O => \acc_0_reg_91[31]_i_6_n_1\
    );
\acc_0_reg_91[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(3),
      I1 => \^y\(3),
      O => \acc_0_reg_91[3]_i_2_n_1\
    );
\acc_0_reg_91[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(2),
      I1 => \^y\(2),
      O => \acc_0_reg_91[3]_i_3_n_1\
    );
\acc_0_reg_91[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(1),
      I1 => \^y\(1),
      O => \acc_0_reg_91[3]_i_4_n_1\
    );
\acc_0_reg_91[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(0),
      I1 => \^y\(0),
      O => \acc_0_reg_91[3]_i_5_n_1\
    );
\acc_0_reg_91[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(7),
      I1 => \^y\(7),
      O => \acc_0_reg_91[7]_i_2_n_1\
    );
\acc_0_reg_91[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(6),
      I1 => \^y\(6),
      O => \acc_0_reg_91[7]_i_3_n_1\
    );
\acc_0_reg_91[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(5),
      I1 => \^y\(5),
      O => \acc_0_reg_91[7]_i_4_n_1\
    );
\acc_0_reg_91[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => mul_ln68_reg_218(4),
      I1 => \^y\(4),
      O => \acc_0_reg_91[7]_i_5_n_1\
    );
\acc_0_reg_91_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[3]_i_1_n_8\,
      Q => \^y\(0),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[11]_i_1_n_6\,
      Q => \^y\(10),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[11]_i_1_n_5\,
      Q => \^y\(11),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_0_reg_91_reg[7]_i_1_n_1\,
      CO(3) => \acc_0_reg_91_reg[11]_i_1_n_1\,
      CO(2) => \acc_0_reg_91_reg[11]_i_1_n_2\,
      CO(1) => \acc_0_reg_91_reg[11]_i_1_n_3\,
      CO(0) => \acc_0_reg_91_reg[11]_i_1_n_4\,
      CYINIT => '0',
      DI(3 downto 0) => mul_ln68_reg_218(11 downto 8),
      O(3) => \acc_0_reg_91_reg[11]_i_1_n_5\,
      O(2) => \acc_0_reg_91_reg[11]_i_1_n_6\,
      O(1) => \acc_0_reg_91_reg[11]_i_1_n_7\,
      O(0) => \acc_0_reg_91_reg[11]_i_1_n_8\,
      S(3) => \acc_0_reg_91[11]_i_2_n_1\,
      S(2) => \acc_0_reg_91[11]_i_3_n_1\,
      S(1) => \acc_0_reg_91[11]_i_4_n_1\,
      S(0) => \acc_0_reg_91[11]_i_5_n_1\
    );
\acc_0_reg_91_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[15]_i_1_n_8\,
      Q => \^y\(12),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[15]_i_1_n_7\,
      Q => \^y\(13),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[15]_i_1_n_6\,
      Q => \^y\(14),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[15]_i_1_n_5\,
      Q => \^y\(15),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_0_reg_91_reg[11]_i_1_n_1\,
      CO(3) => \acc_0_reg_91_reg[15]_i_1_n_1\,
      CO(2) => \acc_0_reg_91_reg[15]_i_1_n_2\,
      CO(1) => \acc_0_reg_91_reg[15]_i_1_n_3\,
      CO(0) => \acc_0_reg_91_reg[15]_i_1_n_4\,
      CYINIT => '0',
      DI(3 downto 0) => mul_ln68_reg_218(15 downto 12),
      O(3) => \acc_0_reg_91_reg[15]_i_1_n_5\,
      O(2) => \acc_0_reg_91_reg[15]_i_1_n_6\,
      O(1) => \acc_0_reg_91_reg[15]_i_1_n_7\,
      O(0) => \acc_0_reg_91_reg[15]_i_1_n_8\,
      S(3) => \acc_0_reg_91[15]_i_2_n_1\,
      S(2) => \acc_0_reg_91[15]_i_3_n_1\,
      S(1) => \acc_0_reg_91[15]_i_4_n_1\,
      S(0) => \acc_0_reg_91[15]_i_5_n_1\
    );
\acc_0_reg_91_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[19]_i_1_n_8\,
      Q => \^y\(16),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[19]_i_1_n_7\,
      Q => \^y\(17),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[19]_i_1_n_6\,
      Q => \^y\(18),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[19]_i_1_n_5\,
      Q => \^y\(19),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_0_reg_91_reg[15]_i_1_n_1\,
      CO(3) => \acc_0_reg_91_reg[19]_i_1_n_1\,
      CO(2) => \acc_0_reg_91_reg[19]_i_1_n_2\,
      CO(1) => \acc_0_reg_91_reg[19]_i_1_n_3\,
      CO(0) => \acc_0_reg_91_reg[19]_i_1_n_4\,
      CYINIT => '0',
      DI(3 downto 0) => mul_ln68_reg_218(19 downto 16),
      O(3) => \acc_0_reg_91_reg[19]_i_1_n_5\,
      O(2) => \acc_0_reg_91_reg[19]_i_1_n_6\,
      O(1) => \acc_0_reg_91_reg[19]_i_1_n_7\,
      O(0) => \acc_0_reg_91_reg[19]_i_1_n_8\,
      S(3) => \acc_0_reg_91[19]_i_2_n_1\,
      S(2) => \acc_0_reg_91[19]_i_3_n_1\,
      S(1) => \acc_0_reg_91[19]_i_4_n_1\,
      S(0) => \acc_0_reg_91[19]_i_5_n_1\
    );
\acc_0_reg_91_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[3]_i_1_n_7\,
      Q => \^y\(1),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[23]_i_1_n_8\,
      Q => \^y\(20),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[23]_i_1_n_7\,
      Q => \^y\(21),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[23]_i_1_n_6\,
      Q => \^y\(22),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[23]_i_1_n_5\,
      Q => \^y\(23),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_0_reg_91_reg[19]_i_1_n_1\,
      CO(3) => \acc_0_reg_91_reg[23]_i_1_n_1\,
      CO(2) => \acc_0_reg_91_reg[23]_i_1_n_2\,
      CO(1) => \acc_0_reg_91_reg[23]_i_1_n_3\,
      CO(0) => \acc_0_reg_91_reg[23]_i_1_n_4\,
      CYINIT => '0',
      DI(3 downto 0) => mul_ln68_reg_218(23 downto 20),
      O(3) => \acc_0_reg_91_reg[23]_i_1_n_5\,
      O(2) => \acc_0_reg_91_reg[23]_i_1_n_6\,
      O(1) => \acc_0_reg_91_reg[23]_i_1_n_7\,
      O(0) => \acc_0_reg_91_reg[23]_i_1_n_8\,
      S(3) => \acc_0_reg_91[23]_i_2_n_1\,
      S(2) => \acc_0_reg_91[23]_i_3_n_1\,
      S(1) => \acc_0_reg_91[23]_i_4_n_1\,
      S(0) => \acc_0_reg_91[23]_i_5_n_1\
    );
\acc_0_reg_91_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[27]_i_1_n_8\,
      Q => \^y\(24),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[27]_i_1_n_7\,
      Q => \^y\(25),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[27]_i_1_n_6\,
      Q => \^y\(26),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[27]_i_1_n_5\,
      Q => \^y\(27),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_0_reg_91_reg[23]_i_1_n_1\,
      CO(3) => \acc_0_reg_91_reg[27]_i_1_n_1\,
      CO(2) => \acc_0_reg_91_reg[27]_i_1_n_2\,
      CO(1) => \acc_0_reg_91_reg[27]_i_1_n_3\,
      CO(0) => \acc_0_reg_91_reg[27]_i_1_n_4\,
      CYINIT => '0',
      DI(3 downto 0) => mul_ln68_reg_218(27 downto 24),
      O(3) => \acc_0_reg_91_reg[27]_i_1_n_5\,
      O(2) => \acc_0_reg_91_reg[27]_i_1_n_6\,
      O(1) => \acc_0_reg_91_reg[27]_i_1_n_7\,
      O(0) => \acc_0_reg_91_reg[27]_i_1_n_8\,
      S(3) => \acc_0_reg_91[27]_i_2_n_1\,
      S(2) => \acc_0_reg_91[27]_i_3_n_1\,
      S(1) => \acc_0_reg_91[27]_i_4_n_1\,
      S(0) => \acc_0_reg_91[27]_i_5_n_1\
    );
\acc_0_reg_91_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[31]_i_2_n_8\,
      Q => \^y\(28),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[31]_i_2_n_7\,
      Q => \^y\(29),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[3]_i_1_n_6\,
      Q => \^y\(2),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[31]_i_2_n_6\,
      Q => \^y\(30),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[31]_i_2_n_5\,
      Q => \^y\(31),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_0_reg_91_reg[27]_i_1_n_1\,
      CO(3) => \NLW_acc_0_reg_91_reg[31]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \acc_0_reg_91_reg[31]_i_2_n_2\,
      CO(1) => \acc_0_reg_91_reg[31]_i_2_n_3\,
      CO(0) => \acc_0_reg_91_reg[31]_i_2_n_4\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => mul_ln68_reg_218(30 downto 28),
      O(3) => \acc_0_reg_91_reg[31]_i_2_n_5\,
      O(2) => \acc_0_reg_91_reg[31]_i_2_n_6\,
      O(1) => \acc_0_reg_91_reg[31]_i_2_n_7\,
      O(0) => \acc_0_reg_91_reg[31]_i_2_n_8\,
      S(3) => \acc_0_reg_91[31]_i_3_n_1\,
      S(2) => \acc_0_reg_91[31]_i_4_n_1\,
      S(1) => \acc_0_reg_91[31]_i_5_n_1\,
      S(0) => \acc_0_reg_91[31]_i_6_n_1\
    );
\acc_0_reg_91_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[3]_i_1_n_5\,
      Q => \^y\(3),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \acc_0_reg_91_reg[3]_i_1_n_1\,
      CO(2) => \acc_0_reg_91_reg[3]_i_1_n_2\,
      CO(1) => \acc_0_reg_91_reg[3]_i_1_n_3\,
      CO(0) => \acc_0_reg_91_reg[3]_i_1_n_4\,
      CYINIT => '0',
      DI(3 downto 0) => mul_ln68_reg_218(3 downto 0),
      O(3) => \acc_0_reg_91_reg[3]_i_1_n_5\,
      O(2) => \acc_0_reg_91_reg[3]_i_1_n_6\,
      O(1) => \acc_0_reg_91_reg[3]_i_1_n_7\,
      O(0) => \acc_0_reg_91_reg[3]_i_1_n_8\,
      S(3) => \acc_0_reg_91[3]_i_2_n_1\,
      S(2) => \acc_0_reg_91[3]_i_3_n_1\,
      S(1) => \acc_0_reg_91[3]_i_4_n_1\,
      S(0) => \acc_0_reg_91[3]_i_5_n_1\
    );
\acc_0_reg_91_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[7]_i_1_n_8\,
      Q => \^y\(4),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[7]_i_1_n_7\,
      Q => \^y\(5),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[7]_i_1_n_6\,
      Q => \^y\(6),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[7]_i_1_n_5\,
      Q => \^y\(7),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \acc_0_reg_91_reg[3]_i_1_n_1\,
      CO(3) => \acc_0_reg_91_reg[7]_i_1_n_1\,
      CO(2) => \acc_0_reg_91_reg[7]_i_1_n_2\,
      CO(1) => \acc_0_reg_91_reg[7]_i_1_n_3\,
      CO(0) => \acc_0_reg_91_reg[7]_i_1_n_4\,
      CYINIT => '0',
      DI(3 downto 0) => mul_ln68_reg_218(7 downto 4),
      O(3) => \acc_0_reg_91_reg[7]_i_1_n_5\,
      O(2) => \acc_0_reg_91_reg[7]_i_1_n_6\,
      O(1) => \acc_0_reg_91_reg[7]_i_1_n_7\,
      O(0) => \acc_0_reg_91_reg[7]_i_1_n_8\,
      S(3) => \acc_0_reg_91[7]_i_2_n_1\,
      S(2) => \acc_0_reg_91[7]_i_3_n_1\,
      S(1) => \acc_0_reg_91[7]_i_4_n_1\,
      S(0) => \acc_0_reg_91[7]_i_5_n_1\
    );
\acc_0_reg_91_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[11]_i_1_n_8\,
      Q => \^y\(8),
      R => acc_0_reg_91
    );
\acc_0_reg_91_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => \acc_0_reg_91_reg[11]_i_1_n_7\,
      Q => \^y\(9),
      R => acc_0_reg_91
    );
\ap_CS_fsm[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44444447"
    )
        port map (
      I0 => ap_start,
      I1 => \ap_CS_fsm_reg_n_1_[0]\,
      I2 => \^c_ce0\,
      I3 => ap_CS_fsm_state7,
      I4 => \ap_CS_fsm[0]_i_2_n_1\,
      O => ap_NS_fsm(0)
    );
\ap_CS_fsm[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => \ap_CS_fsm_reg_n_1_[4]\,
      I1 => ap_CS_fsm_state6,
      I2 => tmp_fu_137_p30_in,
      I3 => ap_CS_fsm_state2,
      I4 => ap_CS_fsm_state4,
      O => \ap_CS_fsm[0]_i_2_n_1\
    );
\ap_CS_fsm[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => ap_CS_fsm_state7,
      I1 => ap_start,
      I2 => \ap_CS_fsm_reg_n_1_[0]\,
      O => ap_NS_fsm(1)
    );
\ap_CS_fsm[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ap_CS_fsm_state2,
      I1 => tmp_fu_137_p30_in,
      O => \ap_CS_fsm[2]_i_1_n_1\
    );
\ap_CS_fsm_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(0),
      Q => \ap_CS_fsm_reg_n_1_[0]\,
      S => ap_rst
    );
\ap_CS_fsm_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_NS_fsm(1),
      Q => ap_CS_fsm_state2,
      R => ap_rst
    );
\ap_CS_fsm_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_CS_fsm[2]_i_1_n_1\,
      Q => \^c_ce0\,
      R => ap_rst
    );
\ap_CS_fsm_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \^c_ce0\,
      Q => ap_CS_fsm_state4,
      R => ap_rst
    );
\ap_CS_fsm_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_CS_fsm_state4,
      Q => \ap_CS_fsm_reg_n_1_[4]\,
      R => ap_rst
    );
\ap_CS_fsm_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => \ap_CS_fsm_reg_n_1_[4]\,
      Q => ap_CS_fsm_state6,
      R => ap_rst
    );
\ap_CS_fsm_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => ap_clk,
      CE => '1',
      D => ap_CS_fsm_state6,
      Q => ap_CS_fsm_state7,
      R => ap_rst
    );
ap_idle_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \ap_CS_fsm_reg_n_1_[0]\,
      I1 => ap_start,
      O => ap_idle
    );
fir_mul_32s_32s_3bkb_U1: entity work.bd_0_hls_inst_0_fir_mul_32s_32s_3bkb
     port map (
      D(31 downto 16) => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(31 downto 16),
      D(15) => fir_mul_32s_32s_3bkb_U1_n_17,
      D(14) => fir_mul_32s_32s_3bkb_U1_n_18,
      D(13) => fir_mul_32s_32s_3bkb_U1_n_19,
      D(12) => fir_mul_32s_32s_3bkb_U1_n_20,
      D(11) => fir_mul_32s_32s_3bkb_U1_n_21,
      D(10) => fir_mul_32s_32s_3bkb_U1_n_22,
      D(9) => fir_mul_32s_32s_3bkb_U1_n_23,
      D(8) => fir_mul_32s_32s_3bkb_U1_n_24,
      D(7) => fir_mul_32s_32s_3bkb_U1_n_25,
      D(6) => fir_mul_32s_32s_3bkb_U1_n_26,
      D(5) => fir_mul_32s_32s_3bkb_U1_n_27,
      D(4) => fir_mul_32s_32s_3bkb_U1_n_28,
      D(3) => fir_mul_32s_32s_3bkb_U1_n_29,
      D(2) => fir_mul_32s_32s_3bkb_U1_n_30,
      D(1) => fir_mul_32s_32s_3bkb_U1_n_31,
      D(0) => fir_mul_32s_32s_3bkb_U1_n_32,
      Q(1) => ap_CS_fsm_state4,
      Q(0) => \^c_ce0\,
      ap_clk => ap_clk,
      c_q0(31 downto 0) => c_q0(31 downto 0),
      icmp_ln61_reg_189 => icmp_ln61_reg_189,
      p_1_in(31 downto 0) => p_1_in(31 downto 0),
      shift_reg_address01 => shift_reg_address01
    );
\i_0_reg_104_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => i_reg_208(0),
      Q => \i_0_reg_104_reg_n_1_[0]\,
      R => acc_0_reg_91
    );
\i_0_reg_104_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => i_reg_208(1),
      Q => \i_0_reg_104_reg_n_1_[1]\,
      S => acc_0_reg_91
    );
\i_0_reg_104_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => i_reg_208(2),
      Q => \i_0_reg_104_reg_n_1_[2]\,
      R => acc_0_reg_91
    );
\i_0_reg_104_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => i_reg_208(3),
      Q => \i_0_reg_104_reg_n_1_[3]\,
      S => acc_0_reg_91
    );
\i_0_reg_104_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state7,
      D => i_reg_208(4),
      Q => tmp_fu_137_p30_in,
      R => acc_0_reg_91
    );
\i_reg_208[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i_0_reg_104_reg_n_1_[0]\,
      O => \i_reg_208[0]_i_1_n_1\
    );
\i_reg_208[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i_0_reg_104_reg_n_1_[1]\,
      I1 => \i_0_reg_104_reg_n_1_[0]\,
      O => \i_reg_208[1]_i_1_n_1\
    );
\i_reg_208[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \i_0_reg_104_reg_n_1_[2]\,
      I1 => \i_0_reg_104_reg_n_1_[0]\,
      I2 => \i_0_reg_104_reg_n_1_[1]\,
      O => \i_reg_208[2]_i_1_n_1\
    );
\i_reg_208[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => \i_0_reg_104_reg_n_1_[3]\,
      I1 => \i_0_reg_104_reg_n_1_[1]\,
      I2 => \i_0_reg_104_reg_n_1_[0]\,
      I3 => \i_0_reg_104_reg_n_1_[2]\,
      O => \i_reg_208[3]_i_1_n_1\
    );
\i_reg_208[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => \i_0_reg_104_reg_n_1_[3]\,
      I1 => \i_0_reg_104_reg_n_1_[1]\,
      I2 => \i_0_reg_104_reg_n_1_[0]\,
      I3 => \i_0_reg_104_reg_n_1_[2]\,
      I4 => tmp_fu_137_p30_in,
      O => \i_reg_208[4]_i_1_n_1\
    );
\i_reg_208_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^c_ce0\,
      D => \i_reg_208[0]_i_1_n_1\,
      Q => i_reg_208(0),
      R => '0'
    );
\i_reg_208_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^c_ce0\,
      D => \i_reg_208[1]_i_1_n_1\,
      Q => i_reg_208(1),
      R => '0'
    );
\i_reg_208_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^c_ce0\,
      D => \i_reg_208[2]_i_1_n_1\,
      Q => i_reg_208(2),
      R => '0'
    );
\i_reg_208_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^c_ce0\,
      D => \i_reg_208[3]_i_1_n_1\,
      Q => i_reg_208(3),
      R => '0'
    );
\i_reg_208_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => \^c_ce0\,
      D => \i_reg_208[4]_i_1_n_1\,
      Q => i_reg_208(4),
      R => '0'
    );
\icmp_ln61_reg_189[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => icmp_ln61_fu_145_p2,
      I1 => ap_CS_fsm_state2,
      I2 => tmp_fu_137_p30_in,
      I3 => icmp_ln61_reg_189,
      O => \icmp_ln61_reg_189[0]_i_1_n_1\
    );
\icmp_ln61_reg_189[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \i_0_reg_104_reg_n_1_[3]\,
      I1 => tmp_fu_137_p30_in,
      I2 => \i_0_reg_104_reg_n_1_[0]\,
      I3 => \i_0_reg_104_reg_n_1_[1]\,
      I4 => \i_0_reg_104_reg_n_1_[2]\,
      O => icmp_ln61_fu_145_p2
    );
\icmp_ln61_reg_189_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => '1',
      D => \icmp_ln61_reg_189[0]_i_1_n_1\,
      Q => icmp_ln61_reg_189,
      R => '0'
    );
\mul_ln68_reg_218_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_32,
      Q => mul_ln68_reg_218(0),
      R => '0'
    );
\mul_ln68_reg_218_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_22,
      Q => mul_ln68_reg_218(10),
      R => '0'
    );
\mul_ln68_reg_218_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_21,
      Q => mul_ln68_reg_218(11),
      R => '0'
    );
\mul_ln68_reg_218_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_20,
      Q => mul_ln68_reg_218(12),
      R => '0'
    );
\mul_ln68_reg_218_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_19,
      Q => mul_ln68_reg_218(13),
      R => '0'
    );
\mul_ln68_reg_218_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_18,
      Q => mul_ln68_reg_218(14),
      R => '0'
    );
\mul_ln68_reg_218_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_17,
      Q => mul_ln68_reg_218(15),
      R => '0'
    );
\mul_ln68_reg_218_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(16),
      Q => mul_ln68_reg_218(16),
      R => '0'
    );
\mul_ln68_reg_218_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(17),
      Q => mul_ln68_reg_218(17),
      R => '0'
    );
\mul_ln68_reg_218_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(18),
      Q => mul_ln68_reg_218(18),
      R => '0'
    );
\mul_ln68_reg_218_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(19),
      Q => mul_ln68_reg_218(19),
      R => '0'
    );
\mul_ln68_reg_218_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_31,
      Q => mul_ln68_reg_218(1),
      R => '0'
    );
\mul_ln68_reg_218_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(20),
      Q => mul_ln68_reg_218(20),
      R => '0'
    );
\mul_ln68_reg_218_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(21),
      Q => mul_ln68_reg_218(21),
      R => '0'
    );
\mul_ln68_reg_218_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(22),
      Q => mul_ln68_reg_218(22),
      R => '0'
    );
\mul_ln68_reg_218_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(23),
      Q => mul_ln68_reg_218(23),
      R => '0'
    );
\mul_ln68_reg_218_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(24),
      Q => mul_ln68_reg_218(24),
      R => '0'
    );
\mul_ln68_reg_218_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(25),
      Q => mul_ln68_reg_218(25),
      R => '0'
    );
\mul_ln68_reg_218_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(26),
      Q => mul_ln68_reg_218(26),
      R => '0'
    );
\mul_ln68_reg_218_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(27),
      Q => mul_ln68_reg_218(27),
      R => '0'
    );
\mul_ln68_reg_218_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(28),
      Q => mul_ln68_reg_218(28),
      R => '0'
    );
\mul_ln68_reg_218_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(29),
      Q => mul_ln68_reg_218(29),
      R => '0'
    );
\mul_ln68_reg_218_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_30,
      Q => mul_ln68_reg_218(2),
      R => '0'
    );
\mul_ln68_reg_218_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(30),
      Q => mul_ln68_reg_218(30),
      R => '0'
    );
\mul_ln68_reg_218_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => \fir_mul_32s_32s_3bkb_MulnS_0_U/p_tmp_reg__1\(31),
      Q => mul_ln68_reg_218(31),
      R => '0'
    );
\mul_ln68_reg_218_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_29,
      Q => mul_ln68_reg_218(3),
      R => '0'
    );
\mul_ln68_reg_218_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_28,
      Q => mul_ln68_reg_218(4),
      R => '0'
    );
\mul_ln68_reg_218_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_27,
      Q => mul_ln68_reg_218(5),
      R => '0'
    );
\mul_ln68_reg_218_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_26,
      Q => mul_ln68_reg_218(6),
      R => '0'
    );
\mul_ln68_reg_218_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_25,
      Q => mul_ln68_reg_218(7),
      R => '0'
    );
\mul_ln68_reg_218_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_24,
      Q => mul_ln68_reg_218(8),
      R => '0'
    );
\mul_ln68_reg_218_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state6,
      D => fir_mul_32s_32s_3bkb_U1_n_23,
      Q => mul_ln68_reg_218(9),
      R => '0'
    );
\sext_ln60_reg_180_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => \i_0_reg_104_reg_n_1_[0]\,
      Q => \^c_address0\(0),
      R => '0'
    );
\sext_ln60_reg_180_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => \i_0_reg_104_reg_n_1_[1]\,
      Q => \^c_address0\(1),
      R => '0'
    );
\sext_ln60_reg_180_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => \i_0_reg_104_reg_n_1_[2]\,
      Q => \^c_address0\(2),
      R => '0'
    );
\sext_ln60_reg_180_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ap_clk,
      CE => ap_CS_fsm_state2,
      D => \i_0_reg_104_reg_n_1_[3]\,
      Q => \^c_address0\(3),
      R => '0'
    );
shift_reg_U: entity work.bd_0_hls_inst_0_fir_shift_reg
     port map (
      Q(1) => \^c_ce0\,
      Q(0) => ap_CS_fsm_state2,
      ap_clk => ap_clk,
      c_address0(3 downto 0) => \^c_address0\(3 downto 0),
      icmp_ln61_reg_189 => icmp_ln61_reg_189,
      p_1_in(31 downto 0) => p_1_in(31 downto 0),
      \q0_reg[0]\(4) => tmp_fu_137_p30_in,
      \q0_reg[0]\(3) => \i_0_reg_104_reg_n_1_[3]\,
      \q0_reg[0]\(2) => \i_0_reg_104_reg_n_1_[2]\,
      \q0_reg[0]\(1) => \i_0_reg_104_reg_n_1_[1]\,
      \q0_reg[0]\(0) => \i_0_reg_104_reg_n_1_[0]\,
      shift_reg_address01 => shift_reg_address01,
      x(31 downto 0) => x(31 downto 0)
    );
y_ap_vld_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => tmp_fu_137_p30_in,
      I1 => ap_CS_fsm_state2,
      O => \^y_ap_vld\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0_hls_inst_0 is
  port (
    y_ap_vld : out STD_LOGIC;
    c_ce0 : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_done : out STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_ready : out STD_LOGIC;
    y : out STD_LOGIC_VECTOR ( 31 downto 0 );
    c_address0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    c_q0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    x : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of bd_0_hls_inst_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of bd_0_hls_inst_0 : entity is "bd_0_hls_inst_0,fir,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of bd_0_hls_inst_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of bd_0_hls_inst_0 : entity is "HLS";
  attribute x_core_info : string;
  attribute x_core_info of bd_0_hls_inst_0 : entity is "fir,Vivado 2019.1";
end bd_0_hls_inst_0;

architecture STRUCTURE of bd_0_hls_inst_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of ap_clk : signal is "xilinx.com:signal:clock:1.0 ap_clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of ap_clk : signal is "XIL_INTERFACENAME ap_clk, ASSOCIATED_RESET ap_rst, FREQ_HZ 125000000.0, PHASE 0.000, CLK_DOMAIN bd_0_ap_clk_0, INSERT_VIP 0";
  attribute x_interface_info of ap_done : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl done";
  attribute x_interface_info of ap_idle : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl idle";
  attribute x_interface_info of ap_ready : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl ready";
  attribute x_interface_info of ap_rst : signal is "xilinx.com:signal:reset:1.0 ap_rst RST";
  attribute x_interface_parameter of ap_rst : signal is "XIL_INTERFACENAME ap_rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of ap_start : signal is "xilinx.com:interface:acc_handshake:1.0 ap_ctrl start";
  attribute x_interface_info of c_address0 : signal is "xilinx.com:signal:data:1.0 c_address0 DATA";
  attribute x_interface_parameter of c_address0 : signal is "XIL_INTERFACENAME c_address0, LAYERED_METADATA undef";
  attribute x_interface_info of c_q0 : signal is "xilinx.com:signal:data:1.0 c_q0 DATA";
  attribute x_interface_parameter of c_q0 : signal is "XIL_INTERFACENAME c_q0, LAYERED_METADATA undef";
  attribute x_interface_info of x : signal is "xilinx.com:signal:data:1.0 x DATA";
  attribute x_interface_parameter of x : signal is "XIL_INTERFACENAME x, LAYERED_METADATA undef";
  attribute x_interface_info of y : signal is "xilinx.com:signal:data:1.0 y DATA";
  attribute x_interface_parameter of y : signal is "XIL_INTERFACENAME y, LAYERED_METADATA undef";
begin
U0: entity work.bd_0_hls_inst_0_fir
     port map (
      ap_clk => ap_clk,
      ap_done => ap_done,
      ap_idle => ap_idle,
      ap_ready => ap_ready,
      ap_rst => ap_rst,
      ap_start => ap_start,
      c_address0(3 downto 0) => c_address0(3 downto 0),
      c_ce0 => c_ce0,
      c_q0(31 downto 0) => c_q0(31 downto 0),
      x(31 downto 0) => x(31 downto 0),
      y(31 downto 0) => y(31 downto 0),
      y_ap_vld => y_ap_vld
    );
end STRUCTURE;
