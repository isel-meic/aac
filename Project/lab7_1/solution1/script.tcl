############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project lab7_1
set_top hls_macc
add_files ../LABS/lab7/hls_macc.cpp
add_files -tb ../LABS/lab7/hls_macc_test.cpp
open_solution "solution1"
set_part {xc7z010clg400-1} -tool vivado
create_clock -period 10 -name default
#source "./lab7_1/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
