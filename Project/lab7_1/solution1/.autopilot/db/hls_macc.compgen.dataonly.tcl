# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_HLS_MACC_PERIPH_BUS {
ap_start { }
ap_done { }
ap_ready { }
ap_idle { }
a_V { 
	dir I
	width 18
	depth 1
	mode ap_none
	offset 16
	offset_end 23
}
b_V { 
	dir I
	width 18
	depth 1
	mode ap_none
	offset 24
	offset_end 31
}
accum_V_i { 
	dir I
	width 18
	depth 1
	mode ap_none
	offset 32
	offset_end 39
}
accum_V_o { 
	dir O
	width 18
	depth 1
	mode ap_vld
	offset 40
	offset_end 47
}
accum_clr { 
	dir I
	width 1
	depth 1
	mode ap_none
	offset 48
	offset_end 55
}
}
dict set axilite_register_dict HLS_MACC_PERIPH_BUS $port_HLS_MACC_PERIPH_BUS


