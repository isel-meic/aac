// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2019.1
// Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="hls_macc,hls_ip_2019_1,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=1,HLS_INPUT_PART=xc7z010-clg400-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=7.380000,HLS_SYN_LAT=1,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=1,HLS_SYN_FF=177,HLS_SYN_LUT=216,HLS_VERSION=2019_1}" *)

module hls_macc (
        ap_clk,
        ap_rst_n,
        s_axi_HLS_MACC_PERIPH_BUS_AWVALID,
        s_axi_HLS_MACC_PERIPH_BUS_AWREADY,
        s_axi_HLS_MACC_PERIPH_BUS_AWADDR,
        s_axi_HLS_MACC_PERIPH_BUS_WVALID,
        s_axi_HLS_MACC_PERIPH_BUS_WREADY,
        s_axi_HLS_MACC_PERIPH_BUS_WDATA,
        s_axi_HLS_MACC_PERIPH_BUS_WSTRB,
        s_axi_HLS_MACC_PERIPH_BUS_ARVALID,
        s_axi_HLS_MACC_PERIPH_BUS_ARREADY,
        s_axi_HLS_MACC_PERIPH_BUS_ARADDR,
        s_axi_HLS_MACC_PERIPH_BUS_RVALID,
        s_axi_HLS_MACC_PERIPH_BUS_RREADY,
        s_axi_HLS_MACC_PERIPH_BUS_RDATA,
        s_axi_HLS_MACC_PERIPH_BUS_RRESP,
        s_axi_HLS_MACC_PERIPH_BUS_BVALID,
        s_axi_HLS_MACC_PERIPH_BUS_BREADY,
        s_axi_HLS_MACC_PERIPH_BUS_BRESP,
        interrupt
);

parameter    ap_ST_fsm_state1 = 2'd1;
parameter    ap_ST_fsm_state2 = 2'd2;
parameter    C_S_AXI_HLS_MACC_PERIPH_BUS_DATA_WIDTH = 32;
parameter    C_S_AXI_HLS_MACC_PERIPH_BUS_ADDR_WIDTH = 6;
parameter    C_S_AXI_DATA_WIDTH = 32;

parameter C_S_AXI_HLS_MACC_PERIPH_BUS_WSTRB_WIDTH = (32 / 8);
parameter C_S_AXI_WSTRB_WIDTH = (32 / 8);

input   ap_clk;
input   ap_rst_n;
input   s_axi_HLS_MACC_PERIPH_BUS_AWVALID;
output   s_axi_HLS_MACC_PERIPH_BUS_AWREADY;
input  [C_S_AXI_HLS_MACC_PERIPH_BUS_ADDR_WIDTH - 1:0] s_axi_HLS_MACC_PERIPH_BUS_AWADDR;
input   s_axi_HLS_MACC_PERIPH_BUS_WVALID;
output   s_axi_HLS_MACC_PERIPH_BUS_WREADY;
input  [C_S_AXI_HLS_MACC_PERIPH_BUS_DATA_WIDTH - 1:0] s_axi_HLS_MACC_PERIPH_BUS_WDATA;
input  [C_S_AXI_HLS_MACC_PERIPH_BUS_WSTRB_WIDTH - 1:0] s_axi_HLS_MACC_PERIPH_BUS_WSTRB;
input   s_axi_HLS_MACC_PERIPH_BUS_ARVALID;
output   s_axi_HLS_MACC_PERIPH_BUS_ARREADY;
input  [C_S_AXI_HLS_MACC_PERIPH_BUS_ADDR_WIDTH - 1:0] s_axi_HLS_MACC_PERIPH_BUS_ARADDR;
output   s_axi_HLS_MACC_PERIPH_BUS_RVALID;
input   s_axi_HLS_MACC_PERIPH_BUS_RREADY;
output  [C_S_AXI_HLS_MACC_PERIPH_BUS_DATA_WIDTH - 1:0] s_axi_HLS_MACC_PERIPH_BUS_RDATA;
output  [1:0] s_axi_HLS_MACC_PERIPH_BUS_RRESP;
output   s_axi_HLS_MACC_PERIPH_BUS_BVALID;
input   s_axi_HLS_MACC_PERIPH_BUS_BREADY;
output  [1:0] s_axi_HLS_MACC_PERIPH_BUS_BRESP;
output   interrupt;

 reg    ap_rst_n_inv;
wire    ap_start;
reg    ap_done;
reg    ap_idle;
(* fsm_encoding = "none" *) reg   [1:0] ap_CS_fsm;
wire    ap_CS_fsm_state1;
reg    ap_ready;
wire   [17:0] a_V;
wire   [17:0] b_V;
wire   [17:0] accum_V_i;
reg   [17:0] accum_V_o;
reg    accum_V_o_ap_vld;
wire    accum_clr;
reg  signed [17:0] b_V_read_reg_112;
reg  signed [17:0] a_V_read_reg_117;
wire   [0:0] accum_clr_read_read_fu_44_p2;
wire    ap_CS_fsm_state2;
wire  signed [36:0] grp_fu_100_p3;
wire   [36:0] grp_fu_100_p2;
reg   [1:0] ap_NS_fsm;

// power-on initialization
initial begin
#0 ap_CS_fsm = 2'd1;
end

hls_macc_HLS_MACC_PERIPH_BUS_s_axi #(
    .C_S_AXI_ADDR_WIDTH( C_S_AXI_HLS_MACC_PERIPH_BUS_ADDR_WIDTH ),
    .C_S_AXI_DATA_WIDTH( C_S_AXI_HLS_MACC_PERIPH_BUS_DATA_WIDTH ))
hls_macc_HLS_MACC_PERIPH_BUS_s_axi_U(
    .AWVALID(s_axi_HLS_MACC_PERIPH_BUS_AWVALID),
    .AWREADY(s_axi_HLS_MACC_PERIPH_BUS_AWREADY),
    .AWADDR(s_axi_HLS_MACC_PERIPH_BUS_AWADDR),
    .WVALID(s_axi_HLS_MACC_PERIPH_BUS_WVALID),
    .WREADY(s_axi_HLS_MACC_PERIPH_BUS_WREADY),
    .WDATA(s_axi_HLS_MACC_PERIPH_BUS_WDATA),
    .WSTRB(s_axi_HLS_MACC_PERIPH_BUS_WSTRB),
    .ARVALID(s_axi_HLS_MACC_PERIPH_BUS_ARVALID),
    .ARREADY(s_axi_HLS_MACC_PERIPH_BUS_ARREADY),
    .ARADDR(s_axi_HLS_MACC_PERIPH_BUS_ARADDR),
    .RVALID(s_axi_HLS_MACC_PERIPH_BUS_RVALID),
    .RREADY(s_axi_HLS_MACC_PERIPH_BUS_RREADY),
    .RDATA(s_axi_HLS_MACC_PERIPH_BUS_RDATA),
    .RRESP(s_axi_HLS_MACC_PERIPH_BUS_RRESP),
    .BVALID(s_axi_HLS_MACC_PERIPH_BUS_BVALID),
    .BREADY(s_axi_HLS_MACC_PERIPH_BUS_BREADY),
    .BRESP(s_axi_HLS_MACC_PERIPH_BUS_BRESP),
    .ACLK(ap_clk),
    .ARESET(ap_rst_n_inv),
    .ACLK_EN(1'b1),
    .ap_start(ap_start),
    .interrupt(interrupt),
    .ap_ready(ap_ready),
    .ap_done(ap_done),
    .ap_idle(ap_idle),
    .a_V(a_V),
    .b_V(b_V),
    .accum_V_o(accum_V_o),
    .accum_V_o_ap_vld(accum_V_o_ap_vld),
    .accum_V_i(accum_V_i),
    .accum_clr(accum_clr)
);

hls_macc_mac_mulabkb #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 18 ),
    .din1_WIDTH( 18 ),
    .din2_WIDTH( 37 ),
    .dout_WIDTH( 37 ))
hls_macc_mac_mulabkb_U1(
    .din0(a_V_read_reg_117),
    .din1(b_V_read_reg_112),
    .din2(grp_fu_100_p2),
    .dout(grp_fu_100_p3)
);

always @ (posedge ap_clk) begin
    if (ap_rst_n_inv == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_state1;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
        a_V_read_reg_117 <= a_V;
        b_V_read_reg_112 <= b_V;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state2)) begin
        accum_V_o = {{grp_fu_100_p3[36:19]}};
    end else if (((ap_start == 1'b1) & (1'd1 == accum_clr_read_read_fu_44_p2) & (1'b1 == ap_CS_fsm_state1))) begin
        accum_V_o = 18'd0;
    end else begin
        accum_V_o = 'bx;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_state2) | ((ap_start == 1'b1) & (1'd1 == accum_clr_read_read_fu_44_p2) & (1'b1 == ap_CS_fsm_state1)))) begin
        accum_V_o_ap_vld = 1'b1;
    end else begin
        accum_V_o_ap_vld = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state2)) begin
        ap_done = 1'b1;
    end else begin
        ap_done = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (1'b1 == ap_CS_fsm_state1))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state2)) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_state1 : begin
            if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
                ap_NS_fsm = ap_ST_fsm_state2;
            end else begin
                ap_NS_fsm = ap_ST_fsm_state1;
            end
        end
        ap_ST_fsm_state2 : begin
            ap_NS_fsm = ap_ST_fsm_state1;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign accum_clr_read_read_fu_44_p2 = accum_clr;

assign ap_CS_fsm_state1 = ap_CS_fsm[32'd0];

assign ap_CS_fsm_state2 = ap_CS_fsm[32'd1];

always @ (*) begin
    ap_rst_n_inv = ~ap_rst_n;
end

assign grp_fu_100_p2 = {{accum_V_i}, {19'd0}};

endmodule //hls_macc
