############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project lab4_2
set_top adders_io
add_files ../LABS/lab4/adders_io.c
add_files -tb ../LABS/lab4/adders_io_test.c
open_solution "solution1"
set_part {xc7z010clg400-1} -tool vivado
create_clock -period 3 -name default
#source "./lab4_2/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
