set clock_constraint { \
    name clk \
    module adders \
    port ap_clk \
    period 3 \
    uncertainty 0.375 \
}

set all_path {}

set false_path {}

