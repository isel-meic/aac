#include "xparameters.h"
#include "xil_io.h"
#include "macc.h"
#include "xtime_l.h"

//====================================================

int main (void) 
{

   int i, result, a, b, c;
   XTime t[2];
	
   xil_printf("-- Start of the Program --\r\n");
 

   while (1)
   {
   	  scanf("%d", &a);
   	  xil_printf("a = %d\r\n", a);
   	  scanf("%d", &b);
   	  xil_printf("b = %d\r\n", b);
   	  scanf("%d", &c);
   	  xil_printf("c = %d\r\n", c);

	  XTime_GetTime(&(t[0]));
  	  MACC_mWriteReg (XPAR_MACC_0_S00_AXI_BASEADDR, 0, a);

	  MACC_mWriteReg (XPAR_MACC_0_S00_AXI_BASEADDR, 4, b);
	  MACC_mWriteReg (XPAR_MACC_0_S00_AXI_BASEADDR, 8, c);

	  result = MACC_mReadReg (XPAR_MACC_0_S00_AXI_BASEADDR, 0);

	  XTime_GetTime(&(t[1]));

	  printf("result a*b+c is = %d took %d %.0f ns.\n\r",
			  result, t[1] - t[0], 1.0 * (t[1] - t[0]) * 1000 / (COUNTS_PER_SECOND / 1000000));


	  
	  for (i=0; i<9999999; i++);
   }



}
