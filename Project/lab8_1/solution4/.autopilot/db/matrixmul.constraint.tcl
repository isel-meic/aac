set clock_constraint { \
    name clk \
    module matrixmul \
    port ap_clk \
    period 30 \
    uncertainty 3.75 \
}

set all_path {}

set false_path {}

